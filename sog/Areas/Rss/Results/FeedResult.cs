﻿using System;
using System.ServiceModel.Syndication;
using System.Text;
using System.Web.Mvc;
using System.Xml;

namespace sog.Areas.Rss.Results
{
    public class FeedResult : ActionResult
    {
        private readonly SyndicationFeedFormatter feed;

        /// <summary>
        /// Create the feed result class
        /// </summary>
        /// <param name="feed"></param>
        public FeedResult(SyndicationFeedFormatter feed)
        {
            this.feed = feed;
        }

        public Encoding ContentEncoding { get; set; }

        public string ContentType { get; set; }

        public SyndicationFeedFormatter Feed
        {
            get { return this.feed; }
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            var response = context.HttpContext.Response;

            response.ContentType = "application/rss+xml";

            if (this.ContentEncoding != null)
                response.ContentEncoding = this.ContentEncoding;

            if (this.feed != null)
                using (var xmlWriter = new XmlTextWriter(response.Output))
                {
                    xmlWriter.Formatting = Formatting.Indented;
                    this.feed.WriteTo(xmlWriter);
                }
        }
    }
}