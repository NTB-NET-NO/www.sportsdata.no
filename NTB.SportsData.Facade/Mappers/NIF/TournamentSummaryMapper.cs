﻿using Glue;
using NTB.SportsData.Services.NIF.NIFProdService;

using NTB.SportsData.Facade.Mappers.Base;

namespace NTB.SportsData.Facade.Mappers.NIF
{
    public class TournamentSummaryMapper : BaseMapper<TournamentSummary, Domain.Classes.Tournament>
    {
        protected override void SetUpMapper(Mapping<TournamentSummary, Domain.Classes.Tournament> mapper)
        {
            mapper.Relate(x => x.TournamentClassNames, y => y.AgeCategory);
            mapper.Relate(x => x.TournamentClassIds, y => y.AgeCategoryId, this.StringToNullableIntConverter());
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.ActivityId, y => y.SportId);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentName, y => y.TournamentName);
            mapper.Relate(x => x.TournamentNo, y => y.TournamentNumber);
            mapper.Relate(x => x.MatchesLive, y => y.MatchesLive);
            mapper.Relate(x => x.HasMatches, y => y.HasMatches);
        }
    }
}