// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddExternalLoginBindingModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The add external login binding model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Models
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The add external login binding model.
    /// </summary>
    public class AddExternalLoginBindingModel
    {
        /// <summary>
        /// Gets or sets the external access token.
        /// </summary>
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }
}