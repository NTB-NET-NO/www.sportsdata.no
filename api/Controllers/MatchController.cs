﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchController.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   Match controller is used to control the match api methods
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Web.Http;

    using log4net;

    using NTB.SportsData.Api.Models.Matches;
    using NTB.SportsData.Api.Models.XML;
    using NTB.SportsData.Api.Transformers.XML;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     Match controller is used to control the match api methods
    /// </summary>
    [Authorize]
    [RoutePrefix("api/match")]
    public class MatchController : ApiController
    {
        #region Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        private readonly ILog logger = LogManager.GetLogger("ApiFileAppender");

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MatchController" /> class.
        /// </summary>
        public MatchController()
        {
            this.logger.Debug("In Match Controller");
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// DEPRECATED: Use GetMatch instead
        /// This method returns more information about the match
        /// </summary>
        /// <param name="id">
        /// Provide the match id for the match you want to get more information.
        /// </param>
        /// <returns>
        /// Data in either JSON or XML containing information about todays matches
        /// </returns>
        [HttpGet]
        public HttpResponseMessage Match(int id)
        {
            this.logger.Info("Getting match by id");
            var accept = this.Request.Headers.Accept.ToString();

            // var tournaments = new List<Tournament>();
            var match = new MatchContext().Get(id);
            
            if (match == null)
            {
                match = new MatchExtended();
            }

            string content;

            var serializer = new ObjectSerializer();

            this.logger.Info("Serializing object");
            var xmlContent = serializer.SerializeObject(
                new List<MatchExtended>()
                    {
                        match
                    });

            try
            {
                var filePath = @"C:\Utvikling\SportsDataApi\Raw-files\";
                var filename = string.Format(
                    "Match-{0}{1}{2}T{3}{4}{5}-{6}.xml",
                    DateTime.Now.Year,
                    DateTime.Now.Month.ToString()
                        .PadLeft(2, '0'),
                    DateTime.Now.Day.ToString()
                        .PadLeft(2, '0'),
                    DateTime.Now.Hour.ToString()
                        .PadLeft(2, '0'),
                    DateTime.Now.Minute.ToString()
                        .PadLeft(2, '0'),
                    DateTime.Now.Second.ToString()
                        .PadLeft(2, '0'),
                        id);

                var di = new DirectoryInfo(filePath);
                if (!di.Exists)
                {
                    di.Create();
                }

                var path = Path.Combine(filePath, filename);

                File.WriteAllText(path, xmlContent);
            }
            catch (Exception exception)
            {
                this.logger.Error(exception);
            }

            if (accept == "application/json")
            {
                this.logger.Info("Generating JSON");
                var model = new MatchTransformer()
                                {
                                    XsltFile = "sportsinjson-matches.xsl"
                                };
                content = model.TransformObject(xmlContent, accept);

                // content = model.SerializeObjects(tournaments.ToList());
            }
            else
            {
                accept = "text/xml";
                this.logger.Info("Generating XML");
                var model = new MatchTransformer()
                                {
                                    XsltFile = "sportsml-matches.xsl"
                                };
                content = model.TransformObject(xmlContent, accept);
            }

            return new HttpResponseMessage
                       {
                           Content = new StringContent(content, Encoding.UTF8, accept)
                       };

            // Now I have an XML-docoument that I can either XSLT into SportsML, or I can do something else with it.
            // todo: consider using XSLT to transform into SportsML
            // todo: consider using XSLT to transform into Sports in JSON
        }

        /// <summary>
        /// Method to use when you want a list of todays matches / events
        /// </summary>
        /// <param name="id">
        /// The Id you use is the match Id you have received from another request. 
        /// you the customer id.
        /// </param>
        /// <returns>
        /// Data in either JSON or XML containing information about todays matches
        /// </returns>
        /// <responsedescription>
        /// Depending on your header information, this method will return either JSON or XML string. <br/>
        /// We recommend you store the information from this response either in a database, or in a file.<br/>
        /// When using this method, you can filter the output using either the eventstartdate parameter or
        /// use the eventstatus parameter.<br/>
        /// The event Status parameter will either contain <strong>post-event</strong> or <strong>pre-event.</strong>
        /// </responsedescription>
        /// <code type="json">
        /// {"events": [{
        ///    "competitionid": 148513,
        ///    "competitionname": "Kvalifisering NM Menn 2016",
        ///    "competitionround": 2,
        ///    "eventid": 6541556,
        ///    "eventname": "Ottestad - Lillehammer",
        ///    "eventstartdate": "2016-03-30T18:30:00",
        ///    "eventvenuename": "",
        ///    "eventstatus": "pre-event",
        ///    "eventoutcome": "loss",
        ///    "teams": [
        ///        {
        ///            "name": "Ottestad",
        ///            "teamid": 108,
        ///            "score": 0
        ///        },
        ///        {
        ///            "name": "Lillehammer",
        ///            "teamid": 667,
        ///            "score": 1
        ///        }
        ///    ]
        /// }]}
        /// </code>
        /// <code type="xml">
        ///   <sports-content>
        ///    <tournament>
        ///        <tournament-metadata tournament-key="148513" tournament-name="Kvalifisering NM Menn 2016">
        ///            <sports-content-codes>
        ///                <sports-content-code code-type="sport" code-name="" />
        ///            </sports-content-codes>
        ///        </tournament-metadata>
        ///        <tournament-division>
        ///            <tournament-round round-number="2">
        ///                <sports-event>
        ///                    <event-metadata event-key="6541556" event-name="Ottestad - Lillehammer" start-date-time="2016-03-30T0::00+0100" />
        ///                    <team>
        ///                        <team-metadata alignment="home" team-key="108">
        ///                            <name full="Ottestad" />
        ///                        </team-metadata>
        ///                        <team-stats>
        ///                            <sub-score period-value="1" sub-score-name="pauseresultat" score="0" />
        ///                            <sub-score period-value="2" sub-score-name="sluttresultat" score="0" />
        ///                        </team-stats>
        ///                    </team>
        ///                    <team>
        ///                        <team-metadata alignment="away" team-key="667">
        ///                            <name full="Lillehammer" />
        ///                        </team-metadata>
        ///                        <team-stats>
        ///                            <sub-score period-value="1" sub-score-name="pauseresultat" score="1" />
        ///                            <sub-score period-value="2" sub-score-name="sluttresultat" score="1" />
        ///                        </team-stats>
        ///                    </team>
        ///                </sports-event>
        ///            </tournament-round>
        ///        </tournament-division>
        ///    </tournament>
        ///   </sports-content>
        /// </code>
        [HttpGet]
        public HttpResponseMessage GetMatch(int id)
        {
            this.logger.Info("Getting match by id");
            var accept = this.Request.Headers.Accept.ToString();

            // var tournaments = new List<Tournament>();
            var match = new MatchContext().Get(id);
            
            if (match == null)
            {
                match = new MatchExtended();
            }

            string content;

            var serializer = new ObjectSerializer();

            this.logger.Info("Serializing object");
            var xmlContent = serializer.SerializeObject(
                new List<MatchExtended>()
                    {
                        match
                    });

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["OutputRawFiles"]))
            {
                this.WriteRawFile(id, xmlContent);
            }
            

            if (accept == "application/json")
            {
                this.logger.Info("Generating JSON");
                var model = new MatchTransformer()
                                {
                                    XsltFile = "sportsinjson-matches.xsl"
                                };
                content = model.TransformObject(xmlContent, accept);

                // content = model.SerializeObjects(tournaments.ToList());
            }
            else
            {
                accept = "text/xml";
                this.logger.Info("Generating XML");
                var model = new MatchTransformer()
                                {
                                    XsltFile = "sportsml-matches.xsl"
                                };
                content = model.TransformObject(xmlContent, accept);
            }

            return new HttpResponseMessage
                       {
                           Content = new StringContent(content, Encoding.UTF8, accept)
                       };

            // Now I have an XML-docoument that I can either XSLT into SportsML, or I can do something else with it.
            // todo: consider using XSLT to transform into SportsML
            // todo: consider using XSLT to transform into Sports in JSON
        }

        /// <summary>
        /// This method returns more information about the match
        /// </summary>
        /// <param name="id">
        /// Provide the match id for the match you want to get more information.
        /// </param>
        /// <returns>
        /// Data in either JSON or XML containing information about todays matches
        /// </returns>
        /// <responsedescription>
        /// This method will only return an XML string. <br/>
        /// We recommend you store the information from this response either in a database, or in a file.<br/>
        /// When using this method, you can filter the output using either the eventstartdate parameter or
        /// use the eventstatus parameter.<br/>
        /// The event Status parameter will either contain <strong>post-event</strong> or <strong>pre-event.</strong>
        /// </responsedescription>
        /// <code type="xml">
        ///  <sports-content>
        ///    <tournament>
        ///        <tournament-metadata tournament-key="148153" tournament-name="Tippeligaen">
        ///            <sports-content-codes>
        ///                <sports-content-code code-type="sport" code-name="" />
        ///            </sports-content-codes>
        ///        </tournament-metadata>
        ///        <tournament-division>
        ///            <tournament-round round-number="5">
        ///                <sports-event>
        ///                    <event-metadata event-key="6390198" event-name="Odd - Start" start-date-time="2016-04-17T0::00+0100" />
        ///                    <team>
        ///                        <team-metadata alignment="home" id="t.270">
        ///                            <name full="Odd" />
        ///                        </team-metadata>
        ///                        <team-stats>
        ///                            <sub-score period-value="1" sub-score-name="pauseresultat" score="2" />
        ///                            <sub-score period-value="2" sub-score-name="sluttresultat" score="3" />
        ///                        </team-stats>
        ///                        <player>
        ///                            <player-metadata id="p.322545" uniform-number="1" position-event="goalkeeper">
        ///                                <name first="Sondre Løvseth" last="Rossbach" full="Sondre Løvseth Rossbach" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.1309093" uniform-number="2" position-event="defender">
        ///                                <name first="Espen" last="Ruud" full="Espen Ruud" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.190338" uniform-number="5" position-event="defender">
        ///                                <name first="Thomas" last="Grøgaard" full="Thomas Grøgaard" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.79682" uniform-number="16" position-event="defender">
        ///                                <name first="Fredrik Semb" last="Berge" full="Fredrik Semb Berge" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.79629" uniform-number="21" position-event="defender">
        ///                                <name first="Steffen" last="Hagen" full="Steffen Hagen" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.97864" uniform-number="8" position-event="midfielder">
        ///                                <name first="Jone" last="Samuelsen" full="Jone Samuelsen" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.205316" uniform-number="14" position-event="midfielder">
        ///                                <name first="Fredrik Lund" last="Nordkvelle" full="Fredrik Lund Nordkvelle" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.79695" uniform-number="20" position-event="midfielder">
        ///                                <name first="Fredrik Oldrup" last="Jensen" full="Fredrik Oldrup Jensen" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.207933" uniform-number="7" position-event="forward">
        ///                                <name first="Ole Jørgen" last="Halvorsen" full="Ole Jørgen Halvorsen" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.22865" uniform-number="10" position-event="forward">
        ///                                <name first="Olivier" last="Occean" full="Olivier Occean" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.79750" uniform-number="26" position-event="forward">
        ///                                <name first="Chukwuma" last="Akabueze" full="Chukwuma Akabueze" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.317635" uniform-number="12" position-event="substitute">
        ///                                <name first="Viljar Røsholt" last="Myhra" full="Viljar Røsholt Myhra" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.299105" uniform-number="4" position-event="substitute">
        ///                                <name first="Vegard Amundsen" last="Bergan" full="Vegard Amundsen Bergan" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.53316" uniform-number="6" position-event="substitute">
        ///                                <name first="Oliver" last="Berg" full="Oliver Berg" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.53380" uniform-number="9" position-event="substitute">
        ///                                <name first="Henrik" last="Kjelsrud Johansen" full="Henrik Kjelsrud Johansen" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.307657" uniform-number="11" position-event="substitute">
        ///                                <name first="Rafik" last="Zekhnini" full="Rafik Zekhnini" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.100795" uniform-number="18" position-event="substitute">
        ///                                <name first="Joakim Våge" last="Nilsen" full="Joakim Våge Nilsen" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.1760133" uniform-number="19" position-event="substitute">
        ///                                <name first="Zakaria" last="Messoudi" full="Zakaria Messoudi" />
        ///                            </player-metadata>
        ///                        </player>
        ///                    </team>
        ///                    <team>
        ///                        <team-metadata alignment="away" id="t.261">
        ///                            <name full="Start" />
        ///                        </team-metadata>
        ///                        <team-stats>
        ///                            <sub-score period-value="1" sub-score-name="pauseresultat" score="0" />
        ///                            <sub-score period-value="2" sub-score-name="sluttresultat" score="0" />
        ///                        </team-stats>
        ///                        <player>
        ///                            <player-metadata id="p.110968" uniform-number="1" position-event="goalkeeper">
        ///                                <name first="Håkon Eikemo" last="Opdal" full="Håkon Eikemo Opdal" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.199600" uniform-number="2" position-event="defender">
        ///                                <name first="Jens Kristian" last="Skogmo" full="Jens Kristian Skogmo" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.1759444" uniform-number="3" position-event="defender">
        ///                                <name first="Tapio Olavi" last="Heikkilä" full="Tapio Olavi Heikkilä" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.87437" uniform-number="15" position-event="defender">
        ///                                <name first="Henrik" last="Robstad" full="Henrik Robstad" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.87317" uniform-number="28" position-event="defender">
        ///                                <name first="Rolf Daniel" last="Vikstøl" full="Rolf Daniel Vikstøl" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.1764367" uniform-number="7" position-event="midfielder">
        ///                                <name first="Chidiebere Chijioke " last="Nwakali" full="Chidiebere Chijioke  Nwakali" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.207779" uniform-number="8" position-event="midfielder">
        ///                                <name first="Espen" last="Hoff" full="Espen Hoff" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.87440" uniform-number="14" position-event="midfielder">
        ///                                <name first="Espen Fjone" last="Børufsen" full="Espen Fjone Børufsen" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.1622448" uniform-number="17" position-event="midfielder">
        ///                                <name first="Gudmundur " last="Kristjansson" full="Gudmundur  Kristjansson" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.20640" uniform-number="6" position-event="forward">
        ///                                <name first="Kristoffer Vassbakk" last="Ajer" full="Kristoffer Vassbakk Ajer" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.1550400" uniform-number="30" position-event="forward">
        ///                                <name first="Lasse" last="Sigurdsen" full="Lasse Sigurdsen" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.88414" uniform-number="25" position-event="substitute">
        ///                                <name first="Sean" last="McDermott" full="Sean McDermott" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.1723768" uniform-number="4" position-event="substitute">
        ///                                <name first="Alexander Ray" last="De John" full="Alexander Ray De John" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.134559" uniform-number="5" position-event="substitute">
        ///                                <name first="Robert Johann" last="Sandnes" full="Robert Johann Sandnes" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.304025" uniform-number="22" position-event="substitute">
        ///                                <name first="Lars-Jørgen" last="Salvesen" full="Lars-Jørgen Salvesen" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.303058" uniform-number="23" position-event="substitute">
        ///                                <name first="Erlend" last="Segberg" full="Erlend Segberg" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.1556954" uniform-number="27" position-event="substitute">
        ///                                <name first="Eirik" last="Wichne" full="Eirik Wichne" />
        ///                            </player-metadata>
        ///                        </player>
        ///                        <player>
        ///                            <player-metadata id="p.91649" uniform-number="32" position-event="substitute">
        ///                                <name first="Matias" last="Rasmussen" full="Matias Rasmussen" />
        ///                            </player-metadata>
        ///                        </player>
        ///                    </team>
        ///                    <event-actions>
        ///                        <event-actions-soccer>
        ///                            <action-soccer-score minutes-elapsed="32" sequence-number="2494655" period-value="1" score-team="1" score-team-opposing="0">
        ///                                <action-soccer-play-participant role="scorer" player-idref="p.205316" team-idref="t.270" />
        ///                            </action-soccer-score>
        ///                            <action-soccer-score minutes-elapsed="40" sequence-number="2494683" period-value="1" score-team="2" score-team-opposing="0">
        ///                                <action-soccer-play-participant role="scorer" player-idref="p.79682" team-idref="t.270" />
        ///                            </action-soccer-score>
        ///                            <action-soccer-score minutes-elapsed="69" sequence-number="2494824" period-value="2" score-team="3" score-team-opposing="0">
        ///                                <action-soccer-play-participant role="scorer" player-idref="p.79682" team-idref="t.270" />
        ///                            </action-soccer-score>
        ///                            <action-soccer-foul minutes-elapsed="38" sequence-number="2494674" fouling-player-idref="p.1622448" fouling-team-idref="t.261" />
        ///                            <action-soccer-penalty minutes-elapsed="38" sequence-number="2494674" recipient-idref="p.1622448" team-idref="t.261" />
        ///                            <action-soccer-foul minutes-elapsed="89" sequence-number="2494896" fouling-player-idref="p.1309093" fouling-team-idref="t.270" />
        ///                            <action-soccer-penalty minutes-elapsed="89" sequence-number="2494896" recipient-idref="p.1309093" team-idref="t.270" />
        ///                            <action-soccer-foul minutes-elapsed="89" sequence-number="2494897" fouling-player-idref="p.134559" fouling-team-idref="t.261" />
        ///                            <action-soccer-penalty minutes-elapsed="89" sequence-number="2494897" recipient-idref="p.134559" team-idref="t.261" />
        ///                            <action-soccer-substitution minutes-elapsed="46" sequence-number="2494734" team-idref="t.261" person-original-idref="p.1550400" person-replacing-idref="p.304025" />
        ///                            <action-soccer-substitution minutes-elapsed="55" sequence-number="2494780" team-idref="t.261" person-original-idref="p.207779" person-replacing-idref="p.134559" />
        ///                            <action-soccer-substitution minutes-elapsed="68" sequence-number="2494821" team-idref="t.270" person-original-idref="p.79750" person-replacing-idref="p.307657" />
        ///                            <action-soccer-substitution minutes-elapsed="72" sequence-number="2494831" team-idref="t.270" person-original-idref="p.97864" person-replacing-idref="p.53316" />
        ///                            <action-soccer-substitution minutes-elapsed="80" sequence-number="2494882" team-idref="t.261" person-original-idref="p.87440" person-replacing-idref="p.91649" />
        ///                            <action-soccer-substitution minutes-elapsed="82" sequence-number="2494884" team-idref="t.270" person-original-idref="p.205316" person-replacing-idref="p.100795" />
        ///                        </event-actions-soccer>
        ///                    </event-actions>
        ///                </sports-event>
        ///            </tournament-round>
        ///        </tournament-division>
        ///    </tournament>
        /// </sports-content>
        /// </code>
        [HttpGet]
        public HttpResponseMessage GetMatchExtended(int id)
        {
            this.logger.Info("Getting Match Extended by id");
            var accept = this.Request.Headers.Accept.ToString();

            // var tournaments = new List<Tournament>();
            var match = new MatchContext().GetMatchExtended(id) ?? new MatchExtended();

            var content = string.Empty;

            var serializer = new ObjectSerializer();

            var xmlContent = serializer.SerializeObject(
                new List<MatchExtended>()
                    {
                        match
                    });

            if (accept == "application/json")
            {
                this.logger.Info("Generating JSON");
                var model = new MatchTransformer()
                {
                    XsltFile = "sportsinjson-matches.xsl"
                };
                content = model.TransformObject(xmlContent, accept);

                // content = model.SerializeObjects(tournaments.ToList());
            }
            else
            {
                    accept = "application/xml";
                    this.logger.Info("Generating XML");
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["OutputRawFiles"]))
                {
                    this.WriteRawFile(id, xmlContent);
                }
                

                    var model = new MatchTransformer()
                                    {
                                        XsltFile = "sportsml-matchextended.xsl"
                                    };
                    content = model.TransformObject(xmlContent, accept);
                
            }

            return new HttpResponseMessage
            {
                Content = new StringContent(content, Encoding.UTF8, accept)
            };

            // Now I have an XML-docoument that I can either XSLT into SportsML, or I can do something else with it.
            // todo: consider using XSLT to transform into SportsML
            // todo: consider using XSLT to transform into Sports in JSON
        }

        private void WriteRawFile(int id, string xmlContent)
        {
            try
            {
                var filePath = ConfigurationManager.AppSettings["RawFilesPath"];
                var filename = string.Format(
                    "MatchExtended-{0}{1}{2}T{3}{4}{5}-{6}.xml",
                    DateTime.Now.Year,
                    DateTime.Now.Month.ToString()
                        .PadLeft(2, '0'),
                    DateTime.Now.Day.ToString()
                        .PadLeft(2, '0'),
                    DateTime.Now.Hour.ToString()
                        .PadLeft(2, '0'),
                    DateTime.Now.Minute.ToString()
                        .PadLeft(2, '0'),
                    DateTime.Now.Second.ToString()
                        .PadLeft(2, '0'),
                    id);

                var di = new DirectoryInfo(filePath);
                if (!di.Exists)
                {
                    di.Create();
                }

                var path = Path.Combine(filePath, filename);

                File.WriteAllText(path, xmlContent);
            }
            catch (Exception exception)
            {
                this.logger.Error(exception);
            }
        }

        /// <summary>
        /// Method to use when you want a list of todays matches / events
        /// </summary>
        /// <param name="id">
        /// The Id you use is the CustomerId you have received from NTB. If you have not received a Customer Id, you a) have not subscribed to our sports data feed or b) we have not sent
        /// you the customer id.
        /// </param>
        /// <returns>
        /// Data in either JSON or XML containing information about todays matches
        /// </returns>
        /// <responsedescription>
        /// Depending on your header information, this method will return either JSON or XML string. <br/>
        /// We recommend you store the information from this response either in a database, or in a file.<br/>
        /// When using this method, you can filter the output using either the eventstartdate parameter or
        /// use the eventstatus parameter.<br/>
        /// The event Status parameter will either contain <strong>post-event</strong> or <strong>pre-event.</strong>
        /// </responsedescription>
        /// <code type="json">
        /// {"events": [{
        ///    "competitionid": 326926,
        ///    "competitionname": "2.divisjon Kvinner avd.04",
        ///    "competitionround": 0,
        ///    "eventid": 4993376,
        ///    "eventname": "NHHI - Fyllingen",
        ///    "eventstartdate": "2014-09-14T16:00:00",
        ///    "eventvenuename": "",
        ///    "eventstatus": "post-event",
        ///    "eventoutcome": "loss",
        ///    "teams": [
        ///        {
        ///            "name": "NHHI",
        ///            "teamid": 224715,
        ///            "score": 14
        ///        },
        ///        {
        ///            "name": "Fyllingen",
        ///            "teamid": 224174,
        ///            "score": 32
        ///        }
        ///    ]
        /// }]}
        /// </code>
        /// <code type="xml">
        /// </code>
        [HttpGet]
        public HttpResponseMessage GetTodayMatches(int id)
        {
            this.logger.Info("Getting all tournaments for customer");
            var accept = this.Request.Headers.Accept.ToString();

            // var tournaments = new List<Tournament>();
            var matches = new MatchContext().GetTodaysMatches(id);

            var listOfMatches = matches as List<Match> ?? matches.ToList();
            if (!listOfMatches.Any())
            {
                // return new HttpResponseMessage
                // {
                // Content = new StringContent("Mo matches today", Encoding.UTF8, accept)
                // };
                listOfMatches = new List<Match>()
                              {
                                  new Match()
                              }.ToList();
            }

            string content;

            var serializer = new ObjectSerializer();

            var xmlContent = serializer.SerializeObject(listOfMatches.ToList());

            if (accept == "application/json")
            {
                this.logger.Info("Generating JSON");
                var model = new MatchTransformer()
                                {
                                    XsltFile = "sportsinjson-matches.xsl"
                                };
                content = model.TransformObject(xmlContent, accept);

                // content = model.SerializeObjects(tournaments.ToList());
            }
            else
            {
                this.logger.Info("Generating XML");
                var model = new MatchTransformer()
                                {
                                    XsltFile = "sportsml-matches.xsl"
                                };
                content = model.TransformObject(xmlContent, accept);
            }

            return new HttpResponseMessage
                       {
                           Content = new StringContent(content, Encoding.UTF8, accept)
                       };

            // Now I have an XML-docoument that I can either XSLT into SportsML, or I can do something else with it.
            // todo: consider using XSLT to transform into SportsML
            // todo: consider using XSLT to transform into Sports in JSON
        }

        #endregion
    }
}