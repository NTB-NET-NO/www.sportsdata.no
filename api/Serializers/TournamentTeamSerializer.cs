﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentTeamSerializer.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Serializers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    using NTB.SportsData.Api.Interfaces;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The tournament table serializer.
    /// </summary>
    public class TournamentTeamSerializer : IObjectSerializer<Team>
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the serializable object property
        /// </summary>
        public object SerializableObject { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The serialize object.
        /// </summary>
        /// <param name="objects">
        /// The objects.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string SerializeObjects(List<Team> objects)
        {
            var serializer = new XmlSerializer(
                typeof(List<Team>),
                new XmlRootAttribute("Tournament") { Namespace = string.Empty });

            Encoding encoding = Encoding.UTF8;
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t" };
            settings.Encoding = encoding;
            settings.OmitXmlDeclaration = false;

            using (var textWriter = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(writer, objects, namespaces);
                }

                return textWriter.ToString().Replace("utf-16", "utf-8");
            }
        }

        /// <summary>
        /// The serialize object.
        /// </summary>
        /// <param name="domainObject">
        /// The domain object.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// This method is not implemented
        /// </exception>
        public string SerializeObject(Team domainObject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The serializer.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public string Serializer()
        {
            XmlSerializer serializer = null;

            if (this.SerializableObject != null)
            {
                if (this.SerializableObject.GetType() == typeof(List<Team>))
                {
                    serializer = new XmlSerializer(
                        typeof(List<Team>),
                        new XmlRootAttribute("Tournament") { Namespace = string.Empty });
                }
                else if (this.SerializableObject.GetType() == typeof(Team))
                {
                    serializer = new XmlSerializer(
                        typeof(Team),
                        new XmlRootAttribute("Tournament") { Namespace = string.Empty });
                }
            }

            Encoding encoding = Encoding.UTF8;
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            var settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "\t",
                Encoding = encoding,
                OmitXmlDeclaration = false
            };

            using (var textWriter = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(textWriter, settings))
                {
                    if (serializer != null)
                    {
                        serializer.Serialize(writer, this.SerializableObject, namespaces);
                    }
                }

                return textWriter.ToString().Replace("utf-16", "utf-8");
            }
        }

        #endregion
    }
}