﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Interfaces
{
    interface ICalendarDataMapper
    {
        List<NumberOfMatches> GetMatchesByDateInterval(DateTime startDate, DateTime endDate);
    }
}
