using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Web.Areas.Admin.Interfaces;
using NTB.SportsData.Web.Exceptions;

namespace NTB.SportsData.Web.Areas.Admin.Repositories
{
    /// <summary>
    /// The tournament repository.
    /// </summary>
    public class TournamentRepository : IRepository<Tournament>, IDisposable, ITournamentDataMapper
    {
        #region Static Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentRepository));

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the job id.
        /// </summary>
        public int JobId { get; set; }

        /// <summary>
        /// Gets or sets the season id.
        /// </summary>
        public int SeasonId { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void Delete(Tournament domainobject)
        {
            // SportsData_DeleteTournamentBySportId
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_DeleteTournamentBySportId", sqlConnection)
                {
                    CommandType =
                        CommandType
                            .StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        /// The delete tournament jobs.
        /// </summary>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        public void DeleteTournamentByJobId(int jobId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                // Consider if the procedure should list up the jobs again after deleting
                var sqlCommand = new SqlCommand("SportsData_CustomerJob_Tournament_Delete", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@JobId", jobId));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        public Tournament Get(int id)
        {
            var tournament = new Tournament();

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetTournamentsById", sqlConnection)
                    {
                        CommandType =
                            CommandType
                                .StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        // throw new Exception("Query returned no rows!");
                        Logger.Info("Query returned no rows");
                        return tournament;
                    }

                    while (sqlDataReader.Read())
                    {
                        // TournamentId, DistrictId, SeasonId, SportId, TournamentName, Push, AgeCategoryId, GenderId, TournamentTypeId, TournamentNumber
                        tournament.TournamentId = (int) sqlDataReader["TournamentId"];
                        tournament.DistrictId = (int) sqlDataReader["DistrictId"];
                        tournament.SeasonId = (int) sqlDataReader["SeasonId"];
                        tournament.SportId = (int) sqlDataReader["SportId"];
                        tournament.TournamentName = sqlDataReader["TournamentName"].ToString();

                        if (sqlDataReader["Push"] != DBNull.Value)
                        {
                            if (sqlDataReader["Push"].ToString() == "0")
                            {
                                tournament.Push = false;
                            }
                            else if (sqlDataReader["Push"].ToString() == "false")
                            {
                                tournament.Push = false;
                            }
                            else if (sqlDataReader["Push"].ToString() == "true")
                            {
                                tournament.Push = true;
                            }
                            else
                            {
                                tournament.Push = true;
                            }
                        }

                        tournament.AgeCategoryId = (int) sqlDataReader["AgeCategoryId"];
                        tournament.TournamentTypeId = (int) sqlDataReader["TournamentTypeId"];
                        tournament.TournamentNumber = sqlDataReader["TournamentNumber"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return tournament;
            }
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public IQueryable<Tournament> GetAll()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_GetTournamentsBySportId", sqlConnection)
                {
                    CommandType =
                        CommandType
                            .StoredProcedure
                };

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    throw new SportsDataWebException("Query returned no rows!");
                }

                var tournaments = new List<Tournament>();
                while (sqlDataReader.Read())
                {
                    var tournament = new Tournament
                    {
                        // TournamentId, DistrictId, SeasonId, SportId, TournamentName, Push, AgeCategoryId, GenderId, TournamentTypeId, TournamentNumber
                        TournamentId = (int) sqlDataReader["TournamentId"],
                        DistrictId = (int) sqlDataReader["DistrictId"],
                        SeasonId = (int) sqlDataReader["SeasonId"],
                        SportId = (int) sqlDataReader["SportId"],
                        TournamentName = sqlDataReader["TournamentName"].ToString(),
                        Push = Convert.ToBoolean(sqlDataReader["Push"]),
                        AgeCategoryId = (int) sqlDataReader["AgeCategoryId"],
                        TournamentTypeId = (int) sqlDataReader["TournamentTypeId"],
                        TournamentNumber = sqlDataReader["TournamentNumber"].ToString()
                    };

                    tournaments.Add(tournament);
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return tournaments.AsQueryable();
            }
        }

        /// <summary>
        /// The get tournament by job id.
        /// </summary>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentByJobId(int jobId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand("SportsData_GetTournamentsInJob", sqlConnection)
                {
                    CommandType =
                        CommandType
                            .StoredProcedure
                };
                sqlCommand.Parameters.Add(new SqlParameter("@JobId", jobId));
                sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", this.SeasonId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var sqlDataReader = sqlCommand.ExecuteReader();

                var selectedTournaments = new List<Tournament>();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        var selectedTournament = new Tournament();
                        if (sqlDataReader["TournamentId"] != DBNull.Value)
                        {
                            selectedTournament.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        }

                        if (sqlDataReader["DistrictId"] != DBNull.Value)
                        {
                            selectedTournament.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        }

                        if (sqlDataReader["Seasonid"] != DBNull.Value)
                        {
                            selectedTournament.SeasonId = Convert.ToInt32(sqlDataReader["Seasonid"]);
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            selectedTournament.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        }

                        if (sqlDataReader["AgeCategoryId"] != DBNull.Value)
                        {
                            selectedTournament.AgeCategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryId"]);
                        }

                        if (sqlDataReader["GenderId"] != DBNull.Value)
                        {
                            selectedTournament.GenderId = Convert.ToInt32(sqlDataReader["GenderId"]);
                        }

                        if (sqlDataReader["TournamentTypeId"] != DBNull.Value)
                        {
                            selectedTournament.TournamentTypeId = Convert.ToInt32(sqlDataReader["TournamentTypeId"]);
                        }

                        selectedTournament.TournamentName = (string) sqlDataReader["TournamentName"];
                        selectedTournament.TournamentSelected = true;

                        selectedTournaments.Add(selectedTournament);
                    }
                }

                sqlDataReader.Close();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return selectedTournaments;
            }
        }

        /// <summary>
        /// The get tournament by municipalities.
        /// </summary>
        /// <param name="selectedMunicipalities">
        /// The selected municipalities.
        /// </param>
        /// <param name="ageCategoryId">
        /// The age category id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentByMunicipalities(List<string> selectedMunicipalities, int ageCategoryId)
        {
            // Creating a JobTournament List
            var listJobsTournament = new List<Tournament>();

            // Looping the string array
            foreach (var stringMunicipalityId in selectedMunicipalities)
            {
                if (stringMunicipalityId == string.Empty)
                {
                    continue;
                }

                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand("SportsData_GetSelectedTournamentsByMunicipalities", sqlConnection);
                    sqlCommand.Parameters.Add(
                        new SqlParameter("@MunicipalityId", Convert.ToInt32(stringMunicipalityId)));

                    // AgeCategoryId
                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", ageCategoryId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", this.SeasonId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlConnection.Open();

                    using (var sqlDataReader = sqlCommand.ExecuteReader())
                    {
                        if (sqlDataReader.HasRows)
                        {
                            while (sqlDataReader.Read())
                            {
                                var districtId = 0;
                                var sportId = 0;
                                var tournamentId = 0;
                                var genderId = 0;
                                var tournamentTypeId = 0;
                                var ageCategoryFilterId = 0;

                                if (sqlDataReader["DistrictId"] != DBNull.Value)
                                {
                                    districtId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                                }

                                if (sqlDataReader["SeasonId"] != DBNull.Value)
                                {
                                    this.SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]);
                                }

                                if (sqlDataReader["SportId"] != DBNull.Value)
                                {
                                    sportId = Convert.ToInt32(sqlDataReader["SportId"]);
                                }

                                if (sqlDataReader["TournamentId"] != DBNull.Value)
                                {
                                    tournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                                }

                                if (sqlDataReader["AgeCategoryId"] != DBNull.Value)
                                {
                                    ageCategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryId"]);
                                }

                                if (sqlDataReader["GenderId"] != DBNull.Value)
                                {
                                    genderId = Convert.ToInt32(sqlDataReader["GenderId"]);
                                }

                                if (sqlDataReader["TournamentTypeId"] != DBNull.Value)
                                {
                                    tournamentTypeId = Convert.ToInt32(sqlDataReader["TournamentTypeId"]);
                                }

                                if (sqlDataReader["AgeCategoryFilterId"] != DBNull.Value)
                                {
                                    ageCategoryFilterId = Convert.ToInt32(sqlDataReader["AgeCategoryFilterId"]);
                                }

                                var tournamentName = sqlDataReader["TournamentName"].ToString();
                                var tournamentNumber = sqlDataReader["TournamentNumber"].ToString();

                                var jobTournament = new Tournament
                                {
                                    DistrictId = districtId,
                                    MunicipalityId =
                                        Convert.ToInt32(stringMunicipalityId),
                                    SeasonId = this.SeasonId,
                                    SportId = sportId,
                                    TournamentId = tournamentId,
                                    TournamentName = tournamentName,
                                    AgeCategoryId = ageCategoryId,
                                    GenderId = genderId,
                                    TournamentTypeId = tournamentTypeId,
                                    AgeCategoryFilterId = ageCategoryFilterId,
                                    TournamentNumber = tournamentNumber
                                };

                                // We are checking if the current tournament is in the list of tournaments already. 
                                var hasTournament =
                                    (from tournament in listJobsTournament
                                        where tournament.TournamentName == jobTournament.TournamentName
                                        select tournament).Any();

                                // We could not find the tournament in the current list, so we are going to add it
                                if (hasTournament == false)
                                {
                                    listJobsTournament.Add(jobTournament);
                                }
                            }
                        }

                        sqlDataReader.Close();
                    }

                    sqlConnection.Close();
                }
            }

            listJobsTournament =
                listJobsTournament.OrderByDescending(x => x.AgeCategoryId)
                    .ThenByDescending(x => x.TournamentName)
                    .ToList();
            return listJobsTournament;
        }

        /// <summary>
        /// The get tournaments by job id.
        /// </summary>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsByJobId(int jobId)
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand("SportsData_GetTournamentsInJob", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@JobId", jobId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", this.SeasonId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                    var tournaments = new List<Tournament>();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var tournament = new Tournament
                            {
                                TournamentId =
                                    Convert.ToInt32(sqlDataReader["TournamentId"]),
                                TournamentName =
                                    sqlDataReader["TournamentName"].ToString(),
                                TournamentNumber =
                                    sqlDataReader["TournamentNumber"].ToString(),
                                TournamentSelected = true,
                                SportId = Convert.ToInt32(sqlDataReader["SportId"])
                            };
                            tournaments.Add(tournament);
                        }
                    }

                    sqlDataReader.Close();

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }

                    return tournaments;
                }

                // start by finding out if the client has any jobs related to it
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return null;
            }
        }

        /// <summary>
        /// The get tournaments by job id and sport id.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Tournament> GetTournamentsByJobIdAndSportId()
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand("SportsData_GetCustomerTournaments", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", this.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@JobId", this.JobId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var tournaments = new List<Tournament>();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var tournament = new Tournament
                            {
                                TournamentId =
                                    Convert.ToInt32(sqlDataReader["TournamentId"]),
                                TournamentNumber =
                                    sqlDataReader["TournamentNumber"].ToString(),
                                TournamentAccepted =
                                    Convert.ToInt32(sqlDataReader["TournamentAccepted"])
                            };
                            tournaments.Add(tournament);
                        }
                    }

                    sqlDataReader.Close();
                    return tournaments.AsQueryable();
                }

                // start by finding out if the client has any jobs related to it
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                Logger.Error(e.StackTrace);

                return null;
            }
        }

        /// <summary>
        /// The get all tournaments in season.
        /// </summary>
        /// <param name="seasonId">
        /// The season Id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetTournamentsBySeasonId", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", seasonId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var tournaments = new List<Tournament>();
                    if (!sqlDataReader.HasRows)
                    {
                        return new List<Tournament>();
                    }

                    while (sqlDataReader.Read())
                    {
                        var tournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        var tournamentName = sqlDataReader["TournamentName"].ToString();
                        var databaseSeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]);
                        var sportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        var agecategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryId"]);
                        var tournamentNumber = sqlDataReader["TournamentNumber"].ToString();

                        var ageCategoryDefinitionId = 0;
                        var ageCategoryDefinition = string.Empty;
                        if (sqlDataReader["AgeCategoryDefinitionId"] != DBNull.Value)
                        {
                            ageCategoryDefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                        }

                        if (sqlDataReader["AgeCategoryDefinition"] != DBNull.Value)
                        {
                            ageCategoryDefinition = sqlDataReader["AgeCategoryDefinition"].ToString();
                        }

                        var tournament = new Tournament
                        {
                            TournamentId = tournamentId,
                            TournamentName = tournamentName,
                            SeasonId = databaseSeasonId,
                            AgeCategoryId = agecategoryId,
                            SportId = sportId,
                            TournamentNumber = tournamentNumber,
                            AgeCategoryDefinitionId = ageCategoryDefinitionId,
                            AgeCategoryDefinition = ageCategoryDefinition
                        };

                        tournaments.Add(tournament);
                    }

                    return tournaments;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return new List<Tournament>();
        }

        /// <summary>
        /// The get tournaments by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsBySportId(int sportId, int seasonId)
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand("SportsData_GetTournamentsBySportIdAndSeasonId", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", seasonId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                    var tournaments = new List<Tournament>();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var tournament = new Tournament
                            {
                                TournamentId =
                                    Convert.ToInt32(sqlDataReader["TournamentId"]),
                                TournamentName =
                                    sqlDataReader["TournamentName"].ToString(),
                                TournamentNumber =
                                    sqlDataReader["TournamentNumber"].ToString(),
                                TournamentSelected = true,
                                SportId = Convert.ToInt32(sqlDataReader["SportId"]),
                                AgeCategoryDefinitionId =
                                    sqlDataReader["AgeCategoryDefinitionId"]
                                    == DBNull.Value
                                        ? 0
                                        : Convert.ToInt32(
                                            sqlDataReader["AgeCategoryDefinitionId"])
                            };
                            tournaments.Add(tournament);
                        }
                    }

                    sqlDataReader.Close();

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }

                    return tournaments;
                }

                // start by finding out if the client has any jobs related to it
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return null;
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobjects">
        /// The domain objects.
        /// </param>
        public void InsertAll(List<Tournament> domainobjects)
        {
            foreach (var domainobject in domainobjects)
            {
                // Maybe we should check if the tournament exists
                var foundTournament = this.Get(domainobject.TournamentId);
                if (foundTournament.TournamentId != 0)
                {
                    // Checking if sportId is the same for both objects
                    if (foundTournament.SportId != domainobject.SportId)
                    {
                        // We must update
                        Logger.Debug("We will update the tournament because the sportId is not the same");
                        this.Update(domainobject);
                    }
                    else
                    {
                        // Just for logging
                        Logger.InfoFormat("Tournament {0} ({3}), with sportId {1} in season {2} existed in the database",
                            domainobject.TournamentName, domainobject.SportId, domainobject.SeasonId, domainobject.TournamentId);
                    }
                }
                else
                {
                    this.InsertOne(domainobject);
                }
            }
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(Tournament domainobject)
        {
            Logger.InfoFormat("About to insert Tournament {0} with sportId {1} in season {2} existed in the database",
                domainobject.TournamentName, domainobject.SportId, domainobject.SeasonId);
            var insertId = 0;
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_InsertTournament", sqlConnection);

                    if (domainobject.AgeCategoryId == null)
                    {
                        domainobject.AgeCategoryId = 0;
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", domainobject.AgeCategoryId));
                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));
                    sqlCommand.Parameters.Add(new SqlParameter("@GenderId", domainobject.GenderId));

                    // We are checking if we are updating a soccer tournament or any other tournament
                    sqlCommand.Parameters.Add(
                        domainobject.SportId == 16
                            ? new SqlParameter("@Push", 1)
                            : new SqlParameter("@Push", DBNull.Value));

                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentName", domainobject.TournamentName));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", domainobject.TournamentNumber));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentTypeId", domainobject.TournamentTypeId));
                    sqlCommand.Parameters.Add(
                        new SqlParameter("@AgeCategoryDefinitionId", domainobject.AgeCategoryDefinitionId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Parameters.Clear();
                    sqlCommand.CommandText = "SELECT @@IDENTITY";
                    sqlCommand.CommandType = CommandType.Text;

                    if (sqlCommand.ExecuteScalar() != DBNull.Value)
                    {
                        insertId = Convert.ToInt32(sqlCommand.ExecuteScalar());
                    }

                    sqlCommand.Dispose();

                    Logger.DebugFormat("{0} inserted successfully", domainobject.TournamentName);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return 0;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return insertId;
            }
        }

        /// <summary>
        /// The search.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Search(Tournament domainobject)
        {
            Logger.DebugFormat("Trying to find {0} with id {1}", domainobject.TournamentName, domainobject.TournamentId);
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand("SportsData_FindTournament", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", this.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", domainobject.TournamentNumber));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    // If we have a row we return true
                    if (sqlDataReader.HasRows)
                    {
                        Logger.Debug("Found tournament");
                        return true;
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }

            Logger.Debug("Could not find tournament");
            return false;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void Update(Tournament domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_UpdateTournament", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));

                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", domainobject.AgeCategoryId));

                    sqlCommand.Parameters.Add(new SqlParameter("@GenderId", domainobject.GenderId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentTypeId", domainobject.TournamentTypeId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", domainobject.TournamentNumber));

                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryDefinitionId", domainobject.AgeCategoryDefinitionId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (SqlException exception)
                {
                    Logger.Error(exception);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public Tournament GetTournamentByTournamentid(int tournamentId)
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand("SportsData_GetTournamentsById", sqlConnection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    // If we have a row we return true
                    if (!sqlDataReader.HasRows)
                    {
                        Logger.Debug("Did not find tournament");
                        return new Tournament();
                    }

                    var tournament = new Tournament();
                    while (sqlDataReader.Read())
                    {
                        tournament.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        tournament.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        tournament.SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]);
                        tournament.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        tournament.TournamentName = sqlDataReader["TournamentName"].ToString();
                        tournament.Push = false;
                        if (sqlDataReader["Push"] != DBNull.Value)
                        {
                            tournament.Push = Convert.ToBoolean(sqlDataReader["Push"]);
                        }
                        tournament.GenderId = Convert.ToInt32(sqlDataReader["AgeCategoryId"]);
                        tournament.AgeCategoryId = Convert.ToInt32(sqlDataReader["GenderId"]);
                        tournament.TournamentTypeId = Convert.ToInt32(sqlDataReader["TournamentTypeId"]);
                        tournament.TournamentNumber = sqlDataReader["TournamentNumber"].ToString();
                        tournament.AgeCategoryDefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                    }

                    return tournament;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }

            Logger.Debug("Could not find tournament");
            return new Tournament();
        }

        /// <summary>
        /// The update tournament age category definition.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="ageCategoryDefinitionId">
        /// The age category definition id.
        /// </param>
        public void UpdateTournamentAgeCategoryDefinition(int tournamentId, int ageCategoryDefinitionId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_UpdateTournamentAgeCategoryDefinition", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryDefinitionId", ageCategoryDefinitionId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        #endregion
    }
}