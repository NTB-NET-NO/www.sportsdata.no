﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.NFF.TournamentAccess;
using NTB.SportsData.Facade.NIF.SportAccess;
using NTB.SportsData.Web.Areas.Admin.Repositories;

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    /// <summary>
    /// The season model.
    /// </summary>
    public class SeasonModel
    {
        /// <summary>
        /// The activate season.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        public void ActivateSeason(int id)
        {
            var repository = new SeasonRepository();
            repository.ActivateSeason(id);
        }

        /// <summary>
        /// The de activate season.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        public void DeActivateSeason(int id)
        {
            var repository = new SeasonRepository();
            repository.DeActivateSeason(id);
        }

        /// <summary>
        /// The delete season.
        /// </summary>
        /// <param name="season">
        /// The season.
        /// </param>
        public void DeleteSeason(Season season)
        {
            var repository = new SeasonRepository();
            repository.Delete(season);
        }

        /// <summary>
        /// The get season by season id.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="Season"/>.
        /// </returns>
        public Season GetSeasonBySeasonId(int seasonId)
        {
            var repository = new SeasonRepository();
            return repository.Get(seasonId);
        }

        public List<Season> GetSeasonByOrgId(int orgId)
        {
            var repository = new SeasonRepository();
            return repository.GetSeasonsByOrgId(orgId);
        }

        /// <summary>
        /// The get seasons by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public IEnumerable<Season> GetSeasonsBySportId(int sportId)
        {
            var repository = new SeasonRepository();
            var seasons = repository.GetSeasonsBySportId(sportId)
                .ToList();

            List<Season> federationSeasons;

            // Now we get the list of seasons from the APIs.
            if (sportId == 16)
            {
                foreach (var season in seasons)
                {
                    season.FederationDiscipline = 207;
                }

                var facade = new TournamentFacade();
                federationSeasons = facade.GetSeasons();

                foreach (var season in federationSeasons)
                {
                    season.SportId = 16;
                    season.FederationDiscipline = 207;
                }
            }
            else
            {
                var federationRespository = new FederationRepository();
                var federation = federationRespository.GetFederationBySportId(sportId);
                var facade = new SportFacade();
                federationSeasons = facade.GetFederationSeasons(federation.FederationId);
            }

            foreach (var season in federationSeasons)
            {
                var season1 = season;
                var foo = from s in seasons where s.SeasonId == season1.SeasonId select s;
                season1.SportId = sportId;
                if (!foo.Any())
                {
                    seasons.Add(season1);
                }
            }

            return seasons;
        }

        /// <summary>
        /// The update season.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        public void UpdateSeason(int id, FormCollection collection)
        {
            var repository = new SeasonRepository();
            var season = new Season
            {
                SeasonId = id,
                SeasonName = collection["SeasonName"],
                SeasonActive = false,
                SeasonStartDate = Convert.ToDateTime(collection["SeasonStartDate"]),
                SeasonEndDate = Convert.ToDateTime(collection["SeasonEndDate"]),
                SportId = Convert.ToInt32(collection["SportId"])
            };

            if (collection["seasonactive"] != null)
            {
                var seasonActive = collection["seasonactive"].ToLower();
                if (seasonActive == "on" || seasonActive == "true" || seasonActive == "1")
                {
                    season.SeasonActive = true;
                }
            }

            repository.Update(season);
        }

        /// <summary>
        /// The insert seasons.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="seasons">
        /// The seasons.
        /// </param>
        public void InsertSeasons(int sportId, IEnumerable<Season> seasons)
        {
            //var repository = new SeasonRepository();

            //foreach (Season season in seasons)
            //{
            //    Season foundSeason = repository.GetSeason(season);

            //    if (foundSeason.SeasonId == season.SeasonId)
            //    {
            //        continue;
            //    }

            //    repository.InsertOne(season);
            //}

            var repository = new SeasonRepository();

            // var foundSeason = repository.GetSeason(season);
            var localSeasons = repository.GetSeasonsBySportId(sportId);

            // If we have the same amount of seasons, then we don't have to insert
            var arraySeason = seasons as Season[] ?? seasons.ToArray();

            var remoteSeasons = new List<Season>();
            if (arraySeason.Any() && localSeasons.Count == 0)
            {
                remoteSeasons.AddRange(arraySeason);
            }
            else
            {
                foreach (var season in arraySeason)
                {
                    var found = false;
                    var currentSeason = season;
                    foreach (var localSeason in localSeasons.Where(localSeason => localSeason.SeasonId == currentSeason.SeasonId))
                    {
                        found = true;
                    }

                    if (found == false)
                    {
                        remoteSeasons.Add(currentSeason);
                    }
                }
            }

            // Looping over remote seasons

            // If we don't find any new seasons, we shall return
            if (!remoteSeasons.Any())
            {
                return;
            }

            if (arraySeason.Count() == localSeasons.Count)
            {
                return;
            }

            foreach (var season in remoteSeasons)
            {
                repository.InsertOne(season);
            }

            // repository.InsertAll(seasons.ToList());
        }

        /// <summary>
        /// The set season disciplines.
        /// </summary>
        /// <param name="seasonDisciplines">
        /// The season disciplines.
        /// </param>
        public void SetSeasonDisciplines(Dictionary<int, int> seasonDisciplines)
        {
            // TODO: Loop over season Disciplines
            // TODO: Create Set Season Discipline Method in Season Repository
            foreach (var kvp in seasonDisciplines)
            {
                var seasonRepository = new SeasonRepository();
                seasonRepository.UpdateSeasonWithDiscipline(kvp.Key, kvp.Value);
            }
        }
    }
}