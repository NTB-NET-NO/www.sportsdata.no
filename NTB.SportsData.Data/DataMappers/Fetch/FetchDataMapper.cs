﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;
using NTB.SportsData.Data.Interfaces.DataMappers;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.DataMappers.Fetch
{
    public class FetchDataMapper : IFetchDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FetchDataMapper));

        public void UpdateTournamentFetchDateTime(int orgId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_UpdateTournamentFetchDateByOrgId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", orgId));
                    
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public DataBaseFetch GetLatestTournamentFetchDateTimeByOrgId(int orgId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetLatestTournamentFetchDateByOrgId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", orgId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var dataBaseFetch = new DataBaseFetch();
                    if (!sqlDataReader.HasRows)
                    {
                        Logger.Debug("There were no rows found");
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["OrganisationId"] != DBNull.Value)
                        {
                            dataBaseFetch.OrganisationId = Convert.ToInt32(sqlDataReader["OrganisationId"]);
                        }

                        if (sqlDataReader["UpdateDateTime"] != DBNull.Value)
                        {
                            dataBaseFetch.LatestFetchDateTime = Convert.ToDateTime(sqlDataReader["UpdateDateTime"]);
                        }
                    }

                    return dataBaseFetch;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}
