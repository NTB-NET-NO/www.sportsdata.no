﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRegisterDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The RegisterDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NFF.Interfaces
{
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The RegisterDataMapper interface.
    /// </summary>
    public interface IRegisterDataMapper
    {
        /// <summary>
        /// The insert match result.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool InsertMatchResult(Match match);
    }
}