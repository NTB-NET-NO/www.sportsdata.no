﻿using System;
using System.Configuration;
using System.ServiceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.UnitTest
{
    [TestClass]
    public class NFFServiceTest
    {
        public NFFServiceTest()
        {
            if (this._tournamentServiceClient.State == CommunicationState.Faulted)
            {
                this._tournamentServiceClient.Close();
                this._tournamentServiceClient = null;

                this._tournamentServiceClient = new TournamentServiceClient();
            }

            if (this._tournamentServiceClient.ClientCredentials == null)
            {
                return;
            }

            this._tournamentServiceClient.ClientCredentials.UserName.UserName =
                ConfigurationManager.AppSettings["NFFServicesUsername"];
            this._tournamentServiceClient.ClientCredentials.UserName.Password =
                ConfigurationManager.AppSettings["NFFServicesPassword"];

            if (this._metaServiceClient.ClientCredentials != null)
            {
                this._metaServiceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServicesUsername"];
                this._metaServiceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicesPassword"];
            }

            if (this._organizationServiceClient.ClientCredentials != null)
            {
                this._organizationServiceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServicesUsername"];
                this._organizationServiceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicesPassword"];
            }
        }
        /// <summary>
        /// The _meta service client.
        /// </summary>
        private readonly MetaServiceClient _metaServiceClient = new MetaServiceClient();

        /// <summary>
        /// The _organization service client.
        /// </summary>
        private readonly OrganizationServiceClient _organizationServiceClient = new OrganizationServiceClient();

        /// <summary>
        /// The _tournament service client.
        /// </summary>
        private readonly TournamentServiceClient _tournamentServiceClient = new TournamentServiceClient();

        [TestMethod]
        public void GetTournamentsBySeasonIdTest()
        {
            try
            {
                var dataMapper = new Services.NFF.Repositories.TournamentRepository();

                var tournaments = dataMapper.GetTournamentsBySeasonId(83);

                var actual = 4007;
                Assert.AreEqual(tournaments.Count, actual);

            }
            catch (Exception exception)
            {
                Assert.Fail(exception.Message);
            }
        }

        [TestMethod]
        public void ResetOrganizationServiceClientTest()
        {
            try
            {
                var dataMapper = new Services.NFF.Repositories.TournamentRepository();

                var service = this._organizationServiceClient;
                if (service.GetType() == new OrganizationServiceClient().GetType())
                {

                }
                dataMapper._ResetOrganizationServiceClient(service);

                Assert.AreEqual("john", "john");
            }
            catch (Exception exception)
            {
                Assert.Fail("test");
            }
        }
    }
}
