﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchContext.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match context.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Models.Matches
{
    using System.Collections.Generic;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Api.Interfaces;
    using NTB.SportsData.Data.Matches;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The match context.
    /// </summary>
    public class MatchContext : IMatchDataMapper
    {
        #region Fields

        /// <summary>
        ///     The _match data mapper.
        /// </summary>
        private Data.Interfaces.IMatchDataMapper _matchDataMapper;

        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(MatchContext));

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        public MatchExtended Get(int id)
        {
            this._matchDataMapper = new MatchRemoteDataMapper();
            var match = this._matchDataMapper.GetMatchById(id);

            //this._matchDataMapper = new MatchDataMapper();

            //var match = this._matchDataMapper.GetMatchById(id);

            //if (match == null)
            //{
            //    this._matchDataMapper = new MatchRemoteDataMapper();
            //    match = this._matchDataMapper.GetMatchById(id);
            //}

            return match;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public IEnumerable<Match> GetAll(Tournament tournament)
        {
            this._matchDataMapper = new MatchDataMapper();
            List<Match> matches = this._matchDataMapper.GetMatchesByTournamentId(tournament);

            return matches.AsEnumerable();
        }

        /// <summary>
        /// The get todays matches.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public IEnumerable<Match> GetTodaysMatches(int customerId)
        {
            this._matchDataMapper = new MatchDataMapper();
            List<Match> matches = this._matchDataMapper.GetTodaysMatchesByCustomerId(customerId);

            return matches;
        }

        #endregion

        /// <summary>
        ///     Gets the match extended data
        /// </summary>
        /// <param name="id">
        /// The value of the match we are fetching
        /// </param>
        /// <returns>
        /// A Match Extended object
        /// </returns>
        public MatchExtended GetMatchExtended(int id)
        {
            Logger.InfoFormat("GetMatchExtended by id: {0}", id);
            this._matchDataMapper = new MatchRemoteDataMapper();
            var match = this._matchDataMapper.GetMatchExtendedById(id);
            
            return match;
        }
    }
}