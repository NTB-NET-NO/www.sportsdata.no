﻿using System;
using NTB.SportsData.Common;

namespace NTB.SportsData.Web.Exceptions
{
    public class SportsDataWebException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SportsDataInputException"/> class. 
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public SportsDataWebException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SportsDataInputException"/> class. 
        /// </summary>
        /// <param name="message">
        /// The error message.
        /// </param>
        /// <param name="exception">
        /// INternal exception.
        /// </param>
        public SportsDataWebException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}