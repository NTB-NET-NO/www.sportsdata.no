﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Form1.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The form 1.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiPostClient
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Windows.Forms;

    /// <summary>
    /// The form 1.
    /// </summary>
    public partial class Form1 : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Form1"/> class.
        /// </summary>
        public Form1()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// The btn send_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnSendClick(object sender, EventArgs e)
        {
            try
            {
                var bytes = System.Text.Encoding.Default.GetBytes(this.txtContent.Text);
                var postData = GetBytes(System.Text.Encoding.UTF8.GetString(bytes));

                const string Uri = "http://stage.sportsdata.no/api/Api/FileClient/OdfTextReceiver";

                byte[] response;

                using (var client = new WebClient())
                {
                    response = client.UploadData(Uri, "POST", postData);
                }

                this.txtResonse.Text += Environment.NewLine;
                this.txtResonse.Text += @"response: " + response;
            }
            catch (Exception exception)
            {
                this.txtResonse.Text += Environment.NewLine + @"Starting loop" + Environment.NewLine;
                foreach (KeyValuePair<string, string> kvp in exception.Data)
                {
                    this.txtResonse.Text += @"key: " + kvp.Key + @", value: " + kvp.Value + Environment.NewLine;
                }

                this.txtResonse.Text += @"ending loop" + Environment.NewLine;
                this.txtResonse.Text += @"exception: " + exception.Message + Environment.NewLine + exception.StackTrace;
            }

        }

        /// <summary>
        /// The get bytes.
        /// </summary>
        /// <param name="str">
        /// The str.
        /// </param>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        private static byte[] GetBytes(string str)
        {
            var bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}