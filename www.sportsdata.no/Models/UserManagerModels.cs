﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using Microsoft.AspNet.Identity;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Data.DataMappers.UserProfiles;
using NTB.SportsData.Web.Models.IdentityModels;
using NTB.SportsData.Utilities;

namespace NTB.SportsData.Web.Models
{
    public class UserManagerModels
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(UserManagerModels));

        public void InsertUser(FormCollection collection)
        {
            var context = new ApplicationDbContext();
            // var manager = new UserManager<User>(new UserStore<User>(new ApplicationDbContext()));
            var userName = collection["username"].Trim();
            var emailaddress = collection["email"].Trim();
            var userFirstName = collection["firstname"].Trim();
            var userLastName = collection["lastname"].Trim();
            var password = collection["password"].Trim();
            var roleName = collection["role"].Trim();

            var applicationId = ApplicationUtilities.GetApplicationId();
            var user = new ApplicationUser
            {
                UserName = userName,
                FirstName = userFirstName,
                LastName = userLastName,
                ApplicationId = applicationId,
                Email = emailaddress,
                LoweredEmail = emailaddress.ToLower()
            };

            var userStore = new ApplicationUserStore();
            var userManager = new ApplicationUserManager(userStore);

            var adminResult = userManager.Create(user, password);

            if (adminResult.Succeeded)
            {
                // var result = userManager.AddToRole(user.Id, roleName);

                userManager.AddToRole(user.Id, roleName);

            }
            // context.SaveChanges();
        }

        public void UpdateUser(FormCollection collection, Guid userId)
        {
            var manager = new ApplicationUserManager(new ApplicationUserStore());
            var user = new ApplicationUser
            {
                FirstName = collection["firstname"].Trim(),
                LastName = collection["lastname"].Trim(),
                Email = collection["email"].Trim(),
                UserName = collection["username"].Trim(),
                Id = userId.ToString()
            };

            // var result = manager.UpdateAsync(user);

            manager.UpdateAsync(user);
        }

        public UserProfileView GetUserProfileView()
        {
            var userProfileViews = new UserProfileView();
            
            var profiles = GetUserProfiles();

            
            var userProfiles = new List<UserProfile>();
            foreach (UserProfile profile in profiles)
            {
                // We need to check which Work Role this user have (today)
                var id = 2;
                if (profile.WorkRole != null)
                {
                    id = profile.WorkRole.Id;
                }
            
                var userProfile = profile;

                userProfiles.Add(userProfile);
            }

            userProfileViews.UserProfiles = userProfiles;

            return userProfileViews;
        }

        public List<UserProfile> GetUserProfiles()
        {
            // (from MembershipUser u in Membership.GetAllUsers() select Membership.GetUser(u.UserName, true)).ToList();

            
            var userProfiles = new List<UserProfile>();

            var userContext = new ApplicationDbContext();
            
            var userList = userContext.Users.ToList();

            
            foreach (ApplicationUser u in userList)
            {
                
                var userProfile = new UserProfile();
                
                var guid = new Guid();
                if (u.Id != null)
                {
                    guid = new Guid(u.Id);
                }
                
                userProfile.FirstName = u.FirstName;
                userProfile.LastName = u.LastName;
                userProfile.UserId = guid;
                userProfile.UserName = u.UserName;
                userProfile.Email = u.Email;
                userProfile.LastLoginDate = u.LastLoginDate;
                
                userProfile.SelectedRole = GetRolesForUser(u);
                userProfiles.Add(userProfile);
            }

            return userProfiles;
        }

        private Role GetRolesForUser(ApplicationUser user)
        {
            
            var roleStore = new ApplicationRoleStore();
            var roleManager = new ApplicationRoleManager(roleStore);

            if (user.Roles == null)
            {
                user.Roles = new List<IRole>();
            }

            var role = new Role();
            if (user.UserName != null)
            {
                role = GetRoleByUserName(user.UserName);
                user.Roles.Add(role);
                return role;

            }
            else if (user.Id != null)
            {
                
                role = GetRoleByUserId(user.Id);
                user.Roles.Add(role);
                return role;
            }
            else
            {
                throw new NullReferenceException(user.UserName);
            }


            //var selectedRole = new Role();

            //foreach (var role in user.Roles)
            //{
            //    var newRole = new Role
            //    {
            //        Id = roleManager.FindByIdAsync(role.Id).Id.ToString(CultureInfo.InvariantCulture),
            //        Name = role.Name
            //    };

            //    selectedRole = newRole;
            //}

            //return selectedRole;
            
        }

        private Role GetRoleByUserName(string userName)
        {
            var roleStore = new ApplicationRoleStore();
            var roleManager = new ApplicationRoleManager(roleStore);
            return roleManager.FindRoleByUserName(userName);

        }

        private Role GetRoleByUserId(string userId)
        {
            var roleStore = new ApplicationRoleStore();
            var roleManager = new ApplicationRoleManager(roleStore);
            return roleManager.FindRoleByUserId(userId);

        }

        public RegisterViewModel GetViewModel()
        {
            // var userContext = new ApplicationDbContext();
            var roleStore = new ApplicationRoleStore();
            var roleManager = new ApplicationRoleManager(roleStore);
            
            var roles = roleManager.Roles.OrderBy(x => x.Name).ToList();

            var viewModel = new RegisterViewModel
            {
                IdentityRoles = roles
            };

            return viewModel;
        }

        public UpdateViewModel GetUserProfileByUserId(Guid userId)
        {
            var viewModel = new UpdateViewModel();
            var userContext = new ApplicationDbContext();

            var roleStore = new ApplicationRoleStore();

            // var userList = userContext.Users.ToList();

            var user = userContext.Users.Single(u => String.Equals(u.Id, userId.ToString(), StringComparison.CurrentCultureIgnoreCase));

            var roles = roleStore.Roles.OrderBy(x => x.Name).ToList();
            
            if (user == null)
            {
                return viewModel;
            }
            else
            {
                var userStore = new ApplicationUserStore();
                var userManager = new ApplicationUserManager(userStore);

                var selectedRole = GetRolesForUser(user);
                //var items = new List<SelectListItem>();
                //foreach (IdentityRole role in roles)
                //{
                //    var item = new SelectListItem
                //    {
                //        Value = role.Id, 
                //        Text = role.Name,
                //        Selected = selectedRole.Id == role.Id // This is same as if (foo == true) else ...
                //    };
                    
                //    items.Add(item);
                //}

                var userProfile = new UpdateViewModel
                {
                    UserName = user.UserName,
                    EmailAddress = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserId = userId.ToString(),
                    IdentityRoles = roles,
                    SelectedRoleId = selectedRole.Id,
                    SelectedRoleName = selectedRole.Name,
                    OldRole = GetRolesForUser(user).Name
                };

                return userProfile;
            }
        }

        public UserProfile GetUserProfile(string username)
        {
            // var user = Membership.GetUser(username);

            
            var userContext = new ApplicationDbContext();
            var userList = userContext.Users.ToList();
            // var userList = userContext.Users.ToList();
            try
            {
                var user = userContext.Users.Single(u => u.Email == username);


                if (user == null)
                {
                    return new UserProfile();
                }

                var dataUser = new UserProfileDataMapper().GetUserProfile(new Guid(user.Id));
                var userProfile = new UserProfile
                {
                    Email = dataUser.Email,
                    FirstName = dataUser.FirstName,
                    LastName = dataUser.LastName,
                    LastLoginDate = user.LastLoginDate
                };

                return userProfile;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    Logger.Error(exception.InnerException.Message);
                    Logger.Error(exception.InnerException.StackTrace);
    
                }

                return new UserProfile();
            }
        }
    }
}