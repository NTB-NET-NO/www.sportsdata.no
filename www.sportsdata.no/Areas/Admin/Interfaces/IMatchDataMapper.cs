﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Interfaces
{
    interface IMatchDataMapper
    {
        List<Match> GetMatchesByTournamentId(int tournamentId);
    }
}
