﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The team remote data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Team
{
    using System.Collections.Generic;

    using NTB.SportsData.Data.Interfaces;
    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Interfaces;
    using NTB.SportsData.Facade.NFF.ApiAccess;
    using NTB.SportsData.Facade.NIF.ApiAccess;

    /// <summary>
    /// The team remote data mapper.
    /// </summary>
    public class TeamRemoteDataMapper : ITeamDataMapper
    {
        /// <summary>
        /// The get tournament teams.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Team> GetTournamentTeams(Tournament tournament)
        {
            IApiFacade apifacade;
            if (tournament.SportId == 16)
            {
                apifacade = new SoccerApiFacade();
            }
            else
            {
                apifacade = new SportApiFacade();
            }

            return apifacade.GetTournamentTeams(tournament.TournamentId);
        }
    }
}