﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SquadIndividualMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The squad individual mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The squad individual mapper.
    /// </summary>
    public class SquadIndividualMapper : BaseMapper<SquadIndividual, ContactPerson>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<SquadIndividual, ContactPerson> mapper)
        {
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.LastName, y => y.LastName);
            mapper.Relate(x => x.SquadIndividualCategoryName, y => y.RoleName);
            mapper.Relate(x => x.SquadIndividualCategoryId, y => y.RoleId);
            mapper.Relate(x => x.OrgId, y => y.TeamId, this.NullableIntToIntConverter());
        }
    }
}