﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HomeController.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The home controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Controllers
{
    using System.Web.Mvc;

    using log4net;

    /// <summary>
    ///     The home controller.
    /// </summary>
    [Authorize]
    [AllowAnonymous]
    public class HomeController : Controller
    {
        #region Static Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger("ApiFileAppender");

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The index.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult Index()
        {
            Logger.Info("In Home Controller");
            this.ViewBag.Title = "Home Page";

            return this.View();
        }

        #endregion
    }
}