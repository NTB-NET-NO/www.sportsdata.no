﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWorkRoleDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The WorkRoleDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    using System;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The WorkRoleDataMapper interface.
    /// </summary>
    public interface IWorkRoleDataMapper
    {
        /// <summary>
        /// The delete all.
        /// </summary>
        void DeleteAll();

        /// <summary>
        /// The get work role by user id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="WorkRole"/>.
        /// </returns>
        WorkRole GetWorkRoleByUserId(Guid userId);
    }
}