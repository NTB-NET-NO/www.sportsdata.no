<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:date="http://exslt.org/dates-and-times"
    extension-element-prefixes="date"
    exclude-result-prefixes="xs" 
    version="1.0">
    
    <xsl:output method="text" indent="yes"/>
    <xsl:variable name="newline">
        <xsl:text>
        </xsl:text>
    </xsl:variable>
    <xsl:template match="/">
        <xsl:text>{"events": [</xsl:text>
        <xsl:for-each select="/Matches/MatchExtended">
            <xsl:text>{</xsl:text>
            <xsl:value-of select="$newline" />
            <xsl:text>"competitionid": </xsl:text>
            <xsl:value-of select="TournamentId"/>
            <xsl:text>, </xsl:text>
            <xsl:value-of select="$newline" />
            <xsl:text>"competitionname": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="TournamentName"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"competitionround": </xsl:text>
            <xsl:value-of select="TournamentRoundNumber"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"eventid": </xsl:text>
            <xsl:value-of select="MatchId"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"eventname": "</xsl:text>
            <xsl:value-of select="HomeTeam"/>
            <xsl:text> - </xsl:text>
            <xsl:value-of select="AwayTeam"/>
            <xsl:text>"</xsl:text>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"eventstartdate": "</xsl:text>
            <xsl:value-of select="substring-before(MatchDate, 'T')"/>
            <xsl:text>T</xsl:text>
            <xsl:value-of select="substring-after(MatchDate, 'T')"/>
            <xsl:text>"</xsl:text>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"eventvenuename": "</xsl:text>
            <xsl:value-of select="VenueName"/>
            <xsl:text>"</xsl:text>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"eventstatus": "</xsl:text>
            
            <!-- <xsl:variable name="today" select="translate(substring-before(date:date-time(), 'T'), '-', '')" />
            <xsl:variable name="start" select="translate(substring-before(MatchDate, 'T'), '-', '')" />
            
            <xsl:if test="$start &lt;= $today">
                <xsl:text>finished</xsl:text>
            </xsl:if> -->
            <xsl:choose>
                <xsl:when test="Played = 'true'">
                    <xsl:text>post-event</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>pre-event</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
            
            <xsl:text>"</xsl:text>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"eventoutcome": "</xsl:text>
            <xsl:choose>
                <xsl:when test="HomeTeamGoal &gt; AwayTeamGoal">
                    <xsl:text>win</xsl:text>
                </xsl:when>
                <xsl:when test="HomeTeamGoal = AwayTeamGoal">
                    <xsl:text>tie</xsl:text>
                </xsl:when>
                <xsl:when test="AwayTeamGoal &gt; HomeTeamGoal">
                    <xsl:text>loss</xsl:text>
                </xsl:when>
            </xsl:choose>
            <xsl:text>"</xsl:text>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"teams": [</xsl:text>
            <!-- Hometeam -->
            <xsl:text>{</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"name": "</xsl:text>
            <xsl:value-of select="HomeTeam"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"teamid": </xsl:text>
            <xsl:value-of select="HomeTeamId"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"score": </xsl:text>
            <xsl:value-of select="HomeTeamGoal"/>
            <xsl:text></xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>},</xsl:text>
            <xsl:value-of select="$newline" />
            <!-- Awayteam -->
            <xsl:text>{</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"name": "</xsl:text>
            <xsl:value-of select="AwayTeam"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"teamid": </xsl:text>
            <xsl:value-of select="AwayTeamId"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"score": </xsl:text>
            <xsl:value-of select="AwayTeamGoal"/>
            <xsl:text></xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>}</xsl:text>
            <xsl:value-of select="$newline" />
            <xsl:text>]</xsl:text>
            <xsl:value-of select="$newline" />
            <xsl:choose>
                <xsl:when test="position() != last()">
                    <xsl:value-of select="$newline" />
                    <xsl:text>},</xsl:text>
                    <xsl:value-of select="$newline" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>}</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
        <xsl:value-of select="$newline" />
        <xsl:text>]</xsl:text>
        <xsl:value-of select="$newline" />
        <xsl:text>}</xsl:text>
    </xsl:template>
</xsl:stylesheet>