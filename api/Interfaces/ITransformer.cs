﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITransformationModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Interfaces
{
    /// <summary>
    ///     Interface used by models who shall transform from raw xml to json or sportsML XML
    /// </summary>
    public interface ITransformer
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the XsltFile used to transform the document
        /// </summary>
        string XsltFile { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The method doing the actuall transformation
        /// </summary>
        /// <param name="document">
        /// The document
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string TransformObject(string document);

        #endregion
    }
}