// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Tournaments.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournaments.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Data.Season;

namespace NTB.SportsData.Web.Areas.Admin.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
    using System.Web.Mvc;
    using System.Xml;

    using log4net;
    using log4net.Config;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.NFF.TournamentAccess;
    using NTB.SportsData.Web.Areas.Admin.Repositories;

    // Adding supprt for log4net

    // Adding reference to the NFF services
    // using NFFTournamentService;

    /// <summary>
    ///     The tournaments.
    /// </summary>
    public class Tournaments
    {
        // GetTeamsByMunicipality 
        #region Static Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Tournaments));

        /// <summary>
        ///     The _instance.
        /// </summary>
        private static Tournaments instance = new Tournaments();

        #endregion

        #region Fields

        /// <summary>
        ///     The _tournament facade
        /// </summary>
        private readonly TournamentFacade tournamentFacade = new TournamentFacade();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Tournaments" /> class.
        ///     Prevents a default instance of the <see cref="Tournaments" /> class from being created.
        /// </summary>
        public Tournaments()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }

            // Setting up different needed remote data sets

            // Districts
            this.tournamentFacade.GetDistricts();

            // Getting municipality
            this.tournamentFacade.GetMunicipalities();

            // Getting seasons
            this.SeasonId = this.tournamentFacade.GetOngoingSeason().SeasonId;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        public static Tournaments Instance
        {
            get
            {
                return instance ?? (instance = new Tournaments());
            }
        }

        /// <summary>
        ///     Gets or sets the job id.
        /// </summary>
        public int JobId { get; set; }

        /// <summary>
        ///     Gets or sets the season id.
        /// </summary>
        public int SeasonId { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The validate server certificate.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="certificate">
        /// The certificate.
        /// </param>
        /// <param name="chain">
        /// The chain.
        /// </param>
        /// <param name="sslPolicyErrors">
        /// The ssl policy errors.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        /// <summary>
        /// The create tournament file.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        public void CreateTournamentFile(int id, FormCollection collection)
        {
            var customerId = id;
            var startDate = DateTime.Today.ToShortDateString();
            var endDate = DateTime.Today.ToShortDateString();
            var foreignCustomerId = 0;

            if (collection["ttcustomerid"] != string.Empty)
            {
                foreignCustomerId = Convert.ToInt32(collection["ttcustomerid"]);
            }

            if (collection["startDate"] != string.Empty)
            {
                startDate = Convert.ToDateTime(collection["startDate"]).ToShortDateString();
            }

            if (collection["endDate"] != string.Empty)
            {
                endDate = Convert.ToDateTime(collection["endDate"]).ToShortDateString();
            }

            if (collection["tournament[]"] == null)
            {
                return;
            }

            var selectedTournaments = collection["tournament[]"].Split(',').ToArray();
            var selectedTournamentSports = collection["sport[]"].Split(',').ToArray();
            var xmlDocument = new XmlDocument();
            var dec = xmlDocument.CreateXmlDeclaration("1.0", null, null);
            xmlDocument.AppendChild(dec);

            var xmlRoot = xmlDocument.CreateElement("SportsData");
            var xmlRootMeta = xmlDocument.CreateElement("SportsData-Meta");
            xmlRootMeta.SetAttribute("type", "customer");

            // We should consider creating a form where we can decide start and end of getting match data
            xmlRootMeta.SetAttribute("startdate", DateTime.Today.ToShortDateString());
            xmlRootMeta.SetAttribute("enddate", DateTime.Today.ToShortDateString());
            xmlRoot.AppendChild(xmlRootMeta);

            // Creating the Customer Element <Customer id="30"> 
            // Done: Should also have the TT-customerId
            var xmlCustomer = xmlDocument.CreateElement("Customer");
            xmlCustomer.SetAttribute("id", id.ToString());
            xmlCustomer.SetAttribute("foreigncustomerid", foreignCustomerId.ToString());
            xmlRoot.AppendChild(xmlCustomer);

            var xmlJob = xmlDocument.CreateElement("Job");
            xmlJob.SetAttribute("startdate", startDate);
            xmlJob.SetAttribute("enddate", endDate);
            xmlCustomer.AppendChild(xmlJob);

            var xmlTournaments = xmlDocument.CreateElement("Tournaments");
            
            foreach (var tournament in selectedTournaments)
            {
                var tournamentId = Convert.ToInt32(tournament);

                // We need to find out discipline for this tournament
                var tournamentRepository = new TournamentRepository();
                
                var foundTournament = tournamentRepository.Get(tournamentId);

                var seasonRepository = new SeasonRepository();

                var season = seasonRepository.Get(foundTournament.SeasonId);
                var disciplineId = season.FederationDiscipline;
                
                if (foundTournament.OrganizationId == null)
                {
                    var sportRepository = new SportRepository();
                    var sport = sportRepository.Get(foundTournament.SportId);
                    if (sport.OrgId != null)
                    {
                        foundTournament.OrganizationId = sport.OrgId.Value;
                    }
                }

                if (disciplineId == 0)
                {
                    if (foundTournament.OrganizationId != null)
                    {
                        var seasonRemoteRepository = new SeasonRemoteDataMapper();
                        int orgId = foundTournament.OrganizationId.Value;
                        var remoteSeason = seasonRemoteRepository.GetFederationSeasonByOrgId(orgId);
                        var currentSeason = remoteSeason.Single(x => x.SeasonId == foundTournament.SeasonId);
                        disciplineId = currentSeason.FederationDiscipline;

                        seasonRepository.UpdateSeasonWithDiscipline(currentSeason.SeasonId, disciplineId);
                    }
                    
                }

                var xmlTournament = xmlDocument.CreateElement("Tournament");
                xmlTournament.SetAttribute("id", tournamentId.ToString());
                xmlTournament.SetAttribute("sportId", foundTournament.SportId.ToString());
                xmlTournament.SetAttribute("disciplineId", disciplineId.ToString());
                xmlTournaments.AppendChild(xmlTournament);

            }

            xmlCustomer.AppendChild(xmlTournaments);

            xmlDocument.AppendChild(xmlRoot);

            // Getting path from Config-file
            var outputFolder = ConfigurationManager.AppSettings["CustomerTriggerFolder"];

            // Checking that the folder exists (in case I forget when I put the system into production)
            var directoryInfo = new DirectoryInfo(outputFolder);
            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }

            var todayDate = DateTime.Today.ToString("yyyyMMdd");
            // var todayDate = DateTime.Today.Year.ToString().PadLeft(2, '0') + DateTime.Today.Month.ToString().PadLeft(2, '0') + DateTime.Today.Day.ToString().PadLeft(2, '0');

            var todayTime = DateTime.Now.ToString("hhmmss");
            // var todayTime = DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0');
            var fileName = string.Format("NTB_SportsData_{0}_{1}_{2}T{3}.xml", customerId, foreignCustomerId, todayDate, todayTime);
            // var filename = "NTB_SportsData_" + customerId + "_" + foreignCustomerId + "_" + todayDate + "T" + todayTime + ".xml";

            var outputFileName = Path.Combine(outputFolder, fileName);
            xmlDocument.Save(outputFileName);
            // xmlDocument.Save(outputFolder + @"\" + fileName);
        }

        /// <summary>
        /// Method to return the Tournaments that are connected to the MunicipalitiesDataAccess
        /// </summary>
        /// <param name="selectedMunicipalities">
        /// string of municipalities separated by the character | (pipe)
        /// </param>
        /// <param name="ageCategoryId">
        /// int of age category id
        /// </param>
        /// <param name="seasonId">
        /// The Id for this season
        /// </param>
        /// <param name="jobId">
        /// The job Id.
        /// </param>
        /// <returns>
        /// List containing JobTournament Object
        /// </returns>
        public List<Tournament> GetTournamentByMunicipalities(List<Municipality> selectedMunicipalities, int ageCategoryId, int seasonId, int jobId)
        {
            // Setting up the tournament array
            Logger.Debug("selectedMunicipalities");

            // Creating a JobTournament List
            var listJobsTournament = new List<Tournament>();

            // Looping the string array
            var teamInstances = new Dictionary<int, List<Team>>();

            // MunicipalitiesDataAccess municipalitiesDataAccess = new MunicipalitiesDataAccess();
            // municipalitiesDataAccess.DeleteMunicipalities(jobId);
            foreach (var municipality in selectedMunicipalities)
            {
                var municipalityId = municipality.MunicipalityId;

                // Getting the teams from the municipalities and find out in which tournaments they are participating in...

                // TODO: fix so that it gets the list from the service and then check if this is in the database for this customer.
                // If it is for the customer, we shall mark it as selected.
                if (municipalityId <= 0)
                {
                    continue;
                }

                try
                {
                    // I also need to store the municipalities in the database
                    // Insert into municipality has been moved to the controller, as we do everything there
                    // Consider - we are looping there, and we are looping here. What is best practice?
                    // could this be a speed issue?

                    // municipalitiesDataAccess.InsertMunicipality(municipalityId, jobId);
                    var facade = new TournamentFacade();
                    var teamResult = facade.GetTeamsByMunicipality(municipalityId, this.SeasonId);

                    teamInstances.Add(municipalityId, teamResult);

                    // municipalityTeams.AddRange(teams);
                    Logger.Debug("Season Id: " + seasonId);

                    // Looping the team object
                    Logger.Debug("Number of teams: " + teamResult.Count);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
            }

            var timerStart = DateTime.Now;
            Logger.Debug("Starting team getting: " + timerStart.ToShortTimeString());
            var listTournaments = new List<Tournament>();
            foreach (var kvp in teamInstances)
            {
                foreach (var team in kvp.Value)
                {
                    try
                    {
                        // Logging this
                        Logger.Debug("Team ID: " + team.TeamId + ", SeasonId: " + seasonId);
                        Logger.Debug("Club ID: " + team.ClubId);

                        // Now getting the tournament by team object
                        // tournaments = mySportsUtils.tclient.GetTournamentsByTeam(team.TeamId, seasonId, null);
                        var timerTournament = DateTime.Now;
                        Logger.Debug("Before GetTournamentsByTeam: " + timerTournament.ToShortTimeString());

                        var facade = new TournamentFacade();
                        var tournaments = facade.GetTournamentsByTeam(team.TeamId, seasonId);
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);
                    }

                    // Looping throught the tournaments that we found
                    if (listTournaments.Any())
                    {
                        List<Tournament> filteredTournaments;
                        if (ageCategoryId == 2)
                        {
                            filteredTournaments = (from myTournaments in listTournaments where (myTournaments.AgeCategoryId == 18) || (myTournaments.AgeCategoryId == 21) orderby myTournaments.Division select myTournaments).ToList();
                        }
                        else
                        {
                            filteredTournaments = (from myTournaments in listTournaments where !(myTournaments.AgeCategoryId == 18 || myTournaments.AgeCategoryId == 21) orderby myTournaments.Division select myTournaments).ToList();
                        }

                        // KeyValuePair<int, NTB.Classes.Team> kvp1 = kvp;
                        var kvp1 = kvp;
                        foreach (
                            var selectedTournament in
                                from tournament in filteredTournaments
                                select
                                    new Tournament
                                        {
                                            DistrictId = tournament.DistrictId, 
                                            SeasonId = tournament.SeasonId, 
                                            TournamentId = tournament.TournamentId, 
                                            TournamentName = tournament.TournamentName, 
                                            AgeCategoryId = tournament.AgeCategoryId, 
                                            AgeCategoryFilterId = ageCategoryId, 
                                            MunicipalityId = kvp1.Key, 
                                            TournamentNumber = tournament.TournamentNumber
                                        }
                                into myJobTournament let hasTournament = (from myTournament in listJobsTournament where myTournament.TournamentName == myJobTournament.TournamentName select myTournament).Any() where hasTournament == false select myJobTournament)
                        {
                            listJobsTournament.Add(selectedTournament);
                        }
                    }
                }
            }

            var timerEnd = DateTime.Now;
            var totalDifference = timerEnd - timerStart;

            Logger.Debug("Total time of producing tournament list: " + totalDifference.TotalSeconds);

            return listJobsTournament;
        }

        #endregion

        /// <summary>
        ///     The trust all certificate policy.
        /// </summary>
        public class TrustAllCertificatePolicy : ICertificatePolicy
        {
            #region Public Methods and Operators

            /// <summary>
            /// The check validation result.
            /// </summary>
            /// <param name="sp">
            /// The sp.
            /// </param>
            /// <param name="cert">
            /// The cert.
            /// </param>
            /// <param name="req">
            /// The req.
            /// </param>
            /// <param name="problem">
            /// The problem.
            /// </param>
            /// <returns>
            /// The <see cref="bool"/>.
            /// </returns>
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }

            #endregion
        }

    }
}