﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The event mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The event mapper.
    /// </summary>
    public class EventMapper : BaseMapper<EventSearchResult, SportEvent>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<EventSearchResult, SportEvent> mapper)
        {
            mapper.Relate(x => x.EventId, y => y.Id);
            mapper.Relate(x => x.EventName, y => y.Name);
            mapper.Relate(x => x.StartDate, y => y.StartDate);
            mapper.Relate(x => x.EndDate, y => y.EndDate);
            mapper.Relate(x => x.ActivityId, y => y.ActivityId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.ActivityName, y => y.ActivityName);
            mapper.Relate(x => x.Location, y => y.Location);
            mapper.Relate(x => x.RegionName, y => y.RegionName);
        }
    }
}