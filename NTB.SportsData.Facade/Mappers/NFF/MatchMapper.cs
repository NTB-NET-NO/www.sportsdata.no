﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NFF
{
    using Glue;

    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The match mapper.
    /// </summary>
    public class MatchMapper : BaseMapper<Match, Domain.Classes.Match>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Match, Domain.Classes.Match> mapper)
        {
            mapper.Relate(x => x.AwayTeamName, y => y.AwayTeam);
            mapper.Relate(x => x.AwayTeamId, y => y.AwayTeamId);
            mapper.Relate(x => x.AwayTeamGoals, y => y.AwayTeamGoal);
            mapper.Relate(x => x.MatchStartDate, y => y.MatchDate);
            mapper.Relate(x => x.HomeTeamName, y => y.HomeTeam);
            mapper.Relate(x => x.HomeTeamId, y => y.HomeTeamId);
            mapper.Relate(x => x.HomeTeamGoals, y => y.HomeTeamGoal);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentName, y => y.TournamentName);
            mapper.Relate(x => x.TournamentAgeCategoryId, y => y.TournamentAgeCategoryId);
            mapper.Relate(x => x.TournamentRoundNumber, y => y.TournamentRoundNumber);
        }
    }
}