// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Role.cs" company="">
//   
// </copyright>
// <summary>
//   The role.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Domain.Classes
{
    using System;

    using Microsoft.AspNet.Identity;

    /// <summary>
    ///     The role.
    /// </summary>
    public class Role : IRole, IRole<int>
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        #endregion

        #region Explicit Interface Properties

        /// <summary>
        ///     Gets the id.
        /// </summary>
        int IRole<int>.Id
        {
            get
            {
                var guid = new Guid(this.Id);
                byte[] bytes = guid.ToByteArray();
                int i = BitConverter.ToInt32(bytes, 0);

                // var l = BitConverter.ToInt64(bytes, 0);
                string now = new DateTime().ToShortTimeString().Replace(":", string.Empty);
                return Convert.ToInt32(now);
            }
        }

        #endregion
    }
}