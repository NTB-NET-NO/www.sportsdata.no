﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PopulateDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The populate data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.DataMappers.Populate
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Data.Interfaces.Common;
    using NTB.SportsData.Data.Interfaces.DataMappers;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The populate data mapper.
    /// </summary>
    public class PopulateDataMapper : IRepository<DataBasePopulate>, IDisposable, IPopulateDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PopulateDataMapper));

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get data base updated.
        /// </summary>
        /// <returns>
        /// The <see cref="DataBasePopulate"/>.
        /// </returns>
        public DataBasePopulate GetDataBaseUpdated()
        {
            var populate = new DataBasePopulate();
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetPopulateDateTime", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["PopulationStart"] != DBNull.Value)
                        {
                            populate.Start = Convert.ToDateTime(sqlDataReader["PopulationStart"]);
                        }

                        if (sqlDataReader["PopulationEnd"] != DBNull.Value)
                        {
                            populate.End = Convert.ToDateTime(sqlDataReader["PopulationEnd"]);
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return populate;
            }
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(DataBasePopulate domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<DataBasePopulate> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(DataBasePopulate domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(DataBasePopulate domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<DataBasePopulate> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="DataBasePopulate"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public DataBasePopulate Get(int id)
        {
            throw new NotImplementedException();
        }
    }
}