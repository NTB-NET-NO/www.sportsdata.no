﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Models.ViewModels
{
    public class AgeCategoryViewModel
    {
        public int OrgId { get; set; }

        public List<AgeCategory> AgeCategories { get; set; }
    }
}