﻿using Newtonsoft.Json;

namespace sog.Models.SportsMl
{
    /// <summary>
    /// The participants.
    /// </summary>
    [JsonObject("participants")]
    public class Participants
    {
        /// <summary>
        /// Gets or sets the participant.
        /// </summary>
        [JsonProperty("participant", NullValueHandling = NullValueHandling.Ignore)]
        public Participant[] Participant { get; set; }
    }
}