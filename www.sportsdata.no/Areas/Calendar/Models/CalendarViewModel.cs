﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CalendarViewModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The calendar view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Web.Areas.Calendar.Models
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The calendar view model.
    /// </summary>
    public class CalendarViewModel
    {
        /// <summary>
        /// Gets or sets the event id.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Gets or sets the event name.
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// Gets or sets the event date time.
        /// </summary>
        public DateTime EventDateTime { get; set; }

        /// <summary>
        /// Gets or sets the sport.
        /// </summary>
        public string Sport { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        public int TournamentId { get; set; }

        public string TournamentName { get; set; }

        /// <summary>
        /// The populate list.
        /// </summary>
        /// <param name="matches">
        /// The matches.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<CalendarViewModel> PopulateList(List<Match> matches)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("nb-NO");

            var modelList =
                matches.OrderBy(x => x.MatchDate)
                    .Select(
                        match =>
                        new CalendarViewModel()
                            {
                                EventId = match.MatchId, 
                                EventName = match.HomeTeam + " - " + match.AwayTeam, 
                                EventDateTime = match.MatchDate, 
                                SportId = match.SportId, 
                                Sport = match.SportName,
                                TournamentId = match.TournamentId,
                                TournamentName = match.TournamentName
                            })
                    .ToList();

            return modelList;
        }
    }
}