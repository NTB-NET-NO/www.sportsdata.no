using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Services.NFF.Interfaces;
using NTB.SportsData.Services.NFF.NFFProdService;
using NTB.SportsData.Services.NFF.Utilities;

namespace NTB.SportsData.Services.NFF.Repositories
{
    /// <summary>
    /// The tournament repository.
    /// </summary>
    public class TournamentRepository : IRepository<Tournament>, IDisposable, ITournamentDataMapper
    {
        #region Static Fields

        /// <summary>
        ///     The Logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentRepository));

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TournamentRepository"/> class.
        /// </summary>
        public TournamentRepository()
        {
            //if (this._tournamentServiceClient.ClientCredentials != null)
            //{
            //    this._tournamentServiceClient.ClientCredentials.UserName.UserName =
            //        ConfigurationManager.AppSettings["NFFServicesUsername"];
            //    this._tournamentServiceClient.ClientCredentials.UserName.Password =
            //        ConfigurationManager.AppSettings["NFFServicesPassword"];
            //}

            if (this._tournamentServiceClient.State == CommunicationState.Faulted)
            {
                this._tournamentServiceClient.Close();
                this._tournamentServiceClient = null;

                this._tournamentServiceClient = new TournamentServiceClient();
            }

            if (this._tournamentServiceClient.ClientCredentials == null)
            {
                return;
            }

            this._tournamentServiceClient.ClientCredentials.UserName.UserName =
                ConfigurationManager.AppSettings["NFFServicesUsername"];
            this._tournamentServiceClient.ClientCredentials.UserName.Password =
                ConfigurationManager.AppSettings["NFFServicesPassword"];
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the age category id.
        /// </summary>
        public int AgeCategoryId { get; set; }

        #endregion

        #region Fields

        /// <summary>
        /// The _meta service client.
        /// </summary>
        private  MetaServiceClient _metaServiceClient = new MetaServiceClient();

        /// <summary>
        /// The _organization service client.
        /// </summary>
        private OrganizationServiceClient _organizationServiceClient = new OrganizationServiceClient();

        /// <summary>
        /// The _tournament service client.
        /// </summary>
        private TournamentServiceClient _tournamentServiceClient = new TournamentServiceClient();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        public Tournament Get(int id)
        {
            this.CheckServiceState(this._tournamentServiceClient);

            return this._tournamentServiceClient.GetTournament(id);
        }

        /// <summary>
        /// The get age categories tournament.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<AgeCategoryTournament> GetAgeCategoriesTournament()
        {
            this.CheckServiceState(this._organizationServiceClient);
            this.CheckServiceState(this._tournamentServiceClient);
            this.CheckServiceState(this._metaServiceClient);

            return this._metaServiceClient.GetAgeCategoriesTournament()
                .ToList();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<Tournament> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get clubs by tournament.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetClubsByTournament(int tournamentId, int seasonId)
        {
            this.CheckServiceState(this._organizationServiceClient);
            this.CheckServiceState(this._tournamentServiceClient);
            this.CheckServiceState(this._metaServiceClient);

            var clubs = this._organizationServiceClient.GetClubsByTournament(tournamentId, null);

            foreach (var club in clubs)
            {
                var clubMatches = this._tournamentServiceClient.GetMatchesByClub(
                    club.ClubId,
                    seasonId,
                    true,
                    false,
                    false,
                    true,
                    null);
                foreach (var clubMatch in clubMatches)
                {
                    foreach (var matchEvent in clubMatch.MatchEventList)
                    {
                        // if (matchEvent.MatchEventTypeId == (int) EventType.Advarsel)
                        // {
                        // // We are doing something
                        // }
                    }
                }
            }

            return new List<Tournament>();
        }

        /// <summary>
        /// The get teams by municipality.
        /// </summary>
        /// <param name="municipalityId">
        /// The municipality id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Team> GetTeamsByMunicipality(int municipalityId)
        {
            this.CheckServiceState(this._organizationServiceClient);
            this.CheckServiceState(this._tournamentServiceClient);
            this.CheckServiceState(this._metaServiceClient);

            return this._organizationServiceClient.GetTeamsByMunicipality(municipalityId, null)
                .ToList();
        }

        /// <summary>
        /// The get tournament.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        public Tournament GetTournament(int tournamentId)
        {
            this.CheckServiceState(this._tournamentServiceClient);
            return this._tournamentServiceClient.GetTournament(tournamentId);
        }

        /// <summary>
        /// The get tournament by municipalities.
        /// </summary>
        /// <param name="municipalities">
        /// The municipalities.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId)
        {
            if (this._organizationServiceClient.ClientCredentials != null)
            {
                this._organizationServiceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServicesUsername"];
                this._organizationServiceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicesPassword"];
            }

            if (this._tournamentServiceClient.ClientCredentials != null)
            {
                this._tournamentServiceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServicesUsername"];
                this._tournamentServiceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicesPassword"];
            }

            var listTournaments = new List<Tournament>();

            // First we get the teams in the municipalities.
            var distinctTeams = new List<Team>();
            foreach (var municipality in municipalities)
            {
                var s = municipality;


                var teamList = new List<Team>();
                var municipalityId = Convert.ToInt32(s);

                // Getting the teams from the municipalities and find out in which tournaments they are participating in...

                // TODO: fix so that it gets the list from the service and then check if this is in the database for this customer.
                // If it is for the customer, we shall mark it as selected.
                if (municipalityId <= 0 || municipalityId == 500)
                {
                    continue;
                }

                try
                {
                    // I also need to store the municipalities in the database
                    // Insert into municipality has been moved to the controller, as we do everything there
                    // Consider - we are looping there, and we are looping here. What is best practice?
                    // could this be a speed issue?

                    // municipalitiesDataAccess.InsertMunicipality(municipalityId, jobId);
                    var teams = this._organizationServiceClient.GetTeamsByMunicipality(
                        municipalityId,
                        null);

                    teamList.AddRange(teams);
                }
                catch (Exception exception)
                {
                    // throw new Exception(exception.Message);
                    Logger.Error(exception);
                }


                var teamComparer = new TeamComparer();
                foreach (var team in teamList.Where(team => !distinctTeams.Contains(team, teamComparer)))
                {
                    distinctTeams.Add(team);
                }

                // Clearing the list of teams
                teamList.Clear();


                // return the list of tournaments
                // return listTournaments;
            }

            var tournaments = new List<Tournament>();
            foreach (var team in distinctTeams)
            {
                try
                {
                    tournaments =
                        this._tournamentServiceClient.GetTournamentsByTeam(team.TeamId, seasonId, null)
                            .ToList();

                    listTournaments.AddRange(tournaments);
                }
                catch (Exception exception)
                {
                    // Logger.Error(exception);

                    // we just log this as it is OK not to receive tournaments 

                    // throw new Exception(exception.Message);
                }
            }

            // First filtration. Checking that we have distinct list of tournaments
            var tournamentComparer = new TournamentComparer();
            var distinctTournaments = new List<Tournament>();
            foreach (var tournament in
                listTournaments.Where(tournament => !distinctTournaments.Contains(tournament, tournamentComparer)))
            {
                distinctTournaments.Add(tournament);
            }

            if (!distinctTournaments.Any())
            {
                return null;
            }

            List<Tournament> filteredTournaments;

            if (this.AgeCategoryId == 2)
            {
                filteredTournaments = (from tournament in distinctTournaments
                    where
                        (tournament.TournamentAgeCategory == 18)
                        || (tournament.TournamentAgeCategory == 21)
                    orderby tournament.Division
                    select tournament).ToList();
            }
            else
            {
                filteredTournaments = (from tournament in distinctTournaments
                    where
                        !(tournament.TournamentAgeCategory == 18
                          || tournament.TournamentAgeCategory == 21)
                    orderby tournament.Division
                    select tournament).ToList();
            }

            return filteredTournaments;
        }

        /// <summary>
        /// The get tournament standing.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="Exception">
        /// Throws an Exception on error
        /// </exception>
        public List<TournamentTableTeam> GetTournamentStanding(int tournamentId)
        {
            if (this._tournamentServiceClient.ClientCredentials != null)
            {
                this._tournamentServiceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServicesUsername"];
                this._tournamentServiceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicesPassword"];
            }

            var tournamentStandings = new List<TournamentTableTeam>();

            try
            {
                var response =
                    this._tournamentServiceClient.GetTournamentStanding(tournamentId, null)
                        .ToList();

                tournamentStandings.AddRange(response);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                throw new Exception(exception.Message);
            }

            return tournamentStandings;
        }

        /// <summary>
        /// The get tournaments by district.
        /// </summary>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            // CheckServiceState(tournamentServiceClient);
            try
            {
                return
                    new List<Tournament>(
                        this._tournamentServiceClient.GetTournamentsByDistrict(districtId, seasonId, null));
            }
            catch (Exception exception)
            {
                Logger.Info("DistrictId: " + districtId + ", SeasonId: " + seasonId);
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return new List<Tournament>();
            }
        }

        /// <summary>
        /// The get tournaments by team.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsByTeam(int teamId, int seasonId)
        {
            this.CheckServiceState(this._tournamentServiceClient);

            return new List<Tournament>(this._tournamentServiceClient.GetTournamentsByTeam(teamId, seasonId, null));
        }

        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            this.CheckServiceState(this._tournamentServiceClient);

            this.CheckServiceState(this._organizationServiceClient);

            var districts = this._organizationServiceClient.GetDistricts();

            var tournaments = new List<Tournament>();
            foreach (var district in districts)
            {
                tournaments.AddRange(this._tournamentServiceClient.GetTournamentsByDistrict(district.DistrictId, seasonId, null));
            }

            return tournaments;
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domain object.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void InsertAll(List<Tournament> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domain object.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public int InsertOne(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domain object.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void Update(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The check service state.
        /// </summary>
        /// <param name="service">
        /// The service.
        /// </param>
        public void CheckServiceState(object service)
        {
            if (service.GetType() == new TournamentServiceClient().GetType())
            {
                this.ResetTournamentServiceClient((TournamentServiceClient) service);
            }
            else if (service.GetType() == new OrganizationServiceClient().GetType())
            {
                this._ResetOrganizationServiceClient((OrganizationServiceClient) service);
            }
            else if (service.GetType() == new MetaServiceClient().GetType())
            {
                this._ResetMetaServiceClient((MetaServiceClient) service);
            }
        }

        /// <summary>
        /// The _ reset meta service client.
        /// </summary>
        /// <param name="client">
        /// The client.
        /// </param>
        public void _ResetMetaServiceClient(MetaServiceClient client)
        {
            client.Close();

            client = new MetaServiceClient();

            if (client.ClientCredentials == null)
            {
                return;
            }

            // Setting username and password again
            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
            client.Open();
            this._metaServiceClient = client;
        }

        /// <summary>
        /// The _ reset organization service client.
        /// </summary>
        /// <param name="client">
        /// The client.
        /// </param>
        public void _ResetOrganizationServiceClient(OrganizationServiceClient client)
        {
            client.Close();
            client = null;

            client = new OrganizationServiceClient();

            
            if (client.ClientCredentials == null)
            {
                Logger.Debug("No client credentials");
                throw new Exception("No Client credentials");
                return;
            }

            // Setting username and password again
            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
            client.Open();
            this._organizationServiceClient = client;
        }

        /// <summary>
        /// The _ reset tournament service client.
        /// </summary>
        /// <param name="client">
        /// The client.
        /// </param>
        public void ResetTournamentServiceClient(TournamentServiceClient client)
        {
            if (client.ClientCredentials == null)
            {
                return;
            }

            if (client.State == CommunicationState.Closed)
            {
                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                client.Open();

                this._tournamentServiceClient = client;
                return;
            }

            if (client.State == CommunicationState.Faulted)
            {
                client.Close();

                client = new TournamentServiceClient();
                client.Open();
                if (client.ClientCredentials == null)
                {
                    return;
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                client.Open();

                this._tournamentServiceClient = client;

                return;
            }

            // Setting username and password again
            if (client.ClientCredentials.UserName.UserName == string.Empty)
            {
                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
            }

            if (client.ClientCredentials.UserName.Password == string.Empty)
            {
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
            }
        }

        #endregion
    }
}