﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Interfaces
{
    interface IClubDataMapper
    {
        List<Club> GetClubsByOrgId(int orgId);
        List<Club> GetClubsByNameAndOrgId(string clubName, int orgId);
        List<Club> GetClubsByParentOrgId(int parentOrgId);
        List<Club> GetClubsByMunicipalities(List<string> selectedMunicipalities, int parentOrgId);
        void UpdateCheckedClub(Club club);
        void UpdateClubAdvanced(Club club);
    }
}
