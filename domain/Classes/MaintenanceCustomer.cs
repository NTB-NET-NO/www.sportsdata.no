﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTB.SportsData.Domain.Classes
{
    public class MaintenanceCustomer
    {
        public int CustomerId { get; set; }

        public int RemoteCustomerId { get; set; }

    }
}
