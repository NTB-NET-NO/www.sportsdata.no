﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Matches
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using log4net;

    using NTB.SportsData.Data.Interfaces;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The match data mapper.
    /// </summary>
    public class MatchDataMapper : IMatchDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(MatchDataMapper));

        /// <summary>
        /// Get list of matches by Tournament Id
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// List of matches in the tournament
        /// </returns>
        public List<Match> GetMatchesByTournamentId(Tournament tournament)
        {
            var matches = new List<Match>();
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("SportsDataApi_GetMatchesByTournamentId", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournament.TournamentId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return matches;
                    }

                    while (sqlDataReader.Read())
                    {
                        var match = new Match();
                        match.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        match.MatchId = Convert.ToInt32(sqlDataReader["MatchId"]);
                        match.MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"]);
                        match.MatchStartTime = Convert.ToInt32(sqlDataReader["MatchStartTime"].ToString());
                        match.TournamentName = sqlDataReader["TournamentName"].ToString();
                        match.SportName = sqlDataReader["Sport"].ToString();
                        match.HomeTeam = sqlDataReader["HomeTeam"].ToString();
                        match.HomeTeamId = sqlDataReader["HomeTeamId"] == DBNull.Value
                                               ? 0
                                               : Convert.ToInt32(sqlDataReader["HomeTeamId"]);
                        match.HomeTeamGoal = sqlDataReader["HomeTeamGoals"] == DBNull.Value
                                                 ? 0
                                                 : Convert.ToInt32(sqlDataReader["HomeTeamGoals"]);
                        match.AwayTeamGoal = sqlDataReader["AwayTeamGoals"] == DBNull.Value
                                                 ? 0
                                                 : Convert.ToInt32(sqlDataReader["AwayTeamGoals"]);
                        match.AwayTeamId = sqlDataReader["AwayTeamId"] == DBNull.Value
                                               ? 0
                                               : Convert.ToInt32(sqlDataReader["AwayTeamId"]);
                        match.AwayTeam = sqlDataReader["AwayTeam"].ToString();
                        match.Spectators = sqlDataReader["Spectators"] == DBNull.Value
                                               ? 0
                                               : Convert.ToInt32(sqlDataReader["Spectators"]);
                        match.VenueName = sqlDataReader["VenueName"].ToString();
                        match.TournamentRoundNumber = sqlDataReader["TournamentRoundNumber"] == DBNull.Value
                                                          ? 0
                                                          : Convert.ToInt32(sqlDataReader["TournamentRoundNumber"]);
                        match.OrganizationNameShort = sqlDataReader["OrgNameShort"].ToString();

                        // Adding match to list of matches
                        matches.Add(match);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return matches;
            }
        }

        /// <summary>
        /// Get the match by id
        /// </summary>
        /// <param name="id">
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        public Match GetMatchById(int id)
        {
            var match = new Match();
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("SportsDataApi_GetMatchesByTournamentId", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", id));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return match;
                    }

                    while (sqlDataReader.Read())
                    {
                        match.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        match.MatchId = Convert.ToInt32(sqlDataReader["MatchId"]);
                        match.MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"]);
                        match.MatchStartTime = Convert.ToInt32(sqlDataReader["MatchStartTime"].ToString());
                        match.TournamentName = sqlDataReader["TournamentName"].ToString();
                        match.SportName = sqlDataReader["Sport"].ToString();
                        match.HomeTeam = sqlDataReader["HomeTeam"].ToString();
                        match.HomeTeamId = Convert.ToInt32(sqlDataReader["HomeTeamId"]);
                        match.HomeTeamGoal = sqlDataReader["HomeTeamGoal"] == DBNull.Value
                                                 ? 0
                                                 : Convert.ToInt32(sqlDataReader["HomeTeamGoal"]);
                        match.AwayTeamGoal = sqlDataReader["AwayTeamGoal"] == DBNull.Value
                                                 ? 0
                                                 : Convert.ToInt32(sqlDataReader["AwayTeamGoal"]);
                        match.AwayTeamId = Convert.ToInt32(sqlDataReader["AwayTeamId"]);
                        match.AwayTeam = sqlDataReader["AwayTeam"].ToString();
                        match.Spectators = sqlDataReader["Spectators"] == DBNull.Value
                                               ? 0
                                               : Convert.ToInt32(sqlDataReader["Spectators"]);
                        match.VenueName = sqlDataReader["VenueName"].ToString();
                        match.TournamentRoundNumber = sqlDataReader["TournamentRoundNumber"] == DBNull.Value
                                                          ? 0
                                                          : Convert.ToInt32(sqlDataReader["TournamentRoundNumber"]);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return match;
            }
        }

        /// <summary>
        /// The get todays matches by customer id.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Match> GetTodaysMatchesByCustomerId(int customerId)
        {
            var matches = new List<Match>();
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("SportsDataApi_GetTodaysMatchesByCustomerId", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return matches;
                    }

                    while (sqlDataReader.Read())
                    {
                        var match = new Match();
                        match.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        match.TournamentName = sqlDataReader["TournamentName"].ToString();
                        match.MatchId = Convert.ToInt32(sqlDataReader["MatchId"]);
                        match.MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"]);
                        match.MatchStartTime = Convert.ToInt32(sqlDataReader["MatchStartTime"].ToString());
                        match.SportName = sqlDataReader["Sport"].ToString();
                        match.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        match.HomeTeam = sqlDataReader["HomeTeam"].ToString();
                        match.HomeTeamId = sqlDataReader["HomeTeamId"] == DBNull.Value
                                               ? 0
                                               : Convert.ToInt32(sqlDataReader["HomeTeamId"]);
                        match.HomeTeamGoal = sqlDataReader["HomeTeamGoals"] == DBNull.Value
                                                 ? 0
                                                 : Convert.ToInt32(sqlDataReader["HomeTeamGoals"]);
                        match.AwayTeamGoal = sqlDataReader["AwayTeamGoals"] == DBNull.Value
                                                 ? 0
                                                 : Convert.ToInt32(sqlDataReader["AwayTeamGoals"]);
                        match.AwayTeamId = sqlDataReader["AwayTeamId"] == DBNull.Value
                                               ? 0
                                               : Convert.ToInt32(sqlDataReader["AwayTeamId"]);
                        match.AwayTeam = sqlDataReader["AwayTeam"].ToString();
                        match.Spectators = sqlDataReader["Spectators"] == DBNull.Value
                                               ? 0
                                               : Convert.ToInt32(sqlDataReader["Spectators"]);
                        match.VenueName = sqlDataReader["VenueName"].ToString();
                        match.TournamentRoundNumber = sqlDataReader["TournamentRoundNumber"] == DBNull.Value
                                                          ? 0
                                                          : Convert.ToInt32(sqlDataReader["TournamentRoundNumber"]);
                        match.OrganizationNameShort = sqlDataReader["OrgNameShort"].ToString();

                        // Adding match to list of matches
                        matches.Add(match);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return matches;
            }
        }
    }
}