﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamResult.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The team result.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Domain.Classes
{
    /// <summary>
    /// The team result.
    /// </summary>
    public class TeamResult
    {
        /// <summary>
        /// Gets or sets the away points.
        /// </summary>
        public int AwayPoints { get; set; }

        /// <summary>
        /// Gets or sets the bonus points away.
        /// </summary>
        public int BonusPointsAway { get; set; }

        /// <summary>
        /// Gets or sets the bonus points home.
        /// </summary>
        public int BonusPointsHome { get; set; }

        /// <summary>
        /// Gets or sets the bonus points total.
        /// </summary>
        public int BonusPointsTotal { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether dispensation.
        /// </summary>
        public bool Dispensation { get; set; }

        /// <summary>
        /// Gets or sets the draws.
        /// </summary>
        public int Draws { get; set; }

        /// <summary>
        /// Gets or sets the draws away.
        /// </summary>
        public int DrawsAway { get; set; }

        /// <summary>
        /// Gets or sets the goal difference.
        /// </summary>
        public int GoalDifference { get; set; }

        /// <summary>
        /// Gets or sets the entry id.
        /// </summary>
        public int EntryId { get; set; }

        /// <summary>
        /// Gets or sets the draws home.
        /// </summary>
        public int DrawsHome { get; set; }

        /// <summary>
        /// Gets or sets the goal ratio.
        /// </summary>
        public double GoalRatio { get; set; }

        /// <summary>
        /// Gets or sets the goals away formatted.
        /// </summary>
        public string GoalsAwayFormatted { get; set; }

        /// <summary>
        /// Gets or sets the goals conceded away.
        /// </summary>
        public int GoalsConcededAway { get; set; }

        /// <summary>
        /// Gets or sets the goals conceded home.
        /// </summary>
        public int GoalsConcededHome { get; set; }

        /// <summary>
        /// Gets or sets the goals conceeded.
        /// </summary>
        public int GoalsConceeded { get; set; }

        /// <summary>
        /// Gets or sets the goals home formatted.
        /// </summary>
        public string GoalsHomeFormatted { get; set; }

        /// <summary>
        /// Gets or sets the goals scored.
        /// </summary>
        public int GoalsScored { get; set; }

        /// <summary>
        /// Gets or sets the goals scored away.
        /// </summary>
        public int GoalsScoredAway { get; set; }

        /// <summary>
        /// Gets or sets the goals scored home.
        /// </summary>
        public int GoalsScoredHome { get; set; }

        /// <summary>
        /// Gets or sets the losses.
        /// </summary>
        public int Losses { get; set; }

        /// <summary>
        /// Gets or sets the losses away.
        /// </summary>
        public int LossesAway { get; set; }

        /// <summary>
        /// Gets or sets the losses fulltime away.
        /// </summary>
        public int LossesFulltimeAway { get; set; }

        /// <summary>
        /// Gets or sets the losses fulltime home.
        /// </summary>
        public int LossesFulltimeHome { get; set; }

        /// <summary>
        /// Gets or sets the losses fulltime total.
        /// </summary>
        public int LossesFulltimeTotal { get; set; }

        /// <summary>
        /// Gets or sets the losses home.
        /// </summary>
        public int LossesHome { get; set; }

        /// <summary>
        /// Gets or sets the losses overtime away.
        /// </summary>
        public int LossesOvertimeAway { get; set; }

        /// <summary>
        /// Gets or sets the losses overtime home.
        /// </summary>
        public int LossesOvertimeHome { get; set; }

        /// <summary>
        /// Gets or sets the losses overtime total.
        /// </summary>
        public int LossesOvertimeTotal { get; set; }

        /// <summary>
        /// Gets or sets the losses penalties away.
        /// </summary>
        public int LossesPenaltiesAway { get; set; }

        /// <summary>
        /// Gets or sets the losses penalties home.
        /// </summary>
        public int LossesPenaltiesHome { get; set; }

        /// <summary>
        /// Gets or sets the losses penalties total.
        /// </summary>
        public int LossesPenaltiesTotal { get; set; }

        /// <summary>
        /// Gets or sets the matches.
        /// </summary>
        public int Matches { get; set; }

        /// <summary>
        /// Gets or sets the matches away.
        /// </summary>
        public int MatchesAway { get; set; }

        /// <summary>
        /// Gets or sets the matches home.
        /// </summary>
        public int MatchesHome { get; set; }

        /// <summary>
        /// Gets or sets the org id.
        /// </summary>
        public int OrgId { get; set; }

        /// <summary>
        /// Gets or sets the org name.
        /// </summary>
        public string OrgName { get; set; }

        /// <summary>
        /// Gets or sets the partial points conceded.
        /// </summary>
        public int PartialPointsConceded { get; set; }

        /// <summary>
        /// Gets or sets the partial points conceded away.
        /// </summary>
        public int PartialPointsConcededAway { get; set; }

        /// <summary>
        /// Gets or sets the partial points conceded home.
        /// </summary>
        public int PartialPointsConcededHome { get; set; }

        /// <summary>
        /// Gets or sets the partial points difference.
        /// </summary>
        public int PartialPointsDifference { get; set; }

        /// <summary>
        /// Gets or sets the partial points difference away.
        /// </summary>
        public int PartialPointsDifferenceAway { get; set; }

        /// <summary>
        /// Gets or sets the partial points difference home.
        /// </summary>
        public int PartialPointsDifferenceHome { get; set; }

        /// <summary>
        /// Gets or sets the partial points scored.
        /// </summary>
        public int PartialPointsScored { get; set; }

        /// <summary>
        /// Gets or sets the partial points scored away.
        /// </summary>
        public int PartialPointsScoredAway { get; set; }

        /// <summary>
        /// Gets or sets the partial points scored home.
        /// </summary>
        public int PartialPointsScoredHome { get; set; }

        /// <summary>
        /// Gets or sets the points away.
        /// </summary>
        public int PointsAway { get; set; }

        /// <summary>
        /// Gets or sets the points home.
        /// </summary>
        public int PointsHome { get; set; }

        /// <summary>
        /// Gets or sets the points start.
        /// </summary>
        public int PointsStart { get; set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Gets or sets the sort.
        /// </summary>
        public string Sort { get; set; }

        /// <summary>
        /// Gets or sets the team penalty.
        /// </summary>
        public string TeamPenalty { get; set; }

        /// <summary>
        /// Gets or sets the team penalty negative.
        /// </summary>
        public int TeamPenaltyNegative { get; set; }

        /// <summary>
        /// Gets or sets the team penalty positive.
        /// </summary>
        public int TeamPenaltyPositive { get; set; }

        /// <summary>
        /// Gets or sets the total goals.
        /// </summary>
        public int TotalGoals { get; set; }

        /// <summary>
        /// Gets or sets the total goals formatted.
        /// </summary>
        public string TotalGoalsFormatted { get; set; }

        /// <summary>
        /// Gets or sets the total matches.
        /// </summary>
        public int TotalMatches { get; set; }

        /// <summary>
        /// Gets or sets the total points.
        /// </summary>
        public int TotalPoints { get; set; }

        /// <summary>
        /// Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        /// Gets or sets the victories.
        /// </summary>
        public int Victories { get; set; }

        /// <summary>
        /// Gets or sets the victories away.
        /// </summary>
        public int VictoriesAway { get; set; }

        /// <summary>
        /// Gets or sets the victories fulltime away.
        /// </summary>
        public int VictoriesFulltimeAway { get; set; }

        /// <summary>
        /// Gets or sets the victories fulltime home.
        /// </summary>
        public int VictoriesFulltimeHome { get; set; }

        /// <summary>
        /// Gets or sets the victories fulltime total.
        /// </summary>
        public int VictoriesFulltimeTotal { get; set; }

        /// <summary>
        /// Gets or sets the victories home.
        /// </summary>
        public int VictoriesHome { get; set; }

        /// <summary>
        /// Gets or sets the victories overtime away.
        /// </summary>
        public int VictoriesOvertimeAway { get; set; }

        /// <summary>
        /// Gets or sets the victories overtime home.
        /// </summary>
        public int VictoriesOvertimeHome { get; set; }

        /// <summary>
        /// Gets or sets the victories overtime total.
        /// </summary>
        public int VictoriesOvertimeTotal { get; set; }

        /// <summary>
        /// Gets or sets the victories penalties away.
        /// </summary>
        public int VictoriesPenaltiesAway { get; set; }

        /// <summary>
        /// Gets or sets the victories penalties home.
        /// </summary>
        public int VictoriesPenaltiesHome { get; set; }

        /// <summary>
        /// Gets or sets the victories penalties total.
        /// </summary>
        public int VictoriesPenaltiesTotal { get; set; }

        /// <summary>
        /// Gets or sets the jersey color.
        /// </summary>
        public string JerseyColor { get; set; }

        /// <summary>
        /// Gets or sets the shorts color.
        /// </summary>
        public string ShortsColor { get; set; }

        /// <summary>
        /// Gets or sets the away goal difference.
        /// </summary>
        public int? AwayGoalDifference { get; set; }

        /// <summary>
        /// Gets or sets the home goal difference.
        /// </summary>
        public int? HomeGoalDifference { get; set; }
    }
}