﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApiIdentity.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   Creating our own identity class
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Classes
{
    using System;
    using System.Security.Principal;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     Creating our own identity class
    /// </summary>
    public class ApiIdentity : IIdentity
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiIdentity"/> class.
        ///     Constructor with parameter
        /// </summary>
        /// <param name="user">
        /// </param>
        public ApiIdentity(ApiUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            this.User = user;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the authentication type.
        /// </summary>
        public string AuthenticationType
        {
            get
            {
                return "Basic";
            }
        }

        /// <summary>
        ///     Gets a value indicating whether is authenticated.
        /// </summary>
        public bool IsAuthenticated
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        ///     Gets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.User.Username;
            }
        }

        /// <summary>
        ///     Gets or sets the Api User
        /// </summary>
        public ApiUser User { get; private set; }

        #endregion
    }
}