﻿namespace NTB.SportsData.Domain.Classes
{
    public class TournamentType
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}