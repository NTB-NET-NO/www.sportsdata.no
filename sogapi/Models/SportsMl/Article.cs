﻿using System.Xml.Serialization;

namespace sogapi.Models.SportsMl
{
    public class Article
    {
        [XmlElement("nitf")] // , typeof(string), Namespace = "http://iptc.org/std/NITF/2006-10-18/"
        public Nitf Nitf { get; set; }

    }
}