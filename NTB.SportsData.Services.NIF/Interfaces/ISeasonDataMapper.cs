﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISeasonDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The SeasonDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NIF.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The SeasonDataMapper interface.
    /// </summary>
    public interface ISeasonDataMapper
    {
        /// <summary>
        /// The get federation seasons by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Season> GetFederationSeasonsByOrgId(int orgId);
    }
}