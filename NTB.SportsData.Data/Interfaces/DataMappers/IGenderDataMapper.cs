﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface IGenderDataMapper
    {
        List<Gender> GetGenders();

        Gender GetGenderById(int id);
        
        void InsertGender(Gender gender);
        
        void UpdateGender(Gender gender);
        
        void DeleteGender(int id);
    }
}