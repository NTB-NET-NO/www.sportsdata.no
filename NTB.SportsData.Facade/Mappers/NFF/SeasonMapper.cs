// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SeasonMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The season mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NFF
{
    using Glue;

    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The season mapper.
    /// </summary>
    public class SeasonMapper : BaseMapper<Season, Domain.Classes.Season>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Season, Domain.Classes.Season> mapper)
        {
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.SeasonNameLong, y => y.SeasonName);
            mapper.Relate(x => x.SeasonStartDate, y => y.SeasonStartDate);
            mapper.Relate(x => x.SeasonEndDate, y => y.SeasonEndDate);
            mapper.Relate(x => x.Ongoing, y => y.SeasonActive);
        }
    }
}