﻿using System;
using System.Web.Mvc;
using NTB.SportsData.Data.DataMappers.ContactPersons;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Input.Models
{
    public class ContactPersonModels
    {
        public ContactPerson UpdateContactPerson(int id, FormCollection collection)
        {
            var dataMapper = new ContactPersonDataMapper();
            var contactPerson = new ContactPerson
                {
                    TeamId = Convert.ToInt32(collection["teamid"]),
                    FirstName = collection["firstname"],
                    PersonId = id,
                    LastName = collection["lastname"],
                    Email = collection["email"],
                    HomePhone = collection["homephone"],
                    OfficePhone = collection["officephone"],
                    MobilePhone = collection["mobilephone"]
                };
            dataMapper.Update(contactPerson);

            return contactPerson;
        }

        public void ChangeContactPersonStatus(int id)
        {
            var dataMapper = new ContactPersonDataMapper();
            dataMapper.ActivateDeactivateContactPerson(id);
        }
    }
}