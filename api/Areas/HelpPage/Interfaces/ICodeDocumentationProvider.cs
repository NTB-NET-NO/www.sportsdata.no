﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICodeDocumentationProvider.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   An interface to create special stuff
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Areas.HelpPage.Interfaces
{
    using System.Web.Http.Controllers;

    /// <summary>
    ///     An interface to create special stuff
    /// </summary>
    public interface ICodeDocumentationProvider
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get code documentation.
        /// </summary>
        /// <param name="actionDescriptor">
        /// The action descriptor.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetCodeDocumentation(HttpActionDescriptor actionDescriptor, string type);

        #endregion
    }
}