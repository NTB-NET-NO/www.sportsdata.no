﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CountyRepository.cs" company="">
//   
// </copyright>
// <summary>
//   The county repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NIF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net;

    using NTB.SportsData.Services.NIF.Interfaces;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The county repository.
    /// </summary>
    public class CountyRepository : IRepository<Region>, IDisposable, ICountyDataMapper
    {
        #region Fields

        /// <summary>
        ///     The region service v2 client.
        /// </summary>
        private readonly RegionServiceClient _regionServiceClient = new RegionServiceClient();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CountyRepository"/> class.
        /// </summary>
        public CountyRepository()
        {
            if (this._regionServiceClient.ClientCredentials == null)
            {
                return;
            }

            this._regionServiceClient.ClientCredentials.UserName.UserName =
                ConfigurationManager.AppSettings["NIFServicesUsername"];
            this._regionServiceClient.ClientCredentials.UserName.Password =
                ConfigurationManager.AppSettings["NIFServicesPassword"];

            ServicePointManager.ServerCertificateValidationCallback =
                (servicePointSender, certificate, chain, sslPolicyErrors) => true; // Ignoring cert
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Region domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Region"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Region Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<Region> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get counties.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Region> GetCounties()
        {
            var request = new EmptyRequest8();

            RegionsResponse response = this._regionServiceClient.GetCounties(request);

            List<Region> counties = response.Regions.ToList();

            return counties;
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<Region> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(Region domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Region domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}