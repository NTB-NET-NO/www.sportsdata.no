﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsData.Services.Profixio.Interfaces;
using NTB.SportsData.Services.Profixio.ProfixioServiceReference;

namespace NTB.SportsData.Services.Profixio.DataMappers
{
    public class TournamentDataMapper : ITournamentDataMapper
    {
        public List<MatchGroup> GetTournamentBySeasonId(int seasonId)
        {
            var applicationKey = ConfigurationManager.AppSettings["NorwayCupApplicationKey"];

            var client = new ForTournamentPortClient();

            var matchClasses = client.getMatchClasses(applicationKey, seasonId.ToString());

            
            return (from matchClass in matchClasses
                from @group in matchClass.MatchGroups
                select new MatchGroup
                {
                    id = @group.id, IsPlayoff = @group.IsPlayoff, MatchClassId = @group.MatchClassId, Name = matchClass.Name + " G-" + @group.Name, PlayoffId = @group.PlayoffId
                }).ToList();
        }
    }
}
