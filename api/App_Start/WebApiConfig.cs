﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebApiConfig.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api
{
    using System.Web.Http;

    /// <summary>
    /// The Web Api Config Static Class
    /// </summary>
    public static class WebApiConfig
    {
        #region Public Methods and Operators

        /// <summary>
        /// The Register method
        /// </summary>
        /// <param name="config">
        /// The Config 
        /// </param>
        public static void Register(HttpConfiguration config)
        {
            // Add log4net
            log4net.Config.XmlConfigurator.Configure();

            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });

            config.Routes.MapHttpRoute(
                "Action route", 
                "api/{controller}/{action}/{id}", 
                new { id = RouteParameter.Optional });
        }

        #endregion
    }
}