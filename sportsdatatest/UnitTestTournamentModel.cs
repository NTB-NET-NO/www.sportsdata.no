﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnitTestTournamentModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The unit test tournament model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.UnitTest
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Web.Areas.Admin.Models;

    /// <summary>
    /// The unit test tournament model.
    /// </summary>
    [TestClass]
    public class UnitTestTournamentModel
    {
        /// <summary>
        /// The get tournament not stored_ one stored.
        /// </summary>
        [TestMethod]
        public void GetTournamentNotStored_OneStored()
        {
            var tournament1 = new Tournament() { TournamentId = 12345, TournamentName = "Tronds testturnering 1", };
            var tournament2 = new Tournament() { TournamentId = 12346, TournamentName = "Tronds testturnering 2", };
            var tournament3 = new Tournament() { TournamentId = 12347, TournamentName = "Tronds testturnering 3", };

            var filteredTournaments = new List<Tournament>() { tournament1, tournament2, tournament3 };
            var tournamentsNotStored = new List<Tournament>();
            var storedTournaments = new List<Tournament>() { tournament1 };

            var tournamentModel = new TournamentModel();

            var result = tournamentModel.GetTournamentsNotStored(storedTournaments, tournamentsNotStored, filteredTournaments);

            var expected = 2;

            Assert.AreEqual(expected, result.Count, "Number of tournaments is: " + result.Count);
        }

        // GetTournamentsNotStored
        /// <summary>
        /// The get tournaments not stored_ none stored.
        /// </summary>
        [TestMethod]
        public void GetTournamentsNotStored_NoneStored()
        {
            // List<Tournament> storedtournaments, 
            // List<Tournament> tournamentsNotStored, 
            // List<Tournament> filteredTournament
            var tournament1 = new Tournament() { TournamentId = 12345, TournamentName = "Tronds testturnering 1", };
            var tournament2 = new Tournament() { TournamentId = 12346, TournamentName = "Tronds testturnering 2", };
            var tournament3 = new Tournament() { TournamentId = 12347, TournamentName = "Tronds testturnering 3", };

            var filteredTournaments = new List<Tournament>() { tournament1, tournament2, tournament3 };
            var tournamentsNotStored = new List<Tournament>();
            var storedTournaments = new List<Tournament>();

            var tournamentModel = new TournamentModel();

            var result = tournamentModel.GetTournamentsNotStored(storedTournaments, tournamentsNotStored, filteredTournaments);

            var expected = 3;

            Assert.AreEqual(expected, result.Count);
        }
    }
}