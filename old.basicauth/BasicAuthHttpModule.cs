﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BasicAuthHttpModule.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.BasicAuth
{
    using System;
    using System.Net.Http.Headers;
    using System.Security.Cryptography;
    using System.Security.Principal;
    using System.Text;
    using System.Threading;
    using System.Web;
    
    using Data.DataMappers.User.Api;
    using Domain.Classes;

    using log4net;

    // using NTB.SportsData.Data.DataMappers.User.Api;

    /// <summary>
    ///     The basic auth http module.
    /// </summary>
    public class BasicAuthHttpModule : IHttpModule
    {
        #region Static Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(BasicAuthHttpModule));

        #endregion

        #region Constants

        /// <summary>
        ///     The realm.
        /// </summary>
        private const string Realm = "sportsdata.no";

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create hash.
        /// </summary>
        /// <param name="apiUsername">
        /// The api username.
        /// </param>
        /// <param name="timestamp">
        /// The timestamp.
        /// </param>
        /// <param name="uri">
        /// The uri.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string CreateHash(string apiUsername, string timestamp, string uri, string password)
        {
            // string createStringToHash = apiUsername + timestamp + uri;
            // byte[] keyInByes = Encoding.UTF8.GetBytes(createStringToHash);
            var tosigndata = apiUsername + timestamp + uri;

            // string tosigndata = "test";
            var alg = MD5.Create();
            var enc = new UTF8Encoding();

            var hash = alg.ComputeHash(enc.GetBytes(tosigndata));
            var hashedString = Convert.ToBase64String(hash);

            return hashedString;

        }

        /// <summary>
        /// The pack h.
        /// </summary>
        /// <param name="hex">
        /// The hex.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>byte[]</cref>
        ///     </see>
        ///     .
        /// </returns>
        public static byte[] PackH(string hex)
        {
            if ((hex.Length % 2) == 1)
            {
                hex += '0';
            }

            var bytes = new byte[hex.Length / 2];
            for (var i = 0; i < hex.Length; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            }

            return bytes;
        }

        /// <summary>
        /// The check signature.
        /// </summary>
        /// <param name="timestamp">
        /// The timestamp.
        /// </param>
        /// <param name="uri">
        /// The uri.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CheckSignature(string timestamp, string uri)
        {
            return false;
        }

        /// <summary>
        ///     The dispose.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// The init.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void Init(HttpApplication context)
        {
            Logger.Debug("Init");

            // Register event handlers
            context.AuthenticateRequest += OnApplicationAuthenticateRequest;
            context.EndRequest += OnApplicationEndRequest;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The authenticate user.
        /// </summary>
        /// <param name="credentials">
        /// The credentials.
        /// </param>
        /// <param name="signature">
        /// The signature.
        /// </param>
        /// <param name="timestamp">
        /// The timestamp.
        /// </param>
        /// <param name="uri">
        /// The uri.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool AuthenticateUser(string credentials, string signature, string timestamp, string uri)
        {
            Logger.Debug("Credentials: " + credentials);

            bool validated;
            try
            {
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                credentials = encoding.GetString(Convert.FromBase64String(credentials));

                int separator = credentials.IndexOf(':');
                string name = credentials.Substring(0, separator);
                string password = credentials.Substring(separator + 1);

                validated = CheckPassword(name, password);
                if (validated)
                {
                    // now we have validated the user, now we are to check the validity of the keys and stuff
                    // note: We have added this check here, because we only want to check this if the user is validated
                    string hash = CreateHash(name, timestamp, uri, password);

                    if (hash != signature)
                    {
                        validated = false;
                    }
                    else
                    {
                        var identity = new GenericIdentity(name);
                        SetPrincipal(new GenericPrincipal(identity, null));
                    }
                }
            }
            catch (FormatException)
            {
                // Credentials were not formatted correctly.
                validated = false;
            }

            return validated;
        }

        /// <summary>
        /// The check password.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool CheckPassword(string username, string password)
        {
            var userDataMapper = new UserDataMapper();
            Customer customer = userDataMapper.GetUser(username, password);
            return customer != null;
        }

        /// <summary>
        /// The on application authenticate request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void OnApplicationAuthenticateRequest(object sender, EventArgs e)
        {
            HttpRequest request = HttpContext.Current.Request;

            string signature = request.Headers["signature"];
            string timestamp = request.Headers["timestamp"];
            string uri = request.RawUrl;

            string authHeader = request.Headers["Authorization"];

            Logger.Debug("signature: " + signature);
            Logger.Debug("timestamp: " + timestamp);
            Logger.Debug("uri: " + uri);
            if (authHeader != null)
            {
                AuthenticationHeaderValue authHeaderVal = AuthenticationHeaderValue.Parse(authHeader);

                // RFC 2617 sec 1.2, "scheme" name is case-insensitive
                if (authHeaderVal.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase)
                    && authHeaderVal.Parameter != null)
                {
                    AuthenticateUser(authHeaderVal.Parameter, signature, timestamp, uri);
                }
            }
        }

        // If the request was unauthorized, add the WWW-Authenticate header 
        // to the response.

        /// <summary>
        /// The on application end request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void OnApplicationEndRequest(object sender, EventArgs e)
        {
            var response = HttpContext.Current.Response;
            Logger.Debug("response: " + response.StatusCode);
            if (response.StatusCode == 401)
            {
                response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", Realm));
            }
        }

        /// <summary>
        /// The set principal.
        /// </summary>
        /// <param name="principal">
        /// The principal.
        /// </param>
        private static void SetPrincipal(IPrincipal principal)
        {
            Thread.CurrentPrincipal = principal;
            if (HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }
        }

        #endregion
    }
}