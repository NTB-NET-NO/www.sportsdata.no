﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMunicipalityDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The MunicipalityDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The MunicipalityDataMapper interface.
    /// </summary>
    public interface IMunicipalityDataMapper
    {
        /// <summary>
        /// The get municipalities by job id.
        /// </summary>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Municipality> GetMunicipalitiesByJobId(int jobId);

        /// <summary>
        /// The get municipalities by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Municipality> GetMunicipalitiesBySportId(int sportId);

        /// <summary>
        /// The store municipalities.
        /// </summary>
        /// <param name="municipalities">
        /// The municipalities.
        /// </param>
        void StoreMunicipalities(List<Municipality> municipalities);

        /// <summary>
        /// The delete all.
        /// </summary>
        void DeleteAll();
    }
}