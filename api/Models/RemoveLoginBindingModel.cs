// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RemoveLoginBindingModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The remove login binding model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Models
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The remove login binding model.
    /// </summary>
    public class RemoveLoginBindingModel
    {
        /// <summary>
        /// Gets or sets the login provider.
        /// </summary>
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        /// <summary>
        /// Gets or sets the provider key.
        /// </summary>
        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }
}