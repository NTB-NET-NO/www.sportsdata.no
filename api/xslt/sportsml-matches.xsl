<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="1.0">
    <!-- Style sheet converting from serialized data to SportsML - matches -->
    <!-- Root element -->

    <xsl:output indent="yes" method="xml"/>
    

    <xsl:template match="/">
        <sports-content>
            <tournament>
                <tournament-metadata>
                    <xsl:attribute name="tournament-key" >
                        <xsl:value-of select="/Matches/MatchExtended[1]/TournamentId"/>
                    </xsl:attribute>
                    <xsl:attribute name="tournament-name" >
                        <xsl:value-of select="/Matches/MatchExtended[1]/TournamentName"/>
                    </xsl:attribute>
                    <sports-content-codes>
                        <sports-content-code code-type="sport">
                            <xsl:attribute name="code-name">
                                <xsl:value-of select="/Matches/MatchExtended[1]/SportName" />
                            </xsl:attribute>
                        </sports-content-code>
                    </sports-content-codes>
                </tournament-metadata>
                <tournament-division>
                    <xsl:for-each select="/Matches/MatchExtended">
                        <tournament-round>
                            <xsl:attribute name="round-number">
                                <xsl:value-of select="TournamentRoundNumber"/>
                            </xsl:attribute>
                            <sports-event>
                                <event-metadata>
                                    <xsl:attribute name="event-key">
                                        <xsl:value-of select="MatchId"/>
                                    </xsl:attribute> 
                                    <xsl:attribute name="event-name">
                                        <xsl:value-of select="HomeTeam"/>
                                        <xsl:text> - </xsl:text>
                                        <xsl:value-of select="AwayTeam"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="start-date-time">
                                        <xsl:value-of select="substring-before(MatchDate, 'T')"/>
                                        <xsl:text>T</xsl:text>
                                        <xsl:value-of select="substring(MatchStartTime,1,2)"/>
                                        <xsl:text>:</xsl:text>
                                        <xsl:value-of select="substring(MatchStartTime,3,2)"/>
                                        <xsl:text>:00+0100</xsl:text>
                                    </xsl:attribute>
                                </event-metadata>
                                <team>
                                    <team-metadata alignment="home">
                                        <xsl:attribute name="team-key">
                                            <xsl:value-of select="OrganizationNameShort"/>
                                            <xsl:value-of select="HomeTeamId"/>
                                        </xsl:attribute>
                                        <name>
                                            <xsl:attribute name="full">
                                                <xsl:value-of select="HomeTeam"/>
                                            </xsl:attribute>
                                        </name>
                                    </team-metadata>
                                    <xsl:if test="MatchResultList/MatchResult">
                                        <team-stats>
                                            <sub-score period-value="1" sub-score-name="pauseresultat">
                                                <xsl:attribute name="score">
                                                    <xsl:value-of select="MatchResultList/MatchResult[ResultTypeName='Pauseresultat']/HomeTeamGoals"/>
                                                </xsl:attribute>
                                            </sub-score>
                                            <sub-score period-value="2" sub-score-name="sluttresultat">
                                                <xsl:attribute name="score">
                                                    <xsl:value-of select="MatchResultList/MatchResult[ResultTypeName='Sluttresultat']/HomeTeamGoals"/>
                                                </xsl:attribute>
                                            </sub-score>
                                        </team-stats>
                                    </xsl:if>
                                </team>
                                <team>
                                    <team-metadata alignment="away">
                                        <xsl:attribute name="team-key">
                                            <xsl:value-of select="OrganizationNameShort"/>
                                            <xsl:value-of select="AwayTeamId"/>
                                        </xsl:attribute>
                                        <name>
                                            <xsl:attribute name="full">
                                                <xsl:value-of select="AwayTeam"/>
                                            </xsl:attribute>
                                        </name>
                                    </team-metadata>
                                    <xsl:if test="MatchResultList/MatchResult">
                                        <team-stats>
                                            <sub-score period-value="1" sub-score-name="pauseresultat">
                                                <xsl:attribute name="score">
                                                    <xsl:value-of select="MatchResultList/MatchResult[ResultTypeName='Pauseresultat']/AwayTeamGoals"/>
                                                </xsl:attribute>
                                            </sub-score>
                                            <sub-score period-value="2" sub-score-name="sluttresultat">
                                                <xsl:attribute name="score">
                                                    <xsl:value-of select="MatchResultList/MatchResult[ResultTypeName='Sluttresultat']/AwayTeamGoals"/>
                                                </xsl:attribute>
                                            </sub-score>
                                        </team-stats>
                                    </xsl:if>
                                </team>
                            </sports-event>
                        </tournament-round>
                    </xsl:for-each>
                </tournament-division>
            </tournament>

        </sports-content>
    </xsl:template>
</xsl:stylesheet>
