﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The UserDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    using System.Linq;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The UserDataMapper interface.
    /// </summary>
    public interface IUserDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The delete user.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        void DeleteUser(ApplicationUser user);

        /// <summary>
        /// The get user by email.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <returns>
        /// The <see cref="ApplicationUser"/>.
        /// </returns>
        ApplicationUser GetUserByEmail(string email);

        /// <summary>
        /// The get user by user id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="ApplicationUser"/>.
        /// </returns>
        ApplicationUser GetUserByUserId(string userId);

        /// <summary>
        /// The get user by user name.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="ApplicationUser"/>.
        /// </returns>
        ApplicationUser GetUserByUserName(string userName);

        /// <summary>
        ///     The get users.
        /// </summary>
        /// <returns>
        ///     The <see cref="IQueryable" />.
        /// </returns>
        IQueryable<ApplicationUser> GetUsers();

        /// <summary>
        /// The insert user.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        void InsertUser(ApplicationUser user);

        /// <summary>
        /// The is user in role by user id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsUserInRoleByUserId(string userId);

        /// <summary>
        /// The update user.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        void UpdateUser(ApplicationUser user);

        /// <summary>
        /// The set phone number.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="phoneNumber">
        /// The phone number.
        /// </param>
        void SetPhoneNumber(ApplicationUser user, string phoneNumber);

        /// <summary>
        /// The get phone number.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetPhoneNumber(ApplicationUser user);

        /// <summary>
        /// The get phone number confirmed.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool GetPhoneNumberConfirmed(ApplicationUser user);

        /// <summary>
        /// The set phone number confirmed.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="confirmed">
        /// The confirmed.
        /// </param>
        void SetPhoneNumberConfirmed(ApplicationUser user, bool confirmed);

        #endregion
    }
}