﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITeamDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TeamDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NIF.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The TeamDataMapper interface.
    /// </summary>
    public interface ITeamDataMapper
    {
        /// <summary>
        /// The get team persons.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Person> GetTeamPersons(int teamId);

        /// <summary>
        /// The get tournament teams.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<TournamentMatchTeam> GetTournamentTeams(int tournamentId);
    }
}