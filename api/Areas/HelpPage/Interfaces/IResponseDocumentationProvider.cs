﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IResponseDocumentationProvider.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   Interface for the Response Documentation Provider
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Areas.HelpPage.Interfaces
{
    using System.Web.Http.Controllers;

    /// <summary>
    ///     Interface for the Response Documentation Provider
    /// </summary>
    public interface IResponseDocumentationProvider
    {
        #region Public Methods and Operators

        /// <summary>
        /// Get Response Documentation is used to render the content of the return element in documentation
        /// </summary>
        /// <param name="actionDescriptor">
        /// The Action Descriptor
        /// </param>
        /// <returns>
        /// The value to present
        /// </returns>
        string GetResponseDocumentation(HttpActionDescriptor actionDescriptor);

        /// <summary>
        /// The get response description.
        /// </summary>
        /// <param name="actionDescriptor">
        /// The action descriptor.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetResponseDescription(HttpActionDescriptor actionDescriptor);

        /// <summary>
        /// The get response information.
        /// </summary>
        /// <param name="actionDescriptor">
        /// The action descriptor.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetResponseInformation(HttpActionDescriptor actionDescriptor);

        #endregion
    }
}