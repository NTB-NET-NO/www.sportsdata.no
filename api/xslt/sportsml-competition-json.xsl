<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs" 
    version="1.0">
    
    <xsl:output method="text" indent="yes"/>
    <xsl:variable name="newline">
        <xsl:text>
        </xsl:text>
    </xsl:variable>
    <xsl:template match="/">
        <xsl:text>{"events": [</xsl:text>
        <xsl:for-each select="/Tournaments/Tournament">
            <xsl:text>{</xsl:text>
            <xsl:value-of select="$newline" />
            <xsl:text>"</xsl:text>
            <xsl:value-of select="TournamentName"/>
            <xsl:text>":</xsl:text>
            <xsl:value-of select="$newline" />
            <xsl:text>{</xsl:text>
            <xsl:value-of select="$newline" />
            <xsl:text>"competitionid": </xsl:text>
            <xsl:value-of select="TournamentId"/>
            <xsl:text>, </xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"competitionname": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="TournamentName"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"competitionstartdate": "</xsl:text>
            <xsl:value-of select="StartDate"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline" />
            <xsl:text>"competitionenddate": "</xsl:text>
            <xsl:value-of select="EndDate"/>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"sportname":</xsl:text>
            <xsl:text> "</xsl:text>
            <xsl:value-of select="SportName"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"sportcode":</xsl:text>
            <xsl:text> "</xsl:text>
            <xsl:value-of select="SportId"/>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>}</xsl:text>
            <xsl:choose>
                <xsl:when test="position() != last()">
                    <xsl:value-of select="$newline" />
                    <xsl:text>},</xsl:text>
                    <xsl:value-of select="$newline" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>}</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
        <xsl:value-of select="$newline" />
        <xsl:text>]</xsl:text>
        <xsl:value-of select="$newline" />
        <xsl:text>}</xsl:text>
    </xsl:template>
</xsl:stylesheet>