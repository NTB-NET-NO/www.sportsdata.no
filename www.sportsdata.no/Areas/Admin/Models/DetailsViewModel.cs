﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DetailsViewModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The details view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Web.Areas.Admin.Repositories;

    /// <summary>
    /// The details view model.
    /// </summary>
    public class DetailsViewModel
    {
        /// <summary>
        /// The _customers data access.
        /// </summary>
        private readonly CustomerRepository _customersDataAccess = new CustomerRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="DetailsViewModel"/> class.
        /// </summary>
        public DetailsViewModel()
        {
        }

        // Getting the details for the view
        /// <summary>
        /// The get details.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see cref="DetailsView"/>.
        /// </returns>
        public DetailsView GetDetails(int customerId)
        {
            var view = new DetailsView();

            // Get the customer object
            var customer = this._customersDataAccess.GetOneCustomer(customerId);
            view.Customer = customer;

            // Get the jobs for this customer
            var jobRepository = new JobRepository { CustomerId = customerId };

            view.Jobs = new List<Job>(jobRepository.GetAll());

            // Get the list of sports
            var sportRepository = new SportRepository();
            view.Sports = new List<Sport>(sportRepository.GetAll());

            // Get the Age Category Definitions
            var ageCategoryDefinitionRepository = new AgeCategoryDefinitionRepository();
            view.AgeCategoryDefinitions = new List<AgeCategoryDefinition>(ageCategoryDefinitionRepository.GetAll());

            return view;
        }
    }
}