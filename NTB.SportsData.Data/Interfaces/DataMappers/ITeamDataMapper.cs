﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITeamDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TeamDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    /// <summary>
    /// The TeamDataMapper interface.
    /// </summary>
    public interface ITeamDataMapper
    {
        /// <summary>
        /// The delete team contact map.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="contactId">
        /// The contact id.
        /// </param>
        void DeleteTeamContactMap(int teamId, int contactId);

        /// <summary>
        /// The insert team contact map.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="contactId">
        /// The contact id.
        /// </param>
        void InsertTeamContactMap(int teamId, int contactId);
    }
}