﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Web.Areas.Input.Models;
using NTB.SportsData.Web.Areas.Input.Models.IdentityModels;

namespace NTB.SportsData.Web.Areas.Input.Controllers
{
    /// <summary>
    /// The user manager controller.
    /// </summary>
    [Authorize(Roles = "Administrator")]
    public class UserManagerController : Controller
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserManagerController"/> class.
        /// </summary>
        public UserManagerController()
            : this(new ApplicationUserManager(new ApplicationUserStore()))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserManagerController"/> class.
        /// </summary>
        /// <param name="userManager">
        /// The user manager.
        /// </param>
        public UserManagerController(UserManager<ApplicationUser> userManager)
        {
            this._userManager = userManager;
        }

        /// <summary>
        /// Gets or sets the _user manager.
        /// </summary>
        private UserManager<ApplicationUser> _userManager { get; set; }

        // GET: /UserManager/

        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            var userManagerModel = new UserManagerModels();

            var userProfiles = userManagerModel.GetUserProfileView();

            return this.View(userProfiles);
        }

        // GET: /UserManager/Details/5

        /// <summary>
        /// The details.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Details(int id)
        {
            return this.View();
        }

        // GET: /UserManager/Create

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Create()
        {
            var models = new UserManagerModels();
            var viewModel = models.GetViewModel();
            return this.View(viewModel);
        }

        // POST: /UserManager/Create

        // POST: /Account/Register
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RegisterViewModel model)
        {
            // public ActionResult Create(FormCollection collection)
            try
            {
                if (!this.ModelState.IsValid)
                {
                    var errors =
                        this.ModelState.Where(x => x.Value.Errors.Count > 0)
                            .Select(x => new {x.Key, x.Value.Errors})
                            .ToArray();

                    throw new Exception(errors.ToString());
                }

                var userContext = new ApplicationDbContext();

                var user = new ApplicationUser
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    UserName = model.UserName,
                    Email = model.EmailAddress,
                };

                var userStore = new ApplicationUserStore();

                var userManager = new ApplicationUserManager(userStore);

                var result = await userManager.CreateAsync(user);

                if (result.Succeeded)
                {
                    var selectedRole = model.SelectedRoleId;

                    userManager.AddToRole<ApplicationUser, string>(user.Id, selectedRole);

                    userManager.Update(user);

                    userContext.SaveChanges();
                }

                return this.RedirectToAction("Index", "UserManager");
            }
            catch
            {
                return this.View();
            }
        }

        // GET: /UserManager/Edit/abcdef

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Edit(string id)
        {
            var models = new UserManagerModels();
            var userProfile = models.GetUserProfileByUserId(new Guid(id));
            return this.View(userProfile);
        }

        /// <summary>
        /// The add errors.
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                this.ModelState.AddModelError(string.Empty, error);
            }
        }

        // POST: /UserManager/Edit/5

        // POST: /Account/Register
        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(UpdateViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                var errors =
                    this.ModelState.Where(x => x.Value.Errors.Count > 0)
                        .Select(x => new {x.Key, x.Value.Errors})
                        .ToArray();

                var sb = new StringBuilder();
                foreach (var foo in errors.SelectMany(error => error.Errors))
                {
                    sb.Append(foo.ErrorMessage)
                        .Append(",")
                        .Append(foo.Exception);
                }

                throw new Exception(sb.ToString());
            }

            var userContext = new ApplicationDbContext();

            var userList = userContext.Users.ToList();

            var user = userList.FirstOrDefault(u => u.UserName == model.UserName);

            if (user != null)
            {
                user.Id = model.UserId;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.Email = model.EmailAddress;
            }

            var userStore = new ApplicationUserStore();
            var roleStore = new ApplicationRoleStore();

            var userManager = new UserManager<ApplicationUser>(userStore);

            // var result = await userManager.UpdateAsync(user);
            await userStore.UpdateAsync(user);

            // if (!result.Succeeded) return RedirectToAction("Index", "UserManager");
            // userContext.SaveChanges();
            if (model.OldRole == model.SelectedRoleId)
            {
                return this.RedirectToAction("Index", "UserManager");
            }

            if (user != null)
            {
                // This method only removes the user from the list of roles that it has internally. not from the database
                // userManager.RemoveFromRole<ApplicationUser, string>(user.Id, model.SelectedRoleId);
                var currentRole = await roleStore.FindByNameAsync(model.OldRole);
                roleStore.RemoveFromRole(user, currentRole.Id);
            }

            var selectedRole = model.SelectedRoleId;

            // Now we are adding the user to the new role
            if (user != null)
            {
                user.UserId = user.Id;
                await userStore.AddToRoleAsync(user, selectedRole);

                // userManager.AddToRole<ApplicationUser, string>(user.UserId, selectedRole);

                // userManager.Update(user);
            }

            // userContext.SaveChanges();
            return this.RedirectToAction("Index", "UserManager");
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> Delete(string id)
        {
            var userContext = new ApplicationDbContext();

            var user =
                this._userManager.Users.SingleOrDefault(
                    u => string.Equals(u.Id, id, StringComparison.CurrentCultureIgnoreCase));

            var userStore = new ApplicationUserStore();

            await this._userManager.DeleteAsync(user);

            // var userManager = new UserManager<User>(userStore);

            // await userManager.DeleteAsync(user);
            return this.RedirectToAction("Index");
        }

        // POST: /UserManager/Delete/5

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        // GET: /Account/Register

        /// <summary>
        /// The register.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Register()
        {
            return this.View();
        }

        // POST: /Account/Register

        /// <summary>
        /// The register.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                // Then we shall also add him/her to a list of roles
                var userContext = new ApplicationDbContext();

                var userStore = new ApplicationUserStore();

                var userManager = new ApplicationUserManager(userStore);

                var user = new ApplicationUser()
                {
                    UserName = model.UserName,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.EmailAddress
                };
                var result = await this._userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var insertuser = userContext.Users.SingleOrDefault(u => u.UserName == model.UserName);

                    if (insertuser == null)
                    {
                        return this.RedirectToAction("Index", "UserManager");
                    }

                    var selectedRole = model.SelectedRoleId;
                    insertuser.UserId = insertuser.Id;

                    // var foo = await userManager.AddToRoleAsync(insertuser.Id, selectedRole);
                    await userStore.AddToRoleAsync(insertuser, selectedRole);

                    // userManager.AddToRole<ApplicationUser, string>(insertuser.Id, selectedRole);
                    return this.RedirectToAction("Index", "UserManager");
                }
                else
                {
                    this.AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return this.View(model);
        }
    }
}