﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Services.NFF.Interfaces;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.Services.NFF.Repositories
{
    /// <summary>
    /// The match repository.
    /// </summary>
    public class MatchRepository : IRepository<Match>, IDisposable, IMatchDataMapper
    {
        /// <summary>
        ///     The Logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(MatchRepository));

        /// <summary>
        /// The service client.
        /// </summary>
        public TournamentServiceClient ServiceClient = new TournamentServiceClient();

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchRepository"/> class.
        /// </summary>
        public MatchRepository()
        {
            if (this.ServiceClient.State == CommunicationState.Faulted)
            {
                this.ServiceClient.Close();
                this.ServiceClient = null;

                this.ServiceClient = new TournamentServiceClient();
            }

            if (this.ServiceClient.ClientCredentials == null)
            {
                return;
            }

            this.ServiceClient.ClientCredentials.UserName.UserName =
                ConfigurationManager.AppSettings["NFFServicesUsername"];
            this.ServiceClient.ClientCredentials.UserName.Password =
                ConfigurationManager.AppSettings["NFFServicesPassword"];
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get matches by tournament.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="includeSquad">
        /// The include squad.
        /// </param>
        /// <param name="includeReferees">
        /// The include referees.
        /// </param>
        /// <param name="includeResults">
        /// The include results.
        /// </param>
        /// <param name="includeEvents">
        /// The include events.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetMatchesByTournament(
            int tournamentId,
            bool includeSquad,
            bool includeReferees,
            bool includeResults,
            bool includeEvents)
        {
            try
            {
                Logger.DebugFormat("Requesting information by tournamentId: {0}", tournamentId);
                return
                    this.ServiceClient.GetMatchesByTournament(
                        tournamentId,
                        includeSquad,
                        includeReferees,
                        includeResults,
                        includeEvents,
                        null)
                        .ToList();
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return new List<Match>();
            }
        }

        public List<Match> GetMatchesbyDistrict(
            int districtId,
            bool includeSquad,
            bool includeReferees,
            bool includeResults,
            bool includeEvents,
            DateTime dateFrom,
            DateTime dateTo)
        {
            try
            {
                dateTo = dateTo.AddHours(23)
                    .AddMinutes(59)
                    .AddSeconds(59);

                return
                    this.ServiceClient.GetMatchesByDistrict(
                        districtId,
                        dateFrom,
                        dateTo,
                        includeSquad,
                        includeReferees,
                        includeResults,
                        includeEvents,
                        null)
                        .ToList();
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return new List<Match>();
            }
        }

        /// <summary>
        /// The get match by match id.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        public Match GetMatchByMatchId(int id)
        {
            return this.ServiceClient.GetMatch(id, true, true, true, true);
        }

        /// <summary>
        /// The get todays matches.
        /// </summary>
        /// <param name="matchDate">
        /// The match date.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetTodaysMatches(DateTime matchDate)
        {
            if (this.ServiceClient.State == CommunicationState.Faulted)
            {
                this.ServiceClient.Close();
                this.ServiceClient = null;

                this.ServiceClient = new TournamentServiceClient();
            }

            if (this.ServiceClient.ClientCredentials != null)
            {
                this.ServiceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServicesUsername"];
                this.ServiceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicesPassword"];
            }

            return this.ServiceClient.GetMatchesByDate(matchDate, false, false, false, false)
                .ToList();
        }

        /// <summary>
        /// The get match extended by match id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        public Match GetMatchExtendedByMatchId(int id)
        {
            try
            {
                return this.ServiceClient.GetMatch(id, true, true, true, true);
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return new Match();
            }
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(Match domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<Match> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Match domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Match domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<Match> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        public Match Get(int id)
        {
            return this.ServiceClient.GetMatch(id, false, false, true, false);
        }
    }
}