﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using NTB.SportsData.Data.DataMappers.Role;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Input.Models.IdentityModels
{
    public class ApplicationRoleManager : RoleManager<Role, int>
    {
        public ApplicationRoleManager(IRoleStore<Role, int> store) : base(store)
        {
        }

        public Task<Role> FindByIdAsync(string roleId)
        {
            var roleDataMapper = new RoleDataMapper();
            var roles = roleDataMapper.GetRoles();

            // return base.FindByIdAsync(roleId);
            return Task.Factory.StartNew(() => roles.SingleOrDefault(x => x.Id == roleId));
        }

        public override Task<Role> FindByNameAsync(string roleName)
        {
            var roleDataMapper = new RoleDataMapper();
            var roles = roleDataMapper.GetRoles();

            // return base.FindByIdAsync(roleId);
            return Task.Factory.StartNew(() => roles.SingleOrDefault(x => x.Name == roleName));
        }

        public Role FindRoleByUserName(string userName)
        {
            var roleDataMapper = new RoleDataMapper();
            var role = roleDataMapper.GetRoleByUserName(userName);

            var identityUserRole = new Role
            {
                Id = role.Id,
                Name = role.Name
                
            };
            return identityUserRole;
        }

        public Role FindRoleByUserId(string userId)
        {
            var roleDataMapper = new RoleDataMapper();
            var role = roleDataMapper.GetRoleByUserId(userId);

            var identityUserRole = new Role
            {
                Id = role.Id,
                Name = role.Name
            };
            return identityUserRole;
        }

        public override IQueryable<Role> Roles
        {
            get
            {
                var roleDataMapper = new RoleDataMapper();
                var roles = roleDataMapper.GetRoles();

                return roles;

                // get { return base.Roles; }
            }
            
        }
    }
}