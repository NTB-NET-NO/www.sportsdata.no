﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentContext.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament context.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Models.Tournaments
{
    using System.Collections.Generic;
    using System.Linq;

    using NTB.SportsData.Api.Interfaces;
    using NTB.SportsData.Data.Tournaments;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The tournament context.
    /// </summary>
    public class TournamentContext : ITournamentDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        public Tournament Get(int id)
        {
            var tournamentDataMapper = new TournamentDataMapper();

            return tournamentDataMapper.GetTournamentById(id);
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public IEnumerable<Tournament> GetAll(int customerId)
        {
            var tournamentDataMapper = new TournamentDataMapper();
            List<Tournament> tournaments = tournamentDataMapper.GetTournamentsByCustomerId(customerId);

            return tournaments.AsEnumerable();
        }

        #endregion
    }
}