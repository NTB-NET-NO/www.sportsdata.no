﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;

namespace NTB.SportsData.Utilities
{
    public class ApplicationUtilities
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ApplicationUtilities));

        public static Guid GetApplicationId()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("Identity_GetApplicationId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@ApplicationName", @"/"));

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        return reader.GetGuid(0);
                    }

                    return Guid.NewGuid();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
    
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return Guid.NewGuid();
            }
        }
    }
}