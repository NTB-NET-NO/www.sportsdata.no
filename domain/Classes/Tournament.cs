// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Tournament.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Domain.Classes
{
    using System;

    /// <summary>
    /// The tournament.
    /// </summary>
    public class Tournament
    {
        /// <summary>
        /// Gets or sets the job id.
        /// </summary>
        public int JobId { get; set; }

        /// <summary>
        /// Gets or sets the tournament type id.
        /// </summary>
        public int TournamentTypeId { get; set; }

        /// <summary>
        /// Gets or sets the tournament accepted.
        /// </summary>
        public int TournamentAccepted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether tournament selected.
        /// </summary>
        public bool TournamentSelected { get; set; }

        /// <summary>
        /// Gets or sets the age category filter id.
        /// </summary>
        public int AgeCategoryFilterId { get; set; }

        /// <summary>
        /// Gets or sets the municipality id.
        /// </summary>
        public int MunicipalityId { get; set; }

        /// <summary>
        /// Gets or sets the age category definition id.
        /// </summary>
        public int AgeCategoryDefinitionId { get; set; }

        /// <summary>
        /// Gets or sets the number of relegated teams.
        /// </summary>
        public int NumberOfRelegatedTeams { get; set; }

        /// <summary>
        /// Gets or sets the number of promoted teams.
        /// </summary>
        public int NumberOfPromotedTeams { get; set; }

        /// <summary>
        /// Gets or sets the teams qualified for promotion.
        /// </summary>
        public int TeamsQualifiedForPromotion { get; set; }

        /// <summary>
        /// Gets or sets the teams qualified for relegation.
        /// </summary>
        public int TeamsQualifiedForRelegation { get; set; }

        /// <summary>
        /// Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        /// Gets or sets the tournament name.
        /// </summary>
        public string TournamentName { get; set; }

        /// <summary>
        /// Gets or sets the age category definition.
        /// </summary>
        public string AgeCategoryDefinition { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the sport name.
        /// </summary>
        public string SportName { get; set; }

        /// <summary>
        /// Gets or sets the age category id.
        /// </summary>
        public int? AgeCategoryId { get; set; }

        /// <summary>
        /// Gets or sets the age category.
        /// </summary>
        public string AgeCategory { get; set; }

        /// <summary>
        /// Gets or sets the season id.
        /// </summary>
        public int SeasonId { get; set; }

        /// <summary>
        /// Gets or sets the season name.
        /// </summary>
        public string SeasonName { get; set; }

        /// <summary>
        /// Gets or sets the discipline id.
        /// </summary>
        public int? DisciplineId { get; set; }

        /// <summary>
        /// Gets or sets the discipline name.
        /// </summary>
        public string DisciplineName { get; set; }

        /// <summary>
        /// Gets or sets the organization id.
        /// </summary>
        public int? OrganizationId { get; set; }

        /// <summary>
        /// Gets or sets the organization name.
        /// </summary>
        public string OrganizationName { get; set; }

        /// <summary>
        /// Gets or sets the organization name short.
        /// </summary>
        public string OrganizationNameShort { get; set; }

        /// <summary>
        /// Gets or sets the district id.
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        /// Gets or sets the gender id.
        /// </summary>
        public int GenderId { get; set; }

        /// <summary>
        /// Gets or sets the tournament number.
        /// </summary>
        public string TournamentNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether push.
        /// </summary>
        public bool Push { get; set; }

        /// <summary>
        /// Gets or sets the division.
        /// </summary>
        public int Division { get; set; }

        public bool MatchesLive { get; set; }

        public bool HasMatches { get; set; }

        public string TournamentClassIds { get; set; }

        public int OrgId { get; set; }
    }
}