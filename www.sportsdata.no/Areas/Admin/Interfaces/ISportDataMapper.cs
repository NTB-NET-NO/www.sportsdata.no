﻿using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Interfaces
{
    interface ISportDataMapper
    {
        Sport GetSportIdBySeasonId(int seasonId);
        int GetSportIdFromOrgId(int orgId);
        int GetFederationUserIdFromSportId(int sportId);
    }
}
