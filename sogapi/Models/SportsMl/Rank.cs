﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace sogapi.Models.SportsMl
{
    /// <summary>
    /// The rank.
    /// </summary>
    [XmlRoot("rank")]
    [JsonObject("rank")]
    public class Rank
    {
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        [XmlAttribute("value")]
        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public int Value { get; set; }
    }
}