﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTB.SportsData.Domain.Classes
{
    public class DataBaseFetch
    {
        public int OrganisationId { get; set; }

        public DateTime LatestFetchDateTime { get; set; }
    }
}
