﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITeamDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TeamDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The TeamDataMapper interface.
    /// </summary>
    internal interface ITeamDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get tournament teams.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Team> GetTournamentTeams(int tournamentId, int sportId);

        #endregion
    }
}