﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CompetitionTransformer.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Transformers.JSON
{
    using System.Configuration;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Xsl;

    using NTB.SportsData.Api.Interfaces;

    /// <summary>
    ///     Competition model is used to transform from RawXML to JSON
    /// </summary>
    public class CompetitionTransformer : ITransformer
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the xslt file.
        /// </summary>
        public string XsltFile { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Method transforming from raw to JSON
        /// </summary>
        /// <param name="document">
        /// String containing the XML structure
        /// </param>
        /// <returns>
        /// String containing json structure
        /// </returns>
        public string TransformObject(string document)
        {
            if (string.IsNullOrEmpty(this.XsltFile))
            {
                throw new System.Exception("No XSLT file defined for this method.");
            }
            var transformer = new XslCompiledTransform();
            string xsltPath = ConfigurationManager.AppSettings["sportsmlxsltfilepath"];
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(document);

            transformer.Load(xsltPath + @"\" + XsltFile);

            var settings = new XmlWriterSettings
                               {
                                   Indent = true,
                                   IndentChars = "\t",
                                   Encoding = Encoding.UTF8,
                                   OmitXmlDeclaration = false
                               };

            using (var textWriter = new StringWriter())
            {
                var writer = new XmlTextWriter(textWriter);

                transformer.Transform(xmlDoc, null, writer);

                return textWriter.ToString();
            }
        }

        #endregion
    }
}