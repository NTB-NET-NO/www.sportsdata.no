﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITeamDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TeamDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NFF.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The TeamDataMapper interface.
    /// </summary>
    public interface ITeamDataMapper
    {
        /// <summary>
        /// The get tournament teams.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Team> GetTournamentTeams(int tournamentId);

        /// <summary>
        /// The get teams by municipality.
        /// </summary>
        /// <param name="municipalityId">
        /// The municipality id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Team> GetTeamsByMunicipality(int municipalityId);

        /// <summary>
        /// The get team persons.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<OrganizationPerson> GetTeamPersons(int teamId);
    }
}