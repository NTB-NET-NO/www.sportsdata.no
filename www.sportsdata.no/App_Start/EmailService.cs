using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;

namespace NTB.SportsData.Web
{
    public class EmailService : IIdentityMessageService
    {
        
        public Task SendAsync(IdentityMessage message)
        {
            
            return SendMail(message);
        }
        public Task SendMail(IdentityMessage message)
        {
            #region formatter
            string text = string.Format("Please click on this link to {0}: {1}", message.Subject, message.Body);
            string html = "Please confirm your account by clicking this link: <a href=\"" + message.Body + "\">link</a><br/>";

            html += HttpUtility.HtmlEncode(@"Or click on the copy the following link on the browser:" + message.Body);
            #endregion

            var msg = new MailMessage {From = new MailAddress("505@ntb.no")};
            if (message.Destination == null)
            {
                return null;
            }

            msg.To.Add(new MailAddress(message.Destination));
            msg.Subject = message.Subject;
            msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
            msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

            var mailClient = new SmtpClient("mail.ntb.no"); // todo: Fix so this one comes from config-file
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            mailClient.UseDefaultCredentials = false;
            // var credentials = new NetworkCredential("joe@contoso.com", "XXXXXX");
            // smtpClient.Credentials = credentials;
            // smtpClient.EnableSsl = true;
            // mailClient.Send(msg);

            //// Plug in your email service here to send an email.
            //MailMessage email = new MailMessage("xxx@hotmail.com", message.Destination);
            //email.Subject = message.Subject;
            //email.Body = message.Body;
            //email.IsBodyHtml = true;
            //var mailClient = new SmtpClient("smtp.live.com", 587)
            //{
            //    Credentials = new NetworkCredential("xxx@hotmail.com", "password"),
            //    EnableSsl = true
            //};
            return mailClient.SendMailAsync(msg);
        }
    }
}