// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITournamentFacade.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TournamentFacade interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Facade.NFF.TournamentInterface
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The TournamentFacade interface.
    /// </summary>
    internal interface ITournamentFacade
    {
        // Returns list
        /// <summary>
        /// The get tournament by municipalities.
        /// </summary>
        /// <param name="municipalities">
        /// The municipalities.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId);

        /// <summary>
        /// The get tournaments by district.
        /// </summary>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId);

        /// <summary>
        /// The get clubs by tournament.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetClubsByTournament(int tournamentId, int seasonId);

        /// <summary>
        /// The get tournaments by team.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentsByTeam(int teamId, int seasonId);

        /// <summary>
        /// The get tournament.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        Tournament GetTournament(int tournamentId);

        /// <summary>
        /// The get districts.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<District> GetDistricts();

        /// <summary>
        /// The get district.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="District"/>.
        /// </returns>
        District GetDistrict(int id);

        /// <summary>
        /// The get municipalities.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Municipality> GetMunicipalities();

        /// <summary>
        /// The get municipalitiesby district.
        /// </summary>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Municipality> GetMunicipalitiesbyDistrict(int districtId);

        /// <summary>
        /// The get seasons.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Season> GetSeasons();

        /// <summary>
        /// The get season.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Season"/>.
        /// </returns>
        Season GetSeason(int id);

        /// <summary>
        /// The get ongoing season.
        /// </summary>
        /// <returns>
        /// The <see cref="Season"/>.
        /// </returns>
        Season GetOngoingSeason();

        /// <summary>
        /// The get age categories tournament.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<AgeCategory> GetAgeCategoriesTournament();

        /// <summary>
        /// The get age category tournament.
        /// </summary>
        /// <returns>
        /// The <see cref="AgeCategory"/>.
        /// </returns>
        AgeCategory GetAgeCategoryTournament();

        /// <summary>
        /// The get teams by municipality.
        /// </summary>
        /// <param name="municipalityId">
        /// The municipality id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Team> GetTeamsByMunicipality(int municipalityId, int seasonId);

        /// <summary>
        /// The get matches by tournament.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="includeSquad">
        /// The include squad.
        /// </param>
        /// <param name="includeReferees">
        /// The include referees.
        /// </param>
        /// <param name="includeResults">
        /// The include results.
        /// </param>
        /// <param name="includeEvents">
        /// The include events.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Match> GetMatchesByTournament(
            int tournamentId, 
            bool includeSquad, 
            bool includeReferees, 
            bool includeResults, 
            bool includeEvents);

        /// <summary>
        /// The get todays matches.
        /// </summary>
        /// <param name="matchDate">
        /// The match date.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Match> GetTodaysMatches(DateTime matchDate);

        /// <summary>
        /// The get team contacts.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<ContactPerson> GetTeamContacts(int teamId);

        /// <summary>
        /// The get clubs.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Club> GetClubs();

        /// <summary>
        /// The insert match result.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <returns>
        /// boolean value
        /// </returns>
        bool InsertMatchResult(Match match);

        /// <summary>
        /// The get matches by tournament.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="includeSquad">
        /// The include squad.
        /// </param>
        /// <param name="includeReferees">
        /// The include referees.
        /// </param>
        /// <param name="includeResults">
        /// The include results.
        /// </param>
        /// <param name="includeEvents">
        /// The include events.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Match> GetMatchesByDistrict(
            int districtId,
            DateTime matchDate);

        List<Tournament> GetTournamentsBySeasonId(int seasonId);
    }
}