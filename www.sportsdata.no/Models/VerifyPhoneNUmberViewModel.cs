﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VerifyPhoneNUmberViewModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    ///     The verify phone number view model.
    /// </summary>
    public class VerifyPhoneNumberViewModel
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the code.
        /// </summary>
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        /// <summary>
        ///     Gets or sets the phone number.
        /// </summary>
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        #endregion
    }
}