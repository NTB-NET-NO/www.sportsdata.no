﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefineViewModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The define view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Web.Areas.Admin.Repositories;

    /// <summary>
    /// The define view model.
    /// </summary>
    public class DefineViewModel
    {
        public CustomerJob CustomerJob { get; set; }

        public DetailsView DetailsView { get; set; }

        public List<Season> Seasons { get; set; }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <returns>
        /// The <see cref="DefineView"/>.
        /// </returns>
        public DefineView Get(int jobId)
        {
            var defineView = new DefineView();

            var jobRepository = new JobRepository();

            var customerjob = jobRepository.GetCustomerJobByJobId(jobId);

            if (customerjob.TeamSport == 1)
            {
                var seasonRepository = new SeasonRepository();
                defineView.Seasons = seasonRepository.GetSeasonsBySportId(customerjob.SportId);
            }
            else
            {
                var year = DateTime.Today.Year - 1;

                var seasons = new List<Season>();
                for (var i = year; i < year + 2; i++)
                {
                    var season = new Season { SeasonId = i, SportId = customerjob.SportId, SeasonName = "Sesong " + i, SeasonActive = true };
                    seasons.Add(season);
                }

                // defineView.Seasons = seasons;
                this.Seasons = seasons;
            }

            var viewModel = new DetailsViewModel();
            defineView.DetailsView = viewModel.GetDetails(customerjob.CustomerId);
            defineView.CustomerJob = customerjob;

            return defineView;
        }
    }
}