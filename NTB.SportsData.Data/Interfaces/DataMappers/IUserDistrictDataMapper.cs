﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserDistrictDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The UserDistrictDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The UserDistrictDataMapper interface.
    /// </summary>
    public interface IUserDistrictDataMapper
    {
        /// <summary>
        /// The insert user map.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        void InsertUserMap(UserDistrictMapper domainobject);

        /// <summary>
        /// The get user by user id and district id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool GetUserByUserIdAndDistrictId(Guid userId, int districtId);

        /// <summary>
        /// The get all user sessions by user id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<DateTime> GetAllUserSessionsByUserId(Guid userId);

        /// <summary>
        /// The delete mapping by date.
        /// </summary>
        /// <param name="dateTime">
        /// The date time.
        /// </param>
        void DeleteMappingByDate(DateTime dateTime);
    }
}