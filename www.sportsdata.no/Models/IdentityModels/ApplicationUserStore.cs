// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationUserStore.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The application user store.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Models.IdentityModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.Ajax.Utilities;
    using Microsoft.AspNet.Identity;

    using NTB.SportsData.Data.DataMappers.Role;
    using NTB.SportsData.Data.DataMappers.User.Web;
    using NTB.SportsData.Data.Interfaces.DataMappers;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The application user store.
    /// </summary>
    public class ApplicationUserStore : IUserStore<ApplicationUser>, 
                                        IUserPasswordStore<ApplicationUser>, 
                                        IUserSecurityStampStore<ApplicationUser>, 
                                        IUserEmailStore<ApplicationUser>, 
                                        IQueryableUserStore<ApplicationUser>, 
                                        IUserRoleStore<ApplicationUser>
    {
        /// <summary>
        /// The _datamapper.
        /// </summary>
        private IUserDataMapper _datamapper;

        /// <summary>
        /// The _role data mapper.
        /// </summary>
        private IRoleDataMapper _roleDataMapper;

        // readonly UserStore<IdentityUser> _userStore = new UserStore<IdentityUser>(new ApplicationDbContext());
        /// <summary>
        /// The _users.
        /// </summary>
        private List<ApplicationUser> _users = new List<ApplicationUser>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationUserStore"/> class.
        /// </summary>
        public ApplicationUserStore()
        {
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        public IQueryable<ApplicationUser> Users
        {
            get
            {
                this._datamapper = new UserDataMapper();
                return this._datamapper.GetUsers();
            }

            private set
            {
                if (value != null)
                {
                    this._users.AddRange(value);
                }
            }
        }

        /// <summary>
        /// The add to role async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NullReferenceException">
        /// </exception>
        public Task AddToRoleAsync(ApplicationUser user, string roleName)
        {
            if (user == null)
            {
                throw new NullReferenceException("user");
            }

            if (roleName.IsNullOrWhiteSpace())
            {
                throw new NullReferenceException("roleName");
            }

            return Task.Factory.StartNew(
                () =>
                    {
                        this._roleDataMapper = new RoleDataMapper();
                        this._roleDataMapper.AddUserToRole(user, roleName);
                    });
        }

        /// <summary>
        /// The remove from role async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NullReferenceException">
        /// </exception>
        public Task RemoveFromRoleAsync(ApplicationUser user, string roleName)
        {
            if (user == null)
            {
                throw new NullReferenceException("user");
            }

            if (roleName.IsNullOrWhiteSpace())
            {
                throw new NullReferenceException("roleName");
            }

            return Task.Factory.StartNew(
                () =>
                    {
                        this._roleDataMapper = new RoleDataMapper();
                        this._roleDataMapper.RemoveUserFromRole(user, roleName);
                    });
        }

        /// <summary>
        /// The get roles async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Task<IList<string>> GetRolesAsync(ApplicationUser user)
        {
            // if (user == null)
            // {
            // throw new NullReferenceException("user");
            // }

            // return Task.Factory.StartNew(() =>
            // {
            // _roleDataMapper = new RoleDataMapper();
            // var roles = _roleDataMapper.GetRoles();
            // var stringRoles = roles.Select(x => x.Name);
            // var listOfRoles = new List<string>();

            // listOfRoles.AddRange(stringRoles);

            // return listOfRoles;
            // });
            throw new NotImplementedException();
        }

        /// <summary>
        /// The is in role async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NullReferenceException">
        /// </exception>
        public Task<bool> IsInRoleAsync(ApplicationUser user, string roleName)
        {
            if (user == null)
            {
                throw new NullReferenceException("user");
            }

            if (roleName.IsNullOrWhiteSpace())
            {
                throw new NullReferenceException("roleName");
            }

            return Task.Factory.StartNew(
                () =>
                    {
                        this._roleDataMapper = new RoleDataMapper();
                        var role = this._roleDataMapper.GetRoleByUserName(user.UserName);

                        if (role.Name == roleName)
                        {
                            return true;
                        }

                        return false;
                    });
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            // throw new NotImplementedException();
        }

        /// <summary>
        /// The create async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public Task CreateAsync(ApplicationUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.Factory.StartNew(
                () =>
                    {
                        user.Id = Guid.NewGuid().ToString();

                        this._datamapper = new UserDataMapper();
                        this._datamapper.InsertUser(user);
                    });
        }

        /// <summary>
        /// The update async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public Task UpdateAsync(ApplicationUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.Factory.StartNew(
                () =>
                    {
                        // user.Id = Guid.NewGuid().ToString();
                        this._datamapper = new UserDataMapper();
                        this._datamapper.UpdateUser(user);
                    });
        }

        /// <summary>
        /// The delete async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public Task DeleteAsync(ApplicationUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.Factory.StartNew(
                () =>
                    {
                        // user.Id = Guid.NewGuid().ToString();
                        this._datamapper = new UserDataMapper();
                        this._datamapper.DeleteUser(user);
                    });
        }

        /// <summary>
        /// The find by id async.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// </exception>
        public Task<ApplicationUser> FindByIdAsync(string userId)
        {
            if (userId.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException("userId");
            }

            Guid parsedUserId;
            if (!Guid.TryParse(userId, out parsedUserId))
            {
                throw new ArgumentOutOfRangeException(
                    "userId", 
                    string.Format("'{0}' is not a valid UserId", new { userId }));
            }

            return Task.Factory.StartNew(
                () =>
                    {
                        var datamapper = new UserDataMapper();
                        return datamapper.GetUserByUserId(userId);
                    });
        }

        /// <summary>
        /// The find by name async.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public Task<ApplicationUser> FindByNameAsync(string userName)
        {
            if (userName.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException("userName");
            }

            return Task.Factory.StartNew(
                () =>
                    {
                        this._datamapper = new UserDataMapper();
                        return this._datamapper.GetUserByUserName(userName);
                    });
        }

        #region IUserPasswordStore

        /// <summary>
        /// The set password hash async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="passwordHash">
        /// The password hash.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.PasswordHash = passwordHash;

            return Task.FromResult(0);
        }

        /// <summary>
        /// The get password hash async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public Task<string> GetPasswordHashAsync(ApplicationUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(user.PasswordHash);
        }

        /// <summary>
        /// The has password async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<bool> HasPasswordAsync(ApplicationUser user)
        {
            return Task.FromResult(!string.IsNullOrEmpty(user.PasswordHash));
        }

        #endregion

        #region IUserSecurityStampStore

        /// <summary>
        /// The set security stamp async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="stamp">
        /// The stamp.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public Task SetSecurityStampAsync(ApplicationUser user, string stamp)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.SecurityStamp = stamp;

            return Task.FromResult(0);
        }

        /// <summary>
        /// The get security stamp async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public Task<string> GetSecurityStampAsync(ApplicationUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(user.SecurityStamp);
        }

        #endregion

        #region IUserEmailStore

        /// <summary>
        /// The set email async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Task SetEmailAsync(ApplicationUser user, string email)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get email async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public Task<string> GetEmailAsync(ApplicationUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(user.Email);
        }

        /// <summary>
        /// The get email confirmed async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public Task<bool> GetEmailConfirmedAsync(ApplicationUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(!string.IsNullOrEmpty(user.Email));
        }

        /// <summary>
        /// The set email confirmed async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="confirmed">
        /// The confirmed.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Task SetEmailConfirmedAsync(ApplicationUser user, bool confirmed)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The find by email async.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public Task<ApplicationUser> FindByEmailAsync(string email)
        {
            if (email.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException("email");
            }

            return Task.Factory.StartNew(
                () =>
                    {
                        this._datamapper = new UserDataMapper();
                        return this._datamapper.GetUserByEmail(email);
                    });
        }

        #endregion
    }
}