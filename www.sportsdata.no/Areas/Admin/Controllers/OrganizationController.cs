﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using NTB.SportsData.Data.DataMappers.PartialResult;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Web.Areas.Admin.Models;
using NTB.SportsData.Web.Areas.Admin.Repositories;

namespace NTB.SportsData.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// The organization controller.
    /// </summary>
    [Authorize]
    public class OrganizationController : Controller
    {
        #region Static Fields

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(OrganizationController));

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Check Club Advanced is a manual process where the user have to find the right municipality, district and even
        ///     country.
        /// </summary>
        /// <param name="id">
        /// Organization Id we are working with
        /// </param>
        /// <returns>
        /// Check Club Advanced View
        /// </returns>
        public ActionResult CheckClubAdvanced(int id)
        {
            if (this.Session["ConnectedOrgId"].ToString() == string.Empty)
            {
                return this.RedirectToAction("ShowClubs/" + id);
            }

            this.Session["OrgId"] = id;
            var model = new OrganizationModel();

            var connectedOrgId = Convert.ToInt32(this.Session["ConnectedOrgId"]);

            return this.View(model.GetAdvancedCheckClubView(id, connectedOrgId));
        }

        /// <summary>
        /// The check clubs.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult CheckClubs(int id, FormCollection collection)
        {
            var model = new OrganizationModel();

            model.UpdateCheckedClubs(id, collection);

            return this.RedirectToAction("ShowClubs/" + id);
        }

        /// <summary>
        /// The connect clubs.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult ConnectClubs(int id, FormCollection collection)
        {
            var orgId = id; // Just so we know which type of Id we are working on
            var fromOrgId = Convert.ToInt32(collection["selectOrg"]);
            this.Session["ConnectedOrgId"] = fromOrgId;

            var model = new OrganizationModel();
            var foundClubs = model.FindClubs(orgId, fromOrgId);

            this.ViewBag.OrgId = id;
            return this.View("CheckClubs", foundClubs);

            // return RedirectToAction("SelectSeason/" + id);
        }

        /// <summary>
        /// The connect clubs.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ConnectClubs(int id)
        {
            var orgModel = new OrganizationModel();
            var orgId = 0;
            if (this.Session["OrgId"].ToString() != string.Empty)
            {
                orgId = Convert.ToInt32(this.Session["OrgId"]);
            }

            orgModel.OrgId = orgId;
            var org = orgModel.GetOrganizationById(orgId);

            var orgs = orgModel.GetOrganizations();

            var viewModel = new ConnectClubsViewModel {Organization = org, Organizations = orgs};

            return this.View(viewModel);
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Create()
        {
            return this.View();
        }

        // POST: /Organization/Create

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                var model = new OrganizationModel();
                model.InsertOrganization(collection);
                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        // GET: /Organization/

        // GET: /Crganization/CreateOrganizationSeason/2000

        /// <summary>
        /// The create organization season.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult CreateOrganizationSeason(int id)
        {
            var model = new OrganizationModel();
            return this.View(model.GetCreateSeasonView(id));
        }

        /// <summary>
        /// The create organization sport.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult CreateOrganizationSport(int id)
        {
            var repository = new SportRepository();
            this.ViewBag.OrgId = id;
            return this.View(repository.GetAll()
                .ToList());
        }

        // POST: /Organization/CreateSeason

        /// <summary>
        /// The create season.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult CreateSeason(int id, FormCollection collection)
        {
            var model = new OrganizationModel();
            model.CreateSeason(id, collection);
            return this.RedirectToAction("Details/" + id);
        }

        /// <summary>
        /// The create sport.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult CreateSport(int id, FormCollection collection)
        {
            var model = new OrganizationModel();
            model.CreateSport(id, collection);
            return this.RedirectToAction("Details/" + id);
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Delete(int id)
        {
            return this.View();
        }

        // POST: /Organization/Delete/5

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var organization = new Organization()
                {
                    OrganizationId = id
                };
                var repository = new OrganizationRepository();


                repository.Delete(organization);

                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        public ActionResult DeleteAgeCategory(int id)
        {
            try
            {
                var repository = new AgeCategoryRepository();
                var ageCategory = new AgeCategory();
                ageCategory.CategoryId = id;
                repository.Delete(ageCategory);
                return this.RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                
                Logger.Error(exception);
                return this.RedirectToAction("Index");
            }
        }

        // GET: /Organization/Details/5
        /// <summary>
        /// The details.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Details(int id)
        {
            var model = new OrganizationModel();
            this.Session["OrgId"] = id;

            return this.View(model.GetOrganizationDetailsById(id));
        }

        public ActionResult EditAgeCategory(int id)
        {
            var model = new AgeCategoryModel();
            var returnValue = model.GetEditCategoryViewModel(id);

            return this.PartialView("EditAgeCategory", returnValue);
        }

        /// <summary>
        /// Get the list of tournaments for the organisation/selected organisation
        /// </summary>
        /// <param name="orgId"></param>
        /// <returns></returns>
        public ActionResult ListSeasons(int id)
        {
            var model = new SeasonModel();
            var viewModel = model.GetSeasonByOrgId(id);
            ViewBag.OrgId = id;
            return this.PartialView("ListTournament", viewModel);
        }

        [HttpPost]
        public ActionResult UpdateSelectedTournament(int id, FormCollection collection)
        {
            var model = new TournamentModel();
            model.UpdateSelectedTournament(id, collection);
            return this.RedirectToAction("Index");
        }

        public ActionResult EditTournament(int id)
        {
            var model = new TournamentModel();
            var viewModel = model.GetTournamentViewModelByTournamentId(id);

            return this.PartialView("EditTournament", viewModel);
        }

        [HttpGet]
        public ActionResult ListTournamentsBySeasonsId(int id)
        {
            var model = new TournamentModel();
            var viewModel = model.GetTournamentsBySeasonId(id);
            
            // Serializing to JSON

            // return this.Json(viewModel, JsonRequestBehavior.AllowGet); // 

            return this.PartialView("ListSelectedSeasonTournament", viewModel);
        }

        [HttpPost]
        public ActionResult UpdateSelectedAgeCategory(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                var model = new AgeCategoryModel();
                model.UpdateAgeCategory(collection);

                var orgId = collection["orgId"];
                return this.RedirectToAction("Details", new { id = orgId, });
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return this.RedirectToAction("Index");
                // return this.View();
            }
        }

        public ActionResult ViewFederation(int id)
        {
            this.ViewBag.OrgId = id;
            var model = new FederationModel();
            return this.PartialView("ViewFederation", model.FederationRemoteRepository.GetFederation(id));
        }

        public ActionResult UpdatePartialResults(int id)
        {
            return this.PartialView("PartialResults");
        }

        public ActionResult PartialResults(int id)
        {
            var repository = new PartialResultRepository();
            List<PartialResultType> model = repository.GetPartialResultsForOrg(id);
            ViewBag.OrgId = id;
            return this.PartialView("PartialResults", model);
        }

        public ActionResult GetSeasons(int id)
        {
            var sportDataMapper = new SportRepository();
            var sportId = sportDataMapper.GetSportIdFromOrgId(id);

            var model = new SeasonModel();
            var seasons = new List<Season>(model.GetSeasonsBySportId(sportId));

            var remoteRepository = new FederationRemoteRepository();

            var federationDisciplines = remoteRepository.GetFederationDisciplines(id);

            model.InsertSeasons(sportId, seasons);

            var view = new SeasonView { SportId = sportId, FederationDisciplines = federationDisciplines, Seasons = seasons };

            return this.PartialView("ListSeasons", view);
        }
        // GET: /Organization/Create

        // GET: /Organization/Edit/5

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Edit(int id)
        {
            var model = new OrganizationModel();
            var organization = model.GetOrganizationById(id);
            return this.View(organization);
        }

        // POST: /Organization/Edit/5

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                var model = new OrganizationModel();
                model.UpdateOrganization(collection);
                return this.RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return this.RedirectToAction("Index");
                // return this.View();
            }
        }

        // GET: /Organization/Delete/5

        /// <summary>
        /// The get clubs.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GetClubs(int id)
        {
            /*
             * Consider some sort of factory pattern here.
             * So to choose which facade is to be used to get the clubs
             * This in case we are to add more outside NIF/NFF event organizors into admin
             * 
             */
            var orgId = Convert.ToInt32(this.Session["OrgId"]);

            if (orgId <= 0)
            {
                return this.RedirectToAction("SelectSeason/" + id);
            }

            var orgModel = new OrganizationModel();
            var clubs = orgModel.GetOrganizationClubs(id, orgId);

            var newClubs = orgModel.FindNewClubs(clubs);

            orgModel.InsertClubs(newClubs);

            return this.RedirectToAction("SelectSeason/" + id);
        }

        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            var organizationModel = new OrganizationModel();
            return this.View(organizationModel.GetOrganizations());
        }

        /// <summary>
        /// The select season.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult SelectSeason(int id)
        {
            var model = new OrganizationModel();
            var orgId = Convert.ToInt32(this.Session["OrgId"]);
            return this.View(model.GetSelectedSeasonDetailBySeasonid(id, orgId));
        }

        /// <summary>
        /// The show clubs.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ShowClubs(int id)
        {
            var model = new OrganizationModel();
            return this.View(model.GetClubsByParentOrgId(id, true));
        }

        /// <summary>
        /// The update club advanced.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult UpdateClubAdvanced(int id, FormCollection collection)
        {
            var clubId = id; // Just so we know what type of Id we are working with
            var parentOrgid = Convert.ToInt32(this.Session["OrgId"]); // Getting the organization we are working with

            var model = new OrganizationModel();
            model.UpdateClubAdvanced(clubId, parentOrgid, collection);

            return this.RedirectToAction("CheckClubAdvanced/" + parentOrgid);
        }

        #endregion
    }
}