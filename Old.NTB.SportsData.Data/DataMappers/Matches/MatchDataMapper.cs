// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.DataMappers.Matches
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Data.Interfaces.Common;
    using NTB.SportsData.Data.Interfaces.DataMappers;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The match data mapper.
    /// </summary>
    public class MatchDataMapper : IRepository<Match>, IDisposable, IMatchDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(MatchDataMapper));

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get matches by tournament id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Match> GetMatchesByTournamentId(int tournamentId)
        {
            var matches = new List<Match>();

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetMatchesByTournamentId", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("Tournamentid", tournamentId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var match = new Match();
                            match.MatchId = (int)sqlDataReader["MatchId"];
                            match.SportId = (int)sqlDataReader["SportId"];
                            match.HomeTeam = sqlDataReader["HomeTeam"].ToString();
                            match.AwayTeam = sqlDataReader["AwayTeam"].ToString();
                            match.MatchDate = Convert.ToDateTime(sqlDataReader["Date"].ToString());
                            match.TournamentId = (int)sqlDataReader["TournamentId"];

                            matches.Add(match);
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return matches;
            }
        }

        /// <summary>
        /// The get local matches by district id.
        /// </summary>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Match> GetLocalMatchesByDistrictId(int districtId)
        {
            var matches = new List<Match>();

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetTodaysMatchesByDistrict", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("DistrictId", districtId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return matches;
                    }

                    while (sqlDataReader.Read())
                    {
                        var match = new Match
                                        {
                                            MatchId = Convert.ToInt32(sqlDataReader["MatchId"]), 
                                            SportId = Convert.ToInt32(sqlDataReader["SportId"]), 
                                            HomeTeam = sqlDataReader["HomeTeamName"].ToString(), 
                                            HomeTeamId = Convert.ToInt32(sqlDataReader["HomeTeamId"]), 
                                            AwayTeam = sqlDataReader["AwayTeamName"].ToString(), 
                                            AwayTeamId = Convert.ToInt32(sqlDataReader["AwayTeamId"]), 
                                            MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"].ToString()), 
                                            MatchStartTime = Convert.ToInt32(sqlDataReader["MatchTime"]), 
                                            TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]), 
                                            TournamentName = sqlDataReader["TournamentName"].ToString(), 
                                            MatchComment = sqlDataReader["MatchComment"].ToString()
                                        };

                        // match.HomeTeamId = Convert.ToInt32(sqlDataReader["HomeTeamGoal"]);
                        // match.AwayTeamId = Convert.ToInt32(sqlDataReader["AwayTeamGoal"]);
                        matches.Add(match);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return matches;
            }
        }

        /// <summary>
        /// The get match comment.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="MatchComment"/>.
        /// </returns>
        public MatchComment GetMatchComment(int matchId)
        {
            var matchComment = new MatchComment();

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetMatchCommentByMatchId", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("MatchId", matchId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return matchComment;
                    }

                    while (sqlDataReader.Read())
                    {
                        matchComment.Id = matchId;
                        matchComment.Comment = sqlDataReader["MatchComment"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return matchComment;
            }
        }

        /// <summary>
        /// The store match comment.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <param name="matchComment">
        /// The match comment.
        /// </param>
        public void StoreMatchComment(int matchId, string matchComment)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_StoreMatchComment", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("MatchId", matchId));
                    sqlCommand.Parameters.Add(new SqlParameter("MatchComment", matchComment));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(Match domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertMatches", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", domainobject.MatchId));
                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@AwayTeamId", domainobject.AwayTeamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@AwayTeamName", domainobject.AwayTeam));
                    sqlCommand.Parameters.Add(new SqlParameter("@HomeTeamId", domainobject.HomeTeamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@HomeTeamName", domainobject.HomeTeam));
                    sqlCommand.Parameters.Add(new SqlParameter("@MatchDate", domainobject.MatchDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@MatchTime", domainobject.MatchStartTime));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return 1;
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertAll(List<Match> domainobject)
        {
            foreach (var match in domainobject)
            {
                this.InsertOne(match);
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Match domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Match domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Match> GetAll()
        {
            var matches = new List<Match>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetMatches", sqlConnection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (sqlDataReader.Read())
                    {
                        var match = new Match
                                        {
                                            MatchId = (int)sqlDataReader["MatchId"], 
                                            SportId = (int)sqlDataReader["SportId"], 
                                            HomeTeam = sqlDataReader["HomeTeam"].ToString(), 
                                            AwayTeam = sqlDataReader["AwayTeam"].ToString(), 
                                            MatchDate = Convert.ToDateTime(sqlDataReader["Date"].ToString()), 
                                            TournamentId = (int)sqlDataReader["TournamentId"]
                                        };
                        matches.Add(match);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return matches.AsQueryable();
            }
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        public Match Get(int id)
        {
            var match = new Match();

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetMatchById", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("MatchId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        match.MatchId = 0;
                        return match;
                    }

                    while (sqlDataReader.Read())
                    {
                        Logger.InfoFormat("Getting information on match with MatchId: {0}", id);
                        match.MatchId = Convert.ToInt32(sqlDataReader["MatchId"]);
                        match.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        match.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);

                        if (sqlDataReader["UserId"] != DBNull.Value)
                        {
                            match.UserId = new Guid(sqlDataReader["UserId"].ToString());
                        }

                        match.HomeTeam = sqlDataReader["HomeTeamName"].ToString();
                        match.HomeTeamId = Convert.ToInt32(sqlDataReader["AwayTeamId"]);
                        match.AwayTeam = sqlDataReader["AwayTeamName"].ToString();
                        match.AwayTeamId = Convert.ToInt32(sqlDataReader["AwayTeamId"]);
                        match.MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"]);
                        if (sqlDataReader["MatchTime"] != DBNull.Value)
                        {
                            match.MatchStartTime = Convert.ToInt32(sqlDataReader["MatchTime"]);
                        }

                        match.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return match;
            }
        }
    }
}