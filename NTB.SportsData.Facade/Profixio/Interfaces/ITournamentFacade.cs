﻿using System.Collections.Generic;

namespace NTB.SportsData.Facade.Profixio.Interfaces
{
    interface ITournamentFacade
    {
        List<Domain.Classes.Club> GetClubsByTournament(int tournamentId);
        List<Domain.Classes.Tournament> GetTournamentByClubs(List<int> clubIds, int tournamentId);
        List<Domain.Classes.Tournament> GetTournamentsBySeasonId(int seasonId);
    }
}
