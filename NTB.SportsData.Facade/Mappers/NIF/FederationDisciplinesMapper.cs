// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FederationDisciplinesMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The federation disciplines mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The federation disciplines mapper.
    /// </summary>
    public class FederationDisciplinesMapper : BaseMapper<FederationDiscipline, Domain.Classes.FederationDiscipline>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<FederationDiscipline, Domain.Classes.FederationDiscipline> mapper)
        {
            mapper.Relate(x => x.ActivityCode, y => y.ActivityCode);
            mapper.Relate(x => x.ActivityId, y => y.ActivityId);
            mapper.Relate(x => x.ActivityName, y => y.ActivityName);
        }
    }
}