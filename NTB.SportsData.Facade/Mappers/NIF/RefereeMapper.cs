﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RefereeMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The referee mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The referee mapper.
    /// </summary>
    public class RefereeMapper : BaseMapper<Referee, ContactPerson>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Referee, ContactPerson> mapper)
        {
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.RefereeName, y => y.FullName);
            mapper.Relate(x => x.RefereeType, y => y.RoleName);
            mapper.Relate(x => x.OrgId, y => y.TeamId);
            mapper.Relate(x => x.OrgName, y => y.TeamName);
        }
    }
}