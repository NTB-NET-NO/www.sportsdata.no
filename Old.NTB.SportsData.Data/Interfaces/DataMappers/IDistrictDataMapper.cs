﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDistrictDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The DistrictDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The DistrictDataMapper interface.
    /// </summary>
    public interface IDistrictDataMapper
    {
        /// <summary>
        /// The get districts by user id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<District> GetDistrictsByUserId(Guid userId);

        /// <summary>
        /// The insert user district map.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        void InsertUserDistrictMap(Guid userId, int districtId);
    }
}