﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Xml;
using log4net;
using NTB.SportsData.Data.Season;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.NFF.TournamentAccess;
using NTB.SportsData.Web.Areas.Admin.Repositories;

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    /// <summary>
    ///     The customer model.
    /// </summary>
    public class CustomerModel
    {
        #region Public Methods and Operators

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CustomerModel));

        /// <summary>
        /// The get customer.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see cref="NTB.SportsData.Domain.Classes.Customer"/> Customer object.
        /// </returns>
        public static Customer GetCustomer(int customerId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("[SportsData_Customer_Get]", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("CustomerId", customerId));

                sqlConnection.Open();

                var sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                var customer = new Customer();
                while (sqlDataReader.Read())
                {
                    customer.CustomerId = Convert.ToInt32(sqlDataReader["CustomerId"]);
                    customer.Name = sqlDataReader["CustomerName"].ToString();
                    if (sqlDataReader["TtCustomerId"] != DBNull.Value)
                    {
                        customer.TtCustomerId = Convert.ToInt32(sqlDataReader["TtCustomerId"]);
                    }
                }

                return customer;
            }
        }

        /// <summary>
        /// The get customer foreign id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/> Customer Foreign Id.
        /// </returns>
        public int GetCustomerForeignId(int id)
        {
            try
            {
                // We are first to get the jobs for the customer
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
                {
                    var sqlCommand = new SqlCommand("SportsData_GetCustomerForeignId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", id));

                    sqlConnection.Open();

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return 0;
                    }

                    var customerForeignId = 0;
                    while (sqlDataReader.Read())
                    {
                        customerForeignId = Convert.ToInt32(sqlDataReader["CustomerForeignId"]);
                    }

                    return customerForeignId;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public List<CustomerJob> GetCustomerJobs(int customerId)
        {
            var customerJobs = new List<CustomerJob>();
            using (
                var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetCustomerJobs", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {
                        var customerJob = new CustomerJob();
                        customerJob.CustomerId = customerId;
                        if (sqlDataReader["JobId"] != DBNull.Value)
                        {
                            customerJob.JobId = Convert.ToInt32(sqlDataReader["JobId"]);
                        }
                        else
                        {
                            continue;
                        }

                        if (sqlDataReader["SportsId"] != DBNull.Value)
                        {
                            customerJob.SportId = Convert.ToInt32(sqlDataReader["SportsId"]);
                        }
                        else
                        {
                            // I Don't want to add a tournament where I don't have this information...
                            continue;
                        }

                        customerJobs.Add(customerJob);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return customerJobs;
        }

        /// <summary>
        /// The get customer tournaments.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <param name="customerJobs">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetCustomerTournamentsByJobId(List<CustomerJob> customerJobs, int customerId)
        {
            try
            {
                var jobTournaments = new List<Tournament>();

                
                if (customerJobs == null || !customerJobs.Any())
                {
                    return jobTournaments;
                }

                // We are first to get the jobs for the customer
                foreach (var customerJob in customerJobs)
                {
                    using (
                        var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
                    {
                        sqlConnection.Open();


                        // Creating the Tournament And Id object list


                        // Then we are to get the tournament and numbers based on the job id that we got in the previous query

                        // Now we have the jobId, and so we can call the command
                        var sqlCommand = new SqlCommand("SportsData_GetCustomerTournaments", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        sqlCommand.Parameters.Add(new SqlParameter("@JobId", customerJob.JobId));
                        sqlCommand.Parameters.Add(new SqlParameter("@SportId", customerJob.SportId));

                        // sqlConnection.Open();


                        var sqlDataReader = sqlCommand.ExecuteReader();

                        if (!sqlDataReader.HasRows)
                        {
                            Logger.DebugFormat("The job {0} did not return any tournaments", customerJob.JobId);
                            continue;
                        }

                        while (sqlDataReader.Read())
                        {
                            var jobTournament = new Tournament
                            {
                                TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]),
                                TournamentNumber = sqlDataReader["TournamentNumber"].ToString(),
                                TournamentName = sqlDataReader["TournamentName"].ToString()
                            };

                            if (sqlDataReader["DisciplineId"] != DBNull.Value)
                            {
                                jobTournament.DisciplineId = Convert.ToInt32(sqlDataReader["DisciplineId"]);
                            }

                            if (sqlDataReader["SportId"] != DBNull.Value)
                            {
                                jobTournament.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                            }

                            if (sqlDataReader["SeasonId"] != DBNull.Value)
                            {
                                jobTournament.SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]);
                            }

                            if (sqlDataReader["OrgId"] != DBNull.Value)
                            {
                                jobTournament.OrganizationId = Convert.ToInt32(sqlDataReader["OrgId"]);
                            }

                            if (sqlDataReader["OrgName"] != DBNull.Value)
                            {
                                jobTournament.OrganizationName = sqlDataReader["OrgName"].ToString();
                            }

                            if (sqlDataReader["OrgNameShort"] != DBNull.Value)
                            {
                                jobTournament.OrganizationNameShort = sqlDataReader["OrgNameShort"].ToString();
                            }

                            jobTournaments.Add(jobTournament);
                        }
                    }

                    // returning the list of tournament Ids and numbers
                }

                return jobTournaments;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return null;
            }
        }

        /// <summary>
        ///     The get number of customer jobs.
        /// </summary>
        /// <returns>
        ///     The <see cref="NTB.SportsData.Domain.Classes.NumberOfCustomers" />.
        /// </returns>
        public NumberOfCustomers GetNumberOfCustomerJobs()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_GetNumberOfCustomers", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlConnection.Open();

                var sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                while (sqlDataReader.Read())
                {
                    var numberOfCustomers = new NumberOfCustomers
                    {
                        Activated = Convert.ToInt32(sqlDataReader["ActivatedCustomers"]),
                        DeActivated = Convert.ToInt32(sqlDataReader["UnActivatedCustomers"]),
                        NumberOfTournaments = Convert.ToInt32(sqlDataReader["NumberOfTournaments"])
                    };

                    return numberOfCustomers;
                }

                return null;
            }
        }

        public void GetTournamentsForAllCustomers(string formStartDate, string formEndDate)
        {
            var customerRepository = new CustomerRepository();
            var customers = customerRepository.GetAllCustomers();

            if (customers.Any())
            {
                customers = customers.OrderBy(x => x.CustomerId).ToList();
            }
            

            Logger.Info("Start creating maintenance files for the customers");
            foreach (var customer in customers)
            {
                Logger.DebugFormat("Working on {0} with id {1}", customer.Name, customer.CustomerId);
                var maintenanceDocument = new MaintenanceDocument();
                maintenanceDocument.StartDate = DateTime.Parse(formStartDate);
                maintenanceDocument.EndDate = DateTime.Parse(formEndDate);

                var customerJobs = this.GetCustomerJobs(customer.CustomerId);

                var tournaments = this.GetCustomerTournamentsByJobId(customerJobs, customer.CustomerId);
                if (tournaments == null || !tournaments.Any())
                {
                    continue;
                }

                var xmlDocument = new XmlDocument();
                var dec = xmlDocument.CreateXmlDeclaration("1.0", null, null);
                xmlDocument.AppendChild(dec);

                var xmlRoot = xmlDocument.CreateElement("SportsData");
                var xmlRootMeta = xmlDocument.CreateElement("SportsData-Meta");
                xmlRootMeta.SetAttribute("type", "customer");

                // We should consider creating a form where we can decide start and end of getting match data
                xmlRootMeta.SetAttribute("startdate", DateTime.Parse(formStartDate).ToString("dd.MM.yyyy"));
                xmlRootMeta.SetAttribute("enddate", DateTime.Parse(formEndDate).ToString("dd.MM.yyyy"));
                xmlRoot.AppendChild(xmlRootMeta);

                // Creating the Customer Element <Customer id="30"> 
                // Done: Should also have the TT-customerId
                var xmlCustomer = xmlDocument.CreateElement("Customer");
                xmlCustomer.SetAttribute("id", customer.CustomerId.ToString());
                xmlCustomer.SetAttribute("foreigncustomerid", customer.TtCustomerId.ToString());
                xmlRoot.AppendChild(xmlCustomer);

                var xmlJob = xmlDocument.CreateElement("Job");
                xmlJob.SetAttribute("startdate", formStartDate);
                xmlJob.SetAttribute("enddate", formEndDate);
                xmlCustomer.AppendChild(xmlJob);

                var xmlTournaments = xmlDocument.CreateElement("Tournaments");

                foreach (var tournament in tournaments)
                {
                    if (tournament.OrganizationId == null)
                    {
                        var sportRepository = new SportRepository();
                        var sport = sportRepository.Get(tournament.SportId);
                        if (sport.OrgId != null)
                        {
                            tournament.OrganizationId = sport.OrgId.Value;
                        }
                    }

                    var disciplineId = Convert.ToInt32(tournament.DisciplineId);
                    if (tournament.DisciplineId == 0)
                    {
                        if (tournament.OrganizationId != null)
                        {
                            var seasonRemoteRepository = new SeasonRemoteDataMapper();
                            var orgId = tournament.OrganizationId.Value;
                            var remoteSeason = seasonRemoteRepository.GetFederationSeasonByOrgId(orgId);
                            var currentSeason = remoteSeason.Single(x => x.SeasonId == tournament.SeasonId);
                            disciplineId = currentSeason.FederationDiscipline;

                            var seasonRepository = new SeasonRepository();
                            seasonRepository.UpdateSeasonWithDiscipline(currentSeason.SeasonId, disciplineId);
                        }
                    }

                    var xmlTournament = xmlDocument.CreateElement("Tournament");
                    xmlTournament.SetAttribute("id", tournament.TournamentId.ToString());
                    xmlTournament.SetAttribute("sportId", tournament.SportId.ToString());
                    xmlTournament.SetAttribute("disciplineId", disciplineId.ToString());
                    xmlTournaments.AppendChild(xmlTournament);
                }

                xmlCustomer.AppendChild(xmlTournaments);

                xmlDocument.AppendChild(xmlRoot);

                // Getting path from Config-file
                var outputFolder = ConfigurationManager.AppSettings["CustomerTriggerFolder"];

                // Checking that the folder exists (in case I forget when I put the system into production)
                var directoryInfo = new DirectoryInfo(outputFolder);
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                var todayDate = DateTime.Today.ToString("yyyyMMdd");
                // var todayDate = DateTime.Today.Year.ToString().PadLeft(2, '0') + DateTime.Today.Month.ToString().PadLeft(2, '0') + DateTime.Today.Day.ToString().PadLeft(2, '0');

                var todayTime = DateTime.Now.ToString("hhmmss");
                // var todayTime = DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0');
                var fileName = string.Format("NTB_SportsData_{0}_{1}_{2}T{3}.xml", customer.CustomerId, customer.TtCustomerId, todayDate, todayTime);
                // var filename = "NTB_SportsData_" + customerId + "_" + foreignCustomerId + "_" + todayDate + "T" + todayTime + ".xml";

                var outputFileName = Path.Combine(outputFolder, fileName);
                xmlDocument.Save(outputFileName);
                // xmlDocument.Save(outputFolder + @"\" + fileName);

            }
        }

        /// <summary>
        /// The get tournaments for all customers.
        /// </summary>
        /// <param name="formStartDate">
        /// The start Date.
        /// </param>
        /// <param name="formEndDate">
        /// The end Date.
        /// </param>
        public void GetTournamentsForAllCustomers_old(string formStartDate, string formEndDate)
        {
            // This one has been created specifically for soccer. Needs more work.
            var facade = new TournamentFacade();
            var seasons = facade.GetSeasons();

            var customerTournaments = new Dictionary<Tournament, List<Customer>>();

            var startDate = DateTime.Parse(formStartDate);
            var endDate = DateTime.Parse(formEndDate);
            if (seasons.Any())
            {
                var filteredSeasons = from s in seasons
                    where
                        s.SeasonEndDate != null && (s.SeasonStartDate != null && (startDate.Date >= s.SeasonStartDate.Value.Date
                                                                                  && endDate.Date <= s.SeasonEndDate.Value.Date
                                                                                  && s.SeasonEndDate.Value.Year == endDate.Year))
                    select s;

                foreach (var season in filteredSeasons)
                {
                    if (season.SeasonId > 0)
                    {
                        var seasonId = season.SeasonId;
                        /**
                         * Now we need to get seasons for the other sports
                         * int seasonId = (from s in seasons
                         * where s.SeasonStartDate == Convert.ToDateTime(formStartDate) &&
                         * s.SeasonEndDate == Convert.ToDateTime(endDate)
                         * select s.SeasonId).Single();
                         */
                        var tournamentRepository = new TournamentRepository();

                        var tournaments = tournamentRepository.GetTournamentsBySeasonId(seasonId);

                        var customerRepository = new CustomerRepository();
                        foreach (var tournament in tournaments)
                        {
                            var customers = customerRepository.GetCustomersByTournamentId(tournament.TournamentId);
                            tournament.SportId = 16;

                            if (!customers.Any())
                            {
                                continue;
                            }

                            customerTournaments.Add(tournament, customers);
                        }
                    }
                }
            }

            // seasons = null;

            // Now we must get seasons for all the other sports and ... so on.. maybe, right?
            var sportRepository = new SportRepository();
            var sports = sportRepository.GetAll();

            // Only works for team sports...
            foreach (var sport in sports.Where(x => x.HasTeamResults == 1 && x.Id != 16))
            {
                // Now we shall get active season for this sport
                var seasonRepository = new SeasonRepository();
                var sportSeasons = seasonRepository.GetSeasonsBySportId(sport.Id);

                // Making sure we have an active season
                var activeSeasons = sportSeasons.Where(x => x.SeasonActive);

                // now we shall get the tournaments for this season

                // We must loop because of Bandy / Floorball
                foreach (var activeSeason in activeSeasons)
                {
                    var tournamentRepository = new TournamentRepository();

                    // var sportFacade = new SportFacade();
                    // sportFacade.GetTournamentsBySeasonId(activeSeason.SeasonId);

                    // now that we have all the tournaments
                    var tournaments = tournamentRepository.GetTournamentsBySeasonId(activeSeason.SeasonId);

                    var customerRepository = new CustomerRepository();
                    foreach (var tournament in tournaments)
                    {
                        int? disciplineId = activeSeason.FederationDiscipline;

                        tournament.DisciplineId = disciplineId;

                        var customers = customerRepository.GetCustomersByTournamentId(tournament.TournamentId)
                            .Distinct()
                            .ToList();

                        if (!customers.Any())
                        {
                            continue;
                        }

                        customerTournaments.Add(tournament, customers);
                    }
                }
            }

            if (customerTournaments.Count == 0)
            {
                return;
            }

            // Todo: Move this code into view
            var xmlDocument = new XmlDocument();

            var dec = xmlDocument.CreateXmlDeclaration("1.0", null, null);
            xmlDocument.AppendChild(dec);

            var xmlRoot = xmlDocument.CreateElement("SportsData");
            var xmlRootMeta = xmlDocument.CreateElement("SportsData-Meta");
            xmlRootMeta.SetAttribute("type", "Full");
            xmlRootMeta.SetAttribute("startdate", formStartDate);
            xmlRootMeta.SetAttribute("enddate", formEndDate);
            xmlRoot.AppendChild(xmlRootMeta);

            var xmlTournaments = xmlDocument.CreateElement("Tournaments");

            // Getting path from Config-file
            var outputFolder = ConfigurationManager.AppSettings["CustomerTriggerFolder"];

            // Checking that the folder exists (in case I forget when I put the system into production)
            var directoryInfo = new DirectoryInfo(outputFolder);
            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }

            var todayDate = DateTime.Today.Year.ToString()
                .PadLeft(2, '0')
                            + DateTime.Today.Month.ToString()
                                .PadLeft(2, '0')
                            + DateTime.Today.Day.ToString()
                                .PadLeft(2, '0');

            var todayTime = DateTime.Now.Hour.ToString()
                .PadLeft(2, '0')
                            + DateTime.Now.Minute.ToString()
                                .PadLeft(2, '0')
                            + DateTime.Now.Second.ToString()
                                .PadLeft(2, '0');

            // Looping over the dictionary containing TournamentId and the customers subscribing to this tournament
            foreach (var kvp in customerTournaments)
            {
                /*
             * <SportsData>
                <Tournaments>
                    <Tournament id="135193" sportId="4" disciplineId="123">
                        <Customers>
                            <Customer id="35" foreigncustomerid="57518" />
                        </Customers>
                    </Tournament>
                </Tournaments>
            </SportsData>
             */
                var tournamentId = kvp.Key.TournamentId;
                var sportId = kvp.Key.SportId;
                var disciplineId = kvp.Key.DisciplineId;

                var xmlTournament = xmlDocument.CreateElement("Tournament");
                xmlTournament.SetAttribute("id", tournamentId.ToString());
                xmlTournament.SetAttribute("sportId", sportId.ToString());
                xmlTournament.SetAttribute("disciplineId", disciplineId.ToString());

                var xmlCustomers = xmlDocument.CreateElement("Customers");

                foreach (var customer in kvp.Value)
                {
                    var xmlCustomer = xmlDocument.CreateElement("Customer");
                    xmlCustomer.SetAttribute("id", customer.CustomerId.ToString());
                    xmlCustomer.SetAttribute("foreigncustomerid", customer.TtCustomerId.ToString());

                    xmlCustomers.AppendChild(xmlCustomer);
                }

                xmlTournament.AppendChild(xmlCustomers);
                xmlTournaments.AppendChild(xmlTournament);
            }

            xmlRoot.AppendChild(xmlTournaments);

            xmlDocument.AppendChild(xmlRoot);

            var filename = "NTB_SportsData_Complete_" + todayDate + "T" + todayTime + ".xml";

            xmlDocument.Save(outputFolder + @"\" + filename);
        }

        #endregion
    }
}