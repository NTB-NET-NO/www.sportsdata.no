﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using sog.Areas.HelpPage;
using sog.Contexts;
using System.Xml;

namespace NTB.SportsData.UnitTest
{
    [TestClass]
    public class UnitTestMedalsContext
    {
        private string _xmlContent;
        public UnitTestMedalsContext()
        {
            this._xmlContent= @"<?xml version='1.0' encoding='utf-8'?>
<newsMessage>
  <header>
    <sent>2016-08-13T18:13:56+01:00</sent>
    <catalogRef href='http://cv.ntb.no/catalog/ntb-catalog_1.xml' />
    <catalogRef href='http://www.iptc.org/std/catalog/catalog.IPTC-G2-Standards_18.xml' />
    <transmitId>767</transmitId>
    <priority>5</priority>
    <timestamp role='ntbtsrole:released'>2016-08-13T18:13:56+01:00</timestamp>
    <channel>A</channel>
    <destination role='desttype:product' qcode='prod:OlympicSportsresults' />
    <destination role='desttype:main' qcode='adest:ALL' />
  </header>
  <itemSet>
    <newsItem xml:lang='no' standard='NewsML-G2' standardversion='2.18' guid='urn:newsml:ntb.no:20160813T181356' version='767'>
      <catalogRef href='http://cv.ntb.no/catalog/ntb-catalog.xml' />
      <catalogRef href='http://www.iptc.org/std/catalog/catalog.IPTC-G2-Standards_18.xml' />
      <itemMeta>
        <itemClass qcode='ninat:data' />
        <provider>
          <name>NTB</name>
        </provider>
        <versionCreated>2016-08-13T18:13:56+01:00</versionCreated>
        <fileName>PROD_OG2016_GL0000000_DT_MEDALS__P_2016-08-13_131355593_181_767.xml</fileName>
      </itemMeta>
      <contentMeta>
        <urgency>5</urgency>
        <audience qcode='ntbaud:ALL' />
        <creator qcode='ntbjourno:system' role='nojrole:automatic' />
        <slugline separator='-'>-resultatliste</slugline>
        <subject qcode='subj:15000000' literal='Sport' />
        <subject literal='' qcode='' />
      </contentMeta>
      <contentSet>
        <inlineXML contenttype='application/sportsml+xml'>
          <sports-content>
            <sports-metadata doc-id='PROD_OG2016_GL0000000_DT_MEDALS__P_2016-08-13_131355593_181_767.xml'>
              <sports-title>OG2016: Medaljeoversikt</sports-title>
            </sports-metadata>
            <sports-event>
              <event-metadata>
                <sports-content-codes>
                  <sports-content-code code-type='spcode:competition' code-name='Olympiske sommerleker' code-key='ntbsubj:15073001' />
                  <sports-content-code code-type='spcode:distributor' code-name='Norsk Telegrambyrå AS' code-key='publisher:ntb.no' />
                  <sports-content-code code-type='spcode:publisher' code-name='Rio 2016 Olympic Summer Games' code-key='publisher:og2016' />
                  <sports-content-code code-type='spct:event' code-name='SHM202000' code-key='og2016:SHM202000'>
                    <sports-content-qualifier gender='mixed' />
                  </sports-content-code>
                  <sports-content-code code-type='spct:sport' code-name='' code-key='' />
                  <sports-content-code code-type='spct:competition' code-name='OG2016' code-key='odf:OG2016' />
                  <sports-content-code code-type='odf:sport' code-name='' code-key='odf:GL' />
                  <sports-content-code code-type='odf:resultstatus' code-name='' code-key='status:' />
                  <sports-content-code code-type='ntbfacet:ntbcompetitiontype' code-name='' code-key='' />
                </sports-content-codes>
              </event-metadata>
              <highlight>Medaljeoversikt etter 123 av 306 øvelser:</highlight>
              <team id='USA'>
                <team-metadata key='team:USA'>
                  <name role='nrol:full'>United States</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='21' />
                  <award award-type='spaward:medal' value='silver' total='14' />
                  <award award-type='spaward:medal' value='bronze' total='17' />
                  <rank value='1' position='1' />
                </team-stats>
              </team>
              <team id='CHN'>
                <team-metadata key='team:CHN'>
                  <name role='nrol:full'>China</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='13' />
                  <award award-type='spaward:medal' value='silver' total='10' />
                  <award award-type='spaward:medal' value='bronze' total='16' />
                  <rank value='2' position='2' />
                </team-stats>
              </team>
              <team id='GBR'>
                <team-metadata key='team:GBR'>
                  <name role='nrol:full'>Great Britain</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='8' />
                  <award award-type='spaward:medal' value='silver' total='10' />
                  <award award-type='spaward:medal' value='bronze' total='6' />
                  <rank value='3' position='3' />
                </team-stats>
              </team>
              <team id='GER'>
                <team-metadata key='team:GER'>
                  <name role='nrol:full'>Germany</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='8' />
                  <award award-type='spaward:medal' value='silver' total='4' />
                  <award award-type='spaward:medal' value='bronze' total='3' />
                  <rank value='4' position='4' />
                </team-stats>
              </team>
              <team id='JPN'>
                <team-metadata key='team:JPN'>
                  <name role='nrol:full'>Japan</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='7' />
                  <award award-type='spaward:medal' value='silver' total='3' />
                  <award award-type='spaward:medal' value='bronze' total='14' />
                  <rank value='5' position='5' />
                </team-stats>
              </team>
              <team id='AUS'>
                <team-metadata key='team:AUS'>
                  <name role='nrol:full'>Australia</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='6' />
                  <award award-type='spaward:medal' value='silver' total='6' />
                  <award award-type='spaward:medal' value='bronze' total='7' />
                  <rank value='6' position='6' />
                </team-stats>
              </team>
              <team id='KOR'>
                <team-metadata key='team:KOR'>
                  <name role='nrol:full'>Republic of Korea</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='6' />
                  <award award-type='spaward:medal' value='silver' total='3' />
                  <award award-type='spaward:medal' value='bronze' total='4' />
                  <rank value='7' position='7' />
                </team-stats>
              </team>
              <team id='RUS'>
                <team-metadata key='team:RUS'>
                  <name role='nrol:full'>Russian Federation</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='5' />
                  <award award-type='spaward:medal' value='silver' total='9' />
                  <award award-type='spaward:medal' value='bronze' total='8' />
                  <rank value='8' position='8' />
                </team-stats>
              </team>
              <team id='FRA'>
                <team-metadata key='team:FRA'>
                  <name role='nrol:full'>France</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='5' />
                  <award award-type='spaward:medal' value='silver' total='8' />
                  <award award-type='spaward:medal' value='bronze' total='5' />
                  <rank value='9' position='9' />
                </team-stats>
              </team>
              <team id='HUN'>
                <team-metadata key='team:HUN'>
                  <name role='nrol:full'>Hungary</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='5' />
                  <award award-type='spaward:medal' value='silver' total='3' />
                  <award award-type='spaward:medal' value='bronze' total='3' />
                  <rank value='10' position='10' />
                </team-stats>
              </team>
              <team id='ITA'>
                <team-metadata key='team:ITA'>
                  <name role='nrol:full'>Italy</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='4' />
                  <award award-type='spaward:medal' value='silver' total='7' />
                  <award award-type='spaward:medal' value='bronze' total='4' />
                  <rank value='11' position='11' />
                </team-stats>
              </team>
              <team id='ESP'>
                <team-metadata key='team:ESP'>
                  <name role='nrol:full'>Spain</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='3' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='2' />
                  <rank value='12' position='12' />
                </team-stats>
              </team>
              <team id='NZL'>
                <team-metadata key='team:NZL'>
                  <name role='nrol:full'>New Zealand</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='2' />
                  <award award-type='spaward:medal' value='silver' total='6' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='13' position='13' />
                </team-stats>
              </team>
              <team id='CAN'>
                <team-metadata key='team:CAN'>
                  <name role='nrol:full'>Canada</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='2' />
                  <award award-type='spaward:medal' value='silver' total='2' />
                  <award award-type='spaward:medal' value='bronze' total='6' />
                  <rank value='14' position='14' />
                </team-stats>
              </team>
              <team id='KAZ'>
                <team-metadata key='team:KAZ'>
                  <name role='nrol:full'>Kazakhstan</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='2' />
                  <award award-type='spaward:medal' value='silver' total='2' />
                  <award award-type='spaward:medal' value='bronze' total='3' />
                  <rank value='15' position='15' />
                </team-stats>
              </team>
              <team id='NED'>
                <team-metadata key='team:NED'>
                  <name role='nrol:full'>Netherlands</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='2' />
                  <award award-type='spaward:medal' value='silver' total='2' />
                  <award award-type='spaward:medal' value='bronze' total='3' />
                  <rank value='15' position='16' />
                </team-stats>
              </team>
              <team id='THA'>
                <team-metadata key='team:THA'>
                  <name role='nrol:full'>Thailand</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='2' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='17' position='17' />
                </team-stats>
              </team>
              <team id='CRO'>
                <team-metadata key='team:CRO'>
                  <name role='nrol:full'>Croatia</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='2' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='18' position='18' />
                </team-stats>
              </team>
              <team id='SUI'>
                <team-metadata key='team:SUI'>
                  <name role='nrol:full'>Switzerland</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='2' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='19' position='19' />
                </team-stats>
              </team>
              <team id='PRK'>
                <team-metadata key='team:PRK'>
                  <name role='nrol:full'>DPR Korea</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='2' />
                  <award award-type='spaward:medal' value='bronze' total='2' />
                  <rank value='20' position='20' />
                </team-stats>
              </team>
              <team id='SWE'>
                <team-metadata key='team:SWE'>
                  <name role='nrol:full'>Sweden</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='2' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='21' position='21' />
                </team-stats>
              </team>
              <team id='BRA'>
                <team-metadata key='team:BRA'>
                  <name role='nrol:full'>Brazil</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='2' />
                  <rank value='22' position='22' />
                </team-stats>
              </team>
              <team id='POL'>
                <team-metadata key='team:POL'>
                  <name role='nrol:full'>Poland</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='2' />
                  <rank value='22' position='23' />
                </team-stats>
              </team>
              <team id='ROU'>
                <team-metadata key='team:ROU'>
                  <name role='nrol:full'>Romania</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='2' />
                  <rank value='22' position='24' />
                </team-stats>
              </team>
              <team id='BEL'>
                <team-metadata key='team:BEL'>
                  <name role='nrol:full'>Belgium</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='25' position='25' />
                </team-stats>
              </team>
              <team id='SLO'>
                <team-metadata key='team:SLO'>
                  <name role='nrol:full'>Slovenia</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='25' position='26' />
                </team-stats>
              </team>
              <team id='COL'>
                <team-metadata key='team:COL'>
                  <name role='nrol:full'>Colombia</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='27' position='27' />
                </team-stats>
              </team>
              <team id='SVK'>
                <team-metadata key='team:SVK'>
                  <name role='nrol:full'>Slovakia</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='27' position='28' />
                </team-stats>
              </team>
              <team id='VIE'>
                <team-metadata key='team:VIE'>
                  <name role='nrol:full'>Vietnam</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='27' position='29' />
                </team-stats>
              </team>
              <team id='CZE'>
                <team-metadata key='team:CZE'>
                  <name role='nrol:full'>Czech Republic</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='2' />
                  <rank value='30' position='30' />
                </team-stats>
              </team>
              <team id='TPE'>
                <team-metadata key='team:TPE'>
                  <name role='nrol:full'>Chinese Taipei</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='2' />
                  <rank value='30' position='31' />
                </team-stats>
              </team>
              <team id='ETH'>
                <team-metadata key='team:ETH'>
                  <name role='nrol:full'>Ethiopia</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='32' position='32' />
                </team-stats>
              </team>
              <team id='GRE'>
                <team-metadata key='team:GRE'>
                  <name role='nrol:full'>Greece</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='32' position='33' />
                </team-stats>
              </team>
              <team id='ARG'>
                <team-metadata key='team:ARG'>
                  <name role='nrol:full'>Argentina</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='34' position='34' />
                </team-stats>
              </team>
              <team id='FIJ'>
                <team-metadata key='team:FIJ'>
                  <name role='nrol:full'>Fiji</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='34' position='35' />
                </team-stats>
              </team>
              <team id='IOA'>
                <team-metadata key='team:IOA'>
                  <name role='nrol:full'>IOA</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='34' position='36' />
                </team-stats>
              </team>
              <team id='IRI'>
                <team-metadata key='team:IRI'>
                  <name role='nrol:full'>IR Iran</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='34' position='37' />
                </team-stats>
              </team>
              <team id='KOS'>
                <team-metadata key='team:KOS'>
                  <name role='nrol:full'>Kosovo</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='34' position='38' />
                </team-stats>
              </team>
              <team id='SIN'>
                <team-metadata key='team:SIN'>
                  <name role='nrol:full'>Singapore</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='1' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='34' position='39' />
                </team-stats>
              </team>
              <team id='RSA'>
                <team-metadata key='team:RSA'>
                  <name role='nrol:full'>South Africa</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='4' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='40' position='40' />
                </team-stats>
              </team>
              <team id='DEN'>
                <team-metadata key='team:DEN'>
                  <name role='nrol:full'>Denmark</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='2' />
                  <award award-type='spaward:medal' value='bronze' total='2' />
                  <rank value='41' position='41' />
                </team-stats>
              </team>
              <team id='UKR'>
                <team-metadata key='team:UKR'>
                  <name role='nrol:full'>Ukraine</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='2' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='42' position='42' />
                </team-stats>
              </team>
              <team id='AZE'>
                <team-metadata key='team:AZE'>
                  <name role='nrol:full'>Azerbaijan</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='2' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='43' position='43' />
                </team-stats>
              </team>
              <team id='INA'>
                <team-metadata key='team:INA'>
                  <name role='nrol:full'>Indonesia</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='2' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='43' position='44' />
                </team-stats>
              </team>
              <team id='CUB'>
                <team-metadata key='team:CUB'>
                  <name role='nrol:full'>Cuba</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='45' position='45' />
                </team-stats>
              </team>
              <team id='GEO'>
                <team-metadata key='team:GEO'>
                  <name role='nrol:full'>Georgia</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='45' position='46' />
                </team-stats>
              </team>
              <team id='LTU'>
                <team-metadata key='team:LTU'>
                  <name role='nrol:full'>Lithuania</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='45' position='47' />
                </team-stats>
              </team>
              <team id='BLR'>
                <team-metadata key='team:BLR'>
                  <name role='nrol:full'>Belarus</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='48' position='48' />
                </team-stats>
              </team>
              <team id='IRL'>
                <team-metadata key='team:IRL'>
                  <name role='nrol:full'>Ireland</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='48' position='49' />
                </team-stats>
              </team>
              <team id='KEN'>
                <team-metadata key='team:KEN'>
                  <name role='nrol:full'>Kenya</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='48' position='50' />
                </team-stats>
              </team>
              <team id='MAS'>
                <team-metadata key='team:MAS'>
                  <name role='nrol:full'>Malaysia</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='48' position='51' />
                </team-stats>
              </team>
              <team id='MGL'>
                <team-metadata key='team:MGL'>
                  <name role='nrol:full'>Mongolia</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='48' position='52' />
                </team-stats>
              </team>
              <team id='PHI'>
                <team-metadata key='team:PHI'>
                  <name role='nrol:full'>Philippines</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='48' position='53' />
                </team-stats>
              </team>
              <team id='TUR'>
                <team-metadata key='team:TUR'>
                  <name role='nrol:full'>Turkey</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='1' />
                  <award award-type='spaward:medal' value='bronze' total='0' />
                  <rank value='48' position='54' />
                </team-stats>
              </team>
              <team id='UZB'>
                <team-metadata key='team:UZB'>
                  <name role='nrol:full'>Uzbekistan</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='3' />
                  <rank value='55' position='55' />
                </team-stats>
              </team>
              <team id='EGY'>
                <team-metadata key='team:EGY'>
                  <name role='nrol:full'>Egypt</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='2' />
                  <rank value='56' position='56' />
                </team-stats>
              </team>
              <team id='ISR'>
                <team-metadata key='team:ISR'>
                  <name role='nrol:full'>Israel</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='2' />
                  <rank value='56' position='57' />
                </team-stats>
              </team>
              <team id='NOR'>
                <team-metadata key='team:NOR'>
                  <name role='nrol:full'>Norway</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='2' />
                  <rank value='56' position='58' />
                </team-stats>
              </team>
              <team id='EST'>
                <team-metadata key='team:EST'>
                  <name role='nrol:full'>Estonia</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='59' position='59' />
                </team-stats>
              </team>
              <team id='KGZ'>
                <team-metadata key='team:KGZ'>
                  <name role='nrol:full'>Kyrgyzstan</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='59' position='60' />
                </team-stats>
              </team>
              <team id='POR'>
                <team-metadata key='team:POR'>
                  <name role='nrol:full'>Portugal</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='59' position='61' />
                </team-stats>
              </team>
              <team id='TUN'>
                <team-metadata key='team:TUN'>
                  <name role='nrol:full'>Tunisia</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='59' position='62' />
                </team-stats>
              </team>
              <team id='UAE'>
                <team-metadata key='team:UAE'>
                  <name role='nrol:full'>United Arab Emirates</name>
                </team-metadata>
                <team-stats>
                  <award award-type='spaward:medal' value='gold' total='0' />
                  <award award-type='spaward:medal' value='silver' total='0' />
                  <award award-type='spaward:medal' value='bronze' total='1' />
                  <rank value='59' position='63' />
                </team-stats>
              </team>
            </sports-event>
          </sports-content>
        </inlineXML>
      </contentSet>
    </newsItem>
  </itemSet>
</newsMessage>";
        }
        [TestMethod]
        public void GetLatestFile()
        {
            const string pathToTest = @"C:\Visual Studio Projects\Websites\www.sportsdata.no\FileStore\Medals";

            var context = new MedalContext();

            var actual = context.GetLatestFile(pathToTest).Name;

            const string expected = @"PROD_OG2016_GL0000000_DT_MEDALS__P_2016-08-13_175740634_188_1177.xml";
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetSportsContent()
        {            
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(this._xmlContent);

            var context = new MedalContext();
            var actual = context.GetSportsContent(xmlDoc).SportsMetaData.DocId;

            const string expected = "PROD_OG2016_GL0000000_DT_MEDALS__P_2016-08-13_131355593_181_767.xml";

            Assert.AreEqual(expected, actual);
        }
    }
}
