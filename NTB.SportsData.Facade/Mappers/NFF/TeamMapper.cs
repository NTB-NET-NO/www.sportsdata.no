// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The team mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NFF
{
    using Glue;

    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The team mapper.
    /// </summary>
    public class TeamMapper : BaseMapper<Team, Domain.Classes.Team>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Team, Domain.Classes.Team> mapper)
        {
            mapper.Relate(x => x.TeamId, y => y.TeamId);
            mapper.Relate(x => x.TeamName, y => y.TeamName);
            mapper.Relate(x => x.TeamNameInTournament, y => y.TeamNameInTournament);
            mapper.Relate(x => x.AgeCategoryId, y => y.AgeCategoryId);
            mapper.Relate(x => x.ClubId, y => y.ClubId);
            mapper.Relate(x => x.GenderId, y => y.GenderId);
            mapper.Relate(x => x.Homepage, y => y.Homepage);
        }
    }
}