﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SimlePersonMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The simle person mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The simle person mapper.
    /// </summary>
    public class SimlePersonMapper : BaseMapper<SimplePerson, ContactPerson>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<SimplePerson, ContactPerson> mapper)
        {
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.LastName, y => y.LastName);
            mapper.Relate(x => x.ContactInfo.PhoneHome, y => y.HomePhone);
            mapper.Relate(x => x.ContactInfo.PhoneMobile, y => y.MobilePhone);
            mapper.Relate(x => x.ContactInfo.PhoneWork, y => y.OfficePhone);
        }
    }
}