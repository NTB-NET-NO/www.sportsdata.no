﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClubMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The club mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NFF
{
    using Glue;

    using NTB.SportsData.Common;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The club mapper.
    /// </summary>
    public class ClubMapper : BaseMapper<Club, Domain.Classes.Club>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Club, Domain.Classes.Club> mapper)
        {
            mapper.Relate(x => x.ClubId, y => y.ClubId);
            mapper.Relate(x => x.ClubName, y => y.ClubName);
            mapper.Relate(x => x.DistrictId, y => y.DistrictId);
            mapper.Relate(x => x.MunicipalityId, y => y.MunicipalityId);
            mapper.Relate(x => x.ClubLogo, y => y.ClubLogo);
            mapper.Relate(x => x.DateFounded, y => y.DateFounded);
            mapper.Relate(x => x.Homepage, y => y.Homepage);
            mapper.Relate(x => x.JerseyColor, y => y.JerseyColor);
            mapper.Relate(x => x.Address, y => y.Address);
            mapper.Relate(x => x.City, y => y.City);
            mapper.Relate(x => x.PostalCode, y => y.PostalCode);
        }
    }
}