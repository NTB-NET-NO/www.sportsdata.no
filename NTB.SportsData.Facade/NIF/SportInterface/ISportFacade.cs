﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISportFacade.cs" company="NTB">
//   Copyright Norsk telegrambyrå
// </copyright>
// <summary>
//   Defines the ISportFacade type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Facade.NIF.SportInterface
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The SportFacade interface.
    /// </summary>
    internal interface ISportFacade
    {
        /// <summary>
        /// The get sports.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<FederationDiscipline> GetSports();

        /// <summary>
        /// The get sport.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="FederationDiscipline"/>.
        /// </returns>
        FederationDiscipline GetSport(int id);

        /// <summary>
        /// The get federations.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Federation> GetFederations();

        /// <summary>
        /// The get federation.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Federation"/>.
        /// </returns>
        Federation GetFederation(int id);

        /// <summary>
        /// The get federation by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="Federation"/>.
        /// </returns>
        Federation GetFederationByOrgId(int orgId);

        /// <summary>
        /// The get federation disciplines.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<FederationDiscipline> GetFederationDisciplines(int orgId);

        /// <summary>
        /// The get federation discipline.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="FederationDiscipline"/>.
        /// </returns>
        FederationDiscipline GetFederationDiscipline(int id);

        /// <summary>
        /// The get districts.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<District> GetDistricts();

        /// <summary>
        /// The get municipalities.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Municipality> GetMunicipalities();

        /// <summary>
        /// The get age categories.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<AgeCategory> GetAgeCategories(int orgId);

        /// <summary>
        /// The get tournament by municipalities.
        /// </summary>
        /// <param name="municipalities">
        /// The municipalities.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId, int orgId);

        /// <summary>
        /// The get class codes.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<AgeCategory> GetClassCodes(int orgId);


        List<AgeCategory> GetClassCodesByTournamentId(int tournamentId);
            /// <summary>
        /// The get federation seasons.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Season> GetFederationSeasons(int orgId);

        /// <summary>
        /// The get matches by tournament id and sport id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Match> GetMatchesByTournamentIdAndSportId(int tournamentId, int sportId);

        /// <summary>
        /// The get tournaments by season id.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Tournament> GetTournamentsBySeasonId(int seasonId);

        /// <summary>
        /// The get tournament by id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        Tournament GetTournamentById(int tournamentId, int seasonId);

        /// <summary>
        /// The get todays matches.
        /// </summary>
        /// <param name="matchDate">
        /// The match date.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Match> GetTodaysMatches(DateTime matchDate, int seasonId);

        /// <summary>
        /// The get todays events.
        /// </summary>
        /// <param name="eventDate">
        /// The event date.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<SportEvent> GetTodaysEvents(DateTime eventDate, int orgId);

        bool InsertMatchResult(Match localMatch, int federationUserId);

        bool UpdateMatchResult(Match localMatch, int federationUserId);

        /// <summary>
        ///     Gets the list of contact persons by match id
        /// </summary>
        /// <param name="matchId">the Id of the match</param>
        /// <returns>List of perosns</returns>
        List<ContactPerson> GetContactPersonsByMatchId(int matchId);

        /// <summary>
        ///     Gets the list of contact persons by team id
        /// </summary>
        /// <param name="teamId">the Id of the team/club/organization</param>
        /// <returns>List of persons</returns>
        List<ContactPerson> GetContactPersonsByTeamId(int teamId);

        /// <summary>
        ///     Gets the list of referees based on match Id
        /// </summary>
        /// <param name="matchId">Id of the match</param>
        /// <returns>List of contact persons</returns>
        List<ContactPerson> GetRefereesByMatchId(int matchId);
    }
}