﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace NTB.SportsData.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // Url with parameters
                new
                {
                    controller = "Home", action = "Index", id = UrlParameter.Optional
                },
                new string[] { "NTB.SportsData.Web.Controllers" });

            routes.MapRoute("API Default", "Api/{controller}/{action}/{id}", defaults: new { id = UrlParameter.Optional });
        }
    }
}
