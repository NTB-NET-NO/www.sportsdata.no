﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegisterDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The register data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NFF.Repositories
{
    using System;
    using System.Configuration;

    using log4net;

    using NTB.SportsData.Services.NFF.Interfaces;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The register data mapper.
    /// </summary>
    public class RegisterDataMapper : IRegisterDataMapper
    {
        /// <summary>
        ///     The Logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(RegisterDataMapper));

        /// <summary>
        ///     The result service client
        /// </summary>
        private readonly ResultServiceClient resultClient = new ResultServiceClient();

        /// <summary>
        /// The insert match result.
        /// We are to insert the match result based on information in the Match Object
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool InsertMatchResult(Match match)
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Testing"]))
            {
                // Waiting 10 seconds just so something could happen
                System.Threading.Thread.Sleep(10000);

                // We are not inserting anything in the external database, just returning true so the rest of the code can continue.
                return true;
            }

            // Username used to register match results
            var regUserName = ConfigurationManager.AppSettings["NFFRegisterUsername"];

            // Password used to register match results
            var regPassWord = ConfigurationManager.AppSettings["NFFRegisterPassword"];

            Logger.InfoFormat("Registering match result for match {0} with following score {1} - {2}", match.MatchId, match.HomeTeamGoals, match.AwayTeamGoals);
            
            // Creating the result object containing the information needed to store this match
            var result = new MatchResultForReporting
            {
                MatchId = match.MatchId,
                MatchNumber = match.MatchNumber,
                HomeTeamGoals = Convert.ToInt32(match.HomeTeamGoals),
                AwayTeamGoals = Convert.ToInt32(match.AwayTeamGoals),
                ResultTypeId = 1,
                ReporterPhoneNumber = "NTB Sportsdata"
            };
            try
            {

                // Running the query against the NFF API
                if (this.resultClient.ClientCredentials != null)
                {
                    this.resultClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                    this.resultClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                }

                var response = this.resultClient.SaveResultAuthenticated(regUserName, regPassWord, result);

                Logger.DebugFormat("Response is: {0}", response);
                
                Logger.InfoFormat(
                    "Result for match {0} with following score {1} - {2} registered successfully",
                    match.MatchId,
                    match.HomeTeamGoals,
                    match.AwayTeamGoals);
            }
            catch (Exception exception)
            {
                // If we get an exception we end up here.
                Logger.Error(exception);

                // Returning false
                return false;
            }
            finally
            {
                // Closing the connection
                this.resultClient.Close();
            }

            // Returns true because we will end up here if we did everything correct.
            return true;
            
        }

    }
}