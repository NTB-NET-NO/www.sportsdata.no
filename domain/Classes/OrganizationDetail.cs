﻿using System.Collections.Generic;

namespace NTB.SportsData.Domain.Classes
{
    public class OrganizationDetail
    {
        /// <summary>
        ///     Gets or sets organization
        /// </summary>
        public Organization Org { get; set; }
    }
}