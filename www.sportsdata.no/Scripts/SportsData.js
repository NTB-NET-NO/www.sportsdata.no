﻿
$(document).ready(function () {
    $(".passivejobsList").hide();
    $(".changelink").click(function () {

        var changeId = $(this).attr("id");
        var newId = changeId.replace("changejob", "");

        var formRowId = $("#FormRowId" + newId).show('slow');

        return false;
    });

    // Check / uncheck municipalities
    $(".checkMunicipalitiesDataAccess").click(function () {
        var checkboxes = $(this).attr("rel");
        var checked = $("." + checkboxes).attr('checked');

        $("." + checkboxes).attr('checked', !checked);
        if ($(this).text() == "Kryss av alle") {
            $(this).text("Avkryss alle");
        } else {
            $(this).text("Kryss av alle");
        }
        return false;

    });

    //$('#velgAlleTurneringer').click(function () {
    //    //var checked = $('.selTournaments').attr('checked');
    //    //$('.selTournaments').attr('checked', !checked);

    //    $("input:checkbox.selTournaments").prop('checked', $(this).prop("checked"));
    //    // $('.selTournaments').not(this).prop('checked', this.checked);

    //    return false;
    //});

    $(".deletelink").click(function () {

        var $link = $(this);

        var thisid = "";
        thisid = this.rel;
        console.log("thisid: " + thisid);
        console.log($link.attr("href"));
        console.log($link.attr("title"));

        $("#" + thisid).dialog({
            resizable: false,
            height: 200,
            modal: true,
            buttons: {
                'Delete ': function () {
                    location.href = $link.attr('href');
                    $(this).dialog('close');
                },
                Cancel: function () {
                    $(this).dialog('close');
                }
            }
        });
        console.log("Done!");
        return false;
    });

    $(".activatelink").click(function () {

        var $link = $(this);

        var thisid = "";
        thisid = this.rel;
        console.log("thisid: " + thisid);
        console.log($link.attr("href"));
        console.log($link.attr("title"));

        $("#" + thisid).dialog({
            resizable: false,
            height: 200,
            modal: true,
            buttons: {
                'Aktiver ': function () {
                    location.href = $link.attr('href');
                    $(this).dialog('close');
                },
                Cancel: function () {
                    $(this).dialog('close');
                }
            }
        });
        console.log("Done!");
        return false;
    });

    $("#hidepassive").click(function () {
        $(".passivejobsList").hide('Slow');

        return false;
    });

    $("#selectedTournaments").visible = true;


    $("a.hideAll").click(function () {
        if ($(this).text() == 'Skjul alle') {
            $("div.KommuneListe").slideUp();
            $(this).text("Vis alle");
        } else {
            $("div.KommuneListe").show("slow");
            $(this).text("Skjul alle");
        }

        return false;

    });

    $(".matchinsertform").submit(function () {
        console.write(matchId);
        return false;
    });

    $(document).on("change", "select#seasonSelect", function () {
        var value = $(this).val();
        console.log("The value is: " + value);

        // Now we need to get the tournaments from the database/remote source and list them
        // That data will be fetched with ajax
        $.ajax({
                method: "GET",
                url: "/Admin/Organization/ListTournamentsBySeasonsId/" + value,
                dataType: "html"
            })
            .done(function(data) {
                $("#seasonSelectResult").html(data);
            
            //success: function(data) {

            //    var myObject = data.parse;
            //    console.log(myObject);
            //}
        });
    });
    
});
