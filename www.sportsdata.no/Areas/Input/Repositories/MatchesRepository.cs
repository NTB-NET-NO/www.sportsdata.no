// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchesRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The matches repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Web.Areas.Input.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Web.Areas.Input.Interfaces;

    /// <summary>
    /// The matches repository.
    /// </summary>
    public class MatchesRepository : IMatchDataMapper
    {
        /// <summary>
        /// The insert match result.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        public void InsertMatchResult(Match match)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsDataInput_InsertMatchResult", sqlConnection)
                                     {
                                         CommandType =
                                             CommandType
                                             .StoredProcedure
                                     };

                sqlCommand.Parameters.Add(new SqlParameter("@MatchId", match.MatchId));
                sqlCommand.Parameters.Add(new SqlParameter("@HomeTeamGoal", match.HomeTeamGoal));
                sqlCommand.Parameters.Add(new SqlParameter("@AwayTeamGoal", match.AwayTeamGoal));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

            }
        }
    }
}