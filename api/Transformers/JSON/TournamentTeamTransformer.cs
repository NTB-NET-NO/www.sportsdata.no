﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentTeamTransformer.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Transformers.JSON
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Xsl;

    using NTB.SportsData.Api.Interfaces;

    /// <summary>
    ///     The tournament table transformer.
    /// </summary>
    public class TournamentTeamTransformer : ITransformer
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the xslt file.
        /// </summary>
        public string XsltFile { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The transform object.
        /// </summary>
        /// <param name="document">
        /// The document.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Throws an exception if Xslt File has not been set
        /// </exception>
        public string TransformObject(string document)
        {
            if (string.IsNullOrEmpty(this.XsltFile))
            {
                throw new Exception("No XsltFile defined. Cannot continue");
            }

            var transformer = new XslCompiledTransform();
            string xsltPath = ConfigurationManager.AppSettings["sportsmlxsltfilepath"];
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(document);

            transformer.Load(xsltPath + @"\" + this.XsltFile);

            var settings = new XmlWriterSettings
                               {
                                   Indent = true, 
                                   IndentChars = "\t", 
                                   Encoding = Encoding.UTF8, 
                                   OmitXmlDeclaration = false
                               };

            using (var textWriter = new StringWriter())
            {
                var writer = new XmlTextWriter(textWriter);

                transformer.Transform(xmlDoc, null, writer);

                return textWriter.ToString();
            }
        }

        #endregion
    }
}