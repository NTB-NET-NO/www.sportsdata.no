﻿using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Interfaces
{
    interface IFederationDataMapper
    {
        Federation GetFederationBySportId(int sportId);
    }
}
