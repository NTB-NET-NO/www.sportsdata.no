using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Data.Interfaces.Common;
using NTB.SportsData.Data.Interfaces.DataMappers;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.DataMappers.Seasons
{
    /// <summary>
    /// The season data mapper.
    /// </summary>
    public class SeasonDataMapper : IRepository<Domain.Classes.Season>, IDisposable, ISeasonDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SeasonDataMapper));

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(Domain.Classes.Season domainobject)
        {
            Logger.Info("About to insert " + domainobject.SeasonName);
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand("SportsData_InsertSeason", sqlConnection);

                    if (domainobject.SeasonStartDate < DateTime.Parse("1/1/1753"))
                    {
                        domainobject.SeasonStartDate = DateTime.Today;
                    }

                    if (domainobject.SeasonEndDate < DateTime.Parse("1/1/1753"))
                    {
                        domainobject.SeasonEndDate = DateTime.Today;
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonName", domainobject.SeasonName));
                    sqlCommand.Parameters.Add(new SqlParameter("@StartDate", domainobject.SeasonStartDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@EndDate", domainobject.SeasonEndDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonActive", domainobject.SeasonActive));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Parameters.Clear();

                    sqlCommand.CommandText = "SELECT @@IDENTITY";
                    sqlCommand.CommandType = CommandType.Text;

                    // int insertId = Convert.ToInt32(sqlCommand.ExecuteScalar());
                    sqlCommand.Dispose();

                    Logger.Info(domainobject.SeasonName + " inserted successfully");

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlCommand.Connection.Close();
                    }

                    return 1;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return 0;
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertAll(List<Domain.Classes.Season> domainobject)
        {
            foreach (var season in domainobject)
            {
                this.InsertOne(season);
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void Update(Domain.Classes.Season domainobject)
        {
            Logger.Info("About to update " + domainobject.SeasonName);
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand("SportsData_UpdateSeason", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonName", domainobject.SeasonName));
                    sqlCommand.Parameters.Add(new SqlParameter("@StartDate", domainobject.SeasonStartDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@EndDate", domainobject.SeasonEndDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonActive", domainobject.SeasonActive.ToString()));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    Logger.Info(domainobject.SeasonName + " updated successfully");

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlCommand.Connection.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }

        /// <summary>
        /// Delete one season from databsae
        /// </summary>
        /// <param name="domainobject">
        /// Domain object containing at least the season id
        /// </param>
        public void Delete(Domain.Classes.Season domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"SportsData_DeleteSeasonBySeasonId", sqlConnection)
                {
                    CommandType =
                        CommandType
                            .StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        /// Get all seasons from the database
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Domain.Classes.Season> GetAll()
        {
            var seasons = new List<Domain.Classes.Season>();

            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"SportsData_GetSeasons", sqlConnection)
                {
                    CommandType =
                        CommandType
                            .StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", DBNull.Value));

                sqlConnection.Open();

                using (var sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["SportId"] == DBNull.Value)
                        {
                            continue;
                        }

                        var season = new Domain.Classes.Season
                        {
                            SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]),
                            SportId = Convert.ToInt32(sqlDataReader["SportId"]),
                            SeasonName = sqlDataReader["SeasonName"].ToString(),
                            SeasonStartDate = Convert.ToDateTime(sqlDataReader["SeasonStartDate"]),
                            SeasonEndDate = Convert.ToDateTime(sqlDataReader["SeasonEndDate"]),
                            SeasonActive = true
                        };

                        if (sqlDataReader["DisciplineId"] != DBNull.Value)
                        {
                            season.FederationDiscipline = Convert.ToInt32(sqlDataReader["DisciplineId"]);
                        }

                        var seasonActive = sqlDataReader["SeasonActive"].ToString()
                            .ToLower();
                        if (seasonActive == "false" || seasonActive == "0")
                        {
                            season.SeasonActive = false;
                        }

                        if (season.SportId == 4)
                        {
                            // Just adding a breakpoint
                            // var foo = 12;
                        }

                        seasons.Add(season);
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();
            }

            Logger.InfoFormat("Dataset contains {0} items", seasons.Count);

            return seasons.AsQueryable();
        }

        /// <summary>
        /// Returns one season from the database
        /// </summary>
        /// <param name="id">
        /// </param>
        /// <returns>
        /// The <see cref="Season"/>.
        /// </returns>
        public Domain.Classes.Season Get(int id)
        {
            try
            {
                Logger.DebugFormat("We are trying to find season with season id {0}", id);
                var seasons = this.GetAll();
                foreach (var season in seasons)
                {
                    Logger.DebugFormat("Season {0} with id {1}", season.SeasonName, season.SeasonId);

                    if (season.SeasonId == id)
                    {
                        Logger.Debug("Found!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        return season;
                    }
                }

                // return GetAll().Single(s => s.SeasonId == id);
                return new Domain.Classes.Season();
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return new Domain.Classes.Season();
            }
        }

        /// <summary>
        /// The get seasons by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Domain.Classes.Season> GetSeasonsBySportId(int sportId)
        {
            var seasons = new List<Domain.Classes.Season>();

            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"SportsData_GetSeasons", sqlConnection)
                {
                    CommandType =
                        CommandType
                            .StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                sqlConnection.Open();

                using (var sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        var season = new Domain.Classes.Season
                        {
                            SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]),
                            SportId = Convert.ToInt32(sqlDataReader["SportId"]),
                            SeasonName = sqlDataReader["SeasonName"].ToString(),
                            SeasonActive = true,
                            SeasonStartDate =
                                Convert.ToDateTime(sqlDataReader["SeasonStartDate"].ToString()),
                            SeasonEndDate =
                                Convert.ToDateTime(sqlDataReader["SeasonEndDate"].ToString()),
                            FederationDiscipline =
                                sqlDataReader["DisciplineId"] == DBNull.Value
                                    ? 0
                                    : Convert.ToInt32(sqlDataReader["DisciplineId"])
                        };

                        // season.FederationDiscipline = sqlDataReader["DisciplineId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DisciplineId"]);
                        var seasonactive = sqlDataReader["SeasonActive"].ToString()
                            .ToLower();
                        if (seasonactive == "false" || seasonactive == "0")
                        {
                            season.SeasonActive = false;
                        }

                        seasons.Add(season);
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();
            }

            return seasons;
        }

        /// <summary>
        /// The activate season.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        public void ActivateSeason(int seasonId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"SportsData_ActivateSeasonBySeasonId", sqlConnection)
                {
                    CommandType =
                        CommandType
                            .StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", seasonId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        /// The de activate season.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        public void DeActivateSeason(int seasonId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"SportsData_DeActivateSeasonBySeasonId", sqlConnection)
                {
                    CommandType
                        =
                        CommandType
                            .StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", seasonId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        /// The update season with discipline.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="disciplineId">
        /// The discipline id.
        /// </param>
        public void UpdateSeasonWithDiscipline(int seasonId, int disciplineId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand(@"SportsData_UpdateSeasonDiscipline", sqlConnection)
                    {
                        CommandType
                            =
                            CommandType
                                .StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", seasonId));

                    sqlCommand.Parameters.Add(new SqlParameter("@DisciplineId", disciplineId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}