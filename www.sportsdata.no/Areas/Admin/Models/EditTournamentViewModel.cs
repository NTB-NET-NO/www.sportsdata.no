﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    public class EditTournamentViewModel
    {
        public int TournamentId { get; set; }

        public string TournamentName { get; set; }

        public List<Sport> Sports { get; set; }

        public List<Gender> Genders { get; set; }

        public List<AgeCategoryDefinition> AgeCategoryDefinitions { get; set; }

        public List<TournamentType> TournamentTypes { get; set; }

        public int SelectedSport { get; set; }

        public int SelectedAgeCategoryDefinition { get; set; }

        public int OrganisationId { get; set; }

        public int SelectedGender { get; set; }

        public int HiddenSeasonId { get; set; }

        public int HiddenAgeCategoryId { get; set; }

        public int HiddenTournamentTypeId { get; set; }

        public string TournamentNumber { get; set; }
    }
}