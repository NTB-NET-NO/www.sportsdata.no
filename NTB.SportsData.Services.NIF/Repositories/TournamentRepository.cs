﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Threading;
using NTB.SportsData.Services.NIF.Exceptions;

namespace NTB.SportsData.Services.NIF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Threading.Tasks;

    using log4net;

    using Interfaces;
    using NIFProdService;
    using Utilities;

    /// <summary>
    ///     The tournament repository.
    /// </summary>
    public class TournamentRepository : IRepository<Tournament>, IDisposable, ITournamentDataMapper
    {
        #region Static Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(TournamentRepository));

        #endregion

        #region Fields

        /// <summary>
        ///     The organization service client.
        /// </summary>
        private readonly OrgServiceClient _orgServiceClient = new OrgServiceClient();

        /// <summary>
        ///     The tournament service client.
        /// </summary>
        private readonly TournamentServiceClient _tournamentServiceClient = new TournamentServiceClient();

        /// <summary>
        ///     The tournament service client.
        /// </summary>
        private readonly SeasonServiceClient _seasonServiceClient = new SeasonServiceClient();

        /// <summary>
        /// The team service client.
        /// </summary>
        private readonly TeamServiceClient _teamServiceClient = new TeamServiceClient();

        /// <summary>
        /// The lock me.
        /// </summary>
        private readonly object _lockMe = new object();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="TournamentRepository" /> class.
        /// </summary>
        public TournamentRepository()
        {
            if (this._tournamentServiceClient.ClientCredentials != null)
            {
                this._tournamentServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
                this._tournamentServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];
            }

            if (this._orgServiceClient.ClientCredentials == null)
            {
                return;
            }

            this._orgServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            this._orgServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            if (this._seasonServiceClient.ClientCredentials == null)
            {
                return;
            }

            this._seasonServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            this._seasonServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            if (this._teamServiceClient.ClientCredentials == null)
            {
                return;
            }

            this._teamServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            this._teamServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        ///     Not implemented
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public Tournament Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get all.
        /// </summary>
        /// <returns>
        ///     The <see cref="IQueryable" />.
        /// </returns>
        /// <exception cref="NotImplementedException">
        ///     Not implemented
        /// </exception>
        public IQueryable<Tournament> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get tournament by id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Throws an exception if we get an error message from the API
        /// </exception>
        public Tournament GetTournamentById(int tournamentId, int seasonId)
        {
            try
            {
                var request = new SeasonIdRequest(seasonId);
                var response = this._seasonServiceClient.GetSeasonTournaments(request);

                if (!response.Success)
                {
                    throw new Exception(string.Format("We've received an error from the NIF API: {0}, code: {1}", response.ErrorMessage, response.ErrorCode));
                }

                var tournament = (from t in response.Tournaments where t.TournamentId == tournamentId select t).SingleOrDefault();
                return tournament;
            }
            catch (Exception exception)
            {
                Log.Error(exception.Message);
                Log.Error(exception.StackTrace);

                return new Tournament();
            }
        }

        /// <summary>
        /// A method to return the tournaments based on municipalities
        /// </summary>
        /// <param name="municipalities">
        /// string of ids
        /// </param>
        /// <param name="seasonId">
        /// season id
        /// </param>
        /// <param name="orgId">
        /// federation id
        /// </param>
        /// <returns>
        /// list of tournaments
        /// </returns>
        public List<Tournament> DeprecatedGetTournamentByMunicipalities(List<string> municipalities, int seasonId, int orgId)
        {
            // todo: Consider using first team based on municipalities, then find out in which tournament this team is involved in
            var distinctTournaments = new List<Tournament>();

            foreach (var municipality in municipalities)
            {
                Log.Debug("Municipality: " + municipality);

                var tournamentRegionsSearchParams = new TournamentRegionsSearchParams
                                                        {
                                                            LocalCouncilIds = municipality, 
                                                            SeasonId = seasonId

                                                            // FederationId = orgId // MArked out because it is not needed - causes problems when another Organization has entered data
                                                        };
                var tournamentRegionsSearchRequest = new TournamentRegionsSearchRequest { SearchParams = tournamentRegionsSearchParams };

                TournamentsResponse searchResult;
                try
                {
                    searchResult = this._tournamentServiceClient.SearchTournamentByRegions(tournamentRegionsSearchRequest);
                }
                catch (Exception exception)
                {
                    Log.Error(exception);
                    throw;
                }

                if (!searchResult.Success)
                {
                    return null;
                }

                IEnumerable<Tournament> tournaments = searchResult.Tournaments;

                var tournamentComparer = new TournamentComparer();
                foreach (var tournament in
                    tournaments.Where(tournament => !distinctTournaments.Contains(tournament, tournamentComparer)))
                {
                    distinctTournaments.Add(tournament);
                }
            }

            distinctTournaments.Sort(
                (a, b) =>
                    {
                        var result = string.Compare(a.TournamentName, b.TournamentName, StringComparison.Ordinal);

                        return result;
                    });

            return distinctTournaments;
        }

        /// <summary>
        /// The get tournament by municipalities.
        /// </summary>
        /// <param name="municipalities">
        /// The municipalities.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TournamentSummary> GetTournamentByMunicipalities(List<string> municipalities, int seasonId, int orgId)
        {
            var stopWatch = new System.Diagnostics.Stopwatch();
            Log.Debug("Starting stopwatch");
            stopWatch.Start();
            // Request
            var request = new OrgAndSeasonIdRequest { SeasonId = seasonId, OrgId = orgId};

            // Trying parallel and such now then.
            TournamentSummariesResponse2 response = null;
            try
            {
                response = this._seasonServiceClient.GetTournamentSummeriesBySeason(request);

            }
            catch (Exception exception)
            {
                Log.Error(exception);
            }

            if (response == null)
            {
                return new List<TournamentSummary>();
            }

            if (!response.Success)
            {
                return new List<TournamentSummary>();
            }

            var tournaments = response.TournamentSummaryList;

            var tournamentList = (from t in tournaments where t.HasMatches select t);

            Log.DebugFormat("Number of tournaments: {0}", tournaments.Length);
            //var tournamentClassesLists = new Dictionary<int, int>();
            //Parallel.ForEach(tournamentList, (seasonTournament, state) =>
            //{
            //    try
            //    {
            //        // log.DebugFormat("Tournament Id: {0} to get tournament classes", seasonTournament.TournamentId);
            //        var tournamentClassesTournamentIdRequest = new TournamentIdRequest2(seasonTournament.TournamentId);
            //        var tournamentClassesResponse =
            //            this.tournamentServiceClient.GetTournamentClassesByTournamentId(
            //                tournamentClassesTournamentIdRequest);
            //        if (!tournamentClassesResponse.Success)
            //        {
            //            state.Break();
            //        }
            //        var tournamentClasses = tournamentClassesResponse.TournamentClassesList.ToList();

            //        foreach (var tc in tournamentClasses)
            //        {
            //            if (tournamentClassesLists.ContainsKey(tc.TournamentId))
            //            {
            //                continue;
            //            }

            //            tournamentClassesLists.Add(tc.TournamentId, tc.ClassId);
            //        }
            //    }
            //    catch (Exception exception)
            //    {
            //        log.Error(exception);
            //    }

            //}
            //    );

            
            ////log.DebugFormat("Number of tournamentClassesLists: {0}", tournamentClassesLists.Count);
            //var tournamentAgeCategoryDictionary = new Dictionary<int, TournamentClass>();
            //Parallel.ForEach(tournamentClassesLists, (kvp, state) =>
            //{
            //    // log.DebugFormat("Tournament Id: {0} to get class for tournament", kvp.Key);
            //    var tournamentClassRequest = new TournamentClassIdRequest(kvp.Value);
            //    var tournamentClassResponse = this.tournamentServiceClient.GetClass(tournamentClassRequest);

            //    if (!tournamentClassResponse.Success)
            //    {
            //        state.Break();
            //    }

            //    if (tournamentAgeCategoryDictionary.ContainsKey(kvp.Key))
            //    {
            //        return;
            //    }

            //    tournamentAgeCategoryDictionary.Add(kvp.Key, tournamentClassResponse.TournamentClass);
            //});

            //log.DebugFormat("Number of tournamentAgeCategoryDictionary: {0}", tournamentAgeCategoryDictionary.Count);
            //var filteredTournaments = (from tac in tournamentAgeCategoryDictionary
            //                           from t in tournaments
            //    where tac.Value.AllowedFromAge >= 12 && tac.Key == t.TournamentId
            //    select t).ToList();

            //log.DebugFormat("Number of filteredTournaments: {0}", filteredTournaments.Count);

            //foreach (var t in tournaments)
            //{
            //    if (t.TournamentId == 337997)
            //    {
            //        log.Debug("Found the tournament.");
            //    }
            //}

            var dictionaryTournamentTeams = new Dictionary<TournamentSummary, List<TeamSummary>>();

            // log.DebugFormat("Working on Tournament {0} with id {1}", currentTournament.TournamentName, currentTournament.TournamentId);
            Log.Debug("Staring the parallel process");
            try
            {
                Parallel.ForEach(
                    tournamentList, 
                    // new ParallelOptions {MaxDegreeOfParallelism = 4},
                    (currentTournament, state) =>
                    {
                        
                            var tournamentRequest = new TournamentIdRequest2 { TournamentId = currentTournament.TournamentId };
                            var tournamentResponse = this._tournamentServiceClient.GetTournamentTeams(tournamentRequest);

                            if (!tournamentResponse.Success)
                            {
                                state.Break();
                            }

                            var teams = tournamentResponse.TournamentMatchTeam;

                            var teamIds = new int[teams.Count()];

                            // log.DebugFormat("Number of teams: {0}", teams.Count());
                            if (!teams.Any())
                            {
                                // log.DebugFormat("No teams in tournament {0}", currentTournament.TournamentName);
                                return;
                            }

                            var counter = 0;
                            foreach (var tournamentMatchTeam in teams)
                            {
                                // log.DebugFormat("Team {0}, Id: {1}", tournamentMatchTeam.Team, tournamentMatchTeam.TeamId);    
                                teamIds[counter] = tournamentMatchTeam.TeamId;
                                counter++;
                            }

                            if (teamIds.Count() <= 0)
                            {
                                state.Break();
                            }

                            try
                            {
                                var teamsRequest = new TeamIdsRequest2 { TeamIds = teamIds };
                                var teamsResponse = this._teamServiceClient.GetTeamsByIds(teamsRequest);

                                if (!teamsResponse.Success)
                                {
                                    state.Break();
                                }

                                dictionaryTournamentTeams.Add(currentTournament, teamsResponse.TeamSummaries.ToList());
                                
                            }
                            catch (Exception exception)
                            {
                                Log.Error(exception);
                                Log.Error(exception.StackTrace);

                            }
                            
                        });
            }
            catch (Exception)
            {
                // Currently not doing anything
            }

            var foundTournaments = new List<TournamentSummary>();
            foreach (var kvp in dictionaryTournamentTeams)
            {
                //if (kvp.Key.TournamentId == 337997)
                //{
                //    log.Debug("fart");
                //}
                var foundTournamentTeams = kvp.Value;

                foreach (var municipality in municipalities)
                {
                    foreach (var foundTournamentTeam in foundTournamentTeams)
                    {
                        if (foundTournamentTeam.LocalCouncilId != Convert.ToInt32(municipality))
                        {
                            continue;
                        }

                        if (!foundTournaments.Contains(kvp.Key))
                        {
                            foundTournaments.Add(kvp.Key);
                        }
                    }
                }
            }

            stopWatch.Stop();
            var ts = stopWatch.Elapsed;

            var elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);
            Log.DebugFormat("Runtime: {0}", elapsedTime);
            return foundTournaments;
        }

        /// <summary>
        /// The get tournament by municipalities.
        /// </summary>
        /// <param name="selectedRegions">
        /// The selected regions.
        /// </param>
        /// <param name="ageCategories">
        /// The age categories.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="Exception">
        /// throws an exception if we cannot find an age category
        /// </exception>
        public List<Tournament> GetTournamentByMunicipalities(List<Region> selectedRegions, List<ClassCode> ageCategories, int seasonId, int jobId, int orgId)
        {
            var selectedTournaments = new List<Tournament>();

            foreach (var region in selectedRegions)
            {
                var tournamentRegionsSearchParams = new TournamentRegionsSearchParams { LocalCouncilIds = region.RegionId.ToString(), SeasonId = seasonId };
                var tournamentRegionsSearchRequest = new TournamentRegionsSearchRequest { SearchParams = tournamentRegionsSearchParams };
                var searchResult = this._tournamentServiceClient.SearchTournamentByRegions(tournamentRegionsSearchRequest);

                if (!searchResult.Success)
                {
                    continue;
                }

                foreach (var tournament in searchResult.Tournaments)
                {
                    foreach (var classCode in ageCategories)
                    {
                        if (classCode.ClassId != tournament.ClassCodeId)
                        {
                            throw new Exception("Age Category not found in database!");
                        }

                        var selectedTournament = new Tournament { TournamentId = tournament.TournamentId, TournamentName = tournament.TournamentName, TournamentNo = tournament.TournamentNo, ClassCodeId = tournament.ClassCodeId };

                        // check if the current tournament is in the selectedtournament list
                        var tournament1 = tournament;
                        var tournaments = from a in selectedTournaments where a.TournamentId == tournament1.TournamentId || a.TournamentNo == tournament1.TournamentNo select a;

                        if (!tournaments.Any())
                        {
                            selectedTournaments.Add(selectedTournament);
                        }
                    }
                }
            }

            return selectedTournaments;
        }

        /// <summary>
        /// The get tournament standing.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TeamResult> GetTournamentStanding(int tournamentId)
        {
            try
            {

                var request = new TournamentTableRequest() { TournamentId = tournamentId };

                var response = this._tournamentServiceClient.GetTournamentResults(request);

                if (!response.Success)
                {
                    throw new Exception(this.GenerateExceptionString(response.ErrorMessage, response.ErrorCode.ToString()));
                }

                return !response.Success ? new List<TeamResult>() : response.Results.ToList();
            }
            catch (Exception exception)
            {
                Log.Error(exception.Message);
                Log.Error(exception.StackTrace);

                //  mailer = new NTB.SportsData.Utilities()
                throw;
            }
        }

        /// <summary>
        /// Method to get this seasons tournaments
        /// </summary>
        /// <param name="seasonId">
        /// Integer with season id
        /// </param>
        /// <param name="orgId">
        /// Integer with season id
        /// </param>
        /// <returns>
        /// List of tournaments
        /// </returns>
        public List<TournamentSummary> GetTournamentSummariesBySeasonAndOrgId(int seasonId, int orgId)
        {
            var request = new OrgAndSeasonIdRequest()
            {
                SeasonId = seasonId,
                OrgId = orgId
            };

            var response = this._seasonServiceClient.GetTournamentSummeriesBySeason(request);

            if (!response.Success)
            {
                throw new SportsDataException(this.GenerateExceptionString(response.ErrorMessage, response.ErrorCode.ToString()));
            }

            return response.TournamentSummaryList.ToList();
        }

        /// <summary>
        /// Method to get this seasons tournaments
        /// </summary>
        /// <param name="seasonId">
        /// Integer with season id
        /// </param>
        /// <returns>
        /// List of tournaments
        /// </returns>
        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            var request = new SeasonIdRequest(seasonId);

            var response = this._seasonServiceClient.GetSeasonTournaments(request);

            if (!response.Success)
            {
                throw new SportsDataException(this.GenerateExceptionString(response.ErrorMessage, response.ErrorCode.ToString()));
            }

            return response.Tournaments.ToList();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void InsertAll(List<Tournament> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public int InsertOne(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void Update(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get tournaments by org id and season id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsByOrgIdAndSeasonId(int orgId, int seasonId)
        {
            var searchParams = new TournamentTeamSearchParams() { SeasonId = seasonId, OrgIds = new[] { orgId } };

            var request = new TournamentTeamSearchRequest(searchParams);

            var response = this._tournamentServiceClient.SearchTournamentByTeam(request);

            if (!response.Success)
            {
                return new List<Tournament>();
            }

            return response.Tournaments.ToList();
        }

        #endregion

        /// <summary>
        /// The generate exception string.
        /// </summary>
        /// <param name="errorMessage">
        /// The error message.
        /// </param>
        /// <param name="errorCode">
        /// The error code.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GenerateExceptionString(string errorMessage, string errorCode)
        {
            return string.Format("We've received an error from the NIF API: {0}, code: {1}", errorMessage, errorCode);
        }
    }
}