﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Interfaces
{
    interface ITournamentModel
    {
        List<Tournament> NormalizeTournaments(string selectedTournaments);
        void InsertTournament(Tournament tournament);
        void InsertSelectedTournaments(List<Tournament> selectedTournaments, int jobId, int ageCategoryId, int customerId, int sportId);
        List<Tournament> GetTournamentsByJobId(int jobId);
        List<Tournament> GetTournamentsBySportId(int sportId, int seasonId);
        Tournament GetTournament(int tournamentId);
        void DeleteTournament(int tournamentId);
        void DeleteTournamentByJobId(int jobId);
    }
}
