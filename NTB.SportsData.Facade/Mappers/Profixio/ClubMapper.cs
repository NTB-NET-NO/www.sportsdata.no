﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClubMapper.cs" company="NTB">
//   Copyright Norsk Telegrambyrå
// </copyright>
// <summary>
//   Defines the ClubMapper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.Profixio
{
    using Glue;

    using NTB.SportsData.Common;
    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Services.Profixio.ProfixioServiceReference;

    /// <summary>
    /// The club mapper.
    /// </summary>
    public class ClubMapper : BaseMapper<TournamentClub, Club>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<TournamentClub, Club> mapper)
        {
            mapper.Relate(x => x.id, y => y.ClubId);
            mapper.Relate(x => x.name, y => y.ClubName);
        }
    }
}
