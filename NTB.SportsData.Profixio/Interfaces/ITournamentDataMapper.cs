﻿using System.Collections.Generic;
using NTB.SportsData.Services.Profixio.ProfixioServiceReference;

namespace NTB.SportsData.Services.Profixio.Interfaces
{
    public interface ITournamentDataMapper
    {
        List<MatchGroup> GetTournamentBySeasonId(int seasonId);
    }
}
