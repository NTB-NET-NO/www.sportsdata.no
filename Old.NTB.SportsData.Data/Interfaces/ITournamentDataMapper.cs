﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITournamentDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TournamentDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The TournamentDataMapper interface.
    /// </summary>
    public interface ITournamentDataMapper
    {
        /// <summary>
        /// The get tournaments by customer id.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentsByCustomerId(int customerId);

        /// <summary>
        /// The get tournament by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        Tournament GetTournamentById(int id);
    }
}