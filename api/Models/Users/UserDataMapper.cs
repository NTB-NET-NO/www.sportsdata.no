﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The user data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Models.Users
{
    using NTB.SportsData.Api.Interfaces;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The user data mapper.
    /// </summary>
    public class UserDataMapper : IUserDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="ApiUser"/>.
        /// </returns>
        public ApiUser Get(string username, string password)
        {
            return new ApiUser { Username = "NTB", Password = password };
        }

        #endregion
    }
}