﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using NTB.SportsData.Services.NIF.Interfaces;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Services.NIF.Repositories
{
    /// <summary>
    /// The match repository.
    /// </summary>
    public class MatchRepository : IRepository<TournamentMatch>, IDisposable, IMatchDataMapper
    {
        #region Static Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentRepository));

        #endregion

        /// <summary>
        /// The insert match.
        /// 1   G   Godkjent
        /// 2   IR  Ikke registrert
        /// 3   R   Registrert
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <param name="federationUserId">
        /// The federation user id. To be used when storing the match result
        /// </param>
        public void InsertMatch(TournamentMatchExtended match, int federationUserId)
        {
            try
            {
                var client = new ResultTeamServiceClient();

                if (client.ClientCredentials == null)
                {
                    return;
                }
                
                // We must store with the user id of the federation
                var userName = ConfigurationManager.AppSettings["NIFServicesUsername"];
                var userNameKey = ConfigurationManager.AppSettings["NIFServiceKey"];
                var federationUserName = userName.Replace(userNameKey, federationUserId.ToString());
                client.ClientCredentials.UserName.UserName = federationUserName;
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

                if (match.HomeGoals == null || match.AwayGoals == null)
                {
                    Logger.Info("object contains no goal information");
                    return;
                }

                // Home Team Score
                var homeTeamScore = Convert.ToInt32(match.HomeGoals);

                // Away Team Score
                var awayTeamScore = Convert.ToInt32(match.AwayGoals);

                // Match Id
                var matchId = match.MatchId;

                // I am checking if the match has a result before I am storing it
                var result = this.GetMatchResult(matchId);

                var update = false;

                if (result != null)
                {
                    if (result.ResultStatusId == 1)
                    {
                        Logger.Info("Match results has been accepted. Not possible to update");
                        return;
                    }

                    if (result.HomeGoals != null && result.AwayGoals != null)
                    {
                        if (result.HomeGoals == homeTeamScore || result.AwayGoals == awayTeamScore)
                        {
                            Logger.Info("Match has been registered already");
                            return;
                        }

                        if (result.HomeGoals != homeTeamScore || result.AwayGoals != awayTeamScore)
                        {
                            if (result.ResultStatusId == 3)
                            {
                                update = true;
                            }
                        }

                        // What if home goals and awaygoals are different? an update?
                        if ((result.ResultStatusId == 3) && update == false)
                        {
                            return;
                        }
                    }
                }

                var matchResult = new Result1
                {
                    HomeGoals = homeTeamScore,
                    AwayGoals = awayTeamScore,
                    MatchId = matchId,
                    ResultStatusId = 3
                };

                // Insert result
                if (update)
                {
                    matchResult.ResultId = result.ResultId;
                }

                var request = new ResultRequest(matchResult);

                var response = client.SaveMatchResult(request);

                if (!response.Success)
                {
                    throw new Exception(response.ErrorMessage);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        /// The get referee by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Referee> GetRefereeByMatchId(int matchId)
        {
            var client = new MatchServiceClient();

            if (client.ClientCredentials == null)
            {
                return new List<Referee>();
            }

            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            var request = new MatchIdRequest2() {MatchId = matchId};
            var response = client.GetRefereeByMatchId(request);

            if (!response.Success)
            {
                return new List<Referee>();
            }

            return response.Referees.ToList();
        }

        /// <summary>
        /// The update match.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <param name="federationUserId">
        /// The user id for the federation. To be used when storing the match result
        /// </param>
        public void UpdateMatch(TournamentMatchExtended match, int federationUserId)
        {
            try
            {
                var client = new MatchServiceClient();

                if (client.ClientCredentials == null)
                {
                    return;
                }

                // We must store with the user id of the federation
                var userName = ConfigurationManager.AppSettings["NIFServicesUsername"];
                var userNameKey = ConfigurationManager.AppSettings["NIFServiceKey"];

                var federationUserName = userName.Replace(userNameKey, federationUserId.ToString());
                
                client.ClientCredentials.UserName.UserName = federationUserName;
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

                if (match.HomeGoals == null || match.AwayGoals == null)
                {
                    return;
                }

                var homeTeamScore = Convert.ToInt32(match.HomeGoals);

                var awayTeamScore = Convert.ToInt32(match.AwayGoals);

                var matchId = match.MatchId;

                // Insert result
                var matchResult = new MatchResult
                {
                    HomeGoals = homeTeamScore,
                    AwayGoals = awayTeamScore,
                    MatchId = matchId,
                    StatusCode = "R"
                };

                var request = new MatchResultRequest(matchResult);

                var response = client.UpdateMatchResult(request);

                if (!response.Success)
                {
                    throw new Exception(response.ErrorMessage);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        /// The get matches by org id and season id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="seasonId">
        /// The seasonid.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<MatchInfo1> GetMatchesByOrgIdAndSeasonId(int orgId, int seasonId)
        {
            var client = new MatchServiceClient();

            if (client.ClientCredentials == null)
            {
                return new List<MatchInfo1>();
            }

            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            var searchParams = new MatchInfoSearchParams() {OrgId = orgId, SeasonId = seasonId};
            var request = new MatchInfoSearchRequest(searchParams);
            var response = client.SearchMatchInfo(request);

            if (!response.Success)
            {
                return new List<MatchInfo1>();
            }

            return response.MatchInfo.ToList();
        }

        /// <summary>
        /// The get match result.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="Result1"/>.
        /// </returns>
        public Result1 GetMatchResult(int matchId)
        {
            var client = new ResultTeamServiceClient();

            if (client.ClientCredentials == null)
            {
                return null;
            }

            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            var matchRequest = new MatchIdRequest()
            {
                MatchId = matchId
            };
            var matchResponse = client.GetMatchResult(matchRequest);

            return !matchResponse.Success ? null : matchResponse.Result;
        }

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(TournamentMatch domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="TournamentMatch"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public TournamentMatch Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<TournamentMatch> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get matches by tournament id and sport id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TournamentMatchExtended> GetMatchesByTournamentIdAndSportId(int tournamentId, int sportId)
        {
            var tournamentServiceClient = new TournamentServiceClient();

            if (tournamentServiceClient.ClientCredentials == null)
            {
                return new List<TournamentMatchExtended>();
            }

            tournamentServiceClient.ClientCredentials.UserName.UserName =
                ConfigurationManager.AppSettings["NIFServicesUsername"];
            tournamentServiceClient.ClientCredentials.UserName.Password =
                ConfigurationManager.AppSettings["NIFServicesPassword"];

            // Creating the database fetch request
            // var fromDate = DateTime.Today.Date;

            /* var toDate = DateTime.Today.Date; */

            var request = new TournamentMatchRequest {TournamentId = tournamentId, FromDate = null, ToDate = null,};
            TournamentMatchesResponse response;

            try
            {
                response = tournamentServiceClient.GetTournamentMatches(request);
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                throw;
            }

            return !response.Success ? new List<TournamentMatchExtended>() : response.TournamentMatch.ToList();
        }

        /// <summary>
        /// The get todays matches.
        /// </summary>
        /// <param name="matchDate">
        /// The match date.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TournamentMatchExtended> GetTodaysMatches(DateTime matchDate, int seasonId)
        {
            var serviceClient = new SeasonServiceClient();

            if (serviceClient.ClientCredentials == null)
            {
                return new List<TournamentMatchExtended>();
            }

            serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            var tournamentServiceClient = new TournamentServiceClient();
            if (tournamentServiceClient.ClientCredentials != null)
            {
                tournamentServiceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NIFServicesUsername"];
                tournamentServiceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NIFServicesPassword"];
            }

            var seasonrequest = new SeasonIdRequest(seasonId);
            var response = serviceClient.GetSeasonTournaments(seasonrequest);

            if (!response.Success)
            {
                return null;
            }

            var request = new TournamentMatchRequest {FromDate = matchDate, ToDate = matchDate};
            var matchesResponse = tournamentServiceClient.GetTournamentMatches(request);

            if (matchesResponse.Success)
            {
                return matchesResponse.TournamentMatch.ToList();
            }

            return new List<TournamentMatchExtended>();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<TournamentMatch> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(TournamentMatch domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(TournamentMatch domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}