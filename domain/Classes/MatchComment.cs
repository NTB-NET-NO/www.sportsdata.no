namespace NTB.SportsData.Domain.Classes
{
    public class MatchComment
    {
        public int Id { get; set; }
        public string Comment { get; set; }
    }
}