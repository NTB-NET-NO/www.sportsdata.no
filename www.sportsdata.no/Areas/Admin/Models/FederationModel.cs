﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Domain.Exceptions;
using NTB.SportsData.Facade.NIF.SportAccess;
using NTB.SportsData.Web.Areas.Admin.Repositories;

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    public class FederationModel
    {
        private readonly FederationRemoteRepository _federationRemoteRepository;

        public FederationModel()
        {
            this._federationRemoteRepository = new FederationRemoteRepository();
        }

        public FederationRemoteRepository FederationRemoteRepository
        {
            get { return this._federationRemoteRepository; }
        }

        public void AddFederationDisciplines(string disciplineList, int orgId)
        {
            // Creating the Federation Data Access object
            var repository = new FederationDisciplinesRepository
                {
                    OrgId = orgId
                };

            repository.DeleteAll();

            // The disciplines comes in a comma-separated string
            string[] disciplines = disciplineList.Split(',');
            var federationDisciplines = new List<FederationDiscipline>();
            foreach (string discipline in disciplines)
            {
                string[] splittedDiscipline = discipline.Split('|');
                int disciplineId = Convert.ToInt32(splittedDiscipline[0]);
                string disciplineName = splittedDiscipline[1];

                var federationDiscipline = new FederationDiscipline
                    {
                        ActivityId = disciplineId,
                        ActivityName = disciplineName
                    };

                federationDisciplines.Add(federationDiscipline);
            }

            // Now we shall add these disciplines to the database
            repository.InsertAll(federationDisciplines);
        }

        //public void FooTemp(int id)
        //{
        //    var sportRepository = new SportRepository();
        //    var sportId = sportRepository.GetSportIdFromOrgId(id);

        //    var seasonModel = new SeasonModel();
        //    var seasons = seasonModel.GetSeasonsBySportId(sportId);

        //    var season = (from s in seasons where s.SeasonActive select s).SingleOrDefault();


        //    if (season == null)
        //    {
        //        return;
        //    }

        //    var facade = new SportFacade();
        //    var tournaments = facade.GetTournamentsBySeasonId(season.SeasonId);

        //    // Some cleaning up is necessary
        //    foreach (var tournament in tournaments)
        //    {
        //        tournament.DisciplineId = tournament.SportId;
        //        tournament.SportId = sportId;
        //    }

        //    var classCodes = new List<AgeCategory>();
        //    foreach (var tournament in tournaments)
        //    {
        //        classCodes.AddRange(facade.GetClassCodesByTournamentId(tournament.TournamentId));

        //    }

        //    foreach (var classCode in classCodes)
        //    {
        //        classCode.SportId = sportId;
        //    }

        //    // We now have some class code definitions, but not all, so we are going to loop again to get the rest
        //    var classCodesIds = new int[classCodes.Count];
        //    var counter = 0;
        //    foreach (var classCode in classCodes)
        //    {
        //        classCodesIds[counter] = classCode.CategoryId;
        //        counter++;
        //    }

        //    var remoteClassCodes = facade.GetClassCodesByClassIds(classCodesIds);

        //    var foo = new List<AgeCategory>(facade.GetAgeCategories(id));

        //    var ageCategories = new List<AgeCategory>(remoteClassCodes);

        //}


        public void InsertFederationAgeCategories(int id)
        {
            var sportRepository = new SportRepository();
            var sportId = sportRepository.GetSportIdFromOrgId(id);

            
            var facade = new SportFacade();

            var ageCategories = new List<AgeCategory>(facade.GetAgeCategories(id));

            foreach (var ageCategory in ageCategories)
            {
                ageCategory.SportId = sportId;
            }
            
            var ageCategoryRepository = new AgeCategoryRepository
                {
                    SportId = sportId
                };

            ageCategoryRepository.DeleteAll(sportId);

            ageCategoryRepository.InsertAll(ageCategories);
        }

        public List<AgeCategory> GetFederationAgeCategories(int id)
        {
            var repository = new SportRepository();
            var sportId = repository.GetSportIdFromOrgId(id);

            var ageCategoryRepository = new AgeCategoryRepository();
            var federationAgeCategories = ageCategoryRepository.GetAgeCategoryBySportId(sportId).ToList();

            return federationAgeCategories;
        }

        public Federation GetFederationBySportId(int sportId)
        {
            if (sportId == 0)
            {
                throw new SportsDataException("Sport Id cannot be zero");
            }

            var repository = new FederationRepository();
            return repository.GetFederationBySportId(sportId);
        }

        public void UpdateFederationAgeCategories(int id, FormCollection collection)
        {
            string[] categoryName = collection["categoryName[]"].Split(',');
            string[] categoryId = collection["categoryId[]"].Split(',');
            string[] minage = collection["minage[]"].Split(',');
            string[] maxage = collection["maxage[]"].Split(',');
            string[] agecategoryId = collection["ageCategory[]"].Split(',');
            string[] sportId = collection["sportId[]"].Split(',');

            int items = sportId.Length;

            for (int i = 0; i < items; i++)
            {
                int intCategoryId = Convert.ToInt32(categoryId[i]);
                int intMinAge = Convert.ToInt32(minage[i]);
                int intMaxAge = Convert.ToInt32(maxage[i]);
                int intAgeCategoryDefinitionId = Convert.ToInt32(agecategoryId[i]);
                int intSportId = Convert.ToInt32(sportId[i]);
                string stringAgeCategoryName = categoryName[i];
                var ageCategory = new AgeCategory
                    {
                        CategoryId = intCategoryId,
                        CategoryName = stringAgeCategoryName,
                        MinAge = intMinAge,
                        MaxAge = intMaxAge,
                        AgeCategoryDefinitionId = intAgeCategoryDefinitionId,
                        SportId = intSportId
                    };
                var ageCategoryRepository = new AgeCategoryRepository
                    {
                        SportId = intSportId
                    };

                ageCategoryRepository.Update(ageCategory);
            }
        }

        public void DeleteFederationAgeCategories(int id)
        {
            var ageCategoryRepository = new AgeCategoryRepository();
            ageCategoryRepository.DeleteAll(id);
        }

        
    }
}