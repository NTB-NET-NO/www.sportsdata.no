﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchManagementModels.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match management models.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Common;

namespace NTB.SportsData.Web.Areas.Input.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Web.Mvc;

    
    using Data.DataMappers.Districts;
    using Data.DataMappers.Matches;
    using Data.DataMappers.Populate;
    using Data.DataMappers.User.Web;
    using Data.DataMappers.UserDistrict;
    using Domain.Classes;
    using Facade.NFF.TournamentAccess;
    using Facade.NIF.SportAccess;

    /// <summary>
    /// The match management models.
    /// </summary>
    public class MatchManagementModels
    {
        // private readonly DistrictMatches _districtMatches;

        // public MatchManagementModels()
        // {

        // }

        /// <summary>
        /// The get matches for district.
        /// </summary>
        /// <param name="districts">
        /// The districts.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>Dictionary</cref>
        ///     </see>
        ///     .
        /// </returns>
        private Dictionary<District, List<Match>> GetMatchesForDistrict(List<District> districts)
        {
            var dictionary = new Dictionary<District, List<Match>>();

            foreach (District district in districts)
            {
                if (district.DistrictId == 0)
                {
                    continue;
                }

                var mapper = new MatchDataMapper();
                var matches = mapper.GetLocalMatchesByDistrictId(district.DistrictId);

                dictionary.Add(district, matches);
            }

            // Returning the dictionary
            return dictionary;
        }

        /// <summary>
        /// The get remote matches.
        /// </summary>
        public void GetRemoteMatches()
        {
            List<District> districts = this.GetDistricts();

            this.GetRemoteMatchesForDistrict(districts);

            // Insert into database
        }

        /// <summary>
        /// The get remote matches for district.
        /// </summary>
        /// <param name="districts">
        /// The districts.
        /// </param>
        private void GetRemoteMatchesForDistrict(List<District> districts)
        {
            // first we do the soccer matches
            var soccerFacade = new TournamentFacade();

            // Setting up the sport facade
            var sportsFacade = new SportFacade();

            var soccerSeason = soccerFacade.GetOngoingSeason();

            // Creating a doneEvents Reset Event Array
            var doneEvents = new ManualResetEvent[districts.Count];

            // Creating a counter to help us with creating the threads
            var counter = 0;

            // Creating a list of threads, so we know what each one is doing
            var threads = new List<Thread>();

            foreach (var district in districts)
            {
                if (district.DistrictId == 0)
                {
                    continue;
                }

                doneEvents[counter] = new ManualResetEvent(false);

                var districtMatches = new DistrictMatches(doneEvents[counter])
                                          {
                                              District = district, 
                                              SoccerFacade = soccerFacade, 
                                              SportSeason = soccerSeason, 
                                              SportsFacade = sportsFacade
                                          };

                var matchThread = new Thread(districtMatches.GetDistrictMatches)
                                         {
                                             Name =
                                                 "districtThread_"
                                                 + district.DistrictName
                                         };
                matchThread.Start();

                threads.Add(matchThread);

                // dictionary.Add(district, matches);
                counter++;
            }

            foreach (var doneEvent in doneEvents)
            {
                doneEvent.WaitOne();
            }
        }

        /// <summary>
        /// The store jobs.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        public void StoreJobs(FormCollection collection)
        {
            // TODO: Delete all current jobb-mappings.
            var dataMapper = new UserDistrictDataMapper();
            dataMapper.DeleteMappingByDate(DateTime.Today.Date);

            string[] jobs = collection["districts[]"].Split(',');

            Dictionary<int, List<Guid>> listofDistrictAndUsers = new Dictionary<int, List<Guid>>();
            BaseUtilities.ListOfDistrictAndUsers = null;

            const char splitChar = '|';
            foreach (string job in jobs)
            {
                listofDistrictAndUsers = BaseUtilities.SplitStringIntoListOfUsers(job, splitChar);
            }

            // now that we have this list of districts and users, we can start updating the match table
            var objUserDistrictMapper = new List<UserDistrictMapper>();

            foreach (KeyValuePair<int, List<Guid>> kvp in listofDistrictAndUsers)
            {
                var district = kvp.Key;
                var users = kvp.Value;
                var listOfUsers = new List<Guid>();
                foreach (Guid user in users)
                {
                    var returnval = dataMapper.GetUserByUserIdAndDistrictId(user, district);

                    if (returnval == false)
                    {
                        listOfUsers.Add(user);
                    }
                }

                var userDistrictMapper = new UserDistrictMapper
                                             {
                                                 DistrictId = district, 
                                                 UserIds = listOfUsers, 
                                                 WorkDate = DateTime.Today.Date
                                             };
                objUserDistrictMapper.Add(userDistrictMapper);

                dataMapper.Delete(userDistrictMapper);
            }

            // var insertDataMapper = new UserDistrictDataMapper();

            // Deleting all 
            dataMapper.DeleteMappingByDate(DateTime.Today.Date);

            // Insert all
            dataMapper.InsertAll(objUserDistrictMapper);
        }

        // todo: Code a method that does not return anything, but receives data from NIF/NFF
        // todo: Code a method that returns the list of districts + number of matches from database
        /// <summary>
        /// The get match management list view.
        /// </summary>
        /// <returns>
        /// The <see cref="MatchManagerView"/>.
        /// </returns>
        public MatchManagerView GetMatchManagementListView()
        {
            var districts = this.GetDistricts();

            // We need to check if the database has been updated
            var populateDataMapper = new PopulateDataMapper();
            var populate = populateDataMapper.GetDataBaseUpdated();

            // if (populate.End == null || populate.End > populate.Start)
            // {
            // return null;
            // }
            if (populate == null || populate.End == null)
            {
                return null;
            }

            var userManagerModels = new UserManagerModels();

            // Get all users
            var userProfiles = new List<UserProfile>();

            var userContext = new ApplicationDbContext();

            var userDataMapper = new UserDataMapper();

            var userList = userDataMapper.GetAll().ToList();

            foreach (ApplicationUser u in userList)
            {
                var userId = this.GetUserIdFromObject(u);

                var user = userManagerModels.GetUserProfile(u.Email);

                var userProfile = new UserProfile
                                      {
                                          FirstName = user.FirstName, 
                                          LastName = user.LastName, 
                                          UserId = userId, 
                                          UserName = u.UserName, 
                                          Email = u.Email, 
                                          LastLoginDate = u.LastLoginDate, 
                                          Districts = this.GetDistrictsForUser(userId)
                                      };

                userProfiles.Add(userProfile);
            }

            Dictionary<District, List<Match>> dictionary = this.GetMatchesForDistrict(districts);

            var viewModel = new MatchManagerView
                                {
                                    UserProfiles = userProfiles, 
                                    Districts = districts, 
                                    DistrictMatches = dictionary
                                };

            return viewModel;
        }

        /// <summary>
        /// The get user id from object.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        private Guid GetUserIdFromObject(ApplicationUser user)
        {
            return user.Id != null ? new Guid(user.Id) : new Guid();
        }

        /// <summary>
        /// The get districts for user.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<District> GetDistrictsForUser(Guid userId)
        {
            var datamapper = new DistrictDataMapper();
            return datamapper.GetDistrictsByUserId(userId);
        }

        /// <summary>
        /// The get districts.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<District> GetDistricts()
        {
            var datamapper = new DistrictDataMapper();
            return new List<District>(datamapper.GetAll());
        }
    }
}