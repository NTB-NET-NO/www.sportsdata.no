﻿namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;
    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NIF.NIFProdService;

    
    public class RecipientMapper : BaseMapper<Recipient, ContactPerson>
    {
        protected override void SetUpMapper(Mapping<Recipient, ContactPerson> mapper)
        {
            mapper.Relate(x => x.RelationId, y=>y.PersonId);
            mapper.Relate(x => x.RecipientName, y => y.FullName);
            mapper.Relate(x => x.Mobile, y => y.MobilePhone);
            mapper.Relate(x => x.Email, y => y.Email);
            mapper.Relate(x => x.RecipientRoleName, y => y.RoleName);
        }
    }
}