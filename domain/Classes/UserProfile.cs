﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserProfile.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The user profile.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Domain.Classes
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The user profile.
    /// </summary>
    public class UserProfile
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets the full name.
        /// </summary>
        public string FullName
        {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
        }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is online.
        /// </summary>
        public bool IsOnline { get; set; }

        /// <summary>
        /// Gets or sets the last login date.
        /// </summary>
        public DateTime LastLoginDate { get; set; }

        /// <summary>
        /// Gets or sets the pass word.
        /// </summary>
        public string PassWord { get; set; }

        /// <summary>
        /// Gets or sets the work role.
        /// </summary>
        public WorkRole WorkRole { get; set; }

        /// <summary>
        /// Gets or sets the districts.
        /// </summary>
        public List<District> Districts { get; set; }

        /// <summary>
        /// Gets or sets the roles.
        /// </summary>
        public List<Role> Roles { get; set; }

        /// <summary>
        /// Gets or sets the selected role.
        /// </summary>
        public Role SelectedRole { get; set; }
    }
}