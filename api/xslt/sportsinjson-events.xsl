<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs" 
    version="1.0">
    
    <xsl:output method="text" indent="yes"/>
    <xsl:variable name="newline">
        <xsl:text>
        </xsl:text>
    </xsl:variable>
    <xsl:template match="/">
        <xsl:text>{"events": [</xsl:text>
        <xsl:value-of select="$newline" />
        <xsl:for-each select="/Matches/Match">
            <xsl:value-of select="$newline"/>
            <xsl:text>{</xsl:text>
            <xsl:value-of select="$newline" />
            <xsl:text>"competitionId": </xsl:text> 
            <xsl:text>"</xsl:text>
            <xsl:value-of select="TournamentId"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text></xsl:text>
            <xsl:text>"competitionName": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="TournamentName"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            <!--<xsl:text>"events": [</xsl:text>
            
            <xsl:text>{</xsl:text>
            <xsl:value-of select="$newline" />
            -->
            <xsl:text>"competitionRound": </xsl:text>
            <xsl:value-of select="TournamentRoundNumber"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"eventId": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="MatchId"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"eventName": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="HomeTeam"/>
            <xsl:text> - </xsl:text>
            <xsl:value-of select="AwayTeam"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"eventStartDate": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="MatchDate"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"eventVenueName": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="VenueName"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"eventStatus": </xsl:text>
            <xsl:text>"planned</xsl:text>
            <!--<xsl:value-of select=""/>-->
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"eventOutcome": </xsl:text>
            <xsl:text>"null</xsl:text>
            <!--<xsl:value-of select="MatchStartTime"/>-->
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"teams": [{</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"name": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="HomeTeam"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"teamId": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="HomeTeamId"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"score": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="HomeTeamGoal"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"url": </xsl:text>
            <xsl:text>"http://www.fotball.no/Community/Lag/Hjem/?fiksId=</xsl:text>
            <xsl:value-of select="HomeTeamId"/>
            <xsl:text>"},</xsl:text>
            <xsl:value-of select="$newline"/>
            <!-- Away team -->
            <xsl:text>{</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"name": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="AwayTeam"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"teamId": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="AwayTeamId"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"score": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="AwayTeamGoal"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <xsl:text>"url": </xsl:text>
            <xsl:text>"http://www.fotball.no/Community/Lag/Hjem/?fiksId=</xsl:text>
            <xsl:value-of select="AwayTeamId"/>
            <xsl:text>"}]</xsl:text>
            <xsl:value-of select="$newline"/>
            
            <!-- Done with home and away team, we shall move to next event -->
            
            
            
            <!--
            <xsl:value-of select="$newline" />
            <xsl:text>{</xsl:text>
            <xsl:value-of select="$newline" />
            <xsl:text>"competitionid": </xsl:text>
            <xsl:value-of select="TournamentId"/>
            <xsl:text>, </xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"competitionname": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="TournamentName"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline" />
            
            <xsl:text>"competitionstartdate": "</xsl:text>
            <xsl:value-of select="StartDate"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$newline" />
            <xsl:text>"competitionenddate": "</xsl:text>
            <xsl:value-of select="EndDate"/>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="$newline" />
            <xsl:text>}</xsl:text>
            <xsl:choose>
                <xsl:when test="position() != last()">
                    <xsl:value-of select="$newline" />
                    <xsl:text>},</xsl:text>
                    <xsl:value-of select="$newline" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>}</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
            
            -->
        
            <xsl:choose>
                <xsl:when test="position() = last()">
                    <xsl:text>}</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>},</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        <xsl:value-of select="$newline" />
        </xsl:for-each>
        <xsl:value-of select="$newline"/>
        <xsl:text>]</xsl:text>
        <xsl:value-of select="$newline"/>
        <xsl:text>}</xsl:text>
    </xsl:template>
</xsl:stylesheet>