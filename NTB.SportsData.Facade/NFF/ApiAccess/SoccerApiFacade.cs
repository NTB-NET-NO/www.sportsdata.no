﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SoccerApiFacade.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The soccer api facade.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.NFF.ApiAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Interfaces;
    using NTB.SportsData.Facade.Mappers.NFF;
    using NTB.SportsData.Services.NFF.Interfaces;
    using NTB.SportsData.Services.NFF.Repositories;

    /// <summary>
    /// The soccer api facade.
    /// </summary>
    public class SoccerApiFacade : IApiFacade
    {
        #region Fields

        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(SoccerApiFacade));

        /// <summary>
        /// The _team data mapper.
        /// </summary>
        private readonly ITeamDataMapper teamDataMapper;

        /// <summary>
        /// The _tournament data mapper.
        /// </summary>
        private readonly ITournamentDataMapper tournamentDataMapper;

        /// <summary>
        /// The _club data mapper.
        /// </summary>
        private readonly IClubDataMapper clubDataMapper;

        /// <summary>
        /// The _match data mapper.
        /// </summary>
        private readonly IMatchDataMapper matchDataMapper;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SoccerApiFacade"/> class.
        /// </summary>
        public SoccerApiFacade()
        {
            this.teamDataMapper = new TeamRepository();
            this.tournamentDataMapper = new TournamentRepository();
            this.clubDataMapper = new ClubRepository();
            this.matchDataMapper = new MatchRepository();
        }

        #endregion

        /// <summary>
        /// The get match extended by match id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="MatchExtended"/>.
        /// </returns>
        public MatchExtended GetMatchExtendedByMatchId(int id)
        {
            try
            {
                Logger.InfoFormat("GetMatchExtendedByMatchId by id: {0}", id);
                var result = this.matchDataMapper.GetMatchExtendedByMatchId(id);

                Logger.InfoFormat("Mapping data using MatchExtendedMapper");
                var mapper = new MatchExtendedMapper();

                Logger.InfoFormat("Return result");
                return mapper.Map(result, new MatchExtended());
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return new MatchExtended();
            }
        }

        /// <summary>
        /// The hello world.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string HelloWorld()
        {
            return "Hello World";
        }

        /// <summary>
        /// The get team result standing.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TeamResult> GetTeamResultStanding(Tournament tournament)
        {
            var result = this.tournamentDataMapper.GetTournamentStanding(tournament.TournamentId);

            var mapper = new TournamentStandingMapper();

            return result.Select(row => mapper.Map(row, new TeamResult())).ToList();
        }

        /// <summary>
        /// The get tournaments by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="seaonsId">
        /// The seaons id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        ///     This is not implemented and thus throws an exception
        /// </exception>
        public List<Tournament> GetTournamentsByOrgId(int orgId, int seaonsId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get seasons by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        ///     This is not implemented and thus throws an exception
        /// </exception>
        public List<Season> GetSeasonsByOrgId(int orgId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get matches by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        ///     This is not implemented and thus throws an exception
        /// </exception>
        public List<Match> GetMatchesByOrgId(int orgId, int seasonId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get match by match id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        public MatchExtended GetMatchByMatchId(int id)
        {
            Logger.InfoFormat("GetMatchByMatchId by id: {0}", id);
            var result = this.matchDataMapper.GetMatchByMatchId(id);

            Logger.InfoFormat("Mapping Match id: {0}", id);
            var mapper = new MatchExtendedMapper();

            // return result.Select(row => mapper.Map(row, new TeamResult())).ToList();
            return mapper.Map(result, new MatchExtended());
        }

        #region Public Methods and Operators

        /// <summary>
        /// The get tournament standing.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TournamentTable> GetTournamentStanding(Tournament tournament)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get tournament teams.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Team> GetTournamentTeams(int tournamentId)
        {
            var result = this.teamDataMapper.GetTournamentTeams(tournamentId);

            var mapper = new TeamMapper();

            return result.Select(row => mapper.Map(row, new Team())).ToList();
        }

        /// <summary>
        /// The get tournament matches.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Match> IApiFacade.GetTournamentMatches(Tournament tournament)
        {
            var result = this.matchDataMapper.GetMatchesByTournament(tournament.TournamentId, false, false, false, false);

            var mapper = new MatchMapper();

            return result.Select(row => mapper.Map(row, new Match())).ToList();
        }

        /// <summary>
        /// The get tournament clubs.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Club> GetTournamentClubs(int tournamentId)
        {
            var result = this.clubDataMapper.GetTournamentClubs(tournamentId);

            var mapper = new ClubMapper();

            return result.Select(row => mapper.Map(row, new Club())).ToList();
        }

        /// <summary>
        /// The get clubs by municipality.
        /// </summary>
        /// <param name="municipalityId">
        /// The municipality id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Club> GetClubsByMunicipality(int municipalityId)
        {
            var result = this.clubDataMapper.GetClubsByMunicipality(municipalityId);

            var mapper = new ClubMapper();

            return result.Select(row => mapper.Map(row, new Club())).ToList();
        }

        /// <summary>
        /// The get club persons.
        /// </summary>
        /// <param name="clubId">
        /// The club id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public Club GetClubPersons(int clubId)
        {
            var result = this.clubDataMapper.GetClubPersons(clubId);

            var mapper = new ClubMapper();

            return mapper.Map(result, new Club());
        }

        #endregion
    }
}