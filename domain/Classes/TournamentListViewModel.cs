﻿using System.Collections.Generic;

namespace NTB.SportsData.Domain.Classes
{
    public class TournamentListViewModel
    {
        public int SportId { get; set; }
        public int SeasonId { get; set; }
        public List<AgeCategoryDefinition> AgeCategoryDefinitions { get; set; }
        public List<Tournament> Tournaments { get; set; }
    }
}