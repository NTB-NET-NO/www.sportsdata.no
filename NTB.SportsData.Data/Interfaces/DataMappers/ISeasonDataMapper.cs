﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISeasonDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The SeasonDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The SeasonDataMapper interface.
    /// </summary>
    public interface ISeasonDataMapper
    {
        /// <summary>
        /// The get seasons by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Season> GetSeasonsBySportId(int sportId);

        /// <summary>
        /// The activate season.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        void ActivateSeason(int seasonId);

        /// <summary>
        /// The de activate season.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        void DeActivateSeason(int seasonId);

        /// <summary>
        /// The update season with discipline.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="disciplineId">
        /// The discipline id.
        /// </param>
        void UpdateSeasonWithDiscipline(int seasonId, int disciplineId);
    }
}