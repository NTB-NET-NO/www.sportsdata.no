﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMatchDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The MatchDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NFF.Interfaces
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The MatchDataMapper interface.
    /// </summary>
    public interface IMatchDataMapper
    {
        /// <summary>
        /// The get matches by tournament.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="includeSquad">
        /// The include squad.
        /// </param>
        /// <param name="includeReferees">
        /// The include referees.
        /// </param>
        /// <param name="includeResults">
        /// The include results.
        /// </param>
        /// <param name="includeEvents">
        /// The include events.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees, bool includeResults, bool includeEvents);

        /// <summary>
        /// The get todays matches.
        /// </summary>
        /// <param name="matchDate">
        /// The match date.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Match> GetTodaysMatches(DateTime matchDate);

        /// <summary>
        /// The get matchesby district.
        /// </summary>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <param name="includeSquad">
        /// The include squad.
        /// </param>
        /// <param name="includeReferees">
        /// The include referees.
        /// </param>
        /// <param name="includeResults">
        /// The include results.
        /// </param>
        /// <param name="includeEvents">
        /// The include events.
        /// </param>
        /// <param name="dateFrom">
        /// The date from.
        /// </param>
        /// <param name="dateTo">
        /// The date to.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Match> GetMatchesbyDistrict(int districtId, bool includeSquad, bool includeReferees, bool includeResults, bool includeEvents, DateTime dateFrom, DateTime dateTo);

        /// <summary>
        /// The get match by match id.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        Match GetMatchByMatchId(int id);

        /// <summary>
        /// The get match extended by match id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        Match GetMatchExtendedByMatchId(int id);
    }
}