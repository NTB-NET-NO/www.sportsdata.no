﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddPhoneNumberViewModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    ///     The add phone number view model.
    /// </summary>
    public class AddPhoneNumberViewModel
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the number.
        /// </summary>
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }

        #endregion
    }
}