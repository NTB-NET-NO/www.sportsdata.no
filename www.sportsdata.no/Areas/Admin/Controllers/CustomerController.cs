﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Web.Areas.Admin.Models;
using NTB.SportsData.Web.Areas.Admin.Repositories;
using NTB.SportsData.Web.Areas.Admin.Utilities;
using NTB.SportsData.Web.Areas.Input.Models.IdentityModels;

namespace NTB.SportsData.Web.Areas.Admin.Controllers
{
    /// <summary>
    ///     The customer controller.
    /// </summary>
    [Authorize]
    public class CustomerController : Controller
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerController"/> class.
        /// </summary>
        public CustomerController()
            : this(new ApplicationUserManager(new ApplicationUserStore()))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerController"/> class.
        /// </summary>
        /// <param name="userManager">
        /// The user manager.
        /// </param>
        public CustomerController(ApplicationUserManager userManager)
        {
            this.UserManager = userManager;
        }

        /// <summary>
        /// Gets the user manager.
        /// </summary>
        public ApplicationUserManager UserManager { get; private set; }

        #region Public Properties

        /// <summary>
        ///     Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        #endregion

        /// <summary>
        /// The keep alive.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        [Authorize]
        public void KeepAlive(int id)
        {
            // really not doing a thing
            this._log.Info("Refreshing sesssion");

            // Doing this to keep this session alive...
            this.Session["alive"] = DateTime.Now;
        }

        /// <summary>
        /// The get federation by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="Federation"/>.
        /// </returns>
        private Federation GetFederationBySportId(int sportId)
        {
            var model = new FederationModel();
            return model.GetFederationBySportId(sportId);
        }

        // GET: /Customer/

        #region Fields

        /// <summary>
        ///     The _customers data access.
        /// </summary>
        private readonly CustomerRepository _customersDataAccess = new CustomerRepository();

        /// <summary>
        ///     The _log.
        /// </summary>
        private readonly ILog _log = LogManager.GetLogger(typeof(CustomerController));

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The activate.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Activate(int id)
        {
            // Deactivating the customer
            var model = new CustomerRepository();
            model.ActivateCustomer(id);

            this.Session.Add("CustomerId", id);

            return this.RedirectToAction("Index");

            // return View();
        }

        /// <summary>
        /// The activate job.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public ActionResult ActivateJob(int id)
        {
            this.ViewBag.CustomerId = id;

            var intJobId = Convert.ToInt32(this.Request.QueryString["JobId"]);

            var repository = new JobRepository {JobId = intJobId};
            repository.Activate();

            // Send the user back to the list
            return new RedirectResult("/Admin/Customer/Details/" + id);
        }

        // GET: /Customer/Create

        /// <summary>
        ///     The create.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult Create()
        {
            return this.View(new Customer());
        }

        // POST: /Customer/Create

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                var customer = new Customer();

                this.TryUpdateModel(customer);

                if (this.ModelState.IsValid)
                {
                    this._customersDataAccess.AddOneCustomer(customer);
                }

                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        /// <summary>
        /// The create job.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult CreateJob(int id)
        {
            var jobId = 0;
            if (this.HttpContext.Request.QueryString["JobId"] != null)
            {
                jobId = Convert.ToInt32(this.Request.QueryString["JobId"]);
            }

            this.ViewBag.CustomerId = id;
            this.ViewBag.JobId = jobId;

            return this.PartialView();
        }

        /// <summary>
        /// The define job.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult DefineJob(int id)
        {
            this.Session.Add("SeasonId", 0);

            var model = new DefineViewModel();
            return this.View(model.Get(id));
        }

        // GET: /Customer/Edit/5

        // GET: /Customer/Delete/5

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Delete(int id)
        {
            // Deactivating the customer
            var model = new CustomerRepository();
            model.DeactivateCustomer(id);

            this.Session.Add("CustomerId", id);

            return this.RedirectToAction("Index");

            // return View();
        }

        // POST: /Customer/Delete/5

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        /// <summary>
        /// The delete job.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult DeleteJob(int id)
        {
            this.ViewBag.CustomerId = id;

            var job = new Job {JobId = Convert.ToInt32(this.Request.QueryString["JobId"])};
            var repository = new JobRepository();

            repository.Delete(job);

            return new RedirectResult("/Admin/Customer/Details/" + id);
        }

        /// <summary>
        /// The details.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Details(int id)
        {
            this.Session["CustomerId"] = id;

            var model = new DetailsViewModel();
            var view = model.GetDetails(id);

            return this.View(view);
        }

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Edit(int id)
        {
            return this.View(this._customersDataAccess.GetOneCustomer(id));
        }

        // POST: /Customer/Edit/5

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var customer = new Customer();

                this.TryUpdateModel(customer);

                if (this.ModelState.IsValid)
                {
                    this._customersDataAccess.EditOneCustomer(customer, id);
                }

                this.Session.Add("CustomerId", id);

                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        /// <summary>
        ///     The get age categories.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        [HttpGet]
        public ActionResult GetAgeCategories()
        {
            var model = new AgeCategoryModel();

            return this.PartialView("GetAgeCategories", model.GetAgeCategories());
        }

        /// <summary>
        /// The get age category definitions.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <param name="federationId">
        /// The federation id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GetAgeCategoryDefinitions(int sportId, int jobId, int customerId, int federationId)
        {
            this.ViewBag.JobId = jobId;
            this.ViewBag.CustomerId = customerId;
            this.ViewBag.SportId = sportId;
            this.SportId = sportId;

            this.Session.Add("CustomerId", customerId);
            this.Session.Add("FederationId", federationId);
            this.Session.Add("SportId", sportId);

            var model = new AgeCategoryDefinitionModel();

            return this.PartialView("getAgeCategoryDefinitions", model.GetDefineAgeCategoryViewBySportId(sportId));
        }

        /// <summary>
        /// The get job name.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GetJobName(int id)
        {
            var jobId = Convert.ToInt32(this.Request.QueryString["JobId"]);
            var ageCategoryId = Convert.ToInt32(this.Request.QueryString["AgeCategoryId"]);
            var jobs = new JobRepository();
            this.ViewBag.JobId = jobId;
            this.ViewBag.CustomerId = id;
            this.ViewBag.AgeCategoryId = ageCategoryId;

            if (this.Session["CustomerId"].ToString() == string.Empty)
            {
                this.Session.Add("CustomerId", id);
            }

            var customerJob = jobs.GetCustomerJobByJobId(jobId);

            var defineView = new DefineView();
            defineView.CustomerJob = customerJob;
            defineView.DetailsView = new DetailsView();
            
            return this.PartialView("DefineJob", defineView);
        }

        /// <summary>
        /// The get municipalities.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult GetMunicipalities(int id, FormCollection collection)
        {
            var model = new SelectionViewModel();
            var sportId = 0;

            if (collection["sportId"] != null)
            {
                if (int.TryParse(collection["sportId"], out sportId))
                {
                }
            }

            // We could try and move this into the if
            if (this.Session["SportId"] != null)
            {
                this._log.DebugFormat("Sport Id: {0}", this.Session["SportId"]);
                if (sportId > 0 && sportId > Convert.ToInt32(this.Session["SportId"]))
                {
                    this.Session["SportId"] = sportId;
                }
                else if (sportId == 0 && sportId != Convert.ToInt32(this.Session["SportId"]))
                {
                    sportId = Convert.ToInt32(this.Session["SportId"]);
                }
            }
            else
            {
                this.Session.Add("SportId", sportId);
            }

            int seasonId;
            if (int.TryParse(collection["seasonId"], out seasonId))
            {
            }

            if (this.Session["SeasonId"] != null)
            {
                if (seasonId > 0 && seasonId > Convert.ToInt32(this.Session["SeasonId"]))
                {
                    this.Session["SeasonId"] = seasonId;
                }
                else if (seasonId == 0 && seasonId != Convert.ToInt32(this.Session["SeasonId"]))
                {
                    seasonId = Convert.ToInt32(this.Session["SeasonId"]);
                }
            }
            else if (this.Session["SeasonId"] == null && seasonId > 0)
            {
                this.Session.Add("SeasonId", seasonId);
            }

            int ageCategoryId;
            if (int.TryParse(collection["ageCategoryId"], out ageCategoryId))
            {
            }

            if (this.Session["AgeCategoryId"] != null)
            {
                if (ageCategoryId > 0 && seasonId > Convert.ToInt32(this.Session["AgeCategoryId"]))
                {
                    this.Session["AgeCategoryId"] = ageCategoryId;
                }
                else if (ageCategoryId == 0 && ageCategoryId != Convert.ToInt32(this.Session["AgeCategoryId"]))
                {
                    ageCategoryId = Convert.ToInt32(this.Session["AgeCategoryId"]);
                }
            }
            else if (this.Session["AgeCategoryId"] == null && ageCategoryId > 0)
            {
                this.Session.Add("AgeCategoryId", ageCategoryId);
            }

            var jobId = id;

            // Adding selected season to session variable
            this.ViewBag.SportId = sportId;
            this.ViewBag.AgeCategoryId = ageCategoryId;
            this.ViewBag.SeasonId = seasonId;
            this.ViewBag.JobId = jobId;
            this.ViewBag.CustomerId = this.Session["CustomerId"];
            return this.View("GetMunicipalities", model.GetSelectionView(sportId, seasonId, jobId));
        }

        /// <summary>
        /// The get municipalities.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <param name="ageCategoryId">
        /// The age category id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="useRemoteData">
        /// The use remote data.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GetMunicipalities(int sportId, int customerId, int jobId, int ageCategoryId, int seasonId,
            bool useRemoteData = true)
        {
            this._log.Debug("CustomerId: " + customerId);
            this.ViewBag.CustomerId = customerId;
            this.ViewBag.SportId = sportId;
            this.ViewBag.JobId = jobId;
            this.ViewBag.AgeCategoryId = ageCategoryId;
            this.ViewBag.SeasonId = seasonId;

            this._log.Debug("I amhere");

            if (Convert.ToInt32(this.Session["CustomerId"]) == 0)
            {
                this.Session.Add("CustomerId", customerId);
            }

            if (Convert.ToInt32(this.Session["AgeCategoryId"]) == 0)
            {
                this.Session.Add("AgeCategoryId", ageCategoryId);
            }

            if (Convert.ToInt32(this.Session["SeasonId"]) == 0)
            {
                this.Session.Add("SeasonId", seasonId);
            }

            var model = new SelectionViewModel();
            this._log.DebugFormat("Season Id: {0}", seasonId);
            return this.View("GetMunicipalities", model.GetSelectionView(sportId, seasonId, jobId));
        }

        /// <summary>
        /// The get selected items.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="selectedItems">
        /// The selected items.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult GetSelectedItems(int id, string selectedItems, int seasonId, FormCollection collection)
        {
            var customerId = Convert.ToInt32(this.Session["CustomerId"]);

            this._log.Debug("selectedItems: " + selectedItems);
            this._log.Debug("SportsId: " + this.Session["SportId"]);
            this._log.Debug("JobId from form: " + collection["JobId"]);
            this._log.Debug("AgeCategoryFilterId: " + collection["AgeCategoryId"]);
            this._log.Debug("Id: " + id);
            this._log.Debug("CustomerId: " + customerId);

            if (Convert.ToInt32(this.Session["CustomerId"]) == 0 && id > 0)
            {
                this.Session.Add("CustomerId", id);
            }

            var sportId = Convert.ToInt32(this.Session["SportId"]);
            var federationId = Convert.ToInt32(this.Session["FederationId"]);

            if (federationId == 0)
            {
                // We need to get the federation Id based on sportId
                federationId = this.GetFederationBySportId(sportId)
                    .FederationId;
            }

            var ageCategoryId = Convert.ToInt32(collection["AgeCategoryId"]);
            var jobId = Convert.ToInt32(collection["Jobid"]);

            var municipalityModel = new MunicipalityModel();
            var selectedMunicipalities = municipalityModel.GetSelectedMunicipalities(collection["selectedItems"], jobId);

            // We are storing the selection (delete first, then insert)
            municipalityModel.InsertMunicipalities(selectedMunicipalities, jobId);

            var tournamentModel = new TournamentModel();

            this.ViewBag.SportId = sportId;
            this.ViewBag.JobId = jobId;
            this.ViewBag.AgeCategoryId = ageCategoryId;
            this.ViewBag.SeasonId = seasonId;
            this.ViewBag.CustomerId = id;

            return this.PartialView("GetSelectedItems",
                tournamentModel.GetTournamentsForView(selectedMunicipalities, jobId, ageCategoryId, federationId,
                    sportId, seasonId));
        }

        /// <summary>
        ///     The index.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult Index()
        {
            this._log.Debug("ActionResult index()");
            return this.View(this._customersDataAccess.GetAllCustomers());
        }

        /// <summary>
        /// The insert job.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult InsertJob(int id, FormCollection collection)
        {
            this.Session.Add("CustomerId", id);
            this.ViewBag.CustomerId = id;
            var customerJobName = Convert.ToString(collection["JobbNavn"]);

            var sportId = Convert.ToInt32(collection["sports"]);
            var ageCategoryId = Convert.ToInt32(collection["agecategorydefinition"]);

            var schedule = false;
            if (collection["schedule"] != null)
            {
                if (collection["schedule"].ToLower() == "on")
                {
                    schedule = true;
                }
            }

            var customerId = id;

            var jobRepository = new JobRepository();
            var job = new Job
            {
                CustomerId = customerId,
                JobName = customerJobName,
                SportId = sportId,
                AgeCategoryId = ageCategoryId,
                Schedule = schedule
            };

            var insertId = jobRepository.InsertOne(job);

            this.ViewBag.JobId = insertId;

            return new RedirectResult("/Admin/Customer/Details/" + id);
        }

        /// <summary>
        /// This method lists up the different jobs - that is the names of the jobs, not what they are containing.
        /// </summary>
        /// <param name="id">
        /// The job Id we use to get jobId informasjon
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ListJobs(int id)
        {
            this.Session.Add("CustomerId", id);
            this.ViewBag.CustomerId = id;

            var jobRepository = new JobRepository {CustomerId = id};

            return this.PartialView(new List<Job>(jobRepository.GetAll()));
        }

        /// <summary>
        ///     This controller is used to create a maintenance file for all customers
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult SendAllResults()
        {
            return this.View("SendAllResults");
        }

        /// <summary>
        /// The send all results.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult SendAllResults(FormCollection collection)
        {
            var startDate = collection["startDate"];
            var endDate = collection["endDate"];

            if (endDate == string.Empty)
            {
                endDate = DateTime.Today.ToLongDateString();
            }
            var customerModel = new CustomerModel();
            customerModel.GetTournamentsForAllCustomers(startDate, endDate);

            // Sending the user to the list of customers
            return this.RedirectToAction("Index", "Customer", new {area = "Admin"});
        }

        /// <summary>
        /// The send results.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult SendResult(int id, FormCollection collection)
        {
            var tournaments = new Tournaments();
            tournaments.CreateTournamentFile(id, collection);

            // Sending the user to the list of customers
            return this.RedirectToAction("Details", "Customer", new {area = "Admin", id = id});
        }

        /// <summary>
        /// The send results.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult SendResults(int id)
        {
            //// Creating the customer Model object
            // CustomerModel customerModel = new CustomerModel();

            //// Getting the Customer Tournaments
            // List<JobTournament> jobTournaments = customerModel.GetCustomerTournaments(id);

            //// Returning the view
            this.ViewBag.CustomerId = id;

            var customerModel = new CustomerModel();
            var customerForeignId = customerModel.GetCustomerForeignId(id);
            this.ViewBag.CustomerForeignId = customerForeignId;

            var repository = new JobRepository();
            return this.View("SendResult", repository.ShowJobs(id));
        }

        /// <summary>
        /// The show jobs.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ShowJobs(int id)
        {
            this.ViewBag.CustomerName = CustomerModel.GetCustomer(id)
                .Name;
            this.ViewBag.CustomerId = CustomerModel.GetCustomer(id)
                .CustomerId;

            var repository = new JobRepository();
            return this.View(repository.ShowJobs(id));
        }

        /// <summary>
        /// The show teams.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ShowTeams(int id)
        {
            //// Returning the view
            this.ViewBag.CustomerId = id;

            // Sending the user to the list of customers
            return this.RedirectToAction("Index");
        }

        /// <summary>
        /// The store tournaments.
        /// </summary>
        /// <param name="id">
        /// Customer id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="RedirectResult"/>.
        /// </returns>
        [HttpPost]
        public RedirectResult StoreTournaments(int id, FormCollection collection)
        {
            // Getting values from URL and session
            var sportId = Convert.ToInt32(collection["sportId"]);
            if (sportId == 0)
            {
                sportId = Convert.ToInt32(this.Session["SportId"]);

                if (sportId == 0)
                {
                    sportId = this.SportId;
                }
            }

            var jobId = Convert.ToInt32(collection["jobId"]);

            // Session variables
            var customerId = Convert.ToInt32(this.Session["customerId"]);
            var ageCategoryId = Convert.ToInt32(this.Session["ageCategoryId"]);
            var seasonId = Convert.ToInt32(this.Session["SeasonId"]);
            this._log.Debug("jobId: " + jobId);
            this._log.Debug("SportsId: " + sportId);
            this._log.Debug("CustomerId: " + customerId);
            this._log.Debug("SeasonId: " + seasonId);

            var model = new TournamentModel();

            // Transforming the list of selected tournaments into a list of tournament objects
            var selectedTournaments = model.NormalizeTournaments(collection["selectedTournaments"]);

            if (!string.IsNullOrWhiteSpace(collection["writtentournament"]))
            {
                // Let's check if the box contains , or ;
                var tournamentString = collection["writtentournament"];
                var tournaments = new List<string>();

                if (tournamentString.Contains(";"))
                {
                    tournaments.AddRange(tournamentString.Split(';'));
                }
                else if (tournamentString.Contains(","))
                {
                    tournaments.AddRange(tournamentString.Split(','));
                }
                else
                {
                    tournaments.Add(tournamentString);
                }

                foreach (var t in tournaments)
                {
                    var tournamentId = Convert.ToInt32(t);
                    if (tournamentId == 0)
                    {
                        continue;
                    }

                    this._log.DebugFormat("Inserting {0} from season {1}", tournamentId, seasonId);
                    var tournament = model.GetTournamentById(tournamentId, seasonId);
                    tournament.TournamentSelected = true;
                    tournament.SportId = sportId;

                    tournament.AgeCategoryId = ageCategoryId;
                    selectedTournaments.Add(tournament);

                    model.InsertTournament(tournament);
                }
            }

            // Insert the information in our database - if we have any tournaments that is
            if (selectedTournaments.Any())
            {
                model.InsertSelectedTournaments(selectedTournaments, jobId, ageCategoryId, customerId, sportId);
            }

            // Redirect the customer/user to the details web page
            return new RedirectResult("/Admin/Customer/Details/" + customerId);
        }

        /// <summary>
        /// The update job.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult UpdateJob(int id, FormCollection collection)
        {
            this.Session.Add("CustomerId", id);
            this.ViewBag.CustomerId = id;

            var jobId = Convert.ToInt32(collection["JobId"]);
            var sportId = Convert.ToInt32(collection["sports"]);
            var ageCategoryDefinition = Convert.ToInt32(collection["agecategorydefinition"]);

            var customerJobName = collection["JobbNavn"];
            var schedule = Convert.ToString(collection["schedule"]) == "on";

            var jobModel = new JobModel();

            jobModel.UpdateJob(jobId, customerJobName, schedule, sportId, ageCategoryDefinition);

            return new RedirectResult("/Admin/Customer/Details/" + id);
        }

        /// <summary>
        /// The view job.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ViewJob(int id)
        {
            var repository = new JobRepository();
            return this.View(repository.CreateCustomerJobs(id));
        }

        #endregion
    }
}