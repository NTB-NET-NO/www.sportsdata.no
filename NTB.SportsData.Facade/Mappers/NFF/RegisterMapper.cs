﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegisterMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The register mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NFF
{
    using Glue;

    using NTB.SportsData.Common;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The register mapper.
    /// </summary>
    public class RegisterMapper : BaseMapper<Domain.Classes.Match, Match>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Domain.Classes.Match, Match> mapper)
        {
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.MatchNumber, y => y.MatchNumber);
            mapper.Relate(x => x.AwayTeamGoal, y => y.AwayTeamGoals);
            mapper.Relate(x => x.HomeTeamGoal, y => y.HomeTeamGoals);
        }
    }
}