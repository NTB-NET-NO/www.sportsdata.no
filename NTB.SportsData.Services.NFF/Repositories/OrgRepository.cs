﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrgRepository.cs" company="NTB">
//   Copyright Norsk Telegrambyrå
// </copyright>
// <summary>
//   Defines the OrgRepository type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NFF.Repositories
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using NTB.SportsData.Services.NFF.Interfaces;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The org repository.
    /// </summary>
    public class OrgRepository : IOrgDataMapper
    {
        /// <summary>
        /// The service client.
        /// </summary>
        private readonly OrganizationServiceClient serviceClient = new OrganizationServiceClient();

        /// <summary>
        /// Initializes a new instance of the <see cref="OrgRepository"/> class.
        /// </summary>
        public OrgRepository()
        {
            if (this.serviceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                this.serviceClient.Close();
                this.serviceClient = null;
            }
            this.serviceClient = new OrganizationServiceClient();
            
            if (this.serviceClient.ClientCredentials != null)
            {
                this.serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                this.serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
            }
              
        }

        /// <summary>
        /// The get clubs.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Club> GetClubs()
        {
            var districts = this.serviceClient.GetDistricts();

            var clubs = new List<Club>();
            foreach (District district in districts)
            {
                var remoteClubs = this.serviceClient.GetClubsByDistrict(district.DistrictId, null).ToList();

                clubs.AddRange(remoteClubs);
            }
            return clubs;
        }
    }
}
