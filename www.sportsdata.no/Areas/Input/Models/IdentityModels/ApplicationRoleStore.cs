﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using NTB.SportsData.Data.DataMappers.Role;
using NTB.SportsData.Data.Interfaces.DataMappers;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Input.Models.IdentityModels
{
    public class ApplicationRoleStore : IQueryableRoleStore<Role, int>, IRoleStore<Role, int>, IDisposable {
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        private IRoleDataMapper _datamapper;
        

        private List<Role> _roles = new List<Role>();

        public Task CreateAsync(Role role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            return Task.Factory.StartNew(() =>
            {
                _datamapper = new RoleDataMapper();
                _datamapper.InsertRole(role);
            });
            
        }

        public Task UpdateAsync(Role role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            return Task.Factory.StartNew(() =>
            {
                _datamapper = new RoleDataMapper();
                _datamapper.UpdateRole(role);
            });
        }

        public Task DeleteAsync(Role role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            return Task.Factory.StartNew(() =>
            {
                _datamapper = new RoleDataMapper();
                _datamapper.DeleteRole(role);
            });
        }

        public Task<Role> FindByIdAsync(int roleId)
        {
            if (roleId == 0)
            {
                throw new NullReferenceException("roleId");
            }

            throw new NotImplementedException();
        }

        public Task<Role> FindByNameAsync(string roleName)
        {
            if (roleName.IsNullOrWhiteSpace())
            {
                throw new NullReferenceException("roleName");
            }

            _datamapper = new RoleDataMapper();
            var roles = _datamapper.GetRoles();

            return Task.Factory.StartNew(() =>
            {
                var foundRole = roles.SingleOrDefault(x => x.Name == roleName);

                return new Role
                {
                    Id = foundRole.Id,
                    Name = foundRole.Name
                };
            });
        }

        public void RemoveFromRole(ApplicationUser user, string roleId)
        {
            if (user == null)
            {
                throw new NullReferenceException("user");
            }

            if (roleId.IsNullOrWhiteSpace())
            {
                throw new NullReferenceException("roleId");
            }

            _datamapper = new RoleDataMapper();
            _datamapper.RemoveUserFromRole(user, roleId);
        }

        public IQueryable<Role> Roles
        {
            get
            {
                _datamapper = new RoleDataMapper();
                return _datamapper.GetRoles();
            }
            private set
            {
                _roles.AddRange(value);
            }
        }
    }
}