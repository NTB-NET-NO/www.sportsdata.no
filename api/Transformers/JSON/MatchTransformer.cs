﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchTransformer.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Transformers.JSON
{
    using System.Configuration;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Xsl;

    using Mvp.Xml.Exslt;

    /// <summary>
    ///     The match model.
    /// </summary>
    public class MatchTransformer
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the xslt file.
        /// </summary>
        public string XsltFile { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Method transforming from raw to JSON
        /// </summary>
        /// <param name="document">
        /// String containing the XML structure
        /// </param>
        /// <returns>
        /// String containing json structure
        /// </returns>
        public string TransformObject(string document)
        {
            var transformer = new XslCompiledTransform();
            var xsltArguments = new XsltArgumentList();
            xsltArguments.AddExtensionObject("http://exslt.org/dates-and-times", new ExsltDatesAndTimes());
            string xsltPath = ConfigurationManager.AppSettings["sportsmlxsltfilepath"];
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(document);

            transformer.Load(xsltPath + @"\" + this.XsltFile);

            var settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "\t",
                Encoding = Encoding.UTF8,
                OmitXmlDeclaration = false
            };

            using (var textWriter = new StringWriter())
            {
                var writer = new XmlTextWriter(textWriter);

                transformer.Transform(xmlDoc, xsltArguments, writer);

                return textWriter.ToString();
            }
        }

        #endregion
    }
}