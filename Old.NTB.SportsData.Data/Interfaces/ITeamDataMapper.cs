﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITeamDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TeamDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The TeamDataMapper interface.
    /// </summary>
    public interface ITeamDataMapper
    {
        /// <summary>
        /// The get tournament teams.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Team> GetTournamentTeams(Tournament tournament);
    }
}