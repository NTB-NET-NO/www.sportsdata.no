﻿using System.Collections.Generic;

namespace NTB.SportsData.Domain.Classes
{
    public class SeasonView
    {
        public int SportId { get; set; }
        public List<Season> Seasons { get; set; }
        public List<FederationDiscipline> FederationDisciplines { get; set; }
    }
}