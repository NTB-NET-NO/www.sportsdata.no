﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegisterBindingModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The register binding model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Models
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The register binding model.
    /// </summary>
    public class RegisterBindingModel
    {
        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the confirm password.
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}