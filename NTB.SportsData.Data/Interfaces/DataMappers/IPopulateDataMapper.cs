﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPopulateDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The PopulateDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The PopulateDataMapper interface.
    /// </summary>
    public interface IPopulateDataMapper
    {
        /// <summary>
        /// The get data base updated.
        /// </summary>
        /// <returns>
        /// The <see cref="DataBasePopulate"/>.
        /// </returns>
        DataBasePopulate GetDataBaseUpdated();
    }
}