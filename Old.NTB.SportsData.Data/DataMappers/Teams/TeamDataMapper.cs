﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The team data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.DataMappers.Teams
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Data.Interfaces.Common;
    using NTB.SportsData.Data.Interfaces.DataMappers;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The team data mapper.
    /// </summary>
    public class TeamDataMapper : IRepository<Team>, IDisposable, ITeamDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(TeamDataMapper));

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(Team domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertTeam", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", domainobject.TeamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamName", domainobject.TeamName));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return 1;
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<Team> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Team domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Team domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<Team> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Team"/>.
        /// </returns>
        public Team Get(int id)
        {
            var team = new Team();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetTeamById", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", id));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return team;
                    }

                    while (sqlDataReader.Read())
                    {
                        team.TeamId = id;
                        team.TeamName = sqlDataReader["TeamName"].ToString();
                        team.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return team;
            }
        }

        /// <summary>
        /// The delete team contact map.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="contactId">
        /// The contact id.
        /// </param>
        public void DeleteTeamContactMap(int teamId, int contactId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteTeamContactMap", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", teamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", contactId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert team contact map.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="contactId">
        /// The contact id.
        /// </param>
        public void InsertTeamContactMap(int teamId, int contactId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertTeamContactMap", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", teamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", contactId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert team roles.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="contactId">
        /// The contact id.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertTeamRoles(int teamId, int contactId)
        {
            throw new NotImplementedException();
        }
    }
}