﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The UserDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces
{
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The UserDataMapper interface.
    /// </summary>
    public interface IUserDataMapper
    {
        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="Customer"/>.
        /// </returns>
        Customer GetUser(string username, string password);
    }
}