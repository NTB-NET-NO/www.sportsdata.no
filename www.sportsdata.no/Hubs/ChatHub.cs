﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace NTB.SportsData.Web.Hubs
{
    using Microsoft.AspNet.SignalR.Hubs;

    [HubName("chatter")]
    public class ChatHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }

        public void ReceiveMessage(string message)
        {
            this.Clients.All.pushMessage(message);

        }
    }
}