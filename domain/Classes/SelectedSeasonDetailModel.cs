﻿using System.Collections.Generic;

namespace NTB.SportsData.Domain.Classes
{
    public class SelectedSeasonDetailModel
    {

        public Season Season { get; set; }

        public Organization Organization { get; set; }

        public List<Club> Clubs { get; set; }
        
    }
}
