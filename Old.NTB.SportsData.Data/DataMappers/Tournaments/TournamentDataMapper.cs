﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.DataMappers.Tournaments
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Data.Interfaces.Common;
    using NTB.SportsData.Data.Interfaces.DataMappers;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The tournament data mapper.
    /// </summary>
    public class TournamentDataMapper : IRepository<Tournament>, IDisposable, ITournamentDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentDataMapper));

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(Tournament domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertTournament", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentName", domainobject.TournamentName));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", domainobject.TournamentNumber));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return 1;
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertAll(List<Tournament> domainobject)
        {
            foreach (var tournament in domainobject)
            {
                this.InsertOne(tournament);
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<Tournament> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        public Tournament Get(int id)
        {
            var tournament = new Tournament();
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetTournamentById", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new Tournament();
                    }

                    while (sqlDataReader.Read())
                    {
                        tournament.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        tournament.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        tournament.SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]);
                        tournament.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        tournament.TournamentName = sqlDataReader["TournamentName"].ToString();
                        tournament.TournamentNumber = sqlDataReader["TournamentNumber"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return tournament;
            }
        }
    }
}