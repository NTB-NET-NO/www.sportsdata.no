﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContactPersonMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The contact person mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NFF
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The contact person mapper.
    /// </summary>
    internal class ContactPersonMapper : BaseMapper<OrganizationPerson, ContactPerson>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<OrganizationPerson, ContactPerson> mapper)
        {
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.SurName, y => y.LastName);
            mapper.Relate(x => x.RoleId, y => y.RoleId);
            mapper.Relate(x => x.RoleName, y => y.RoleName);
            mapper.Relate(x => x.MobilePhone, y => y.MobilePhone);
            mapper.Relate(x => x.TeamId, y => y.TeamId);
            mapper.Relate(x => x.TeamName, y => y.TeamName);
            mapper.Relate(x => x.Email, y => y.Email);
        }
    }
}