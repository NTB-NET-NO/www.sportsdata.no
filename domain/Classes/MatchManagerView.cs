﻿using System.Collections.Generic;

namespace NTB.SportsData.Domain.Classes
{
    public class MatchManagerView
    {
        public List<District> Districts { get; set; }

        public List<UserProfile> UserProfiles { get; set; }

        public Dictionary<District, List<Match>> DistrictMatches { get; set; }
    }
}