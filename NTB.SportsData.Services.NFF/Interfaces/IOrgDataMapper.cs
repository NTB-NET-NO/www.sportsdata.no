﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IOrgDataMapper.cs" company="NTB">
//   Copyright Norsk Telegrambyrå
// </copyright>
// <summary>
//   The OrgDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NFF.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The OrgDataMapper interface.
    /// </summary>
    public interface IOrgDataMapper
    {
        /// <summary>
        /// The get clubs.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Club> GetClubs();
    }
}
