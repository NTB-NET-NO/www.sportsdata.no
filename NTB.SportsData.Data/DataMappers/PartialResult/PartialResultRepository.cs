﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;

namespace NTB.SportsData.Data.DataMappers.PartialResult
{
    public class PartialResultRepository
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PartialResultRepository));

        public List<Domain.Classes.PartialResultType> GetPartialResultsForOrg(int orgId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {

                    var sqlCommand = new SqlCommand("SportsData_InsertPartialResults", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", orgId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var partialResults = new List<Domain.Classes.PartialResultType>();
                    while (sqlDataReader.Read())
                    {
                        var partialResult = new Domain.Classes.PartialResultType();

                        if (sqlDataReader["TypeId"] != DBNull.Value)
                        {
                            partialResult.TypeId = Convert.ToInt32(sqlDataReader["TypeId"]);
                        }

                        if (sqlDataReader["TypeName"] != DBNull.Value)
                        {
                            partialResult.TypeName = sqlDataReader["TypeId"].ToString();
                        }

                        if (sqlDataReader["ActivityId"] != DBNull.Value)
                        {
                            partialResult.ActivityId = Convert.ToInt32(sqlDataReader["ActivityId"]);
                        }

                        if (sqlDataReader["OrgId"] != DBNull.Value)
                        {
                            partialResult.OrgId = Convert.ToInt32(sqlDataReader["OrgId"]);
                        }

                        if (sqlDataReader["Sorting"] != DBNull.Value)
                        {
                            partialResult.Sorting = Convert.ToInt32(sqlDataReader["Sorting"]);
                        }

                        partialResults.Add(partialResult);
                    }

                    return partialResults;
                }
                catch (SqlException exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertPartialResult(Domain.Classes.PartialResultType partialResult)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_InsertPartialResults", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@TypeId", partialResult.TypeId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TypeName", partialResult.TypeName));
                    sqlCommand.Parameters.Add(new SqlParameter("@ActivityId", partialResult.ActivityId));
                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", partialResult.OrgId));
                    sqlCommand.Parameters.Add(new SqlParameter("@Sorting", partialResult.Sorting));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (SqlException exception)
                {
                    Logger.Error(exception);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}