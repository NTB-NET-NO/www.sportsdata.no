﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The user data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Data.DataMappers.User.Api
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using log4net;

    using NTB.SportsData.Data.Interfaces;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The user data mapper.
    /// </summary>
    public class UserDataMapper : IUserDataMapper
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(UserDataMapper));

        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="Customer"/>.
        /// </returns>
        public Customer GetUser(string username, string password)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("SportsDataApi_GetUserByCredentials", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@Username", username));
                    sqlCommand.Parameters.Add(new SqlParameter("@Password", password));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    // var customer = new Customer();
                    while (sqlDataReader.Read())
                    {
                        return new Customer
                                   {
                                       CustomerId = Convert.ToInt32(sqlDataReader["Customerid"]), 
                                       Name = sqlDataReader["CustomerName"].ToString()
                                   };
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return null;
        }
    }
}