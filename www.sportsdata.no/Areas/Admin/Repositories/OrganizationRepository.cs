using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Web.Areas.Admin.Interfaces;

namespace NTB.SportsData.Web.Areas.Admin.Repositories
{
    /// <summary>
    ///     The organization repository.
    /// </summary>
    public class OrganizationRepository : IRepository<Organization>, IDisposable, IOrganizationDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(OrganizationRepository));

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get organization by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="Organization"/>.
        /// </returns>
        public Organization GetOrganizationBySportId(int sportId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_GetOrganizationBySportId", sqlConnection)
                {
                    CommandType =
                        CommandType
                            .StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@sportId", sportId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                while (sqlDataReader.Read())
                {
                    var organization = new Organization();

                    if (sqlDataReader["OrgId"] != DBNull.Value)
                    {
                        organization.OrganizationId = Convert.ToInt32(sqlDataReader["OrgId"].ToString());
                    }

                    if (sqlDataReader["SingleSport"] != DBNull.Value)
                    {
                        organization.SingleSport = Convert.ToInt32(sqlDataReader["SingleSport"].ToString());
                    }

                    if (sqlDataReader["TeamSport"] != DBNull.Value)
                    {
                        organization.TeamSport = Convert.ToInt32(sqlDataReader["TeamSport"].ToString());
                    }

                    if (sqlDataReader["SportId"] != DBNull.Value)
                    {
                        organization.SportId = Convert.ToInt32(sqlDataReader["SportId"].ToString());
                    }

                    organization.Sport = sqlDataReader["Sport"].ToString();
                    organization.OrganizationName = sqlDataReader["OrgName"].ToString();

                    return organization;
                }
            }

            return null;
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domain object.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(Organization domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_InsertSport", sqlConnection)
                    {
                        CommandType =
                            CommandType
                                .StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", domainobject.OrganizationId));
                    sqlCommand.Parameters.Add(new SqlParameter("@OrgName", domainobject.OrganizationName));
                    sqlCommand.Parameters.Add(new SqlParameter("@OrgUserId", domainobject.OrganizationUserId));
                    sqlCommand.Parameters.Add(new SqlParameter("@OrgNameShort", domainobject.OrganizationNameShort));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportName", domainobject.Sport));
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamSport", domainobject.TeamSport));
                    sqlCommand.Parameters.Add(new SqlParameter("@SingleSport", domainobject.SingleSport));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    Logger.InfoFormat("Organization {0} inserted successfully", domainobject.OrganizationName);
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return 1;
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domain object.
        /// </param>
        public void InsertAll(List<Organization> domainobject)
        {
            foreach (var org in domainobject)
            {
                this.InsertOne(org);
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domain object.
        /// </param>
        public void Update(Organization domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_UpdateOrganizationById", sqlConnection)
                    {
                        CommandType
                            =
                            CommandType
                                .StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", domainobject.OrganizationId));
                    sqlCommand.Parameters.Add(new SqlParameter("@OrgName", domainobject.OrganizationName));
                    sqlCommand.Parameters.Add(new SqlParameter("@OrgNameShort", domainobject.OrganizationNameShort));
                    sqlCommand.Parameters.Add(new SqlParameter("@OrgUserId", domainobject.OrganizationUserId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@Sport", domainobject.Sport));
                    sqlCommand.Parameters.Add(new SqlParameter("@SingleSport", domainobject.SingleSport));
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamSport", domainobject.TeamSport));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domain object.
        /// </param>
        public void Delete(Organization domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_DeleteOrganization", sqlConnection)
                    {
                        CommandType =
                            CommandType
                                .StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", domainobject.OrganizationId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    Logger.InfoFormat("Organization, with Id {0}, deleted successfully", domainobject.OrganizationId);
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Organization> GetAll()
        {
            var organizations = new List<Organization>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetOrganizations", sqlConnection)
                    {
                        CommandType =
                            CommandType
                                .StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {
                        var organization = new Organization();
                        if (sqlDataReader["OrgId"] != DBNull.Value)
                        {
                            organization.OrganizationId = Convert.ToInt32(sqlDataReader["OrgId"].ToString());
                        }

                        organization.OrganizationName = sqlDataReader["OrgName"].ToString();
                        organization.OrganizationNameShort = sqlDataReader["OrgNameShort"].ToString();

                        organizations.Add(organization);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return organizations.AsQueryable();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Organization"/>.
        /// </returns>
        public Organization Get(int id)
        {
            var organization = new Organization();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetOrganizationsById", sqlConnection)
                    {
                        CommandType =
                            CommandType
                                .StoredProcedure
                    };
                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["OrgId"] != DBNull.Value)
                        {
                            organization.OrganizationId = Convert.ToInt32(sqlDataReader["OrgId"].ToString());
                        }

                        if (sqlDataReader["OrgUserId"] != DBNull.Value)
                        {
                            organization.OrganizationUserId = Convert.ToInt32(sqlDataReader["OrgUserId"].ToString());
                        }

                        if (sqlDataReader["SingleSport"] != DBNull.Value)
                        {
                            organization.SingleSport = Convert.ToInt32(sqlDataReader["SingleSport"].ToString());
                        }

                        if (sqlDataReader["TeamSport"] != DBNull.Value)
                        {
                            organization.TeamSport = Convert.ToInt32(sqlDataReader["TeamSport"].ToString());
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            organization.SportId = Convert.ToInt32(sqlDataReader["SportId"].ToString());
                        }

                        if (sqlDataReader["Description"] != DBNull.Value)
                        {
                            organization.Description = sqlDataReader["Description"].ToString(); 
                        }

                        organization.Sport = sqlDataReader["Sport"].ToString();
                        organization.OrganizationName = sqlDataReader["OrgName"].ToString();
                        organization.OrganizationNameShort = sqlDataReader["OrgNameShort"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return organization;
        }
    }
}