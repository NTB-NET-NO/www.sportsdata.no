﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MunicipalityMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The municipality mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The municipality mapper.
    /// </summary>
    public class MunicipalityMapper : BaseMapper<Region, Municipality>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Region, Municipality> mapper)
        {
            mapper.Relate(x => x.RegionId, y => y.MunicipalityId);
            mapper.Relate(x => x.RegionName, y => y.MuniciaplityName);
        }
    }
}