﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Services.NFF.NFFProdService;

namespace NTB.SportsData.Services.NFF.Interfaces
{
    public interface IClubDataMapper
    {
        List<Club> GetTournamentClubs(int tournamentId);
        List<Club> GetClubsByMunicipality(int municipalityId);
        Club GetClubPersons(int clubId);
    }
}
