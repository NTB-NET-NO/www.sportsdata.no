﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentsController.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   When you want to get more information about the different tournaments, you use the methods under Tournaments.
//   In the list below you will find more information about the different methods under this controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Web.Http;
    using System.Web.Http.Description;
    using System.Xml;
    using System.Xml.Linq;

    using log4net;

    using NTB.SportsData.Api.Interfaces;
    using NTB.SportsData.Api.Models.Tournaments;
    using NTB.SportsData.Api.Models.XML;
    using NTB.SportsData.Api.Serializers;
    using NTB.SportsData.Api.Transformers.JSON;
    using NTB.SportsData.Api.Transformers.XML;
    using NTB.SportsData.Data.Matches;
    using NTB.SportsData.Data.Team;
    using NTB.SportsData.Data.Tournaments;
    using NTB.SportsData.Domain.Classes;

    using MatchModel = NTB.SportsData.Api.Transformers.XML.MatchTransformer;

    /// <summary>
    ///     When you want to get more information about the different tournaments, you use the methods under Tournaments.
    ///     In the list below you will find more information about the different methods under this controller.
    /// </summary>
    [Authorize]
    [RoutePrefix("api/tournaments")]
    public class TournamentsController : ApiController
    {
        #region Static Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger("ApiFileAppender");

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="TournamentsController" /> class.
        ///     Constructor for Tournament Controller
        /// </summary>
        public TournamentsController()
        {
            Logger.Debug("In Tournament Controller");
        }

        #endregion

        // [Route("Get/{id:int}")]
        #region Public Methods and Operators

        /// <summary>
        /// This method is used to get information about tournaments the customer subscribes to. The content in the returned
        ///     data can be used to get information when using
        ///     the other methods in this API.<br/>
        ///     We recommend you store this information in some database or as a file and reference this file later. We also recommend you update the database or file from
        ///     time to time
        /// </summary>
        /// <param name="id">
        /// When using this method you are using the Customer ID which you have received from NTB.<br/> 
        /// If you have not received such an ID, please do get in touch with us to get your Customer ID
        /// </param>
        /// <returns>
        /// Depending on parameters this method will return either JSON or XML
        /// </returns>
        /// <responsedescription>
        /// Depending on your header information, this method will return either JSON or XML string. <br/>
        /// The information in the reponse can be used to get more information on the Tournament. <br/>
        /// We recommend you store the information from this response either in a database, or in a file.
        /// </responsedescription>
        /// <code type="xml">
        /// <sports-event>
        ///         <event-metadata event-key="133990" event-name="2. div Kvinner avd 01" start-date-time="2013-04-01T00:00:00" end-date-time="2012-11-15T00:00:00">
        ///             <sports-content-codes>
        ///                 <sports-content-code code-type="sport" code-name="Fotball" code-key="16"/>
        ///             </sports-content-codes>
        ///         </event-metadata>
        ///     </sports-event>
        ///  </code>
        /// <code type="json">
        /// {"events": [
        ///     {"1 div menn veteran 13/14": {
        ///         "competitionid": 319975,
        ///         "competitionname": "1 div menn veteran 13/14",
        ///         "competitionstartdate": "2013-10-22T00:00:00",
        ///         "competitionenddate": "2014-10-22T00:00:00",
        ///         "sportname": "soccer",
        ///         "sportcode": 16
        ///     }},
        ///     {"1. div Kvinner": {
        ///         "competitionid": 138981,
        ///         "competitionname": "1. div Kvinner",
        ///         "competitionstartdate": "2014-01-01T00:00:00",
        ///         "competitionenddate": "2014-12-31T00:00:00"
        ///         "sportname" : "handball",
        ///         "sportcode" : 23
        ///     }}
        /// ]}
        /// </code>
        [HttpGet]
        public HttpResponseMessage GetCustomerTournaments(int id)
        {
            Logger.Info("Getting all tournaments for customer");
            var accept = this.Request.Headers.Accept.ToString();

            // var tournaments = new List<Tournament>();
            var tournaments = new TournamentContext().GetAll(id) ?? new List<Tournament>();

            var serializer = new ObjectSerializer();

            var xmlContent = serializer.SerializeObject(tournaments.ToList());

            ITransformer model;
            if (accept == "application/json")
            {
                Logger.Info("Generating JSON");
                model = new CompetitionTransformer { XsltFile = "sportsinjson-competitions.xsl" };
            }
            else
            {
                Logger.Info("Generating XML");
                model = new TournamentTransformer { XsltFile = "sportsml-tournament.xsl" };
            }

            var content = model.TransformObject(xmlContent);

            if (accept == "text/xml")
            {
                content = content.Replace("utf-16", "utf-8");
            }

            return new HttpResponseMessage { Content = new StringContent(content, Encoding.UTF8, accept) };
        }

        /// <summary>
        /// Get more information about the tournament by using its Id.
        /// </summary>
        /// <param name="id">
        /// Integer value to be used to get tournament information.
        /// </param>
        /// <returns>
        /// Returns information about the tournament
        /// </returns>
        /// <responsedescription>
        /// Depending on your header information, this method will return either JSON or XML string. <br/>
        /// The information in the reponse contains information about the tournament.<br/>
        /// </responsedescription>
        /// <code type="xml">
        /// <sports-content>
        ///  <sports-event>
        ///    <event-metadata event-key="144516" event-name="M39 Menn veteran 7-er" start-date-time="2015-01-01T00:00:00" end-date-time="2015-12-31T00:00:00">
        ///      <sports-content-codes>
        ///        <sports-content-code code-type="sport" code-name="Fotball" code-key="16"/>
        ///      </sports-content-codes>
        ///    </event-metadata>
        ///  </sports-event>
        /// </sports-content>
        /// </code>
        /// <code type="json">
        /// {"events": 
        ///     {
        ///     "M39 Menn veteran 7-er":
        ///         {
        ///             "competitionid": 144516, 
        ///             "competitionname": "M39 Menn veteran 7-er",
        ///             "competitionstartdate": "2015-01-01T00:00:00",
        ///             "competitionenddate": "2015-12-31T00:00:00"
        ///         }
        ///     }
        /// }
        /// </code>
        [HttpGet]
        public HttpResponseMessage GetTournamentById(int id)
        {
            Logger.Info("Getting tournament number " + id);
            var accept = this.Request.Headers.Accept.ToString();

            var contentObject = new TournamentContext().Get(id);

            var xmlContent = this.DataSerializer(contentObject);

            var model = GetTransformationModel(accept);

            model.XsltFile = string.Empty; // This will throw an exception...

            // Making sure we set the correct xslt-files
            if (accept == "application/json")
            {
                Logger.Info("Generating JSON");
                model.XsltFile = "sportsinjson-competition.xsl";
            }
            else
            {
                Logger.Info("Generating XML");
                model.XsltFile = "sportsml-tournaments.xsl";
            }

            var contentString = model.TransformObject(xmlContent);

            if (accept == "text/xml")
            {
                contentString = contentString.Replace("utf-16", "utf-8");
            }

            return new HttpResponseMessage { Content = new StringContent(contentString, Encoding.UTF8, accept) };
        }

        /// <summary>
        /// Get matches for the selected tournament
        /// </summary>
        /// <param name="id">
        /// In order to get all the matches for tournament, you have to provide the Tournament Id. 
        /// </param>
        /// <returns>
        /// Depending on your input, either a JSON array of matches or a SportsML-XML-structure
        /// </returns>
        /// <responsedescription>
        /// Depending on your header information, this method will return either JSON or XML string. <br/>
        /// We recommend you store the information from this response either in a database, or in a file.<br/>
        /// When using this method, you can filter the output using either the eventstartdate parameter or
        /// use the eventstatus parameter.<br/>
        /// The event Status parameter will either contain <strong>post-event</strong> or <strong>pre-event.</strong>
        /// </responsedescription>
        /// <code type="xml">
        /// <sports-content>
        ///    <tournament>
        ///        <tournament-metadata tournament-key="326926" tournament-name="2.divisjon Kvinner avd.04">
        ///            <sports-content-codes>
        ///                <sports-content-code code-type="sport" code-name="Håndball"/>
        ///            </sports-content-codes>
        ///        </tournament-metadata>
        ///        <tournament-division>
        ///            <tournament-round round-number="12">
        ///                <sports-event>
        ///                    <event-metadata event-key="5003777" event-name="Home team - Away team" start-date-time="2014-12-11T19:00:00+0100"/>
        ///                    <team>
        ///                        <team-metadata alignment="home" team-key="NHF225017">
        ///                            <name full="Home team"/>
        ///                        </team-metadata>
        ///                        <team-stats score="0" event-outcome="tie"/>
        ///                    </team>
        ///                    <team>
        ///                        <team-metadata alignment="away" team-key="NHF225453">
        ///                            <name full="Away team"/>
        ///                        </team-metadata>
        ///                        <team-stats score="0" event-outcome="tie"/>
        ///                    </team>
        ///                </sports-event>
        ///            </tournament-round>
        ///        </tournament-division>
        ///    </tournament>
        /// </sports-content>
        /// </code>
        /// <code type="json">
        /// {
        ///  "events": [
        ///    {
        ///      "competitionid": 326926,
        ///      "competitionname": "2.divisjon Kvinner avd.04",
        ///      "competitionround": 12,
        ///      "eventid": 5003777,
        ///      "eventname": "Bjørnar - Kjøkkelvik",
        ///      "eventstartdate": "2014-12-11T19:00:00",
        ///      "eventvenuename": "",
        ///      "eventstatus": "finished",
        ///      "eventoutcome": "tie",
        ///      "teams": [
        ///        {
        ///          "name": "Bjørnar",
        ///          "teamid": 225017,
        ///          "score": 0
        ///        },
        ///        {
        ///          "name": "Kjøkkelvik",
        ///          "teamid": 225453,
        ///          "score": 0
        ///        }
        ///      ]
        ///    }
        ///  ]
        /// }
        /// </code> j
        [HttpGet]
        public HttpResponseMessage GetTournamentMatches(int id)
        {
            Logger.Info("Getting matches for tournament");
            var accept = this.Request.Headers.Accept.ToString();

            // IEnumerable<Match> matches = new MatchContext().GetAll(id);
            var tournament = new TournamentContext().Get(id);

            var remoteDataMapper = new MatchRemoteDataMapper();
            var matches = remoteDataMapper.GetMatchesByTournamentId(tournament);

            var matchesList = new List<Match>();

            foreach (var match in matches)
            {
                var now = DateTime.Now;
                if (match.TournamentId > 0)
                {
                    if (now > match.MatchDate)
                    {
                        match.Played = true;
                    }

                    matchesList.Add(match);
                    continue;
                }

                var m = match;

                m.TournamentId = tournament.TournamentId;
                m.TournamentName = tournament.TournamentName;
                m.SportId = tournament.SportId;
                m.DistrictId = tournament.DistrictId;
                m.DistrictName = tournament.DisciplineName;

                if (match.MatchStartTime > 0)
                {
                    var matchString = match.MatchStartTime.ToString();
                    if (matchString.Length < 4)
                    {
                        matchString = "0" + matchString;
                    }

                    var dt = DateTime.ParseExact(matchString, "HHmm", CultureInfo.CurrentCulture);
                    var timestring = dt.ToString("HH:mm:ss");
                    var shortDatestring = match.MatchDate.ToShortDateString();

                    var combinedMatchDateString = shortDatestring + " " + timestring;

                    m.MatchDate = DateTime.Parse(combinedMatchDateString);
                }

                // m.MatchDate.AddHours(match.MatchStartTime);
                if (now > m.MatchDate)
                {
                    m.Played = true;
                }

                matchesList.Add(m);
            }

            matches = matchesList;

            var model = new MatchModel();

            var xmlContent = model.SerializeObject(matches.ToList());

            string content;
            if (accept == "application/json")
            {
                Logger.Info("Generating JSON");
                var jsonTransformer = new Transformers.JSON.MatchTransformer() { XsltFile = "sportsinjson-matches.xsl" };
                content = jsonTransformer.TransformObject(xmlContent);
            }
            else
            {
                Logger.Info("Generating XML");

                // content = // model.TransformObject(xmlContent);
                var xmlTransformer = new Transformers.XML.MatchTransformer { XsltFile = "sportsml-matches.xsl" };

                content = xmlTransformer.TransformObject(xmlContent, accept);
            }

            if (accept == "text/xml")
            {
                content = content.Replace("utf-16", "utf-8");
            }

            return new HttpResponseMessage { Content = new StringContent(content, Encoding.UTF8, accept) };
        }

        /// <summary>
        /// This method returns the current tournament table for the tournament. 
        /// </summary>
        /// <param name="id">
        /// Use the Tournament Id for the tournament you want to get the tournament table for.
        /// </param>
        /// <returns>
        /// Either XML or JSON based on the header parameter.
        /// <responsedescription>
        /// Depending on your header information, this method will return either JSON or XML string. <br/>
        /// </responsedescription>
        /// </returns>
        /// <code type="xml">
        /// <sports-content>
        ///    <tournament>
        ///        <tournament-metadata tournament-key="144516" tournament-name="M39 Menn veteran 7-er"/>
        ///        <tournament-division>
        ///            <standing>
        ///                <standing-metadata alignment-scope="events-all"/>
        ///                <team>
        ///                    <team-metadata team-key="171286">
        ///                        <name full="Rygge "/>
        ///                    </team-metadata>
        ///                    <team-stats alignment-scope="events-all">
        ///                        <outcome-totals events-played="0" wins="0" losses="0" ties="0" standing-points="0"/>
        ///                        <rank>1</rank>
        ///                    </team-stats>
        ///                    <team-stats alignment-scope="events-home">
        ///                        <outcome-totals events-played="0" wins="0" losses="0" ties="0"/>
        ///                    </team-stats>
        ///                    <team-stats alignment-scope="events-away">
        ///                        <outcome-totals events-played="0" wins="0" losses="0" ties="0"/>
        ///                    </team-stats>
        ///                </team>
        ///            </standing>
        ///        </tournament-division>
        ///    </tournament>
        /// </sports-content>
        /// </code>
        /// <code type="json">
        /// {
        ///  "standing": {
        ///    "competitionname": "M39 Menn veteran 7-er",
        ///    "competitionid": 144516,
        ///    "generateddate": "2015-01-01",
        ///    "teams": [
        ///      {
        ///        "teamid": 171286,
        ///        "teamname": "Rygge ",
        ///        "rank": 1,
        ///        "totalplayed": 0,
        ///        "totalwins": 0,
        ///        "totalties": 0,
        ///        "totallosses": 0,
        ///        "totalpoints": 0,
        ///        "homeplayed": 0,
        ///        "homewins": 0,
        ///        "hometies": 0,
        ///        "homelosses": 0,
        ///        "awayplayed": 0,
        ///        "awaywins": 0,
        ///        "awayties": 0,
        ///        "awaylosses": 0
        ///      }
        ///    ]
        ///  }
        /// }
        /// </code>
        [HttpGet]
        public HttpResponseMessage GetTournamentStanding(int id)
        {
            Logger.Info("Getting tournament Standing " + id);
            var accept = this.Request.Headers.Accept.ToString();

            var tournamentDataMapper = new TournamentDataMapper();
            var tournament = tournamentDataMapper.GetTournamentById(id);

            IObjectSerializer<Tournament> tournamentSerializer = new TournamentSerializer();
            var xmlTournament = tournamentSerializer.SerializeObject(tournament);

            var xmlDocument = XDocument.Parse(xmlTournament);
            var tournamentElement = xmlDocument.Descendants("SportsData").FirstOrDefault();
            var generatedElement = new XElement("DocumentGenerated", new XAttribute("value", DateTime.Now));
            var sportsdataElement = xmlDocument.Element("SportsData");
            if (sportsdataElement != null)
            {
                sportsdataElement.Add(generatedElement);
            }

            var remoteDataMapper = new TournamentRemoteDataMapper();
            var standings = remoteDataMapper.GetTeamResultStanding(tournament);

            IObjectSerializer<TeamResult> serializer = new TournamentTableSerializer();
            var xmlContent = serializer.SerializeObjects(standings);

            var xmlTable = XDocument.Parse(xmlContent);
            var tournamentTableElement = xmlTable.Descendants("TournamentTables").FirstOrDefault();

            if (tournamentElement != null)
            {
                tournamentElement.Add(tournamentTableElement);

                var combinedDocument = XDocument.Parse(tournamentElement.ToString());

                var builder = new StringBuilder();
                using (TextWriter writer = new StringWriter(builder))
                {
                    combinedDocument.Save(writer);
                }

                xmlContent = builder.ToString().Replace("utf-16", "utf-8");
            }

            string content;

            if (accept == "application/json")
            {
                Logger.Info("Generating JSON");
                var jsonTransformer = new Transformers.JSON.TournamentTableTransformer() { XsltFile = "sportsinjson-standings.xsl" };
                content = jsonTransformer.TransformObject(xmlContent);
            }
            else
            {
                Logger.Info("Generating XML");

                if (standings == null)
                {
                    return null;
                }

                var xmlTransformer = new Transformers.XML.TournamentTableTransformer();
                xmlTransformer.XsltFile = "sportsml-standings.xsl";

                content = xmlTransformer.TransformObject(xmlContent);
            }

            if (accept == "text/xml")
            {
                content = content.Replace("utf-16", "utf-8");
            }

            return new HttpResponseMessage { Content = new StringContent(content, Encoding.UTF8, accept) };
        }

        /// <summary>
        /// Get a list of teams in the tournament from the specified tournament
        /// </summary>
        /// <param name="id">
        /// Integer value to tell which tournament we are to get data from
        /// </param>
        /// <returns>
        /// List of teams
        /// </returns>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage GetTournamentTeamsAdvanced(int id)
        {
            Logger.Info("Getting Tournament Teams Advanced " + id);
            var accept = this.Request.Headers.Accept.ToString();

            var tournamentDataMapper = new TournamentDataMapper();
            var tournament = tournamentDataMapper.GetTournamentById(id);

            var teamRemoteDataMapper = new TeamRemoteDataMapper();

            var teams = teamRemoteDataMapper.GetTournamentTeams(tournament);
            IObjectSerializer<Team> serializer = new TournamentTeamSerializer();
            var xmlContent = serializer.SerializeObjects(teams);

            string content;

            if (accept == "application/json")
            {
                Logger.Info("Generating JSON");
                var jsonTransformer = new Transformers.JSON.TournamentTeamTransformer() { XsltFile = "sportsinjson-standings.xsl" };
                content = jsonTransformer.TransformObject(xmlContent);
            }
            else
            {
                Logger.Info("Generating XML");

                if (teams == null)
                {
                    return null;
                }

                var xmlTransformer = new Transformers.XML.TournamentTeamTransformer();
                xmlTransformer.XsltFile = "sportsml-standings.xsl";

                content = xmlTransformer.TransformObject(xmlContent);
            }

            if (accept == "text/xml")
            {
                content = content.Replace("utf-16", "utf-8");
            }

            return new HttpResponseMessage { Content = new StringContent(content, Encoding.UTF8, accept) };
        }

        /// <summary>
        ///     Hello Message is just a test method
        /// </summary>
        /// <returns>HttpResponseMessage containing bogus information</returns>
        [HttpGet]
        public HttpResponseMessage HelloMessage()
        {
            var xmlDoc = new XmlDocument();
            XmlNode docNode = xmlDoc.CreateXmlDeclaration("1.0", "utf8", string.Empty);
            xmlDoc.AppendChild(docNode);

            var xmlRootElement = xmlDoc.CreateElement("hello");
            var xmlMainElement = xmlDoc.CreateElement("message");
            var xmlText = xmlDoc.CreateTextNode("Hello from SportsData Api");
            xmlMainElement.AppendChild(xmlText);
            xmlRootElement.AppendChild(xmlMainElement);

            xmlDoc.AppendChild(xmlRootElement);

            return new HttpResponseMessage { Content = new StringContent(xmlDoc.OuterXml, Encoding.Unicode, "application/xml") };
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get the model to be used to transform the data
        /// </summary>
        /// <param name="accept">
        /// Request Header Accept string
        /// </param>
        /// <returns>
        /// model object to be used to transform the serialized data
        /// </returns>
        private static ITransformer GetTransformationModel(string accept)
        {
            ITransformer model;
            if (accept == "application/json")
            {
                Logger.Info("Generating JSON");
                model = new CompetitionTransformer();
            }
            else
            {
                Logger.Info("Generating XML");
                model = new TournamentTransformer();
            }

            return model;
        }

        /// <summary>
        /// The data serializer.
        /// </summary>
        /// <param name="content">
        /// The content.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string DataSerializer(object content)
        {
            var serializer = new ObjectSerializer { SerializableObject = content };

            return serializer.Serializer();
        }

        #endregion
    }
}