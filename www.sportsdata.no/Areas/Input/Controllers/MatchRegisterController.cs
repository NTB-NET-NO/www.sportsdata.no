﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Threading;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using log4net;
using Microsoft.AspNet.Identity;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Utilities;
using NTB.SportsData.Web.Areas.Input.Models;
using NTB.SportsData.Web.Areas.Input.Models.IdentityModels;

namespace NTB.SportsData.Web.Areas.Input.Controllers
{
    /// <summary>
    /// The match register controller.
    /// </summary>
    [Authorize]
    public class MatchRegisterController : Controller
    {
        /// <summary>
        ///     The _log.
        /// </summary>
        private readonly ILog _log = LogManager.GetLogger(typeof(MatchRegisterController));

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchRegisterController"/> class.
        /// </summary>
        public MatchRegisterController()
            : this(new ApplicationUserManager(new ApplicationUserStore()))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchRegisterController"/> class.
        /// </summary>
        /// <param name="userManager">
        /// The user manager.
        /// </param>
        public MatchRegisterController(ApplicationUserManager userManager)
        {
            this.UserManager = userManager;
        }

        /// <summary>
        /// Gets the user manager.
        /// </summary>
        public ApplicationUserManager UserManager { get; private set; }

        // GET: /MatchRegister/

        /// <summary>
        /// The unregistered matches.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult UnregisteredMatches()
        {
            var username = this.User.Identity.GetUserName();
            var userid = this.User.Identity.GetUserId();

            // var user = UserManager.Users.SingleOrDefault(u => u.UserName == User.Identity.GetUserName());
            if (username == null)
            {
                return this.RedirectToAction("Index", "Home");
            }

            Thread.CurrentThread.CurrentCulture = new CultureInfo("nb-NO");

            var modelView = new InputViewModel();
            modelView.GetUnregisteredMatches();
            return this.View(modelView);
        }

        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            var username = this.User.Identity.GetUserName();
            var userid = this.User.Identity.GetUserId();

            // var user = UserManager.Users.SingleOrDefault(u => u.UserName == User.Identity.GetUserName());
            if (username == null)
            {
                return this.RedirectToAction("Index", "Home");
            }

            Thread.CurrentThread.CurrentCulture = new CultureInfo("nb-NO");

            var modelView = new InputViewModel();

            modelView.GetUserMatches(new Guid(userid));

            // We should check if any of the matches have resuls or not
            return this.View(modelView);
        }

        // GET: /MatchRegister/Details/5

        /// <summary>
        /// The details.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Details(int id)
        {
            return this.View();
        }

        /// <summary>
        /// The get unregistered matches.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        [HttpGet]
        public int GetUnregisteredMatches()
        {
            // Creating the Model
            var model = new MatchRegisterModels();

            return model.GetMatchesUnregisterableCount();
        }

        /// <summary>
        /// The register match.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        [HttpPost]
        public void RegisterMatch(FormCollection collection)
        {
            // Get the values from the form / ajax call
            var model = new MatchRegisterModels();
            model.RegisterMatchScore(collection);
        }

        /// <summary>
        /// The pub nub receiver.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult PubNubReceiver(FormCollection collection)
        {
            var model = new MatchRegisterModels();
            var sportEvent = model.ModelMatch(collection);

            model.StoreMatchScore(sportEvent);

            return this.Json(sportEvent);
        }

        /// <summary>
        /// The store match result.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult StoreMatchResult(FormCollection collection)
        {
            var model = new MatchRegisterModels();
            var sportEvent = model.ModelMatch(collection);

            model.StoreMatchScore(sportEvent);

            return this.Json(sportEvent);
        }

        // GET: /MatchRegister/Create

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Create()
        {
            return this.View();
        }

        /// <summary>
        /// The search contact.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult SearchContact(int id, FormCollection collection)
        {
            var listofPersons = new List<Person>();

            var searchString = collection["person"].Replace(' ', '+');

            var searchQuery =
                @"https://api.1881bedrift.no/search/search?userName=AndersSivesind&password=nyhe2cop3&msisdn=98843575&query=" +
                searchString + "&level=1&catalogueIds=0|5&pageSize=5";
            var testUri = new Uri(searchQuery);

            var testrequest = (HttpWebRequest) WebRequest.Create(testUri);
            testrequest.Method = "GET";
            testrequest.ContentLength = 0;

            var testResponse = (HttpWebResponse) testrequest.GetResponse();

            var namespaceManager = new XmlNamespaceManager(new NameTable());
            namespaceManager.AddNamespace("empty", "http://1881.no/api/PartnerSearch");
            XDocument doc;
            using (var responseStream = testResponse.GetResponseStream())
            {
                doc = XDocument.Load(responseStream);
                XNamespace ns = "http://1881.no/api/PartnerSearch";

                var resultItems = doc.XPathSelectElements("//empty:ResultItem[empty:ResultType='Person']",
                    namespaceManager);
                foreach (var resultItem in resultItems)
                {
                    var firstname = resultItem.XPathSelectElement("./empty:FirstName", namespaceManager)
                        .Value;
                    var lastname = resultItem.XPathSelectElement("./empty:LastName", namespaceManager)
                        .Value;
                    var streetAddress = resultItem.XPathSelectElement(
                        "./empty:Addresses/empty:Address_Search[empty:AddressType='Visiting']/empty:Street",
                        namespaceManager)
                        .Value + @" "
                                        +
                                        resultItem.XPathSelectElement(
                                            "./empty:Addresses/empty:Address_Search[empty:AddressType='Visiting']/empty:StreetId",
                                            namespaceManager)
                                            .Value;
                    var city =
                        resultItem.XPathSelectElement(
                            "./empty:Addresses/empty:Address_Search[empty:AddressType='Visiting']/empty:City",
                            namespaceManager)
                            .Value;
                    var postalCode =
                        resultItem.XPathSelectElement(
                            "./empty:Addresses/empty:Address_Search[empty:AddressType='Visiting']/empty:Zip",
                            namespaceManager)
                            .Value;
                    var mobile =
                        resultItem.XPathSelectElement(
                            "./empty:ContactPoints[empty:ContactPoint_Search/empty:ContactPointType='Mobile']/empty:ContactPoint_Search/empty:Address",
                            namespaceManager)
                            .Value;

                    var person = new Person
                    {
                        FirstName = firstname,
                        LastName = lastname,
                        PhoneMobile = mobile,
                        City = city,
                        PostalCode = postalCode,
                        StreetAddress = streetAddress
                    };

                    listofPersons.Add(person);
                }

                // /search?userName=<username>&msisdn=<msisdn>&password=<password>&query=<query>&level=0&format=xml
                // https://api.1881bedrift.no/search/search?userName=AndersSivesind&password=nyhe2cop3&msisdn=98843575&query=TEST&level=2&catalogueIds=0|1|5&pageSize=25
            }

            return this.PartialView(listofPersons);
        }

        // POST: /MatchRegister/Create

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        // Post: /MatchRegister/AddComment/MatchId
        /// <summary>
        /// The add comment.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult AddComment(int id, FormCollection collection)
        {
            // we are to update or insert comment for this match
            if (collection["txtBox"] == null)
            {
                return null;
            }

            var comment = collection["txtBox"];

            var model = new MatchRegisterModels();
            var storedComment = model.StoreMatchComment(comment, id);

            return this.PartialView("AddComment", new MatchComment {Comment = storedComment});
        }

        /// <summary>
        /// The edit contact.
        /// POST: /MatchRegister/EditContact/5
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult EditContact(int id, FormCollection collection)
        {
            var model = new ContactPersonModels();
            var contactPerson = model.UpdateContactPerson(id, collection);

            return this.Json(contactPerson);
        }

        /// <summary>
        /// The change status.
        /// Get: /MatchRegister/ChangeStatus/5
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ChangeStatus(int id)
        {
            var model = new ContactPersonModels();
            model.ChangeContactPersonStatus(id);

            return this.RedirectToAction("Index");
        }

        // GET: /MatchRegister/Edit/5

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Edit(int id)
        {
            return this.View();
        }

        // POST: /MatchRegister/Edit/5

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        // GET: /MatchRegister/Delete/5

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Delete(int id)
        {
            return this.View();
        }

        /// <summary>
        /// The ajax errors.
        /// </summary>
        /// <param name="xhrStatus">
        /// The xhr status.
        /// </param>
        /// <param name="error">
        /// The error.
        /// </param>
        [HttpPost]
        public void AjaxErrors(string xhrStatus, string error)
        {
            // Generating message
            var message =
                string.Format("An error occured while doing Ajax call. The status is {0}, and the error is {1}",
                    xhrStatus, error);

            // We shall log this to the log-file and we shall also send an error message to someone
            this._log.Error(message);

            // Sending an email with the error
            var errorMailer = new Mailer();

            errorMailer.MailSend(message);
        }

        // POST: /MatchRegister/Delete/5

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        /// <summary>
        /// The match unregister.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [HttpPost]
        public bool MatchUnregister(int id, FormCollection collection)
        {
            try
            {
                var model = new MatchRegisterModels();
                model.SetMatchUnregisterable(id, collection);

                return true;
            }
            catch (Exception exception)
            {
                return false;
            }
        }

        /// <summary>
        /// The keep alive.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        [Authorize]
        public void KeepAlive(int id)
        {
            // really not doing a thing
            this._log.Info("Refreshing sesssion");

            // Doing this to keep this session alive...
            this.Session["alive"] = DateTime.Now;
        }
    }
}