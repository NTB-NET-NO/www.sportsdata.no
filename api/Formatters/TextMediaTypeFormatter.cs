﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TextMediaTypeFormatter.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The text media type formatter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Formatters
{
    using System;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;

    using log4net;

    /// <summary>
    /// The text media type formatter.
    /// </summary>
    public class TextMediaTypeFormatter : MediaTypeFormatter
    {
        /// <summary>
        ///     The _log.
        /// </summary>
        private readonly ILog log = LogManager.GetLogger(typeof(TextMediaTypeFormatter));

        /// <summary>
        /// Initializes a new instance of the <see cref="TextMediaTypeFormatter"/> class.
        /// </summary>
        public TextMediaTypeFormatter()
        {
            this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/xml"));
            this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/plain"));
            this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/javascript"));
        }

        /// <summary>
        /// The read from stream async.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="readStream">
        /// The read stream.
        /// </param>
        /// <param name="content">
        /// The content.
        /// </param>
        /// <param name="formatterLogger">
        /// The formatter logger.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            var taskCompletionSource = new TaskCompletionSource<object>();
            try
            {
                var memoryStream = new MemoryStream();
                readStream.CopyTo(memoryStream);
                var s = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                taskCompletionSource.SetResult(s);
            }
            catch (Exception e)
            {
                this.log.Error(e);
                taskCompletionSource.SetException(e);
            }

            return taskCompletionSource.Task;
        }

        /// <summary>
        /// The can read type.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool CanReadType(Type type)
        {
            return type == typeof(string);
        }

        /// <summary>
        /// The can write type.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool CanWriteType(Type type)
        {
            return false;
        }
    }
}