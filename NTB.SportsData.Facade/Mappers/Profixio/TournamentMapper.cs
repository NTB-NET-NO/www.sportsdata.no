﻿using Glue;
using NTB.SportsData.Common;
using NTB.SportsData.Services.Profixio.ProfixioServiceReference;

namespace NTB.SportsData.Facade.Mappers.Profixio
{
    public class TournamentMapper : BaseMapper<MatchGroup, Domain.Classes.Tournament>
    {
        protected override void SetUpMapper(Mapping<MatchGroup, Domain.Classes.Tournament> mapper)
        {
            mapper.Relate(x => x.id, y => y.TournamentId);
            mapper.Relate(x => x.Name, y => y.TournamentName);
            mapper.Relate(x => x.MatchClassId, y=>y.TournamentNumber);
        }
    }
}