﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamRoleDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The team role data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.DataMappers.Teams
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Data.Interfaces.Common;
    using NTB.SportsData.Data.Interfaces.DataMappers;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The team role data mapper.
    /// </summary>
    public class TeamRoleDataMapper : IRepository<Function>, IDisposable, ITeamRoleDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(TeamDataMapper));

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(Function domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<Function> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Function domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Function domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<Function> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Function"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Function Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete team role.
        /// </summary>
        /// <param name="functionId">
        /// The function id.
        /// </param>
        public void DeleteTeamRole(int functionId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteTeamRole", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@RoleId", functionId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert team role.
        /// </summary>
        /// <param name="function">
        /// The function.
        /// </param>
        public void InsertTeamRole(Function function)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertTeamRole", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@RoleId", function.FunctionTypeId));
                    sqlCommand.Parameters.Add(new SqlParameter("@RoleName", function.FunctionTypeName));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get team role by id.
        /// </summary>
        /// <param name="functionId">
        /// The function id.
        /// </param>
        /// <returns>
        /// The <see cref="Function"/>.
        /// </returns>
        public Function GetTeamRoleById(int functionId)
        {
            var function = new Function();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetTeamRoleById", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@RoleId", functionId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return function;
                    }

                    while (sqlDataReader.Read())
                    {
                        function.FunctionTypeId = Convert.ToInt32(sqlDataReader["TeamRoleId"]);
                        function.FunctionTypeName = sqlDataReader["TeamRoleName"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return function;
            }
        }
    }
}