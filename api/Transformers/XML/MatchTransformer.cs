﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Transformers.XML
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;
    using System.Xml.Xsl;

    using Mvp.Xml.Exslt;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The match model.
    /// </summary>
    public class MatchTransformer
    {

        /// <summary>
        ///     Gets or sets the xslt file.
        /// </summary>
        public string XsltFile { get; set; }

        #region Public Methods and Operators

        /// <summary>
        /// The serialize object.
        /// </summary>
        /// <param name="matches">
        /// The matches.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string SerializeObject(List<Match> matches)
        {
            var serializer = new XmlSerializer(
                typeof(List<Match>), 
                new XmlRootAttribute("Matches") { Namespace = string.Empty });

            Encoding encoding = Encoding.UTF8;
            var contentStream = new StringWriter();
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            var settings = new XmlWriterSettings
                               {
                                   Indent = true,
                                   IndentChars = "\t",
                                   Encoding = encoding,
                                   OmitXmlDeclaration = false
                               };

            using (var textWriter = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(textWriter, settings))
                {
                    // xDoc.CreateNavigator().AppendChild();
                    serializer.Serialize(writer, matches, namespaces);

                    // output = XmlWriter.Create(xDoc.OuterXml, settings);
                }

                return textWriter.ToString().Replace("utf-16", "utf-8");
            }
        }

        /// <summary>
        /// The transform object.
        /// </summary>
        /// <param name="document">
        /// The document.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string TransformObject(string document, string acceptString)
        {
            var transformer = new XslCompiledTransform();
            var xsltArguments = new XsltArgumentList();
            xsltArguments.AddExtensionObject("http://exslt.org/dates-and-times", new ExsltDatesAndTimes());
            string xsltPath = ConfigurationManager.AppSettings["sportsmlxsltfilepath"];
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(document);

            transformer.Load(xsltPath + @"\" + this.XsltFile);

            var settings = new XmlWriterSettings
                               {
                                   Indent = true,
                                   IndentChars = " ",
                                   Encoding = new UTF8Encoding(false),
                                   OmitXmlDeclaration = false
                               };

            if (acceptString.Contains("json"))
            {
                settings = new XmlWriterSettings
                {
                    Indent = true,
                    IndentChars = " ",
                    Encoding = new UTF8Encoding(false),
                    OmitXmlDeclaration = true,
                    ConformanceLevel = ConformanceLevel.Fragment
                };
            }
            using (var textWriter = new StringWriter())
            {
                using (var writer = XmlWriter.Create(textWriter, settings))
                {
                    transformer.Transform(xmlDoc, xsltArguments, writer);
                }

                return textWriter.ToString().Replace("utf-16", "utf-8");
            }
        }

        #endregion
    }
}