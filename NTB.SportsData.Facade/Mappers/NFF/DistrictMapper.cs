// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DistrictMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The district mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NFF
{
    using Glue;

    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The district mapper.
    /// </summary>
    public class DistrictMapper : BaseMapper<District, Domain.Classes.District>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<District, Domain.Classes.District> mapper)
        {
            mapper.Relate(x => x.DistrictId, y => y.DistrictId);
            mapper.Relate(x => x.DistrictName, y => y.DistrictName);
        }
    }
}