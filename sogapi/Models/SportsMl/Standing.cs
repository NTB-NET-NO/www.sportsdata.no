﻿using System.Xml.Serialization;

namespace sogapi.Models.SportsMl
{
    /// <summary>
    /// The standing.
    /// </summary>
    [XmlRoot("standing")]
    public class Standing
    {
        /// <summary>
        /// Gets or sets the standing meta data.
        /// </summary>
        [XmlElement("standing-metadata")]
        public StandingMetaData StandingMetaData { get; set; }

        /// <summary>
        /// Gets or sets the team.
        /// </summary>
        [XmlElement("team")]
        public Team[] Team { get; set; }
    }
}