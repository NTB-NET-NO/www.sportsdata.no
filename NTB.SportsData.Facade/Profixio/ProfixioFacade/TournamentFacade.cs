﻿using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Mappers.Profixio;
using NTB.SportsData.Facade.Profixio.Interfaces;
using NTB.SportsData.Services.Profixio.DataMappers;
using NTB.SportsData.Services.Profixio.Interfaces;
using NTB.SportsData.Services.Profixio.ProfixioServiceReference;
using Tournament = NTB.SportsData.Domain.Classes.Tournament;

namespace NTB.SportsData.Facade.Profixio.ProfixioFacade
{
    public class TournamentFacade : ITournamentFacade
    {
        private readonly IClubDataMapper _clubDataMapper;
        private readonly ITournamentDataMapper _tournamentDataMapper;

        public TournamentFacade()
        {
            _clubDataMapper = new ClubDataMapper();
            _tournamentDataMapper = new TournamentDataMapper();
        }

        public List<Club> GetClubsByTournament(int tournamentId)
        {
            
            var result = _clubDataMapper.GetClubsByTournament(tournamentId);

            var mapper = new ClubMapper();
            if (result == null)
            {
                result = new List<TournamentClub>();
            }

            return result.Select(row => mapper.Map(row, new Club())).ToList();

        }

        public List<Tournament> GetTournamentByClubs(List<int> clubIds, int tournamentId)
        {
            var result = _clubDataMapper.GetTournamentByClubs(clubIds, tournamentId);

            if (result == null)
            {
                return new List<Tournament>();
            }

            var mapper = new TournamentMapper();
            return result.Select(row => mapper.Map(row, new Tournament())).ToList();
        }

        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            var result = _tournamentDataMapper.GetTournamentBySeasonId(seasonId);

            if (result == null)
            {
                return new List<Tournament>();
            }

            var mapper = new TournamentMapper();
            return result.Select(row => mapper.Map(row, new Tournament())).ToList();
        }
    }
}
