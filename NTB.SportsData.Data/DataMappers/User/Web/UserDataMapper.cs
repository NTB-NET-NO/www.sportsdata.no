﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The user data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Data.DataMappers.User.Web
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;

    using log4net;

    using Microsoft.AspNet.Identity;

    using NTB.SportsData.Data.Interfaces.Common;
    using NTB.SportsData.Data.Interfaces.DataMappers;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The user data mapper.
    /// </summary>
    public class UserDataMapper : IRepository<ApplicationUser>, IUserDataMapper, IDisposable
    {
        #region Fields

        /// <summary>
        ///     The _logger.
        /// </summary>
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void Delete(ApplicationUser domainobject)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteUser", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", domainobject.Id));

                    sqlCommand.ExecuteNonQuery();

                    this._logger.Info("User deleted successfully");

                    sqlCommand.Dispose();
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The delete user.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        public void DeleteUser(ApplicationUser user)
        {
            this.Delete(user);
        }

        /// <summary>
        ///     The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ApplicationUser"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public ApplicationUser Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get all.
        /// </summary>
        /// <returns>
        ///     The <see cref="IQueryable" />.
        /// </returns>
        public IQueryable<ApplicationUser> GetAll()
        {
            // now that we have the user, we can insert the rest of the information
            var applicationUsers = new List<ApplicationUser>();
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetAllUsers", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return applicationUsers.AsQueryable();
                    }

                    while (sqlDataReader.Read())
                    {
                        var applicationUser = new ApplicationUser
                                                  {
                                                      Id = sqlDataReader["Id"].ToString(), 
                                                      FirstName = sqlDataReader["FirstName"].ToString(), 
                                                      LastName = sqlDataReader["LastName"].ToString(), 
                                                      Email = sqlDataReader["Email"].ToString(), 
                                                      UserName = sqlDataReader["UserName"].ToString()
                                                  };

                        applicationUsers.Add(applicationUser);
                    }

                    sqlCommand.Dispose();

                    return applicationUsers.AsQueryable();
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }

                    return applicationUsers.AsQueryable();
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get phone number.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetPhoneNumber(ApplicationUser user)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsData_GetUserPhoneNumber", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", user.Id));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return string.Empty;
                    }

                    var phoneNumber = string.Empty;
                    while (sqlDataReader.Read())
                    {
                        phoneNumber = sqlDataReader["PhoneNumber"].ToString();
                    }

                    sqlDataReader.Close();

                    sqlCommand.Dispose();

                    return phoneNumber;
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }

                    return string.Empty;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get phone number confirmed.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool GetPhoneNumberConfirmed(ApplicationUser user)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsData_GetPhoneNumberConfirmed", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", user.Id));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return false;
                    }

                    sqlDataReader.Close();

                    return true;
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }

                    return false;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get user by email.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <returns>
        /// The <see cref="ApplicationUser"/>.
        /// </returns>
        public ApplicationUser GetUserByEmail(string email)
        {
            this._logger.Info("GetUserByEmail: " + email);

            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetUserByEmail", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@Email", email));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var user = new ApplicationUser();
                    while (sqlDataReader.Read())
                    {
                        user.FirstName = sqlDataReader["FirstName"].ToString();
                        user.Email = sqlDataReader["Email"].ToString();
                        user.Id = sqlDataReader["Id"].ToString();
                        user.LastName = sqlDataReader["LastName"].ToString();
                        user.UserName = sqlDataReader["UserName"].ToString();
                        user.PasswordHash = sqlDataReader["PasswordHash"].ToString();
                        user.SecurityStamp = sqlDataReader["SecurityStamp"].ToString();

                        var role = new Role
                                       {
                                           Name = sqlDataReader["Name"].ToString(), 
                                           Id = sqlDataReader["RoleId"].ToString()
                                       };

                        user.Roles = new Collection<IRole> { role };
                    }

                    sqlCommand.Dispose();

                    return user;
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get user by user id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="ApplicationUser"/>.
        /// </returns>
        public ApplicationUser GetUserByUserId(string userId)
        {
            this._logger.Info("GetUserByUserId: " + userId);

            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetUserByUserId", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var user = new ApplicationUser();
                    while (sqlDataReader.Read())
                    {
                        user.FirstName = sqlDataReader["FirstName"].ToString();
                        user.Email = sqlDataReader["Email"].ToString();
                        user.Id = sqlDataReader["Id"].ToString();
                        user.LastName = sqlDataReader["LastName"].ToString();
                        user.UserName = sqlDataReader["UserName"].ToString();
                        user.PasswordHash = sqlDataReader["PasswordHash"].ToString();
                        user.SecurityStamp = sqlDataReader["SecurityStamp"].ToString();

                        var role = new Role
                                       {
                                           Name = sqlDataReader["Name"].ToString(), 
                                           Id = sqlDataReader["RoleId"].ToString()
                                       };

                        user.Roles = new Collection<IRole> { role };
                    }

                    this._logger.Info("User Found in database");

                    sqlDataReader.Close();

                    return user;
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get user by user name.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="ApplicationUser"/>.
        /// </returns>
        public ApplicationUser GetUserByUserName(string userName)
        {
            this._logger.Info("GetUserByUserName: " + userName);

            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetUserByUserName", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserName", userName));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var user = new ApplicationUser();
                    while (sqlDataReader.Read())
                    {
                        user.FirstName = sqlDataReader["FirstName"].ToString();
                        user.Email = sqlDataReader["Email"].ToString();
                        user.Id = sqlDataReader["Id"].ToString();
                        user.LastName = sqlDataReader["LastName"].ToString();
                        user.UserName = sqlDataReader["UserName"].ToString();
                        user.PasswordHash = sqlDataReader["PasswordHash"].ToString();
                        user.SecurityStamp = sqlDataReader["SecurityStamp"].ToString();

                        var role = new Role
                                       {
                                           Name = sqlDataReader["Name"].ToString(), 
                                           Id = sqlDataReader["RoleId"].ToString()
                                       };

                        user.Roles = new Collection<IRole> { role };
                    }

                    sqlDataReader.Close();

                    return user;
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        ///     The get users.
        /// </summary>
        /// <returns>
        ///     The <see cref="IQueryable" />.
        /// </returns>
        public IQueryable<ApplicationUser> GetUsers()
        {
            return this.GetAll();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertAll(List<ApplicationUser> domainobject)
        {
            foreach (var applicationUser in domainobject)
            {
                this.InsertOne(applicationUser);
            }
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(ApplicationUser domainobject)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertUser", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", domainobject.Id));

                    sqlCommand.Parameters.Add(new SqlParameter("@UserName", domainobject.UserName));

                    sqlCommand.Parameters.Add(new SqlParameter("@Password", domainobject.PasswordHash));

                    sqlCommand.Parameters.Add(new SqlParameter("@Email", domainobject.Email));

                    sqlCommand.Parameters.Add(new SqlParameter("@FirstName", domainobject.FirstName));

                    sqlCommand.Parameters.Add(new SqlParameter("@LastName", domainobject.LastName));

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Dispose();

                    this._logger.Info("User inserted successfully");

                    return 1;
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }

                    return 0;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert user.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        public void InsertUser(ApplicationUser user)
        {
            this.InsertOne(user);
        }

        /// <summary>
        /// The is user in role by user id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsUserInRoleByUserId(string userId)
        {
            this._logger.Info("IsUserInRoleByUserId: " + userId);

            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetUserRoleByUserId", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return false;
                    }

                    sqlDataReader.Close();

                    return true;
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }

                    return false;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The set phone number.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="phoneNumber">
        /// The phone number.
        /// </param>
        public void SetPhoneNumber(ApplicationUser user, string phoneNumber)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsData_SetUserPhoneNumber", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", user.Id));

                    sqlCommand.Parameters.Add(new SqlParameter("@PhoneNumber", phoneNumber));

                    sqlCommand.ExecuteNonQuery();

                    this._logger.Info("Phone number set successfully");

                    sqlCommand.Dispose();
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The set phone number confirmed.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="confirmed">
        /// The confirmed.
        /// </param>
        public void SetPhoneNumberConfirmed(ApplicationUser user, bool confirmed)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsData_SetPhoneNumberConfirmed", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", user.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@Confirmed", confirmed));

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Dispose();
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domain object.
        /// </param>
        public void Update(ApplicationUser domainobject)
        {
            // now that we have the user, we can insert the rest of the information
            this._logger.Debug("Starting updating user " + domainobject.UserName);
            this._logger.Debug("Password: " + domainobject.PasswordHash);

            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_UpdateUser", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", domainobject.Id));

                    sqlCommand.Parameters.Add(new SqlParameter("@UserName", domainobject.UserName));

                    sqlCommand.Parameters.Add(new SqlParameter("@PassWord", domainobject.PasswordHash));

                    sqlCommand.Parameters.Add(new SqlParameter("@Email", domainobject.Email));

                    sqlCommand.Parameters.Add(new SqlParameter("@FirstName", domainobject.FirstName));

                    sqlCommand.Parameters.Add(new SqlParameter("@LastName", domainobject.LastName));

                    sqlCommand.ExecuteNonQuery();

                    this._logger.Info("User updated successfully");
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The update user.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        public void UpdateUser(ApplicationUser user)
        {
            this._logger.Debug("Running UpdateUser with user parameter " + user.UserName);
            this.Update(user);
        }

        #endregion
    }
}