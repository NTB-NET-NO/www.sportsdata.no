﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using sogapi.Contexts;
using sogapi.Models.SportsMl;
using Formatting = Newtonsoft.Json.Formatting;

namespace sogapi.Controllers
{
    public class MedalsController : ApiController
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private readonly ILog logger = LogManager.GetLogger("SogApiFileAppender");

        public HttpResponseMessage GetMedalStatistic()
        {
            // Find out if this is a JSON or an XML request
            var accept = this.Request.Headers.Accept.ToString();

            var medalsContext = new MedalContext();
            var xmlDoc = medalsContext.GetMedalsFromDisk();
            var sportsContent = medalsContext.GetSportsContent(xmlDoc);

            var response = new HttpResponseMessage(HttpStatusCode.OK);

            if (accept == "")
            {
                accept = "text/xml";
            }

            if (accept == "application/json")
            {
                // The json return 
                var json = JsonConvert.SerializeObject(
                        sportsContent,
                        Formatting.Indented,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        });

                response.Content = new StringContent(json);
            }

            var xmlserializer = new XmlSerializer(typeof(SportsContent));

            var settings = new XmlWriterSettings();
            settings.Encoding = new UnicodeEncoding(false, false); // no BOM in a .NET string
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            using (var textWriter = new StringWriter())
            {
                // 
                using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    xmlserializer.Serialize(xmlWriter, sportsContent);
                }

                response.Content = new StringContent(textWriter.ToString());
                
            }

            return response;

            // This is how I am going to do it
            // @todo: Code so we return either json or XML
            //ITransformer model;
            //if (accept == "application/json")
            //{
            //    Logger.Info("Generating JSON");
            //    model = new CompetitionTransformer { XsltFile = "sportsinjson-competitions.xsl" };
            //}
            //else
            //{
            //    Logger.Info("Generating XML");
            //    model = new TournamentTransformer { XsltFile = "sportsml-tournament.xsl" };
            //}

            //var content = model.TransformObject(xmlContent);

            //if (accept == "text/xml")
            //{
            //    content = content.Replace("utf-16", "utf-8");
            //}

            //return new HttpResponseMessage { Content = new StringContent(content, Encoding.UTF8, accept) };
        }
    }
}
