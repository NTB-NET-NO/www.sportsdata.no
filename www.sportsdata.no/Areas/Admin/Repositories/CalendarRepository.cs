// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CalendarRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Web.Areas.Admin.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Web.Areas.Admin.Interfaces;

    /// <summary>
    ///     The calendars.
    /// </summary>
    public class CalendarRepository : IRepository<Calendar>, IDisposable, ICalendarDataMapper
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int? SportId { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime? StartDate { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Calendar domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Calendar"/>.
        /// </returns>
        public Calendar Get(int id)
        {
            return this.GetAll().Single(s => s.CalendarId == id);
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Calendar> GetAll()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // @Done: Move this code to the database
                var sqlCommand = new SqlCommand("SportsData_GetAllEvents", sqlConnection)
                                     {
                                         CommandType =
                                             CommandType
                                             .StoredProcedure
                                     };

                sqlCommand.Parameters.Add(new SqlParameter("@ActivityId", this.SportId));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", this.StartDate));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", this.EndDate));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var calendars = new List<Calendar>();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return calendars.AsQueryable();
                }

                while (sqlDataReader.Read())
                {
                    var calendar = new Calendar
                                       {
                                           CalendarId = (int)sqlDataReader["EventId"], 
                                           Name = sqlDataReader["Name"].ToString(), 
                                           StartDateTime = Convert.ToDateTime(sqlDataReader["DateStart"]), 
                                           SportId = Convert.ToInt32(sqlDataReader["ActivityId"])
                                       };

                    calendars.Add(calendar);
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return calendars.AsQueryable();
            }
        }

        /// <summary>
        /// Gets list of calendar events
        /// </summary>
        /// <param name="startDate">
        /// </param>
        /// <param name="endDate">
        /// </param>
        /// <returns>
        /// Returns a list of matches based on start and end date
        /// </returns>
        public List<NumberOfMatches> GetMatchesByDateInterval(DateTime startDate, DateTime endDate)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // @Done: Move this code to the database
                var sqlCommand = new SqlCommand("SportsData_GetNumberOfMatchesByDateInterval", sqlConnection)
                                     {
                                         CommandType
                                             =
                                             CommandType
                                             .StoredProcedure
                                     };

                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", startDate));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", endDate));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var numberOfMatches = new List<NumberOfMatches>();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return numberOfMatches;
                }

                while (sqlDataReader.Read())
                {
                    var calendar = new NumberOfMatches
                                       {
                                           MatchDate = Convert.ToDateTime(sqlDataReader["Date"]), 
                                           TotalMatches = Convert.ToInt32(sqlDataReader["Matches"]), 
                                           SportId = Convert.ToInt32(sqlDataReader["SportId"])
                                       };

                    numberOfMatches.Add(calendar);
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return numberOfMatches;
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<Calendar> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(Calendar domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Calendar domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}