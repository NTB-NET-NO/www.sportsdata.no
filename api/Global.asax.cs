﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace NTB.SportsData.Api
{
    using System.Web;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;

    using NTB.SportsData.Api.Formatters;

    /// <summary>
    /// The web api application.
    /// </summary>
    public class WebApiApplication : HttpApplication
    {
        #region Methods

        /// <summary>
        /// The application_ start.
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // GlobalConfiguration.Configuration.Formatters.Insert(0, new TextMediaTypeFormatter());
            GlobalConfiguration.Configuration.Formatters.Insert(0, new TextMediaTypeFormatter());
            GlobalConfiguration.Configuration.Formatters.Insert(1, new BinaryTextMediaTypeFormatter());
        }

        #endregion
    }
}