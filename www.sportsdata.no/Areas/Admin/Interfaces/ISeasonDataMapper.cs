﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Interfaces
{
    interface ISeasonDataMapper
    {
        List<Season> GetSeasonsBySportId(int sportId);
        void ActivateSeason(int seasonId);
        void DeActivateSeason(int seasonId);
        void UpdateSeasonWithDiscipline(int seasonId, int disciplineId);
    }
}
