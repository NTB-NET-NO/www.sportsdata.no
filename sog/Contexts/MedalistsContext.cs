﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using sog.Helpers;
using sog.Models.SportsMl;

namespace sog.Contexts
{
    public class MedalistsContext
    {
        public List<FileInfo> GetFiles(string path)
        {
            var di = new DirectoryInfo(path);
            var files = di.GetFiles("*.xml").ToList();
            
            if (files.Count == 0)
            {
                return null;
            }

            return files;
        }

        public Dictionary<string, FileInfo> GetDistinctEvents(List<FileInfo> listOfFiles)
        {
            var dictionary = new Dictionary<string, FileInfo>();
            foreach (var file in listOfFiles)
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(file.FullName);

                // Get the event id
                var eventId = string.Empty;
                var eventIdNode = xmlDoc.SelectSingleNode("//sports-content/sports-event");
                if (eventIdNode != null && eventIdNode.Attributes != null)
                {
                    if (eventIdNode.Attributes["id"] != null)
                    {
                        eventId = eventIdNode.Attributes["id"].Value; // .Split(':').Last();
                    }
                }
                if (eventId == string.Empty)
                {
                    continue;
                }

                if (!dictionary.ContainsKey(eventId))
                {
                    dictionary.Add(eventId, file);
                }
            }

            return dictionary;
        }

        public List<SportsContent> GetSportsContents(Dictionary<string, FileInfo> fileDictionary)
        {
            var sportsContents = new List<SportsContent>();
            foreach (KeyValuePair<string, FileInfo> kvp in fileDictionary)
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(kvp.Value.FullName);

                // now we shall serialize
                using (var stringReader = new StringReader(xmlDoc.InnerXml))
                {
                    using (var xmlReader = XmlReader.Create(stringReader))
                    {
                        var startElement = "sports-content";

                        xmlReader.ReadToDescendant(startElement);

                        var xmlserializer = new XmlSerializer(typeof(SportsContent), new XmlRootAttribute(startElement));

                        var sportsContent = (SportsContent)xmlserializer.Deserialize(xmlReader.ReadSubtree());

                        sportsContents.Add(sportsContent);
                    }
                }
            }

            return sportsContents;
        }

        public SportsContent GetMedalistSportsContent(string countryCode, List<SportsContent> listOfSportsContents)
        {
            var sportsContent = new SportsContent();

            // var sportsEvents = new List<SportsEvent>();
            var sportsEvents = (from sc in listOfSportsContents
                from se in sc.SportsEvent
                from t in se.Team
                where t.TeamMetaData.Nationality == countryCode
                select se).ToList();

            var newSportsEvents = new List<SportsEvent>();
            foreach (var sportsEvent in sportsEvents)
            {
                var newSportsEvent = new SportsEvent();
                newSportsEvent.EventMetaData = new EventMetaData();
                newSportsEvent.EventMetaData = sportsEvent.EventMetaData;

                var teams = (from sportseventTeam in sportsEvent.Team
                    where sportseventTeam.TeamMetaData.Nationality == countryCode
                    select sportseventTeam).ToList();

                newSportsEvent.Team = new Team[teams.Count];
                newSportsEvent.Team = teams.ToArray();

                newSportsEvents.Add(newSportsEvent);
            }

            sportsContent.SportsEvent = new SportsEvent[newSportsEvents.Count];
            sportsContent.SportsEvent = newSportsEvents.ToArray();
            return sportsContent;
        }

        public List<FileInfo> GetMedalistsFiles(string countryCode, List<FileInfo> files)
        {
            var foundFiles = new List<FileInfo>();
            foreach (var file in files)
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(file.FullName);

                var xpathString = string.Format("//sports-content[sports-event/team/team-metadata/@nationality='{0}']",
                    countryCode);
                var medalNodes = xmlDoc.SelectNodes(xpathString);

                if (medalNodes == null)
                {
                    continue;
                }

                foundFiles.Add(file);
            }

            return foundFiles;
        }
    }
}