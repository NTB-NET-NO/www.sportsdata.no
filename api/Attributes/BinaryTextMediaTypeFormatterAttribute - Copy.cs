﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BinaryTextMediaTypeFormatterAttribute.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The binary text media type formatter attribute.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Attributes
{
    using System;
    using System.Web.Http.Controllers;

    using NTB.SportsData.Api.Formatters;

    /// <summary>
    /// The binary text media type formatter attribute.
    /// </summary>
    public class BinaryTextMediaTypeFormatterAttribute : Attribute, IControllerConfiguration
    {
        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="descriptor">
        /// The descriptor.
        /// </param>
        public void Initialize(HttpControllerSettings settings, HttpControllerDescriptor descriptor)
        {
            // Clear the formatters list.
            settings.Formatters.Clear();

            // Add a custom media-type formatter.
            settings.Formatters.Add(new BinaryTextMediaTypeFormatter());
        }
    }
}