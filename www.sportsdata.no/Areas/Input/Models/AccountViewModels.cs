﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Input.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Brukernavn")]
        public string UserName { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Gjeldende passord")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} Må være minst {2} tegn langt.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nytt passord")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Bekreft nytt password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "Passordene er ikke like.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Brukernavn")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Passord")]
        public string Password { get; set; }

        [Display(Name = "Husk meg?")]
        public bool RememberMe { get; set; }
    }

    public class UpdateViewModel
    {
        public List<Role> IdentityRoles { get; set; }
        [Required]
        [HiddenInput(DisplayValue = false)]
        public string UserId { get; set; }

        [Required]
        [Display(Name = "Brukernavn")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Fornavn")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Etternavn")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-post adresse")]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "Rolle")]
        public string SelectedRoleId { get; set; }

        public IEnumerable<SelectListItem> RoleItems
        {
            get { return new SelectList(IdentityRoles, "Name", "Name"); }
        }
        //public IEnumerable<IdentityRole> Roles { get; set; }
        //public IdentityRole SelectedRole { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string OldRole { get; set; }

        public string SelectedRoleName { get; set; }
    }

    public class RegisterViewModel
    {
        public List<Role> IdentityRoles { get; set; }
        
        [Required]
        [Display(Name = "Brukernavn")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Fornavn")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Etternavn")]
        public string LastName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} må være minst {2} tegn langt.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Passord")] 
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Bekreft passord")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Passordene er ikke like.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-post adresse")]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "Rolle")]
        public string SelectedRoleId { get; set; }

        public IEnumerable<SelectListItem> RoleItems
        {
            get { return new SelectList(IdentityRoles, "Name", "Name"); }
        }
    }

    public class ResetPasswordViewModel
    {
        
        [Required]
        public string UserId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
