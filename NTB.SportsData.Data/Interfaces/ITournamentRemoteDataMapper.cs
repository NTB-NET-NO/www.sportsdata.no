﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITournamentRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TournamentRemoteDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The TournamentRemoteDataMapper interface.
    /// </summary>
    public interface ITournamentRemoteDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get tournament standings.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<TeamResult> GetTeamResultStanding(Tournament tournament);

        /// <summary>
        /// The get tournaments by org id and season id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentsByOrgIdAndSeasonId(int orgId, int seasonId);

        #endregion
    }
}