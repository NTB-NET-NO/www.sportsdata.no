﻿using System;
using Glue;

namespace NTB.SportsData.Common
{
    public abstract class BaseMapper<TLeftType, TRightType>
    {
        private readonly Mapping<TLeftType, TRightType> _map;

        protected BaseMapper()
            : this(null, null)
        {
        }

        protected BaseMapper(Func<TRightType, TLeftType> creatorTowardsLeft,
            Func<TLeftType, TRightType> creatorTowardsRight)
        {
            this._map = new Mapping<TLeftType, TRightType>(creatorTowardsLeft, creatorTowardsRight);
            this.SetUpMapper(this._map);
        }

        protected abstract void SetUpMapper(Mapping<TLeftType, TRightType> mapper);

        public virtual TRightType Map(TLeftType from, TRightType to)
        {
            return this._map.Map(from, to);
        }

        public virtual TLeftType Map(TRightType from, TLeftType to)
        {
            return this._map.Map(from, to);
        }

        public Mapping<TLeftType, TRightType> GetMapper()
        {
            return this._map;
        }
    }
}