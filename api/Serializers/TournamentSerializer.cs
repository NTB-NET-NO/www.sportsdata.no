﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentSerializer.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Serializers
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    using NTB.SportsData.Api.Interfaces;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The object serializer.
    /// </summary>
    public class TournamentSerializer : IObjectSerializer<Tournament>
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the serializable object property
        /// </summary>
        public object SerializableObject { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The serialize object.
        /// </summary>
        /// <param name="objects">
        /// The objects.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string SerializeObjects(List<Tournament> objects)
        {
            var serializer = new XmlSerializer(
                typeof(List<Tournament>), 
                new XmlRootAttribute("Tournaments") { Namespace = string.Empty });

            Encoding encoding = Encoding.UTF8;
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t" };
            settings.Encoding = encoding;
            settings.OmitXmlDeclaration = false;

            using (var textWriter = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(writer, objects, namespaces);
                }

                return textWriter.ToString().Replace("utf-16", "utf-8");
            }
        }

        /// <summary>
        /// The serialize object.
        /// </summary>
        /// <param name="domainObject">
        /// The domain object.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string SerializeObject(Tournament domainObject)
        {
            var serializer = new XmlSerializer(
                typeof(Tournament),
                new XmlRootAttribute("SportsData") { Namespace = string.Empty });

            Encoding encoding = Encoding.UTF8;
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t" };
            settings.Encoding = encoding;
            settings.OmitXmlDeclaration = false;

            using (var textWriter = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(writer, domainObject, namespaces);
                }

                return textWriter.ToString().Replace("utf-16", "utf-8");
            }
        }

        /// <summary>
        ///     Serializes a single tournament object
        /// </summary>
        /// <returns>String containing an XML structure for this object</returns>
        public string Serializer()
        {
            XmlSerializer serializer = null;

            if (this.SerializableObject != null)
            {
                if (this.SerializableObject.GetType() == typeof(List<object>))
                {
                    serializer = new XmlSerializer(
                        typeof(List<object>), 
                        new XmlRootAttribute("SportsData") { Namespace = string.Empty });
                }
                else if (this.SerializableObject.GetType() == typeof(object))
                {
                    serializer = new XmlSerializer(
                        typeof(object), 
                        new XmlRootAttribute("SportsData") { Namespace = string.Empty });
                }
            }

            Encoding encoding = Encoding.UTF8;
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            var settings = new XmlWriterSettings
                               {
                                   Indent = true, 
                                   IndentChars = "\t", 
                                   Encoding = encoding, 
                                   OmitXmlDeclaration = false
                               };

            using (var textWriter = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(textWriter, settings))
                {
                    if (serializer != null)
                    {
                        serializer.Serialize(writer, this.SerializableObject, namespaces);
                    }
                }

                return textWriter.ToString().Replace("utf-16", "utf-8");
            }
        }

        #endregion
    }
}