﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NFF
{
    using Glue;

    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NFF.NFFProdService;

    using Match = NTB.SportsData.Services.NFF.NFFProdService.Match;
    using MatchResult = NTB.SportsData.Services.NIF.NIFProdService.MatchResult;

    /// <summary>
    /// The match mapper.
    /// </summary>
    public class MatchExtendedMapper : BaseMapper<Match, Domain.Classes.MatchExtended>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Match, Domain.Classes.MatchExtended> mapper)
        {
            var matchResultMapping = new Mapping<Services.NFF.NFFProdService.MatchResult, Domain.Classes.MatchResult>(x => new Services.NFF.NFFProdService.MatchResult(), y => new Domain.Classes.MatchResult());
            matchResultMapping.AutoRelateEqualNames();

            var matchEventMapping = new Mapping<MatchEvent, Domain.Classes.MatchEventExtended>(x => new MatchEvent(), y => new Domain.Classes.MatchEventExtended());
            matchEventMapping.AutoRelateEqualNames();

            var matchPlayerMapping = new Mapping<Player, Domain.Classes.PlayerExtended>(x => new Player(), y => new Domain.Classes.PlayerExtended());
            matchPlayerMapping.AutoRelateEqualNames();

            mapper.Relate(x => x.AwayTeamName, y => y.AwayTeam);
            mapper.Relate(x => x.AwayTeamId, y => y.AwayTeamId);
            mapper.Relate(x => x.AwayTeamGoals, y => y.AwayTeamGoal);
            mapper.Relate(x => x.MatchStartDate, y => y.MatchDate);
            mapper.Relate(x => x.HomeTeamName, y => y.HomeTeam);
            mapper.Relate(x => x.HomeTeamId, y => y.HomeTeamId);
            mapper.Relate(x => x.HomeTeamGoals, y => y.HomeTeamGoal);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentName, y => y.TournamentName);
            mapper.Relate(x => x.TournamentAgeCategoryId, y => y.TournamentAgeCategoryId);
            mapper.Relate(x => x.TournamentRoundNumber, y => y.TournamentRoundNumber);
            mapper.Relate(x => x.MatchEventList, y => y.MatchEventList, matchEventMapping);
            mapper.Relate(x => x.AwayTeamPlayers, y => y.AwayTeamPlayers, matchPlayerMapping);
            mapper.Relate(x => x.HomeTeamPlayers, y => y.HomeTeamPlayers, matchPlayerMapping);
            mapper.Relate(x => x.MatchResultList, y => y.MatchResultList, matchResultMapping);
        }
    }
}