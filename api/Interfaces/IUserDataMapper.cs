﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The UserDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Interfaces
{
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The UserDataMapper interface.
    /// </summary>
    internal interface IUserDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="ApiUser"/>.
        /// </returns>
        ApiUser Get(string username, string password);

        #endregion
    }
}