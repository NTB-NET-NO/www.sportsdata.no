<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="1.0"
>
    <!-- Style sheet converting from serialized data to SportsML - matches -->
    <!-- Root element -->

    <xsl:output indent="yes" method="xml"/>
    
    <xsl:strip-space elements="*"/>
    

    <xsl:template match="/">
        <sports-content>
            <tournament>
                <tournament-metadata>
                    <xsl:attribute name="tournament-key">
                        <xsl:value-of select="/Matches/MatchExtended[1]/TournamentId"/>
                    </xsl:attribute>
                    <xsl:attribute name="tournament-name">
                        <xsl:value-of select="/Matches/MatchExtended[1]/TournamentName"/>
                    </xsl:attribute>
                    <sports-content-codes>
                        <sports-content-code code-type="sport">
                            <xsl:attribute name="code-name">
                                <xsl:value-of select="/Matches/MatchExtended[1]/SportName"/>
                            </xsl:attribute>
                        </sports-content-code>
                    </sports-content-codes>
                </tournament-metadata>
                <tournament-division>
                    <xsl:for-each select="/Matches/MatchExtended">
                        <tournament-round>
                            <xsl:attribute name="round-number">
                                <xsl:value-of select="TournamentRoundNumber"/>
                            </xsl:attribute>
                            <sports-event>
                                <event-metadata>
                                    <xsl:attribute name="event-key">
                                        <xsl:value-of select="MatchId"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="event-name">
                                        <xsl:value-of select="HomeTeam"/>
                                        <xsl:text> - </xsl:text>
                                        <xsl:value-of select="AwayTeam"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="start-date-time">
                                        <xsl:value-of select="substring-before(MatchDate, 'T')"/>
                                        <xsl:text>T</xsl:text>
                                        <xsl:value-of select="substring(MatchStartTime,1,2)"/>
                                        <xsl:text>:</xsl:text>
                                        <xsl:value-of select="substring(MatchStartTime,3,2)"/>
                                        <xsl:text>:00+0100</xsl:text>
                                    </xsl:attribute>
                                </event-metadata>
                                <team>
                                    <team-metadata alignment="home">
                                        <xsl:attribute name="id">
                                            <xsl:text>t.</xsl:text>
                                            <xsl:value-of select="HomeTeamId"/>
                                        </xsl:attribute>
                                        <name>
                                            <xsl:attribute name="full">
                                                <xsl:value-of select="HomeTeam"/>
                                            </xsl:attribute>
                                        </name>
                                    </team-metadata>
                                    
                                    <xsl:if test="MatchResultList/MatchResult">
                                        <team-stats>
                                            <sub-score period-value="1" sub-score-name="pauseresultat">
                                                <xsl:attribute name="score">
                                                    <xsl:value-of select="MatchResultList/MatchResult[ResultTypeName='Pauseresultat']/HomeTeamGoals"/>
                                                </xsl:attribute>
                                            </sub-score>
                                            <sub-score period-value="2" sub-score-name="sluttresultat">
                                                <xsl:attribute name="score">
                                                    <xsl:value-of select="MatchResultList/MatchResult[ResultTypeName='Sluttresultat']/HomeTeamGoals"/>
                                                </xsl:attribute>
                                            </sub-score>
                                        </team-stats>
                                    </xsl:if>
                                    <xsl:if test="HomeTeamPlayers/PlayerExtended">
                                        <xsl:for-each select="HomeTeamPlayers/PlayerExtended">
                                            <player>
                                                
                                                <player-metadata>
                                                    <xsl:attribute name="id">
                                                        <xsl:text>p.</xsl:text>
                                                        <xsl:value-of select="PlayerId"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="uniform-number">
                                                        <xsl:value-of select="PlayerShirtNumber"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="position-event">
                                                        <xsl:choose>
                                                            <xsl:when test="Position='Keeper'">
                                                                <xsl:text>goalkeeper</xsl:text>
                                                            </xsl:when>
                                                            <xsl:when test="Position='Forsvar'">
                                                                <xsl:text>defender</xsl:text>
                                                            </xsl:when>
                                                            <xsl:when test="Position='Midtbane'">
                                                                <xsl:text>midfielder</xsl:text>
                                                            </xsl:when>
                                                            <xsl:when test="Position='Angrep'">
                                                                <xsl:text>forward</xsl:text>
                                                            </xsl:when>
                                                            <xsl:when test="Position='Reserve'">
                                                                <xsl:text>substitute</xsl:text>
                                                            </xsl:when>
                                                            <xsl:when test="Position='Reserve (keeper)'">
                                                                <xsl:text>substitute</xsl:text>
                                                            </xsl:when>
                                                        </xsl:choose>
                                                        
                                                    </xsl:attribute>
                                                    <name>
                                                        <xsl:attribute name="first">
                                                            <xsl:value-of select="FirstName"/>
                                                        </xsl:attribute>
                                                        <xsl:attribute name="last">
                                                            <xsl:value-of select="SurName"/>
                                                        </xsl:attribute>
                                                        <xsl:attribute name="full">
                                                            <xsl:value-of select="FirstName"/>
                                                            <xsl:text> </xsl:text>
                                                            <xsl:value-of select="SurName"/>
                                                        </xsl:attribute>
                                                    </name>
                                                </player-metadata>
                                                
                                            </player>
                                        </xsl:for-each>
                                    </xsl:if>
                                </team>
                                <team>
                                    <team-metadata alignment="away">
                                        <xsl:attribute name="id">
                                            <xsl:text>t.</xsl:text>
                                            <xsl:value-of select="AwayTeamId"/>
                                        </xsl:attribute>
                                        <name>
                                            <xsl:attribute name="full">
                                                <xsl:value-of select="AwayTeam"/>
                                            </xsl:attribute>
                                        </name>
                                    </team-metadata>
                                    <xsl:if test="MatchResultList/MatchResult">
                                        <team-stats>
                                            <sub-score period-value="1" sub-score-name="pauseresultat">
                                                <xsl:attribute name="score">
                                                    <xsl:value-of select="MatchResultList/MatchResult[ResultTypeName='Pauseresultat']/AwayTeamGoals"/>
                                                </xsl:attribute>
                                            </sub-score>
                                            <sub-score period-value="2" sub-score-name="sluttresultat">
                                                <xsl:attribute name="score">
                                                    <xsl:value-of select="MatchResultList/MatchResult[ResultTypeName='Sluttresultat']/AwayTeamGoals"/>
                                                </xsl:attribute>
                                            </sub-score>
                                        </team-stats>
                                    </xsl:if>
                                    
                                    <xsl:if test="AwayTeamPlayers/PlayerExtended">
                                        <xsl:for-each select="AwayTeamPlayers/PlayerExtended">
                                            <player>
                                                
                                                <player-metadata>
                                                    <xsl:attribute name="id">
                                                        <xsl:text>p.</xsl:text>
                                                        <xsl:value-of select="PlayerId"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="uniform-number">
                                                        <xsl:value-of select="PlayerShirtNumber"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="position-event">
                                                        <xsl:choose>
                                                            <xsl:when test="Position='Keeper'">
                                                                <xsl:text>goalkeeper</xsl:text>
                                                            </xsl:when>
                                                            <xsl:when test="Position='Forsvar'">
                                                                <xsl:text>defender</xsl:text>
                                                            </xsl:when>
                                                            <xsl:when test="Position='Midtbane'">
                                                                <xsl:text>midfielder</xsl:text>
                                                            </xsl:when>
                                                            <xsl:when test="Position='Angrep'">
                                                                <xsl:text>forward</xsl:text>
                                                            </xsl:when>
                                                            <xsl:when test="Position='Reserve'">
                                                                <xsl:text>substitute</xsl:text>
                                                            </xsl:when>
                                                            <xsl:when test="Position='Reserve (keeper)'">
                                                                <xsl:text>substitute</xsl:text>
                                                            </xsl:when>
                                                        </xsl:choose>
                                                        
                                                    </xsl:attribute>
                                                    <name>
                                                        <xsl:attribute name="first">
                                                            <xsl:value-of select="FirstName"/>
                                                        </xsl:attribute>
                                                        <xsl:attribute name="last">
                                                            <xsl:value-of select="SurName"/>
                                                        </xsl:attribute>
                                                        <xsl:attribute name="full">
                                                            <xsl:value-of select="FirstName"/>
                                                            <xsl:text> </xsl:text>
                                                            <xsl:value-of select="SurName"/>
                                                        </xsl:attribute>
                                                    </name>
                                                </player-metadata>
                                                
                                            </player>
                                        </xsl:for-each>
                                    </xsl:if>
                                </team>
                                <xsl:if test="MatchEventList/MatchEventExtended">
                                    <event-actions>
                                        <event-actions-soccer>
                                            <xsl:for-each select="MatchEventList/MatchEventExtended[MatchEventType='Spillemål']">
                                                <action-soccer-score>
                                                    <xsl:attribute name="minutes-elapsed">
                                                        <xsl:value-of select="Minute"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="sequence-number">
                                                        <xsl:value-of select="MatchEventId"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="period-value">
                                                    <xsl:choose>
                                                        <xsl:when test="Minute &lt;45">
                                                            <xsl:text>1</xsl:text>
                                                        </xsl:when>
                                                        <xsl:when test="Minute &gt;= 45">
                                                            <xsl:text>2</xsl:text>
                                                        </xsl:when>
                                                    </xsl:choose>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="score-team">
                                                        <xsl:value-of select="HomeGoals"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="score-team-opposing">
                                                        <xsl:value-of select="AwayGoals"/>
                                                    </xsl:attribute>
                                                    <action-soccer-play-participant role="scorer">
                                                        <xsl:attribute name="player-idref">
                                                            <xsl:text>p.</xsl:text>
                                                            <xsl:value-of select="PlayerId"/>
                                                        </xsl:attribute>
                                                        <xsl:attribute name="team-idref">
                                                            <xsl:text>t.</xsl:text>
                                                            <xsl:value-of select="TeamId"/>
                                                        </xsl:attribute>
                                                    </action-soccer-play-participant>
                                                </action-soccer-score>
                                            </xsl:for-each>
                                            <xsl:for-each select="MatchEventList/MatchEventExtended[MatchEventType='Advarsel']">
                                            <action-soccer-foul>
                                                <xsl:attribute name="minutes-elapsed">
                                                    <xsl:value-of select="Minute"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="sequence-number">
                                                    <xsl:value-of select="MatchEventId"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="fouling-player-idref">
                                                    <xsl:text>p.</xsl:text>
                                                    <xsl:value-of select="PlayerId"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="fouling-team-idref">
                                                    <xsl:text>t.</xsl:text>
                                                    <xsl:value-of select="TeamId"/>
                                                </xsl:attribute>
                                            </action-soccer-foul>
                                            <action-soccer-penalty>
                                                <xsl:attribute name="minutes-elapsed">
                                                    <xsl:value-of select="Minute"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="sequence-number">
                                                    <xsl:value-of select="MatchEventId"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="fouling-player-idref">
                                                    <xsl:text>p.</xsl:text>
                                                    <xsl:value-of select="PlayerId"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="fouling-team-idref">
                                                    <xsl:text>t.</xsl:text>
                                                    <xsl:value-of select="TeamId"/>
                                                </xsl:attribute>
                                                
                                            </action-soccer-penalty>
                                            </xsl:for-each>
                                            <xsl:for-each select="MatchEventList/MatchEventExtended[MatchEventType='Bytte ut']">
                                            <action-soccer-substitution>
                                                <xsl:attribute name="minutes-elapsed">
                                                    <xsl:value-of select="Minute"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="sequence-number">
                                                    <xsl:value-of select="MatchEventId"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="team-idref">
                                                    <xsl:text>t.</xsl:text>
                                                    <xsl:value-of select="TeamId"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="person-original-idref">
                                                    <xsl:text>p.</xsl:text>
                                                    <xsl:value-of select="PlayerId"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="person-replacing-idref">
                                                    <xsl:variable name="connectedToEvent">
                                                        <xsl:value-of select="ConnectedToEvent"/>
                                                    </xsl:variable>
                                                    <xsl:text>p.</xsl:text>
                                                    <xsl:value-of select="preceding-sibling::MatchEventExtended[MatchEventType='Bytte inn' and ConnectedToEvent=$connectedToEvent]/PlayerId"/>
                                                </xsl:attribute>
                                            </action-soccer-substitution>
                                            </xsl:for-each>
                                            
                                        </event-actions-soccer>
                                    </event-actions>
                                </xsl:if>
                            </sports-event>
                        </tournament-round>
                    </xsl:for-each>
                </tournament-division>
            </tournament>

        </sports-content>
    </xsl:template>
</xsl:stylesheet>
