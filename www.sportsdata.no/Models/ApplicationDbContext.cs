﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationDbContext.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The application db context.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using NTB.SportsData.Data.DataMappers.Role;
    using NTB.SportsData.Data.DataMappers.User.Web;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The application db context.
    /// </summary>
    public class ApplicationDbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationDbContext"/> class.
        /// </summary>
        public ApplicationDbContext()
        {
        }

        // public List<ApplicationUser> Users { get; set; }

        /// <summary>
        /// Gets or sets the _users.
        /// </summary>
        private List<ApplicationUser> _users
        {
            get
            {
                var userDataMapper = new UserDataMapper();
                return userDataMapper.GetAll().ToList();
            }

            set
            {
                this._users.AddRange(value);
            }
        }

        /// <summary>
        /// Gets or sets the _roles.
        /// </summary>
        private List<Role> _roles
        {
            get
            {
                var roleDataMapper = new RoleDataMapper();
                return roleDataMapper.GetRoles().ToList();
            }

            set
            {
                this._roles.AddRange(value);
            }
        }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public List<ApplicationUser> Users
        {
            get
            {
                return this._users;
            }

            set
            {
                if (this._users != null)
                {
                    this._users.AddRange(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the roles.
        /// </summary>
        public List<Role> Roles { get; set; }

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="ApplicationDbContext"/>.
        /// </returns>
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        /// <summary>
        /// The get application id.
        /// </summary>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        public Guid GetApplicationId()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                // running the command
                var sqlCommand = new SqlCommand("AspNet_Identity_GetApplicationId", sqlConnection) { CommandType = CommandType.StoredProcedure };

                sqlCommand.Parameters.Add(new SqlParameter("@ApplicationName", @"/"));

                var sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return Guid.NewGuid();
                }

                while (sqlDataReader.Read())
                {
                    return sqlDataReader.GetGuid(0);
                }
            }

            return Guid.NewGuid();
        }

        /// <summary>
        /// This method shall save the changes to the database
        /// </summary>
        internal void SaveChanges()
        {
            throw new NotImplementedException();
        }
    }
}