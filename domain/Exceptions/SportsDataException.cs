﻿using System;

namespace NTB.SportsData.Domain.Exceptions
{
    public class SportsDataException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SportsDataException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public SportsDataException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SportsDataException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public SportsDataException(string message, Exception exception)
            : base(message, exception)
        { }
    }
}
