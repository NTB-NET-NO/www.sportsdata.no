﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContactPersonDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The contact person data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Data.DataMappers.ContactPersons
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Data.Interfaces.Common;
    using NTB.SportsData.Data.Interfaces.DataMappers;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The contact person data mapper.
    /// </summary>
    public class ContactPersonDataMapper : IRepository<ContactPerson>, IDisposable, IContactPersonDataMapper
    {
        #region Static Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(ContactPersonDataMapper));

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The activate deactivate contact person.
        /// </summary>
        /// <param name="personId">
        /// The person id.
        /// </param>
        public void ActivateDeactivateContactPerson(int personId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_ActivateDeactivateContactPerson", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", personId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(ContactPerson domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete contact team map.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="contactId">
        /// The contact id.
        /// </param>
        public void DeleteContactTeamMap(int teamId, int contactId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteTeamContactMap", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", teamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", contactId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        ///     The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ContactPerson"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public ContactPerson Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get all.
        /// </summary>
        /// <returns>
        ///     The <see cref="IQueryable" />.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<ContactPerson> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get contact person by team id.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ContactPerson> GetContactPersonByTeamId(int teamId)
        {
            var contactPersons = new List<ContactPerson>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetContactPersonsByTeamId", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", teamId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return contactPersons;
                    }

                    while (sqlDataReader.Read())
                    {
                        var contactPerson = new ContactPerson
                                                {
                                                    PersonId =
                                                        Convert.ToInt32(sqlDataReader["ContactPersonId"]), 
                                                    FirstName = sqlDataReader["FirstName"].ToString(), 
                                                    LastName = sqlDataReader["LastName"].ToString(), 
                                                    HomePhone = sqlDataReader["HomePhone"].ToString(), 
                                                    OfficePhone = sqlDataReader["OfficePhone"].ToString(), 
                                                    MobilePhone = sqlDataReader["MobilePhone"].ToString(), 
                                                    TeamId = Convert.ToInt32(sqlDataReader["TeamId"]), 
                                                    TeamName = sqlDataReader["TeamName"].ToString(), 
                                                    RoleId = Convert.ToInt32(sqlDataReader["TeamRoleId"]), 
                                                    RoleName = sqlDataReader["TeamRoleName"].ToString(), 
                                                    Active = Convert.ToInt32(sqlDataReader["Active"])
                                                };

                        contactPersons.Add(contactPerson);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return contactPersons;
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertAll(List<ContactPerson> domainobject)
        {
            if (!domainobject.Any())
            {
                return;
            }

            foreach (var contactPerson in domainobject)
            {
                this.InsertOne(contactPerson);
            }
        }

        /// <summary>
        /// The insert contact team map.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="contactId">
        /// The contact id.
        /// </param>
        public void InsertContactTeamMap(int teamId, int contactId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertTeamContactMap", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", teamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", contactId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(ContactPerson domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertContactPerson", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", domainobject.TeamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", domainobject.PersonId));
                    sqlCommand.Parameters.Add(new SqlParameter("@FirstName", domainobject.FirstName));
                    sqlCommand.Parameters.Add(new SqlParameter("@LastName", domainobject.LastName));
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamRoleId", domainobject.RoleId));
                    sqlCommand.Parameters.Add(new SqlParameter("@HomePhone", domainobject.HomePhone));
                    sqlCommand.Parameters.Add(new SqlParameter("@OfficePhone", domainobject.OfficePhone));
                    sqlCommand.Parameters.Add(new SqlParameter("@MobilePhone", domainobject.MobilePhone));
                    sqlCommand.Parameters.Add(new SqlParameter("@Active", 1));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return 1;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void Update(ContactPerson domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_UpdateContactPerson", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", domainobject.PersonId));
                    sqlCommand.Parameters.Add(new SqlParameter("@FirstName", domainobject.FirstName));
                    sqlCommand.Parameters.Add(new SqlParameter("@LastName", domainobject.LastName));
                    sqlCommand.Parameters.Add(new SqlParameter("@HomePhone", domainobject.HomePhone));
                    sqlCommand.Parameters.Add(new SqlParameter("@OfficePhone", domainobject.OfficePhone));
                    sqlCommand.Parameters.Add(new SqlParameter("@MobilePhone", domainobject.MobilePhone));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        #endregion
    }
}