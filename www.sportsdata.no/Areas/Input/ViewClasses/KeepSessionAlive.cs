﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KeepSessionAlive.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The keep session alive.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Areas.Input.ViewClasses
{
    using System;
    using System.Web;
    using System.Web.SessionState;

    /// <summary>
    /// The keep session alive.
    /// </summary>
    public class KeepSessionAlive : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Session["KeepSessionAlive"] = DateTime.Now;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}