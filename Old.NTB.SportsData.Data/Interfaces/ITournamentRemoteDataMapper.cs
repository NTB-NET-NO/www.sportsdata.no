﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITournamentRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TournamentRemoteDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Data.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The TournamentRemoteDataMapper interface.
    /// </summary>
    public interface ITournamentRemoteDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get tournament standings.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<TournamentTable> GetTournamentStandings(Tournament tournament);

        #endregion
    }
}