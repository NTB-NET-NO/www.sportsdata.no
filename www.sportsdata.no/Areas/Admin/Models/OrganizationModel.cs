﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.NFF.TournamentAccess;
using NTB.SportsData.Web.Areas.Admin.Repositories;

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    /// <summary>
    /// The organization model.
    /// </summary>
    public class OrganizationModel
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(OrganizationModel));

        /// <summary>
        /// Gets or sets the org id.
        /// </summary>
        public int OrgId { get; set; }

        /// <summary>
        /// The get selected season detail by seasonid.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="SelectedSeasonDetailModel"/>.
        /// </returns>
        public SelectedSeasonDetailModel GetSelectedSeasonDetailBySeasonid(int seasonId, int orgId)
        {
            var org = this.GetOrganizationById(orgId);

            var seasonRepository = new SeasonRepository();
            var season = seasonRepository.Get(seasonId);

            var clubRepository = new ClubRepository();
            var clubs = clubRepository.GetClubsByOrgId(orgId);

            return new SelectedSeasonDetailModel {Season = season, Organization = org, Clubs = clubs};
        }

        /// <summary>
        /// The update checked clubs.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        public void UpdateCheckedClubs(int orgId, FormCollection collection)
        {
            var clubs = new List<Club>();

            var checkedClubs = collection["club[]"].Split(',');

            foreach (var checkedClub in checkedClubs)
            {
                var splittedValue = checkedClub.Split('|');
                var clubId = Convert.ToInt32(splittedValue[0]);
                var districtId = Convert.ToInt32(splittedValue[1]);
                var municipalityId = Convert.ToInt32(splittedValue[2]);

                var club = new Club
                {
                    ClubId = clubId,
                    ParentOrgId = orgId,
                    DistrictId = districtId,
                    MunicipalityId = municipalityId
                };

                Logger.Debug("clubId: " + club.ClubId);
                Logger.Debug("DistrictId: " + club.DistrictId);
                Logger.Debug("MunicipalityId: " + club.MunicipalityId);

                clubs.Add(club);
            }

            Logger.Debug("Number of clubs: " + clubs.Count());

            var repository = new ClubRepository();
            foreach (var club in clubs)
            {
                repository.UpdateCheckedClub(club);
            }
        }

        /// <summary>
        /// The get organization details by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="OrganizationDetail"/>.
        /// </returns>
        public OrganizationDetail GetOrganizationDetailsById(int id)
        {
            var org = this.GetOrganizationById(id);

            return new OrganizationDetail {Org = org };
        }

        /// <summary>
        /// The get organization clubs.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Club> GetOrganizationClubs(int seasonId, int orgId)
        {
            // Soccer
            var clubs = new List<Club>();
            if (orgId == 365)
            {
                var facade = new TournamentFacade();
                clubs = facade.GetClubs();

                var alteredClub = new List<Club>();
                foreach (var club in clubs)
                {
                    club.ParentOrgId = orgId;
                    alteredClub.Add(club);
                }

                clubs = alteredClub;
            }
            else if (orgId == 2000)
            {
                var facade = new Facade.Profixio.ProfixioFacade.TournamentFacade();
                clubs = facade.GetClubsByTournament(seasonId);
                var alteredClub = new List<Club>();
                foreach (var club in clubs)
                {
                    club.ParentOrgId = orgId;
                    alteredClub.Add(club);
                }

                clubs = alteredClub;
            }

            // else
            // {
            // // marked out as it is currently not being used
            // // var facade = new Facade.NIF.TournamentAccess.TournamentFacade();
            // }
            return clubs;
        }

        /// <summary>
        /// The create season.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        public void CreateSeason(int orgId, FormCollection collection)
        {
            var season = new Season
            {
                SeasonId = Convert.ToInt32(collection["seasonId"]),
                SeasonName = collection["seasonName"],
                SeasonStartDate = Convert.ToDateTime(collection["seasonStartDate"]),
                SeasonEndDate = Convert.ToDateTime(collection["seasonEndDate"]),
                SportId = Convert.ToInt32(collection["sport"])
            };
            var seasonActive = Convert.ToInt32(collection["seasonActive"]) == 1;

            season.SeasonActive = seasonActive;

            var repository = new SeasonRepository();
            repository.InsertOne(season);
        }

        /// <summary>
        /// The get create season view.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="CreateSeasonView"/>.
        /// </returns>
        public CreateSeasonView GetCreateSeasonView(int orgId)
        {
            var view = new CreateSeasonView {OrgId = orgId};
            var sportRepository = new SportRepository();

            view.Sports = sportRepository.GetAll()
                .ToList();

            return view;
        }

        /// <summary>
        /// The get clubs by parent org id.
        /// </summary>
        /// <param name="parentOrgId">
        /// The parent org id.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Club> GetClubsByParentOrgId(int parentOrgId, bool filter)
        {
            var repository = new ClubRepository();
            var clubs = repository.GetClubsByParentOrgId(parentOrgId);

            if (filter)
            {
                return clubs.Where(c => c.DistrictId > 0 || c.MunicipalityId > 0)
                    .ToList();
            }

            return clubs;
        }

        /// <summary>
        /// The find clubs.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="selectedOrgId">
        /// The selected org id.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        public Dictionary<Club, List<Club>> FindClubs(int orgId, int selectedOrgId)
        {
            var repository = new ClubRepository();
            var orgs = repository.GetClubsByOrgId(orgId);

            var clubDictionary = new Dictionary<Club, List<Club>>();

            // We only want to list clubs that do not have values for district or municipality
            foreach (var org in orgs.Where(o => o.DistrictId == 0 || o.MunicipalityId == 0))
            {
                var splitClub = org.ClubName.Split(' ');
                var mainPart = splitClub[0].Length > 2 ? splitClub[0] : splitClub[1];

                var foundClubs = repository.GetClubsByNameAndOrgId(
                    mainPart.Replace(",", string.Empty)
                        .Trim(),
                    selectedOrgId);
                clubDictionary.Add(org, foundClubs);
            }

            return clubDictionary;
        }

        /// <summary>
        /// The get organizations.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Organization> GetOrganizations()
        {
            var repository = new OrganizationRepository();
            var orgs = repository.GetAll()
                .ToList();
            var newOrgs = new List<Organization>();
            foreach (var org in orgs)
            {
                if (org.OrganizationId == this.OrgId)
                {
                    continue;
                }

                var newOrg = new Organization
                {
                    OrganizationId = org.OrganizationId,
                    OrganizationName = org.OrganizationName,
                    OrganizationNameShort = org.OrganizationNameShort,
                    SingleSport = org.SingleSport,
                    Sport = org.Sport,
                    SportId = org.SportId,
                    TeamSport = org.TeamSport
                };
                newOrgs.Add(newOrg);
            }

            orgs = newOrgs;
            return orgs;
        }

        /// <summary>
        /// The get organization by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Organization"/>.
        /// </returns>
        public Organization GetOrganizationById(int id)
        {
            var repository = new OrganizationRepository();
            return repository.Get(id);
        }

        /// <summary>
        /// The insert organization.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        public void InsertOrganization(FormCollection collection)
        {
            var singlesport = 0;
            var teamsport = 0;

            if (collection["sporttype"] == "SingleSport")
            {
                singlesport = 1;
            }

            if (collection["sporttype"] == "TeamSport")
            {
                teamsport = 1;
            }

            var organization = new Organization
            {
                OrganizationName = collection["orgname"],
                OrganizationId = Convert.ToInt32(collection["orgid"]),
                OrganizationUserId = Convert.ToInt32(collection["orgUserId"]),
                OrganizationNameShort = collection["orgshortname"].ToUpper(),
                SingleSport = singlesport,
                TeamSport = teamsport,
                Sport = collection["sport"],
                SportId = Convert.ToInt32(collection["sportid"])
            };

            var repository = new OrganizationRepository();
            repository.InsertOne(organization);
        }

        /// <summary>
        /// The update organization.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        public void UpdateOrganization(FormCollection collection)
        {
            var singlesport = 0;
            var teamsport = 0;
            if (collection["sporttype"] == "SingleSport")
            {
                singlesport = 1;
            }

            if (collection["sporttype"] == "TeamSport")
            {
                teamsport = 1;
            }

            var organization = new Organization
            {
                OrganizationName = collection["orgname"],
                OrganizationId = Convert.ToInt32(collection["orgid"]),
                OrganizationUserId = Convert.ToInt32(collection["orgUserid"]),
                OrganizationNameShort = collection["orgshortname"].ToUpper(),
                SingleSport = singlesport,
                TeamSport = teamsport,
                Sport = collection["sport"],
                SportId = Convert.ToInt32(collection["sportid"])
            };

            var repository = new OrganizationRepository();
            repository.Update(organization);
        }

        /// <summary>
        /// The create sport.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        public void CreateSport(int id, FormCollection collection)
        {
            var sportrepository = new SportRepository();

            var sport = new Sport {Id = Convert.ToInt32(collection["sportId"]), Name = collection["sportName"]};
            
            if (collection["sportName"] == string.Empty)
            {
                // he or she has selected a radiobutton
                sport.Name = sportrepository.Get(Convert.ToInt32(collection["radioSport"]))
                    .Name;
            }

            sport.HasTeamResults = 0;
            sport.HasIndividualResult = 0;
            switch (collection["sportType"])
            {
                case "TeamSport":
                    sport.HasTeamResults = 1;
                    break;
                case "SingleSport":
                    sport.HasIndividualResult = 1;
                    break;
            }

            sport.OrgId = id;

            sportrepository.InsertOne(sport);
        }

        /// <summary>
        /// The find new clubs.
        /// </summary>
        /// <param name="clubs">
        /// The clubs.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Club> FindNewClubs(List<Club> clubs)
        {
            var listNewClubs = new List<Club>();

            var clubRepository = new ClubRepository();

            foreach (var club in clubs)
            {
                var foundClub = clubRepository.Get(club.ClubId);

                if (foundClub == null)
                {
                    listNewClubs.Add(club);
                }
            }

            return listNewClubs;
        }

        /// <summary>
        /// The insert clubs.
        /// </summary>
        /// <param name="clubs">
        /// The clubs.
        /// </param>
        public void InsertClubs(List<Club> clubs)
        {
            var clubRepository = new ClubRepository();
            foreach (var club in clubs)
            {
                clubRepository.InsertOne(club);
            }
        }

        /// <summary>
        /// The update club advanced.
        /// </summary>
        /// <param name="clubId">
        /// The club id.
        /// </param>
        /// <param name="parentOrgid">
        /// The parent orgid.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        public void UpdateClubAdvanced(int clubId, int parentOrgid, FormCollection collection)
        {
            var districtValue = Convert.ToInt32(collection["district_" + clubId]);
            var municipalityValue = Convert.ToInt32(collection["municipality_" + clubId]);
            var countryValue = collection["country_" + clubId];

            var club = new Club
            {
                ClubId = clubId,
                DistrictId = districtValue,
                MunicipalityId = municipalityValue,
                ParentOrgId = parentOrgid,
                CountryCode = countryValue
            };

            var clubRepository = new ClubRepository();
            clubRepository.UpdateClubAdvanced(club);
        }

        /// <summary>
        /// The get advanced check club view.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="connectedOrgId">
        /// The connected org id.
        /// </param>
        /// <returns>
        /// The <see cref="AdvancedCheckClubViewModel"/>.
        /// </returns>
        public AdvancedCheckClubViewModel GetAdvancedCheckClubView(int orgId, int connectedOrgId)
        {
            var municipalityRepository = new MunicipalityRepository();
            var sportId = 0;
            if (connectedOrgId == 365)
            {
                sportId = 16;
            }

            if (sportId == 0)
            {
                return null;
            }

            var municipalities = municipalityRepository.GetMunicipalitiesBySportId(sportId);

            var districtRepository = new DistrictRepository();
            var districts = districtRepository.GetDistrictsBySportId(sportId);

            var countryRepository = new CountryRepository();
            var countries = countryRepository.GetAll()
                .ToList();

            var clubRepository = new ClubRepository();
            var clubs = clubRepository.GetClubsByOrgId(orgId);
            if (clubs.Count > 0)
            {
                clubs = clubs.Where(x => x.DistrictId == 0 || x.MunicipalityId == 0)
                    .ToList();
            }

            var viewModel = new AdvancedCheckClubViewModel
            {
                Countries = countries,
                Districts = districts,
                Municipalities = municipalities,
                Clubs = clubs
            };

            return viewModel;
        }
    }
}