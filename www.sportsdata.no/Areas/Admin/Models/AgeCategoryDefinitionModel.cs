﻿using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.NFF.TournamentAccess;
using NTB.SportsData.Facade.NIF.SportAccess;
using NTB.SportsData.Web.Areas.Admin.Repositories;

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    public class AgeCategoryDefinitionModel
    {
        public DefineAgeCategoryView GetDefineAgeCategoryViewBySportId(int sportId)
        {
            AgeCategoryDefinitionRepository repository = new AgeCategoryDefinitionRepository();
            repository.GetAll();

            var seasonRepository = new SeasonRepository();
            var seasons = new List<Season>(seasonRepository.GetSeasonsBySportId(sportId));

            if (!seasons.Any())
            {
                if (sportId == 16)
                {
                    var facade = new TournamentFacade();
                    seasons = facade.GetSeasons();
                }
                else
                {
                    var organizationRepository = new OrganizationRepository();
                    Organization organization = organizationRepository.GetOrganizationBySportId(sportId);
                    var facade = new SportFacade();
                    seasons = facade.GetFederationSeasons(organization.OrganizationId);
                }

            }

            var defineAgeCategoryView = new DefineAgeCategoryView
                {
                    AgeCategoryDefinitions = new List<AgeCategoryDefinition>(repository.GetAll()),
                    Seasons = seasons
                };

            return defineAgeCategoryView;
        }

        public List<AgeCategoryDefinition> GetAgeCategoryDefinitions()
        {
            var repository = new AgeCategoryDefinitionRepository();
            return repository.GetAll().ToList();
        }
    }
}