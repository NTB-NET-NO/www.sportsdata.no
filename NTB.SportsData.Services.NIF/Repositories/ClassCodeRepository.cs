﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassCodeRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The class code repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Services.NIF.Exceptions;

namespace NTB.SportsData.Services.NIF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net;

    using NTB.SportsData.Services.NIF.Interfaces;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The class code repository.
    /// </summary>
    public class ClassCodeRepository : IRepository<ClassCode>, IDisposable, IClassCodeDataMapper
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassCodeRepository"/> class.
        /// </summary>
        public ClassCodeRepository()
        {
            if (this._matchServiceClient.ClientCredentials == null)
            {
                return;
            }

            if (this._activityServiceClient.ClientCredentials == null)
            {
                return;
            }

            if (this._tournamentServiceClient.ClientCredentials == null)
            {
                return;
            }

            this._matchServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            this._matchServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            this._activityServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            this._activityServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            this._tournamentServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            this._tournamentServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            ServicePointManager.ServerCertificateValidationCallback = (servicePointSender, certificate, chain, sslPolicyErrors) => true; // Ignoring cert
        }

        #endregion

        #region Fields

        /// <summary>
        /// The _activity service 1 client.
        /// </summary>
        private readonly ActivityServiceClient _activityServiceClient = new ActivityServiceClient();

        /// <summary>
        ///     The region service v2 client.
        /// </summary>
        private readonly MatchServiceClient _matchServiceClient = new MatchServiceClient();

        /// <summary>
        ///     The region service v2 client.
        /// </summary>
        private readonly TournamentServiceClient _tournamentServiceClient = new TournamentServiceClient();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(ClassCode domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ClassCode"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public ClassCode Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<ClassCode> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get class codes by tournament id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Throws an exception if something goes wrong
        /// </exception>
        public List<TournamentClasses> GetClassCodesByTournamentId(int tournamentId)
        {
            var request = new TournamentIdRequest2() { TournamentId = tournamentId };

            var result = this._tournamentServiceClient.GetTournamentClassesByTournamentId(request);

            if (!result.Success)
            {
                var errorMessage = string.Format("An error has occured. ErrorCode: {0}. Message: {1}", result.ErrorCode, result.ErrorMessage);
                throw new SportsDataException(errorMessage);
            }

            return result.TournamentClassesList.ToList();
        }

        /// <summary>
        /// The get class codes by class ids.
        /// </summary>
        /// <param name="classIds">
        /// The class ids.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public List<TournamentClass> GetClassCodesByClassIds(int[] classIds)
        {
            // var classIds = new[] { classId };
            var request = new TournamentClassIdsRequest() { TournamentClassIds = classIds };
            var result = this._tournamentServiceClient.GetClassByClassIds(request);

            if (!result.Success)
            {
                var errorMessage = string.Format("An error has occured. ErrorCode: {0}. Message: {1}", result.ErrorCode, result.ErrorMessage);
                throw new Exception(errorMessage);
            }

            return result.TournamentClassList.ToList();
        }

        /// <summary>
        /// The get class codes by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public List<TournamentClass> GetClassCodesByOrgId(int orgId)
        {
            var request = new OrgIdRequest18() { OrgId = orgId };

            var result = this._tournamentServiceClient.GetClassByOrg(request);

            if (!result.Success)
            {
                var errorMessage = string.Format("An error has occured. ErrorCode: {0}. Message: {1}", result.ErrorCode, result.ErrorMessage);
                throw new Exception(errorMessage);
            }

            return result.TournamentClassList.ToList();
        }

        /// <summary>
        /// The get class codes.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ClassCode> GetClassCodes(int orgId)
        {
            var genders = new List<int> { (int)Gender.Female, (int)Gender.Male, (int)Gender.Unknown };

            var orgActivityRequest = new ActivitiesByOrgIdRequest { Id = orgId };

            var orgActivityResponse = this._activityServiceClient.GetActivitiesByOrg(orgActivityRequest);

            if (!orgActivityResponse.Success)
            {
                return null;
            }

            var activities = orgActivityResponse.Activities.ToList();

            var classCodes = new List<ClassCode>();
            foreach (var activity in activities)
            {
                foreach (var genderId in genders)
                {
                    var request = new ClassCodesRequest { FederationId = orgId, Sex = genderId, ActivityId = activity.Id };

                    var response = this._matchServiceClient.GetClassCodes(request);

                    if (!response.Success)
                    {
                        return null;
                    }

                    classCodes.AddRange(response.ClassCode);
                }
            }

            return classCodes;
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<ClassCode> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(ClassCode domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(ClassCode domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}