﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FunctionRepository.cs" company="">
//   
// </copyright>
// <summary>
//   The function repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NIF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using NTB.SportsData.Services.NIF.Interfaces;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The function repository.
    /// </summary>
    public class FunctionRepository : IRepository<Function>, IDisposable, IFunctionDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Function domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Function"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Function Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<Function> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get team functions.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Function> GetTeamFunctions(int teamId)
        {
            var client = new FunctionServiceClient();
            if (client.ClientCredentials == null)
            {
                return new List<Function>();
            }

            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            var request = new FunctionByOrgRequest { OrgId = teamId };

            FunctionsResponse response = client.GetFunctionsForOrganisation(request);

            if (!response.Success)
            {
                return new List<Function>();
            }

            return response.Functions.ToList();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<Function> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(Function domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Function domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}