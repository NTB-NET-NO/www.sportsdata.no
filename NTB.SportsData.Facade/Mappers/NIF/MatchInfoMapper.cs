﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchInfoMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match info mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NIF.NIFProdService;

    using Match = NTB.SportsData.Domain.Classes.Match;

    /// <summary>
    /// The match info mapper.
    /// </summary>
    public class MatchInfoMapper : BaseMapper<MatchInfo1, Match>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<MatchInfo1, Match> mapper)
        {
            mapper.Relate(x => x.Awayteam, y => y.AwayTeam);
            mapper.Relate(x => x.AwayteamId, y => y.AwayTeamId);
            mapper.Relate(x => x.AwayGoals, y => y.AwayTeamGoal);
            mapper.Relate(x => x.Hometeam, y => y.HomeTeam);
            mapper.Relate(x => x.HometeamId, y => y.HomeTeamId);
            mapper.Relate(x => x.HomeGoals, y => y.HomeTeamGoal);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.MatchDate, y => y.MatchDate);
            mapper.Relate(x => x.MatchStartTime, y => y.MatchStartTime);
        }
    }
}