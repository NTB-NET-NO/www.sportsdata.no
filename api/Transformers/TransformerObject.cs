﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TransformerObject.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Transformers
{
    using System.Configuration;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Xsl;

    /// <summary>
    /// The transformer object.
    /// </summary>
    public static class TransformerObject
    {
        #region Public Methods and Operators

        /// <summary>
        /// The transform object.
        /// </summary>
        /// <param name="document">
        /// The document.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string TransformObject(string document)
        {
            var transformer = new XslCompiledTransform();
            string xsltPath = ConfigurationManager.AppSettings["sportsmlxsltfilepath"];
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(document);

            transformer.Load(xsltPath + @"\sportsml.xsl");

            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t" };
            settings.Encoding = Encoding.UTF8;
            settings.OmitXmlDeclaration = false;

            using (var textWriter = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(textWriter, settings))
                {
                    transformer.Transform(xmlDoc, writer);
                }

                return textWriter.ToString();
            }
        }

        #endregion
    }
}