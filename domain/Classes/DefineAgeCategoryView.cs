﻿using System.Collections.Generic;

namespace NTB.SportsData.Domain.Classes
{
    public class DefineAgeCategoryView
    {
        public List<AgeCategoryDefinition> AgeCategoryDefinitions { get; set; }

        public List<Season> Seasons { get; set; }
    }
}