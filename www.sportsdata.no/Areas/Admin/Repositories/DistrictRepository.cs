// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DistrictRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The district repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Areas.Admin.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using log4net;
    using log4net.Config;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Web.Areas.Admin.Interfaces;

    /// <summary>
    /// The district repository.
    /// </summary>
    public class DistrictRepository : IRepository<District>, IDisposable, IDistrictDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(DistrictRepository));

        /// <summary>
        /// Initializes a new instance of the <see cref="DistrictRepository"/> class.
        /// </summary>
        public DistrictRepository()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get districts by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<District> GetDistrictsBySportId(int sportId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // DONE: Move code to SportsData Stored Procedures
                var sqlCommand = new SqlCommand("SportsData_GetDistricts", sqlConnection)
                                     {
                                         CommandType =
                                             CommandType
                                             .StoredProcedure
                                     };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                sqlConnection.Open();
                var districts = new List<District>();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        var district = new District
                                           {
                                               DistrictId = (int)sqlDataReader["DistrictId"], 
                                               DistrictName = sqlDataReader["DistrictName"].ToString()
                                           };

                        districts.Add(district);
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();

                return districts;
            }
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(District domainobject)
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsData_InsertCountyTable", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@CountyId", domainobject.DistrictId));

                    sqlCommand.Parameters.Add(new SqlParameter("@CountyName", domainobject.DistrictName));

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Parameters.Clear();
                    sqlCommand.CommandText = "SELECT @@IDENTITY";
                    sqlCommand.CommandType = CommandType.Text;

                    int insertId = Convert.ToInt32(sqlCommand.ExecuteScalar());

                    sqlCommand.Dispose();

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }

                    return insertId;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return 0;
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertAll(List<District> domainobject)
        {
            foreach (District district in domainobject)
            {
                this.InsertOne(district);
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(District domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(District domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<District> GetAll()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // DONE: Move code to SportsData Stored Procedures
                var sqlCommand = new SqlCommand("SportsData_GetDistricts", sqlConnection)
                                     {
                                         CommandType =
                                             CommandType
                                             .StoredProcedure
                                     };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", DBNull.Value));

                sqlConnection.Open();
                var districts = new List<District>();
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var district = new District
                                               {
                                                   DistrictId = (int)sqlDataReader["DistrictId"], 
                                                   DistrictName = sqlDataReader["DistrictName"].ToString()
                                               };

                            districts.Add(district);
                        }

                        sqlDataReader.Close();
                    }

                    sqlConnection.Close();
                }

                return districts.AsQueryable();
            }
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="District"/>.
        /// </returns>
        public District Get(int id)
        {
            return this.GetAll().Single(d => d.DistrictId == id);
        }

        /// <summary>
        /// The delete all.
        /// </summary>
        public void DeleteAll()
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand("SportsData_TruncateCountiesTable", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }
    }
}