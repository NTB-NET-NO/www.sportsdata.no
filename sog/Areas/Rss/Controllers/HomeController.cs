﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Web;
using System.Web.Mvc;
using log4net;
using sog.Areas.Rss.Results;
using sog.Contexts;

namespace sog.Areas.Rss.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private readonly ILog logger = LogManager.GetLogger("SogApiFileAppender");

        // GET: Rss/Home
        public ActionResult Index()
        {
            this.logger.Debug("Creating medalists context");
            // return View();
            var medalistsContext = new MedalistsContext();
            var files = medalistsContext.GetFiles(ConfigurationManager.AppSettings["medalistsinputpath"])
                .OrderBy(x => x.CreationTime)
                .ToList();
            var distinctEvents = medalistsContext.GetDistinctEvents(files);
            var sportsContents = medalistsContext.GetSportsContents(distinctEvents);

            var items = (from sc in sportsContents
                from se in sc.SportsEvent
                select se);
            
            var syndicationItems = new List<SyndicationItem>();
            foreach (var item in items)
            {
                try
                {
                    var medalistsString = new StringBuilder();
                    var playerName = string.Empty;

                    var teamGoldMedalist = (from t in item.Team
                        from a in t.TeamStats.Award
                        where a.Value == "gold"
                        select t).Single();
                    if (teamGoldMedalist.Player != null)
                    {
                        playerName = teamGoldMedalist.Player[0].PlayerMetaData.PlayerNames[0].FullName;
                    }
                    var goldMedalist = teamGoldMedalist.TeamMetaData.Nationality;
                    
                    if (playerName != string.Empty)
                    {
                        goldMedalist = playerName + ", " + teamGoldMedalist.TeamMetaData.Nationality;
                    }
                    this.logger.Debug(goldMedalist);


                    var teamSilverMedalist = (from t in item.Team
                                            from a in t.TeamStats.Award
                                            where a.Value == "silver"
                                            select t).Single();
                    if (teamSilverMedalist.Player != null)
                    {
                        playerName = teamSilverMedalist.Player[0].PlayerMetaData.PlayerNames[0].FullName;
                    }
                    var silverMedalist = teamSilverMedalist.TeamMetaData.Nationality;
                    if (playerName != string.Empty)
                    {
                        silverMedalist = playerName + ", " + teamSilverMedalist.TeamMetaData.Nationality;
                    }



                    var teamBronzeMedalist = (from t in item.Team
                                              from a in t.TeamStats.Award
                                              where a.Value == "bronze"
                                              select t).Single();
                    if (teamBronzeMedalist.Player != null)
                    {
                        playerName = teamBronzeMedalist.Player[0].PlayerMetaData.PlayerNames[0].FullName;
                    }
                    var bronzeMedalist = teamBronzeMedalist.TeamMetaData.Nationality;
                    if (playerName != string.Empty)
                    {
                        bronzeMedalist = playerName + ", " + teamBronzeMedalist.TeamMetaData.Nationality;
                    }

                    medalistsString.Append("Gull: " + goldMedalist + Environment.NewLine);
                    medalistsString.Append("Sølv: " + silverMedalist + Environment.NewLine);
                    medalistsString.Append("Bronse: " + bronzeMedalist + Environment.NewLine);
                    var syndicationItem = new SyndicationItem(item.EventMetaData.EventName.FullName,
                        medalistsString.ToString(), new Uri("http://dev.sportsdata.no/sog/rss"));
                    syndicationItems.Add(syndicationItem);
                }
                catch (Exception exception)
                {
                    this.logger.Error(exception);
                }
            }

            
            var syndicationFeed = new SyndicationFeed("Olympiske medalister", "Liste over medaljevinnere i OL", new Uri("http://dev.sportsdata.no/sog/rss"));
            syndicationFeed.Items = syndicationItems;

            return new FeedResult(new Rss20FeedFormatter(syndicationFeed));
        }
    }
}