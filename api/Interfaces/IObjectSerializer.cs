﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IObjectSerializer.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// The ObjectSerializer interface.
    /// </summary>
    /// <typeparam name="T">
    /// General definition of a parameter
    /// </typeparam>
    internal interface IObjectSerializer<T>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The serialize object.
        /// </summary>
        /// <param name="objects">
        /// The objects.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string SerializeObjects(List<T> objects);

        /// <summary>
        /// The serialize object.
        /// </summary>
        /// <param name="domainObject">
        /// The domain object.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string SerializeObject(T domainObject);

        /// <summary>
        ///     The serializer.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        string Serializer();

        #endregion
    }
}