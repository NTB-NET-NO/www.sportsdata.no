﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserProfileDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The UserProfileDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    using System;
    using System.Linq;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The UserProfileDataMapper interface.
    /// </summary>
    public interface IUserProfileDataMapper
    {
        /// <summary>
        /// The get user profiles.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        IQueryable<UserProfile> GetUserProfiles();

        /// <summary>
        /// The get user profile.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="UserProfile"/>.
        /// </returns>
        UserProfile GetUserProfile(Guid userId);
    }
}