﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CalendarModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The calendar model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Areas.Calendar.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.NFF.TournamentAccess;
    using NTB.SportsData.Facade.NIF.SportAccess;
    using NTB.SportsData.Web.Areas.Admin.Repositories;
    
    /// <summary>
    /// The calendar model.
    /// </summary>
    public class CalendarModel
    {
        /// <summary>
        /// The get todays matches.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<CalendarViewModel> GetTodaysMatches()
        {
            var modelList = new List<CalendarViewModel>();

            var soccerFacade = new TournamentFacade();
            var matches = soccerFacade.GetTodaysMatches(DateTime.Today);
            foreach (var match in matches)
            {
                match.SportId = 16;
                match.SportName = "Fotball";

                var model = new CalendarViewModel()
                                {
                                    EventId = match.MatchId,
                                    EventName = match.HomeTeam + " " + match.AwayTeam,
                                    EventDateTime = match.MatchDate,
                                    Sport = match.SportName,
                                    SportId = match.SportId
                                };
                modelList.Add(model);
            }

            var sportFacade = new SportFacade();
            var federations = sportFacade.GetFederations();

            foreach (var federation in federations)
            {
                if (federation.HasTeamResults == 1)
                {
                    var seasons = sportFacade.GetFederationSeasons(federation.FederationId);

                    var activeSeason = seasons.SingleOrDefault(x => x.SeasonActive == true);
                    if (activeSeason != null)
                    {
                        var sportMatches = sportFacade.GetTodaysMatches(DateTime.Today, activeSeason.SeasonId);
                        foreach (var sportMatch in sportMatches)
                        {
                            sportMatch.SportId = activeSeason.SportId;

                            // Adding to list
                            var model = new CalendarViewModel()
                            {
                                EventId = sportMatch.MatchId,
                                EventName = sportMatch.HomeTeam + " " + sportMatch.AwayTeam,
                                EventDateTime = sportMatch.MatchDate,
                                Sport = sportMatch.SportName,
                                SportId = sportMatch.SportId
                            };
                            modelList.Add(model);
                        }
                        
                    }
                }
                else
                {
                    var sportEvents = sportFacade.GetTodaysEvents(DateTime.Today, federation.FederationId);

                    foreach (var sportEvent in sportEvents)
                    {
                        // Adding to list
                        var model = new CalendarViewModel()
                        {
                            EventId = sportEvent.Id,
                            EventName = sportEvent.Name,
                            EventDateTime = sportEvent.StartDate,
                            Sport = sportEvent.ActivityName,
                            SportId = sportEvent.ActivityId
                        };
                        modelList.Add(model);
                    }
                }
            }


            return modelList;
        }

        /// <summary>
        ///     The get calendar items.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>Dictionary</cref>
        ///     </see>
        ///     .
        /// </returns>
        private Dictionary<int, List<Calendar>> GetCalendarItems()
        {
            // Creating and populating the sports list which we need for the dropdown
            var sportRepository = new SportRepository();
            var listSports = new List<Sport>(sportRepository.GetAll());

            var dictionary = new Dictionary<int, List<Calendar>>();
            var calendarRepository = new CalendarRepository();
            foreach (Sport sport in listSports)
            {
                calendarRepository.SportId = sport.Id;

                dictionary.Add(sport.Id, new List<Calendar>(calendarRepository.GetAll()));
            }

            return dictionary;
        }
    }
}