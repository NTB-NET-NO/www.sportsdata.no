﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using NTB.SportsData.Web.Models.IdentityModels;

namespace NTB.SportsData.Input.Models
{
    public class SdiUserStore : IUserStore<User>
    {
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task CreateAsync(User user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(User user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(User user)
        {
            throw new NotImplementedException();
        }

        public Task<User> FindByIdAsync(string userId)
        {
            throw new NotImplementedException();
        }

        public Task<User> FindByNameAsync(string userName)
        {
            throw new NotImplementedException();
        }
    }
}