// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SportMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The sport mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;

    using FederationDiscipline = NTB.SportsData.Services.NIF.NIFProdService.FederationDiscipline;

    /// <summary>
    /// The sport mapper.
    /// </summary>
    public class SportMapper : BaseMapper<FederationDiscipline, Sport>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<FederationDiscipline, Sport> mapper)
        {
            mapper.Relate(x => x.ActivityName, y => y.Name);
            mapper.Relate(x => x.ActivityId, y => y.Id);
        }
    }
}