﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTB.SportsData.Domain.Classes
{
    public class MaintenanceDocument
    {
        public MaintenanceCustomer MaintenanceCustomer { get; set; }

        public List<MaintenanceTournament> MaintenanceTournaments { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
