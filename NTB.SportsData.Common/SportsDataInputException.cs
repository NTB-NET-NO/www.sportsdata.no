﻿using System;

namespace NTB.SportsData.Common
{
    public class SportsDataInputException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SportsDataInputException"/> class. 
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public SportsDataInputException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SportsDataInputException"/> class. 
        /// </summary>
        /// <param name="message">
        /// The error message.
        /// </param>
        /// <param name="exception">
        /// INternal exception.
        /// </param>
        public SportsDataInputException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}