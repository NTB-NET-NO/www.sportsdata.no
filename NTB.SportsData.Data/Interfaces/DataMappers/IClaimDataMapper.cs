﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IClaimsDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The ClaimsDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    using System.Collections.Generic;
    using System.Security.Claims;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The ClaimsDataMapper interface.
    /// </summary>
    public interface IClaimDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The add user claim.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="claim">
        /// The claim.
        /// </param>
        void AddUserClaim(ApplicationUser user, Claim claim);

        /// <summary>
        /// The get user claims.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Claim> GetUserClaims(ApplicationUser user);

        /// <summary>
        /// The remove user claim.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="claim">
        /// The claim.
        /// </param>
        void RemoveUserClaim(ApplicationUser user, Claim claim);

        #endregion
    }
}