﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITournamentDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TournamentDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Areas.Admin.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The TournamentDataMapper interface.
    /// </summary>
    internal interface ITournamentDataMapper
    {
        /// <summary>
        /// The get tournaments by job id.
        /// </summary>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentsByJobId(int jobId);

        /// <summary>
        /// The get tournaments by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentsBySportId(int sportId, int seasonId);

        /// <summary>
        /// The get tournaments by season id.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentsBySeasonId(int seasonId);

        /// <summary>
        /// The update tournament age category definition.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="ageCategoryDefintionId">
        /// The age category defintion id.
        /// </param>
        void UpdateTournamentAgeCategoryDefinition(int tournamentId, int ageCategoryDefintionId);

        /// <summary>
        /// The get tournament by tournamentid.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        Tournament GetTournamentByTournamentid(int tournamentId);
    }
}