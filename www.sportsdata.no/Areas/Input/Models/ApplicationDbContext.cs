﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsData.Data.DataMappers.Role;
using NTB.SportsData.Data.DataMappers.User.Web;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Input.Models
{
    public class ApplicationDbContext
    {
        public ApplicationDbContext() 
        {
        }

        // public List<ApplicationUser> Users { get; set; }

        private List<ApplicationUser> _users
        {
            get
            {
                var userDataMapper = new UserDataMapper();
                return userDataMapper.GetAll().ToList();
            }
            set
            {
                _users.AddRange(value);
            }
        }

        private List<Role> _roles
        {
            get
            {
                var roleDataMapper = new RoleDataMapper();
                return roleDataMapper.GetRoles().ToList();
            }

            set
            {
                _roles.AddRange(value);
            }
        }

        public List<ApplicationUser> Users
        {
            get { return _users; }

            set
            {
                if (_users != null)
                {
                    _users.AddRange(value);
                }
            }
        }

        public List<Role> Roles { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }



        public Guid GetApplicationId()
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                // running the command
                var sqlCommand = new SqlCommand("AspNet_Identity_GetApplicationId", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@ApplicationName", @"/"));

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return Guid.NewGuid();
                }

                while (sqlDataReader.Read())
                {
                    return sqlDataReader.GetGuid(0);
                }

            }

            return Guid.NewGuid();
        }


        /// <summary>
        /// This method shall save the changes to the database
        /// </summary>
        internal void SaveChanges()
        {

            throw new NotImplementedException();
        }
    }
}