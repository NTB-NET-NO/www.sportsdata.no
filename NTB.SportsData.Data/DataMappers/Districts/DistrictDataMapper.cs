﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DistrictDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The district data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.DataMappers.Districts
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Data.Interfaces.Common;
    using NTB.SportsData.Data.Interfaces.DataMappers;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The district data mapper.
    /// </summary>
    public class DistrictDataMapper : IRepository<District>, IDisposable, IDistrictDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(DistrictDataMapper));

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get districts by user id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<District> GetDistrictsByUserId(Guid userId)
        {
            var districts = new List<District>();
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetDistrictsByUserid", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return districts;
                    }

                    while (sqlDataReader.Read())
                    {
                        var district = new District();
                        if (sqlDataReader["DistrictId"] != DBNull.Value)
                        {
                            district.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                            district.DistrictName = sqlDataReader["DistrictName"].ToString();
                            district.SportId = 0;
                            if (sqlDataReader["SportId"] != DBNull.Value)
                            {
                                district.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                            }
                        }

                        districts.Add(district);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return districts;
            }
        }

        /// <summary>
        /// The insert user district map.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertUserDistrictMap(Guid userId, int districtId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(District domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<District> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(District domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(District domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<District> GetAll()
        {
            var districts = new List<District>();
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetDistricts", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return districts.AsQueryable();
                    }

                    while (sqlDataReader.Read())
                    {
                        var district = new District();
                        if (sqlDataReader["CountyId"] != DBNull.Value)
                        {
                            district.DistrictId = Convert.ToInt32(sqlDataReader["CountyId"]);
                        }

                        if (sqlDataReader["CountyName"] != DBNull.Value)
                        {
                            district.DistrictName = sqlDataReader["CountyName"].ToString();
                        }

                        if (sqlDataReader["District"] != DBNull.Value)
                        {
                            district.SecondaryDistrictId = Convert.ToInt32(sqlDataReader["District"]);
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            district.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        }

                        districts.Add(district);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return districts.AsQueryable();
            }
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="District"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public District Get(int id)
        {
            throw new NotImplementedException();
        }
    }
}