﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DetailsViewModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Web.Areas.Input.Models
{
    using System;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The details view model.
    /// </summary>
    public class DetailsViewModel
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     Gets or sets the fullname.
        /// </summary>
        public string Fullname { get; set; }

        /// <summary>
        ///     Gets or sets the phone number.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        ///     Gets or sets the role.
        /// </summary>
        public Role Role { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        #endregion
    }
}