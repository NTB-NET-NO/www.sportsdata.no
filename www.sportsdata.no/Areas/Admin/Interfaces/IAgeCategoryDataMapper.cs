﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Interfaces
{
    interface IAgeCategoryDataMapper
    {
        List<AgeCategory> GetAgeCategoryBySportId(int sportId);

        List<AgeCategory> GetAgeCategoriesByOrgId(int orgId);

        void DeleteAgeCategoriesByOrgId(int orgId);

        void DeleteAgeCategoriesBySportId(int orgId);
    }
}
