﻿namespace NTB.SportsData.Domain.Classes
{
    public class ApiUser
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}