﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentFacade.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament facade.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.NFF.TournamentAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.NFF;
    using NTB.SportsData.Facade.NFF.TournamentInterface;
    using NTB.SportsData.Services.NFF.Interfaces;
    using NTB.SportsData.Services.NFF.Repositories;

    /// <summary>
    /// The tournament facade.
    /// </summary>
    public class TournamentFacade : ITournamentFacade
    {
        /// <summary>
        /// The _age category data mapper.
        /// </summary>
        private readonly IAgeCategoryDataMapper ageCategoryDataMapper;

        /// <summary>
        /// The _district data mapper.
        /// </summary>
        private readonly IDistrictDataMapper districtDataMapper;

        /// <summary>
        /// The _match data mapper.
        /// </summary>
        private readonly IMatchDataMapper matchDataMapper;

        /// <summary>
        /// The _municipality data mapper.
        /// </summary>
        private readonly IMunicipalityDataMapper municipalityDataMapper;

        /// <summary>
        /// The _municipality repository.
        /// </summary>
        private readonly IRepository<Municipality> municipalityRepository;

        /// <summary>
        /// The _org data mapper.
        /// </summary>
        private readonly IOrgDataMapper orgDataMapper;

        /// <summary>
        /// The _register data mapper.
        /// </summary>
        private readonly IRegisterDataMapper registerDataMapper;

        /// <summary>
        /// The _season data mapper.
        /// </summary>
        private readonly ISeasonDataMapper seasonDataMapper;

        /// <summary>
        /// The _team data mapper.
        /// </summary>
        private readonly ITeamDataMapper teamDataMapper;

        /// <summary>
        /// The _tournament data mapper.
        /// </summary>
        private readonly ITournamentDataMapper tournamentDataMapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="TournamentFacade"/> class.
        /// </summary>
        public TournamentFacade()
        {
            this.tournamentDataMapper = new TournamentRepository();
            this.districtDataMapper = new DistrictRepository();
            this.municipalityDataMapper = new MunicipalityRepository();
            this.seasonDataMapper = new SeasonRepository();
            this.ageCategoryDataMapper = new AgeCategoryRepository();
            this.teamDataMapper = new TeamRepository();
            this.matchDataMapper = new MatchRepository();
            this.orgDataMapper = new OrgRepository();
            this.registerDataMapper = new RegisterDataMapper();

            // Repository code
        }

        /// <summary>
        /// Gets or sets the age category.
        /// </summary>
        public int AgeCategory { get; set; }

        /// <summary>
        /// The get tournament by municipalities.
        /// </summary>
        /// <param name="municipalities">
        /// The municipalities.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId)
        {
            this.tournamentDataMapper.AgeCategoryId = this.AgeCategory;

            var result = this.tournamentDataMapper.GetTournamentByMunicipalities(municipalities, seasonId);

            var mapper = new TournamentMapper();

            return result.Select(row => mapper.Map(row, new Tournament())).ToList();
            
        }

        /// <summary>
        /// The get tournaments by district.
        /// </summary>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            var result = this.tournamentDataMapper.GetTournamentsByDistrict(districtId, seasonId);

            var mapper = new TournamentMapper();

            var tournaments = result.Select(row => mapper.Map(row, new Tournament())).ToList();

            var t2 = new List<Tournament>();
            foreach (var tournament in tournaments)
            {
                var t1 = tournament;
                t1.SportId = 16;

                t2.Add(t1);
            }

            return t2;
        }

        /// <summary>
        /// The get tournaments by team.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsByTeam(int teamId, int seasonId)
        {
            var result = this.tournamentDataMapper.GetTournamentsByTeam(teamId, seasonId);

            var mapper = new TournamentMapper();

            return result.Select(row => mapper.Map(row, new Tournament())).ToList();
        }

        /// <summary>
        /// The get districts.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<District> GetDistricts()
        {
            var districts = this.districtDataMapper.GetDistricts();

            var districtMapper = new DistrictMapper();

            return districts.Select(row => districtMapper.Map(row, new District())).ToList();
        }

        /// <summary>
        /// The get district.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="District"/>.
        /// </returns>
        public District GetDistrict(int id)
        {
            var result = this.districtDataMapper.GetDistrict(id);

            var districtMapper = new DistrictMapper();

            return districtMapper.Map(result, new District());
        }

        /// <summary>
        /// The get municipalities.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Municipality> GetMunicipalities()
        {
            var result = this.municipalityDataMapper.GetAll();

            var municipalityMapper = new MunicipalityMapper();

            return result.Select(row => municipalityMapper.Map(row, new Municipality())).ToList();
        }

        /// <summary>
        /// The get municipalities by district.
        /// </summary>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Municipality> GetMunicipalitiesbyDistrict(int districtId)
        {
            var results = this.municipalityDataMapper.GetMunicipalitiesbyDistrict(districtId);

            var municipalityMapper = new MunicipalityMapper();

            return results.Select(row => municipalityMapper.Map(row, new Municipality())).ToList();
        }

        /// <summary>
        /// The get clubs by tournament.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public List<Tournament> GetClubsByTournament(int tournamentId, int seasonId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get seasons.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Season> GetSeasons()
        {
            var mapper = new SeasonMapper();

            var result = this.seasonDataMapper.GetSeasons();

            return result.Select(row => mapper.Map(row, new Season())).ToList();
        }

        /// <summary>
        /// The get season.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Season"/>.
        /// </returns>
        public Season GetSeason(int id)
        {
            var result = this.seasonDataMapper.GetSeason(id);

            var mapper = new SeasonMapper();

            return mapper.Map(result, new Season());
        }

        /// <summary>
        /// The get ongoing season.
        /// </summary>
        /// <returns>
        /// The <see cref="Season"/>.
        /// </returns>
        public Season GetOngoingSeason()
        {
            var result = this.seasonDataMapper.GetOngoingSeason();

            var mapper = new SeasonMapper();

            return mapper.Map(result, new Season());
        }

        /// <summary>
        /// The get age categories tournament.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<AgeCategory> GetAgeCategoriesTournament()
        {
            var mapper = new AgeCategoryMapper();
            var result = this.ageCategoryDataMapper.GetAgeCategoriesTournament();

            return result.Select(row => mapper.Map(row, new AgeCategory())).ToList();
        }

        /// <summary>
        /// The get matches by tournament.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="includeSquad">
        /// The include squad.
        /// </param>
        /// <param name="includeReferees">
        /// The include referees.
        /// </param>
        /// <param name="includeResults">
        /// The include results.
        /// </param>
        /// <param name="includeEvents">
        /// The include events.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees, bool includeResults, bool includeEvents)
        {
            var mapper = new MatchMapper();

            var result = this.matchDataMapper.GetMatchesByTournament(tournamentId, includeSquad, includeReferees, includeResults, includeEvents);
            return result.Select(row => mapper.Map(row, new Match())).ToList();
        }

        /// <summary>
        /// The get age category tournament.
        /// </summary>
        /// <returns>
        /// The <see cref="AgeCategory"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public AgeCategory GetAgeCategoryTournament()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get teams by municipality.
        /// </summary>
        /// <param name="municipalityId">
        /// The municipality id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public List<Team> GetTeamsByMunicipality(int municipalityId, int seasonId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get tournament.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        public Tournament GetTournament(int tournamentId)
        {
            var result = this.tournamentDataMapper.GetTournament(tournamentId);

            var mapper = new TournamentMapper();

            return mapper.Map(result, new Tournament());
        }

        /// <summary>
        /// The get todays matches.
        /// </summary>
        /// <param name="matchDate">
        /// The match date.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetTodaysMatches(DateTime matchDate)
        {
            var result = this.matchDataMapper.GetTodaysMatches(matchDate);

            var mapper = new MatchMapper();

            return result.Select(row => mapper.Map(row, new Match())).ToList();
        }

        /// <summary>
        /// The get team contacts.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<ContactPerson> GetTeamContacts(int teamId)
        {
            var result = this.teamDataMapper.GetTeamPersons(teamId);

            var mapper = new ContactPersonMapper();

            return result.Select(row => mapper.Map(row, new ContactPerson())).ToList();
        }

        /// <summary>
        /// The get clubs.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Club> GetClubs()
        {
            var mapper = new ClubMapper();
            var result = this.orgDataMapper.GetClubs();

            return result.Select(row => mapper.Map(row, new Club())).ToList();
        }

        /// <summary>
        /// The insert match result.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool InsertMatchResult(Match match)
        {
            var mapper = new RegisterMapper();
            var remoteMatch = mapper.Map(match, new Services.NFF.NFFProdService.Match());
            var result = this.registerDataMapper.InsertMatchResult(remoteMatch);

            return result;
        }

        /// <summary>
        /// The get matches by district.
        /// </summary>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <param name="matchDate">
        /// The match date.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Match> GetMatchesByDistrict(int districtId, DateTime matchDate)
        {
            var result = this.matchDataMapper.GetMatchesbyDistrict(districtId, false, false, true, false, matchDate, matchDate);

            var mapper = new MatchMapper();

            return result.Select(row => mapper.Map(row, new Match())).ToList();

            // return new List<Match>();
        }

        /// <summary>
        /// The get tournaments by season id.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            var result = this.tournamentDataMapper.GetTournamentsBySeasonId(seasonId);

            var mapper = new TournamentMapper();

            return result.Select(row => mapper.Map(row, new Tournament())).ToList();
        }
    }
}