﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament remote data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Data.Tournaments
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Data.Interfaces;
    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Interfaces;
    using NTB.SportsData.Facade.NFF.ApiAccess;
    using NTB.SportsData.Facade.NIF.ApiAccess;

    /// <summary>
    ///     The tournament remote data mapper.
    /// </summary>
    public class TournamentRemoteDataMapper : ITournamentRemoteDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get tournament standings.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TeamResult> GetTeamResultStanding(Tournament tournament)
        {
            if (tournament.SportId == 16)
            {
                IApiFacade facade = new SoccerApiFacade();
                return facade.GetTeamResultStanding(tournament);
            }
            else
            {
                try
                {
                    IApiFacade facade = new SportApiFacade();
                    return facade.GetTeamResultStanding(tournament);
                }
                catch (Exception exception)
                {

                    IApiFacade facade = new SoccerApiFacade();
                    return facade.GetTeamResultStanding(tournament);
                }
            }            
        }


        /// <summary>
        /// The get tournaments by org id and season id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Tournament> GetTournamentsByOrgIdAndSeasonId(int orgId, int seasonId)
        {
            IApiFacade facade = new SportApiFacade();

            return facade.GetTournamentsByOrgId(orgId, seasonId);
        }

        #endregion
    }
}