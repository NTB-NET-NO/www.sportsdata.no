﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MunicipalityRepository.cs" company="">
//   
// </copyright>
// <summary>
//   The municipality repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NFF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.ServiceModel;

    using NTB.SportsData.Services.NFF.Interfaces;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The municipality repository.
    /// </summary>
    public class MunicipalityRepository : IRepository<Municipality>, IDisposable, IMunicipalityDataMapper
    {
        #region Fields

        /// <summary>
        ///     The service client.
        /// </summary>
        public MetaServiceClient ServiceClient = new MetaServiceClient();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Municipality"/>.
        /// </returns>
        public Municipality Get(int id)
        {
            return this.GetAll().Single(s => s.Id == id);
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Municipality> GetAll()
        {
            if (this.ServiceClient.State == CommunicationState.Faulted)
            {
                this.ServiceClient.Close();
                this.ServiceClient = null;

                this.ServiceClient = new MetaServiceClient();
            }

            if (this.ServiceClient.ClientCredentials == null)
            {
                return this.ServiceClient.GetMunicipalities().AsQueryable();
            }

            this.ServiceClient.ClientCredentials.UserName.UserName =
                ConfigurationManager.AppSettings["NFFServicesUsername"];
            this.ServiceClient.ClientCredentials.UserName.Password =
                ConfigurationManager.AppSettings["NFFServicesPassword"];

            return this.ServiceClient.GetMunicipalities().AsQueryable();
        }

        /// <summary>
        /// The get foo.
        /// </summary>
        public void GetFoo()
        {
            this.ServiceClient.GetAgeCategoriesTournament();
        }

        /// <summary>
        /// The get municipalitiesby district.
        /// </summary>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Municipality> GetMunicipalitiesbyDistrict(int districtId)
        {
            if (this.ServiceClient.State == CommunicationState.Faulted)
            {
                this.ServiceClient.Close();
                this.ServiceClient = null;

                this.ServiceClient = new MetaServiceClient();
            }

            if (this.ServiceClient.ClientCredentials == null)
            {
                return new List<Municipality>(this.ServiceClient.GetMunicipalitiesbyDistrict(districtId));
            }

            this.ServiceClient.ClientCredentials.UserName.UserName =
                ConfigurationManager.AppSettings["NFFServicesUsername"];
            this.ServiceClient.ClientCredentials.UserName.Password =
                ConfigurationManager.AppSettings["NFFServicesPassword"];

            return new List<Municipality>(this.ServiceClient.GetMunicipalitiesbyDistrict(districtId));
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<Municipality> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}