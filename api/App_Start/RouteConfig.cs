﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RouteConfig.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The route config.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api
{
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    ///     The route config.
    /// </summary>
    public class RouteConfig
    {
        #region Public Methods and Operators

        /// <summary>
        /// The register routes.
        /// </summary>
        /// <param name="routes">
        /// The routes.
        /// </param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Default", "api/{controller}/{action}/{id}", defaults: new { id = UrlParameter.Optional });

            // new { controller = "Home", action = "Index", id = UrlParameter.Optional }


        }

        #endregion
    }
}