﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    public class EditCategoryViewModel
    {
        public int AgeCategoryId { get; set; }

        public string AgeCategoryName { get; set; }

        public int MaxAge { get; set; }

        public int MinAge { get; set; }

        public List<Sport> Sports { get; set; }

        public List<AgeCategoryDefinition> AgeCategoryDefinitions { get; set; }

        public int SelectedSport { get; set; }

        public int SelectedAgeCategoryDefinition { get; set; }

        public int OrganisationId { get; set; }
    }
}