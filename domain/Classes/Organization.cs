namespace NTB.SportsData.Domain.Classes
{
    public class Organization
    {
        public int OrganizationId { get; set; }

        public int OrganizationUserId { get; set; }

        public string OrganizationName { get; set; }

        public int SingleSport { get; set; }

        public int TeamSport { get; set; }

        public string Sport { get; set; }

        public int SportId { get; set; }

        public string OrganizationNameShort { get; set; }

        public string Description { get; set; }
    }
}