﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITournamentDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TournamentDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    /// <summary>
    /// The TournamentDataMapper interface.
    /// </summary>
    public interface ITournamentDataMapper
    {
    }
}