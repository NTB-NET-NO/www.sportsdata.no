﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConnectClubsViewModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved
// </copyright>
// <summary>
//   The connect clubs view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Domain.Classes
{
    using System.Collections.Generic;

    /// <summary>
    /// The connect clubs view model.
    /// </summary>
    public class ConnectClubsViewModel
    {
        /// <summary>
        /// Gets or sets Organization
        /// </summary>
        public Organization Organization { get; set; }

        /// <summary>
        /// Gets or sets List of Organizations
        /// </summary>
        public List<Organization> Organizations { get; set; }
    }
}
