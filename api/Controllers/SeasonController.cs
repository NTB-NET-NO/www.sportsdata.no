﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SeasonController.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The season controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Controllers
{
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Web.Http;

    using log4net;

    using NTB.SportsData.Api.Interfaces;
    using NTB.SportsData.Api.Models.XML;
    using NTB.SportsData.Api.Transformers.JSON;
    using NTB.SportsData.Api.Transformers.XML;
    using NTB.SportsData.Data.Season;

    /// <summary>
    /// The season controller.
    /// </summary>
    public class SeasonController : ApiController
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger("ApiFileAppender");

        /// <summary>
        /// The get org seasons.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// </returns>
        [HttpGet]
        public HttpResponseMessage GetOrgSeasons(int id)
        {
            Logger.Info("Getting all tournaments for customer");
            var accept = this.Request.Headers.Accept.ToString();

            var remoteDataMapper = new SeasonRemoteDataMapper();
            var seasons = remoteDataMapper.GetFederationSeasonByOrgId(id);

            var serializer = new ObjectSerializer();

            var xmlContent = serializer.SerializeObject(seasons.ToList());

            ITransformer model;
            if (accept == "application/json")
            {
                Logger.Info("Generating JSON");
                model = new CompetitionTransformer { XsltFile = "sportsinjson-competitions.xsl" };
            }
            else
            {
                Logger.Info("Generating XML");
                model = new TournamentTransformer { XsltFile = "sportsml-tournament.xsl" };
            }

            var content = model.TransformObject(xmlContent);

            if (accept == "text/xml")
            {
                content = content.Replace("utf-16", "utf-8");
            }

            return new HttpResponseMessage { Content = new StringContent(content, Encoding.UTF8, accept) };
        }
    }
}