﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    using System.Collections.Generic;
    using System.Linq;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.NFF.TournamentAccess;
    using NTB.SportsData.Facade.NIF.SportAccess;
    using NTB.SportsData.Web.Areas.Admin.Repositories;

    /// <summary>
    /// The match model.
    /// </summary>
    public class MatchModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MatchModel"/> class. 
        ///     A constructor that at the moment does not do much
        /// </summary>
        public MatchModel()
        {
        }

        /// <summary>
        /// The get matches.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Match> GetMatches(int tournamentId, int sportId)
        {
            var repostitory = new MatchesRepository();
            var matches = repostitory.GetMatchesByTournamentId(tournamentId);

            if (matches.Any())
            {
                return matches;
            }

            if (sportId == 16)
            {
                var facade = new TournamentFacade();
                return facade.GetMatchesByTournament(tournamentId, false, false, false, false);
            }
            else
            {
                var facade = new SportFacade();
                return facade.GetMatchesByTournamentIdAndSportId(tournamentId, sportId);
            }
        }
    }
}