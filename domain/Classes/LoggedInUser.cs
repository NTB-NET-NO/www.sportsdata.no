﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LoggedInUser.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The logged in user.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Domain.Classes
{
    /// <summary>
    /// The logged in user.
    /// </summary>
    public class LoggedInUser
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }
    }
}