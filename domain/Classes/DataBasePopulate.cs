﻿using System;

namespace NTB.SportsData.Domain.Classes
{
    public class DataBasePopulate
    {
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
    }
}