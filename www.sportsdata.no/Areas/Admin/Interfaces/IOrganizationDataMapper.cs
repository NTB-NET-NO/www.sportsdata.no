﻿using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Interfaces
{
    interface IOrganizationDataMapper
    {
        Organization GetOrganizationBySportId(int sportId);
    }
}
