using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Web.Areas.Admin.Interfaces;

namespace NTB.SportsData.Web.Areas.Admin.Repositories
{
    public class AgeCategoryRepository : IRepository<AgeCategory>, IDisposable, IAgeCategoryDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AgeCategoryRepository));

        public int SportId { get; set; }

        public int OrgId { get; set; }

        public List<AgeCategory> GetAgeCategoryBySportId(int sportId)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand("SportsData_GetAgeCategoriesBySportId", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var sqlDataReader = sqlCommand.ExecuteReader();
                var ageCategories = new List<AgeCategory>();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        var ageCategory = new AgeCategory();
                        ageCategory.CategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryID"]);
                        ageCategory.CategoryName = sqlDataReader["AgeCategoryName"].ToString();
                        if (sqlDataReader["MinAge"] != DBNull.Value)
                        {
                            ageCategory.MinAge = Convert.ToInt32(sqlDataReader["MinAge"]);
                        }

                        if (sqlDataReader["MaxAge"] != DBNull.Value)
                        {
                            ageCategory.MaxAge = Convert.ToInt32(sqlDataReader["MaxAge"]);
                        }

                        if (sqlDataReader["AgeCategoryDefinitionId"] != DBNull.Value)
                        {
                            ageCategory.AgeCategoryDefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            ageCategory.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        }
                        
                        ageCategories.Add(ageCategory);
                    }
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return ageCategories;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<AgeCategory> GetAgeCategoriesByOrgId(int orgId)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand("SportsData_GetFederationAgeCategories", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@OrgId", orgId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var sqlDataReader = sqlCommand.ExecuteReader();
                var ageCategories = new List<AgeCategory>();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        var ageCategory = new AgeCategory();
                        ageCategory.CategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryID"]);
                        ageCategory.CategoryName = sqlDataReader["AgeCategoryName"].ToString();
                        if (sqlDataReader["MinAge"] != DBNull.Value)
                        {
                            ageCategory.MinAge = Convert.ToInt32(sqlDataReader["MinAge"]);
                        }

                        if (sqlDataReader["MaxAge"] != DBNull.Value)
                        {
                            ageCategory.MaxAge = Convert.ToInt32(sqlDataReader["MaxAge"]);
                        }

                        if (sqlDataReader["AgeCategoryDefinitionId"] != DBNull.Value)
                        {
                            ageCategory.AgeCategoryDefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            ageCategory.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        }

                        if (sqlDataReader["AgeCategoryDefinition"] != DBNull.Value)
                        {
                            ageCategory.AgeCategoryDefinition = sqlDataReader["AgeCategoryDefinition"].ToString();
                        }

                        ageCategories.Add(ageCategory);
                    }
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return ageCategories;
            }
        }

        public void DeleteAgeCategoriesByOrgId(int orgId)
        {
            Logger.DebugFormat("OrgId: {0}", orgId);
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand("SportsData_DeleteAgeCategoriesByOrgId", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@OrgId", orgId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();
                
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void DeleteAgeCategoriesBySportId(int sportId)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_DeleteAgeCategoriesBySportId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public int InsertOne(AgeCategory domainobject)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                if (this.GetAgeCategoryById(domainobject.CategoryId))
                {
                    return 0;
                }

                var sqlCommand = new SqlCommand("SportsData_InsertFederationAgeCategories", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@CategoryId", domainobject.CategoryId));
                sqlCommand.Parameters.Add(new SqlParameter("@MaxAge", domainobject.MaxAge));
                sqlCommand.Parameters.Add(new SqlParameter("@MinAge", domainobject.MinAge));
                sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryDefinitionId", domainobject.AgeCategoryDefinitionId));
                sqlCommand.Parameters.Add(new SqlParameter("@CategoryName", domainobject.CategoryName));
                sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                sqlCommand.Parameters.Clear();
                sqlCommand.CommandText = "SELECT @@IDENTITY";
                sqlCommand.CommandType = CommandType.Text;

                var o = sqlCommand.ExecuteScalar();
                var insertId = 0;
                if (o != DBNull.Value)
                {
                    var id1 = 0;
                    if (Int32.TryParse(o.ToString(), out id1))
                    {
                        insertId = id1 > 0 ? id1 : 0;
                    }
                }

                sqlCommand.Dispose();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return insertId;
            }
        }

        public void InsertAll(List<AgeCategory> domainobject)
        {
            foreach (var ageCategory in domainobject)
            {
                this.InsertOne(ageCategory);
            }
        }

        public void Update(AgeCategory domainobject)
        {
            Logger.Debug("In update");
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[SportsData_UpdateAgeCategoryById]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    Logger.DebugFormat("AgeCategoryId: {0}", domainobject.CategoryId);
                    Logger.DebugFormat("AgeCategoryName: {0}", domainobject.CategoryName);
                    Logger.DebugFormat("MaxAge: {0}", domainobject.MaxAge);
                    Logger.DebugFormat("MinAge: {0}", domainobject.MinAge);
                    Logger.DebugFormat("AgeCategoryDefinitionId: {0}", domainobject.AgeCategoryDefinitionId);
                    Logger.DebugFormat("SportId: {0}", domainobject.SportId);
                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", domainobject.CategoryId));
                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryName", domainobject.CategoryName));
                    sqlCommand.Parameters.Add(new SqlParameter("@MaxAge", domainobject.MaxAge));
                    sqlCommand.Parameters.Add(new SqlParameter("@MinAge", domainobject.MinAge));
                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryDefinitionId", domainobject.AgeCategoryDefinitionId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void Delete(AgeCategory domainobject)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand("SportsData_DeleteAgeCategoriesById", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@Id", domainobject.CategoryId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        public IQueryable<AgeCategory> GetAll()
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[SportsData_GetAgeCategoriesBySportId]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    // This one shall return all, so we send null as parameter to the SP.
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", DBNull.Value));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    var ageCategories = new List<AgeCategory>();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var ageCategory = new AgeCategory();

                            ageCategory.CategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryID"]);
                            ageCategory.CategoryName = sqlDataReader["AgeCategoryName"].ToString();
                            if (sqlDataReader["MinAge"] != DBNull.Value)
                            {
                                ageCategory.MinAge = Convert.ToInt32(sqlDataReader["MinAge"]);
                            }

                            if (sqlDataReader["MaxAge"] != DBNull.Value)
                            {
                                ageCategory.MaxAge = Convert.ToInt32(sqlDataReader["MaxAge"]);
                            }

                            if (sqlDataReader["AgeCategoryDefinitionId"] != DBNull.Value)
                            {
                                ageCategory.AgeCategoryDefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                            }

                            if (sqlDataReader["SportId"] != DBNull.Value)
                            {
                                ageCategory.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                            }



                            ageCategories.Add(ageCategory);
                        }
                    }

                    return ageCategories.AsQueryable();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return null;
        }


        public AgeCategory Get(int id)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[SportsData_GetAgeCategoryById]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    var ageCategory = new AgeCategory();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            ageCategory.CategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryID"]);
                            ageCategory.CategoryName = sqlDataReader["AgeCategoryName"].ToString();
                            if (sqlDataReader["MinAge"] != DBNull.Value)
                            {
                                ageCategory.MinAge = Convert.ToInt32(sqlDataReader["MinAge"]);
                            }

                            if (sqlDataReader["MaxAge"] != DBNull.Value)
                            {
                                ageCategory.MaxAge = Convert.ToInt32(sqlDataReader["MaxAge"]);
                            }

                            if (sqlDataReader["AgeCategoryDefinitionId"] != DBNull.Value)
                            {
                                ageCategory.AgeCategoryDefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                            }

                            if (sqlDataReader["SportId"] != DBNull.Value)
                            {
                                ageCategory.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                            }
                        }
                    }

                    return ageCategory;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            return null;
        }

        private bool GetAgeCategoryById(int ageCategoryId)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetAgeCategoryById", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", ageCategoryId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    if (sqlDataReader.HasRows)
                    {
                        if (sqlConnection.State == ConnectionState.Open)
                        {
                            sqlConnection.Close();
                        }

                        return true;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            return false;
        }

        public void DeleteAll(int sportId)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_DeleteFederationAgeCategories", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}