﻿using Glue;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Facade.Mappers.NIF
{
    /// <summary>
    /// The age category mapper.
    /// </summary>
    public class AgeCategoryMapper : BaseMapper<ClassCode, AgeCategory>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<ClassCode, AgeCategory> mapper)
        {
            mapper.Relate(x => x.ClassId, y => y.CategoryId);
            mapper.Relate(x => x.Name, y => y.CategoryName);
            mapper.Relate(x => x.ToAge, y => y.MaxAge);
            mapper.Relate(x => x.FromAge, y => y.MinAge);
            mapper.Relate(x => x.ActivityId, y => y.SportId);
        }
    }
}