﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PersonRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The person repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NIF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using NTB.SportsData.Services.NIF.Interfaces;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The person repository.
    /// </summary>
    public class PersonRepository : IRepository<Person>, IDisposable, IPersonDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Person domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Person"/>.
        /// </returns>
        public Person Get(int id)
        {
            var personClient = new PersonServiceClient();

            if (personClient.ClientCredentials == null)
            {
                return new Person();
            }

            personClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            personClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            var personRequest = new PersonIdRequest4 { Id = id };

            var response = personClient.GetPerson(personRequest);
            return !response.Success ? new Person() : response.Person;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        ///     Throws an exception because we have not implemented this method
        /// </exception>
        public IQueryable<Person> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get person by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Person"/>.
        /// </returns>
        public Person GetPersonById(int id)
        {
            return this.Get(id);
        }

        /// <summary>
        /// The get simple person by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="SimplePerson"/>.
        /// </returns>
        public SimplePerson GetSimplePersonById(int id)
        {
            var personClient = new PersonServiceClient();

            if (personClient.ClientCredentials == null)
            {
                return new SimplePerson();
            }

            personClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            personClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            var personRequest = new SimplePersonIdRequest() { Id = id };

            var response = personClient.GetSimplePerson(personRequest);
            return !response.Success ? new SimplePerson() : response.SimplePerson;
        }

        /// <summary>
        /// The get persons by match id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<SquadIndividual> GetPersonsByMatchId(int id)
        {
            var client = new TeamServiceClient();

            if (client.ClientCredentials == null)
            {
                return new List<SquadIndividual>();
            }

            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            var request = new MatchIdRequest4(id);

            var response = client.GetSquadIndividualsForMatch(request);
            return !response.Success ? new List<SquadIndividual>() : response.SquadIndividuals.ToList().Where(x => x.SquadIndividualCategoryId == 2).ToList();
        }

        /// <summary>
        /// The get persons by team id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<SquadIndividual> GetPersonsByTeamId(int id)
        {
            var client = new TeamServiceClient();

            if (client.ClientCredentials == null)
            {
                return new List<SquadIndividual>();
            }

            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            var request = new OrgIdRequest24(id);

            var response = client.GetSquadIndividualsForOrg(request);
            return !response.Success ? new List<SquadIndividual>() : response.SquadIndividuals.ToList().Where(x => x.SquadIndividualCategoryId == 2).ToList();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<Person> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(Person domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Person domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}