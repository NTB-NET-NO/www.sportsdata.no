﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DistrictMatches.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The district matches.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Areas.Input.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    using log4net;

    using NTB.SportsData.Data.DataMappers.Matches;
    using NTB.SportsData.Data.DataMappers.Municipalities;
    using NTB.SportsData.Data.DataMappers.Seasons;
    using NTB.SportsData.Data.DataMappers.Sports;
    using NTB.SportsData.Data.DataMappers.Tournaments;
    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.NFF.TournamentAccess;
    using NTB.SportsData.Facade.NIF.SportAccess;

    /// <summary>
    /// The district matches.
    /// </summary>
    public class DistrictMatches
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(DistrictMatches));

        /// <summary>
        /// The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ThreadAppenderLogger");

        /// <summary>
        /// The _done event.
        /// </summary>
        private ManualResetEvent _doneEvent = new ManualResetEvent(true);

        /// <summary>
        /// The done.
        /// </summary>
        public bool Done = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistrictMatches"/> class.
        /// </summary>
        /// <param name="doneEvent">
        /// The done event.
        /// </param>
        public DistrictMatches(ManualResetEvent doneEvent)
        {
            this._doneEvent = doneEvent;
        }

        /// <summary>
        /// Gets or sets the district.
        /// </summary>
        public District District { get; set; }

        /// <summary>
        /// Gets or sets the soccer facade.
        /// </summary>
        public TournamentFacade SoccerFacade { get; set; }

        /// <summary>
        /// Gets or sets the sport season.
        /// </summary>
        public Season SportSeason { get; set; }

        /// <summary>
        /// Gets or sets the sports facade.
        /// </summary>
        public SportFacade SportsFacade { get; set; }

        /// <summary>
        /// The get district matches.
        /// </summary>
        public void GetDistrictMatches()
        {
            var fileContent = "Start GetDistrictMatches with District " + this.District.DistrictName + ": " + DateTime.Now.ToLongTimeString() + Environment.NewLine;

            var tournaments = new List<Tournament>();
            var matches = new List<Match>();

            var soccerDistrict = new District { DistrictId = this.District.SportId == 16 ? Convert.ToInt32(this.District.DistrictId) : Convert.ToInt32(this.District.SecondaryDistrictId) };

            if (soccerDistrict.DistrictId > 0)
            {
                Logger.Info("Getting tournaments for district: " + soccerDistrict.DistrictId + " (" + soccerDistrict.DistrictName + ")");
                tournaments.AddRange(this.SoccerFacade.GetTournamentsByDistrict(soccerDistrict.DistrictId, this.SportSeason.SeasonId));
            }

            var sportDistrict = new District();
            if (this.District.SportId != 16)
            {
                sportDistrict = this.District;
                var municipalityDataMapper = new MunicipalityDataMapper();
                var counties = municipalityDataMapper.GetMunicipalitiesBySportId(23);

                // sportsFacade.GetMunicipalities().Where(x => x.DistrictId == sportDistrict.DistrictId);
                var municipalties = (from county in counties where county.DistrictId == this.District.DistrictId select county.MunicipalityId.ToString()).ToList();

                var sports = this.GetTeamSports();

                foreach (var sport in sports.Where(x => x.Id != 16))
                {
                    var seasonId = this.GetSeason(sport.Id).SeasonId;

                    var foundTournaments = this.SportsFacade.GetTournamentByMunicipalities(municipalties, seasonId, Convert.ToInt32(sport.OrgId));

                    foreach (var tournament in foundTournaments)
                    {
                        tournament.SportId = sport.Id;

                        tournaments.Add(tournament);
                    }
                }
            }

            foreach (var tournament in tournaments)
            {
                List<Match> foundMatches;
                if (tournament.SportId == 16)
                {
                    fileContent += "Soccer tournamentId: " + tournament.TournamentId + " (" + tournament.TournamentName + ")" + Environment.NewLine;
                    Logger.Debug("Soccer tournamentid " + tournament.TournamentId + " (" + tournament.TournamentName + ")");
                    foundMatches = this.SoccerFacade.GetMatchesByTournament(tournament.TournamentId, false, false, false, false).Where(x => x.MatchDate.Date == DateTime.Today.Date).ToList();
                }
                else
                {
                    // Just making sure we have everything under control
                    fileContent += tournament.SportId + " tournamentId: " + tournament.TournamentId + " (" + tournament.TournamentName + ")" + Environment.NewLine;
                    Logger.Debug("Non-soccer tournament id: " + tournament.TournamentId + " (" + tournament.TournamentName + ")");
                    foundMatches = this.SportsFacade.GetMatchesByTournamentIdAndSportId(tournament.TournamentId, tournament.SportId).Where(x => x.MatchDate.Date == DateTime.Today.Date).ToList();
                }

                Logger.Debug("Looping over found matches");
                foreach (var match in foundMatches)
                {
                    match.SportId = tournament.SportId;
                    tournament.DistrictId = this.District.DistrictId;

                    if (match.SportId == 16)
                    {
                        match.MatchStartTime = Convert.ToInt32(match.MatchDate.ToShortTimeString().Replace(":", ""));
                    }

                    match.DistrictId = this.District.DistrictId;

                    // Making sure we have a tournament Id on the match object
                    match.TournamentId = tournament.TournamentId;

                    var tournamentRepo = new TournamentDataMapper();

                    Logger.Debug("Checking if we have the tournament in the database");
                    var t = tournamentRepo.Get(match.TournamentId);

                    if (t.TournamentId == 0)
                    {
                        fileContent += "inserting tournament - " + tournament.TournamentName + " into database." + Environment.NewLine;
                        Logger.Info("Inserting tournament " + tournament.TournamentName);
                        tournamentRepo.InsertOne(tournament);
                    }

                    this.InsertMatch(match);

                    // matches.Add(match);
                }
            }

            fileContent += "inserting into database: " + DateTime.Now.ToLongTimeString() + Environment.NewLine;

            // We must also check if the tournament is in the database

            // InsertMatches(matches);
            fileContent += "Done inserting into database: " + DateTime.Now.ToLongTimeString() + Environment.NewLine;

            fileContent += "Done with GetDistrictMatches for District: " + this.District.DistrictName + Environment.NewLine;
            var fileName = DateTime.Today.Year + DateTime.Today.Month.ToString().PadLeft(2, '0') + DateTime.Today.Day.ToString().PadLeft(2, '0') + "T" + DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0')
                           + @"_" + this.District.DistrictName.Replace(" ", "_") + ".txt";

            var linesToSave = fileContent.Split(Convert.ToChar("\n")).ToList();
            System.IO.File.WriteAllLines(@"C:\Utvikling\SportsDataInput\Test\" + fileName, linesToSave.ToArray());

            this._doneEvent.Set();
        }

        /// <summary>
        /// The insert match.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        private void InsertMatch(Match match)
        {
            var matchDataMapper = new MatchDataMapper();

            var matchId = matchDataMapper.Get(match.MatchId).MatchId;
            Logger.Debug("Checking if match " + match.MatchId + " is in the database");
            if (matchId != 0)
            {
                return;
            }

            Logger.Debug("Inserting match " + match.MatchId + " into database");
            matchDataMapper.InsertOne(match);
        }

        /// <summary>
        /// The insert matches.
        /// </summary>
        /// <param name="matches">
        /// The matches.
        /// </param>
        private void InsertMatches(List<Match> matches)
        {
            var matchDataMapper = new MatchDataMapper();

            foreach (var match in matches)
            {
                var matchId = matchDataMapper.Get(match.MatchId).MatchId;
                Logger.Debug("Checking if match " + match.MatchId + " is in the database");
                if (matchId == 0)
                {
                    Logger.Debug("Inserting match " + match.MatchId + " into database");
                    matchDataMapper.InsertOne(match);
                }
            }
        }

        /// <summary>
        /// The get team sports.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<Sport> GetTeamSports()
        {
            var sportDataMapper = new SportDataMapper();

            return sportDataMapper.GetAll().Where(x => x.HasTeamResults == 1).ToList();
        }

        /// <summary>
        /// The get season.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="Season"/>.
        /// </returns>
        private Season GetSeason(int sportId)
        {
            var seasonDataMapper = new SeasonDataMapper();

            var seasons = seasonDataMapper.GetSeasonsBySportId(sportId);

            if (!seasons.Any())
            {
                return new Season();
            }

            return sportId != 4 ? seasonDataMapper.GetSeasonsBySportId(sportId).Single(x => x.SeasonActive) : seasonDataMapper.GetSeasonsBySportId(sportId).First(x => x.SeasonActive);
        }
    }
}