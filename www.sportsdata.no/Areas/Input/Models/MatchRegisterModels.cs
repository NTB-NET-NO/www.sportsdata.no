﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchRegisterModels.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match register models.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Web.Areas.Admin.Repositories;

namespace NTB.SportsData.Web.Areas.Input.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using log4net;

    using NTB.SportsData.Data.DataMappers.ContactPersons;
    using NTB.SportsData.Data.DataMappers.Matches;
    using NTB.SportsData.Data.DataMappers.Referees;
    using NTB.SportsData.Data.DataMappers.Sports;
    using NTB.SportsData.Data.DataMappers.Teams;
    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.NFF.TournamentAccess;
    using NTB.SportsData.Facade.NIF.SportAccess;
    using NTB.SportsData.Web.Areas.Input.Repositories;

    /// <summary>
    /// The match register models.
    /// </summary>
    public class MatchRegisterModels
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(MatchRegisterModels));

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchRegisterModels"/> class.
        /// </summary>
        public MatchRegisterModels()
        {
            // Just an empty constructor
        }

        /// <summary>
        /// The model match.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        public Match ModelMatch(FormCollection collection)
        {
            return new Match() { MatchId = Convert.ToInt32(collection["matchId"]), HomeTeamGoal = Convert.ToInt32(collection["homeTeamGoal"]), AwayTeamGoal = Convert.ToInt32(collection["awayTeamGoal"]), SportId = Convert.ToInt32(collection["sportId"]), MatchNumber = collection["matchNumber"] };
        }

        /// <summary>
        /// The register match score.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool RegisterMatchScore(FormCollection collection)
        {
            Logger.Info("We are in Register Match Score");

            Logger.DebugFormat("MatchId:{0}", collection["matchId"]);
            Logger.DebugFormat("HomeTeamGoal:{0}", collection["homeTeamGoal"]);
            Logger.DebugFormat("AwayTeamGoal:{0}", collection["awayTeamGoal"]);
            Logger.DebugFormat("SportId:{0}", collection["sportId"]);
            Logger.DebugFormat("MatchNumber:{0}", collection["matchNumber"]);

            var matchId = Convert.ToInt32(collection["matchId"]);
            if (matchId == 0)
            {
                throw new Exception("Match Id cannot be zero");
            }

            if (collection["homeTeamGoal"] == string.Empty)
            {
                throw new Exception("Home Team Goal cannot be emtpy");
            }

            if (collection["awayTeamGoal"] == string.Empty)
            {
                throw new Exception("Away Team Goal cannot be emtpy");
            }

            var homeTeamGoal = Convert.ToInt32(collection["homeTeamGoal"]);

            var awayTeamGoal = Convert.ToInt32(collection["awayTeamGoal"]);
            var sportId = Convert.ToInt32(collection["sportId"]);
            if (sportId == 0)
            {
                throw new Exception("Sport Id cannot be zero");
            }

            // Match Number is a string
            var matchNumber = collection["matchNumber"];

            if (matchNumber == string.Empty)
            {
                throw new Exception("Match Number cannot be zero");
            }

            var localMatch = new Match() { MatchId = matchId, HomeTeamGoal = homeTeamGoal, AwayTeamGoal = awayTeamGoal, SportId = sportId, MatchNumber = matchNumber };

            bool result;

            if (localMatch.SportId == 16)
            {
                // Soccer uses the TournamentFacade
                var facade = new TournamentFacade();

                result = facade.InsertMatchResult(localMatch);
            }
            else
            {
                // All other sports uses this facade
                var facade = new SportFacade();

                var federationUserId = this.GetFederationUserIdFromSportId(localMatch.SportId);
                // I need to check if this match has a score already
                var matchChecked = this.CheckLocalMatch(localMatch);

                if (matchChecked == false)
                {
                    result = facade.InsertMatchResult(localMatch, federationUserId);
                }
                else
                {
                    result = facade.UpdateMatchResult(localMatch, federationUserId);
                }
            }

            if (result)
            {
                this.StoreMatchScore(localMatch);
            }

            return result;
        }

        /// <summary>
        /// The store match score.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        public void StoreMatchScore(Match match)
        {
            var repostitory = new MatchesRepository();
            repostitory.InsertMatchResult(match);
        }

        /// <summary>
        /// Gets the federation user id from sport id
        /// </summary>
        /// <param name="sportId">the id of the sport</param>
        /// <returns>the user id of the organisation</returns>
        public int GetFederationUserIdFromSportId(int sportId)
        {
            var repository = new SportRepository();
            var federationUserId = repository.GetFederationUserIdFromSportId(sportId);

            return federationUserId;
        }

        /// <summary>
        /// The store match comment.
        /// </summary>
        /// <param name="comment">
        /// The comment.
        /// </param>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string StoreMatchComment(string comment, int matchId)
        {
            var dataMapper = new MatchDataMapper();
            dataMapper.StoreMatchComment(matchId, comment);

            return comment;
        }

        /// <summary>
        /// The pub nub receiver.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool PubNubReceiver(FormCollection collection)
        {
            var localMatch = new Match()
                                 {
                                     MatchId = Convert.ToInt32(collection["matchId"]), 
                                     HomeTeamGoal = Convert.ToInt32(collection["homeTeamGoal"]), 
                                     AwayTeamGoal = Convert.ToInt32(collection["awayTeamGoal"]), 
                                     SportId = Convert.ToInt32(collection["sportId"]), 
                                     MatchNumber = collection["matchNumber"]
                                 };

            if (this.CheckLocalMatch(localMatch) == false)
            {
                this.StoreMatchScore(localMatch);
            }

            // We'll return true no matter what.
            return true;
        }

        /// <summary>
        /// The get matches unregisterable count.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetMatchesUnregisterableCount()
        {
            var dataMapper = new MatchDataMapper();
            var matches = dataMapper.GetUnRegisterableMatches();

            if (matches.Count == 0)
            {
                return matches.Count;
            }

            // now we have a list of matches, let's check if any of these has gotten a result
            var matchGroups = from m in matches group m by m.TournamentId into matchGroup select matchGroup.ToList();

            var foundResultMatches = new List<Match>();
            foreach (var match in matchGroups)
            {
                if (match[0].SportId == 16)
                {
                    var facade = new TournamentFacade();

                    var tournamentMatches = facade.GetMatchesByTournament(match[0].TournamentId, false, false, true, false);

                    // now we are to check if any of the matches in the list of matches has gotten a result
                    foreach (var localMatch in matches)
                    {
                        var loopLocalMatch = localMatch;
                        foreach (var remoteMatch in tournamentMatches.Where(remoteMatch => remoteMatch.MatchId == loopLocalMatch.MatchId).Where(remoteMatch => remoteMatch.HomeTeamGoal != null && remoteMatch.AwayTeamGoal != null))
                        {
                            loopLocalMatch.AwayTeamGoal = remoteMatch.AwayTeamGoal;
                            loopLocalMatch.HomeTeamGoal = remoteMatch.HomeTeamGoal;
                            loopLocalMatch.RegisterAble = false;
                            foundResultMatches.Add(loopLocalMatch);
                        }
                    }
                }

                // We have to do something here based on the fact this is not soccer
                if (match[0].SportId != 16)
                {
                    
                }
            }

            foreach (var foundMatch in foundResultMatches)
            {
                dataMapper.Update(foundMatch);
            }

            // Finally returning the number of matches without results
            return dataMapper.GetUnRegisterableMatches().Count;
        }

        /// <summary>
        /// The set match unregisterable.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool SetMatchUnregisterable(int matchId, FormCollection collection)
        {
            var collectionMatchId = collection["matchId"];

            if (collectionMatchId != null)
            {
                if (Convert.ToInt32(collectionMatchId) != matchId)
                {
                    return false;
                }
            }

            var dataMapper = new MatchDataMapper();

            return dataMapper.SetMatchUnRegisterable(matchId);
        }

        /// <summary>
        /// Get the matches the logged in user is to register results for
        /// </summary>
        /// <param name="userId">
        /// GUID of the current user
        /// </param>
        /// <param name="districts">
        /// The districts.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetUserMatches(Guid userId, List<District> districts)
        {
            var mapper = new MatchDataMapper();
            var contactMapper = new ContactPersonDataMapper();
            var refereeMapper = new RefereeDataMapper();

            var matches = new List<Match>();

            foreach (var district in districts)
            {
                /*
                var remoteMatches = new List<Match>();
                if (district.SportId == 16)
                {
                    var facade = new TournamentFacade();

                    // We need to create a getMatchById method here, alternative get match by district
                    remoteMatches.AddRange(facade.GetMatchesByDistrict(district.DistrictId, DateTime.Today));
                }
                else
                {
                    // This could be used to get the latest matches for this district, just that districts aren't the same
                    // in other sports like it is in soccer.
                    // var facade = new SportFacade();
                }
                */

                var districtMatches = mapper.GetLocalMatchesByDistrictId(district.DistrictId);

                // Now we are to get contact persons
                foreach (var match in districtMatches)
                {
                    match.HomeTeamContactPersons = this.CheckContacts(contactMapper.GetContactPersonByTeamId(match.HomeTeamId), match.HomeTeamId, match.SportId);

                    match.AwayTeamContactPersons = this.CheckContacts(contactMapper.GetContactPersonByTeamId(match.AwayTeamId), match.AwayTeamId, match.SportId);

                    var referees = refereeMapper.GetRefereeByMatchId(match.MatchId);

                    if (referees.Any())
                    {
                        Logger.Debug("Adding referees");
                        match.HomeTeamContactPersons.AddRange(referees);
                        match.AwayTeamContactPersons.AddRange(referees);
                    }
                    
                    match.DistrictId = district.DistrictId;
                    match.DistrictName = district.DistrictName;

                    if (!matches.Contains(match))
                    {
                        matches.Add(match);
                    }
                }
            }

            // returning the list of matches
            return matches.ToList();
        }

        /// <summary>
        /// The get matches unregisterable.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetMatchesUnregisterable()
        {
            var dataMapper = new MatchDataMapper();
            var contactMapper = new ContactPersonDataMapper();
            var matches = dataMapper.GetUnRegisterableMatches();

            foreach (var match in matches)
            {
                match.HomeTeamContactPersons = this.CheckContacts(contactMapper.GetContactPersonByTeamId(match.HomeTeamId), match.HomeTeamId, match.SportId);

                match.AwayTeamContactPersons = this.CheckContacts(contactMapper.GetContactPersonByTeamId(match.AwayTeamId), match.AwayTeamId, match.SportId);

                var refereeMapper = new RefereeDataMapper();
                match.MatchContactPersons = refereeMapper.GetRefereeByMatchId(match.MatchId);

                var sportDataMapper = new SportDataMapper();
                match.SportName = sportDataMapper.Get(match.SportId).Name;
            }

            return matches;
        }

        /// <summary>
        /// The check local match.
        /// We are checking if the match has results or not
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool CheckLocalMatch(Match match)
        {
            var dataMapper = new MatchDataMapper();
            var localMatch = dataMapper.Get(match.MatchId);

            if (localMatch == null)
            {
                return false;
            }

            if (localMatch.MatchId != match.MatchId)
            {
                return localMatch.MatchId == match.MatchId;
            }

            if (localMatch.HomeTeamGoal != match.HomeTeamGoal)
            {
                return false;
            }

            if (localMatch.AwayTeamGoal != match.AwayTeamGoal)
            {
                return false;
            }

            return true;

            // This one should then return false
        }

        /// <summary>
        /// The check contacts.
        /// </summary>
        /// <param name="contacts">
        /// The contacts.
        /// </param>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<ContactPerson> CheckContacts(List<ContactPerson> contacts, int teamId, int sportId)
        {
            if (contacts.Count > 0)
            {
                return contacts;
            }

            // So this check isn't really needed.
            return contacts;
            //else
            //{
            // var contactMapper = new ContactPersonDataMapper();
            // var teamRoleMapper = new TeamRoleDataMapper();
            //    try
            //    {
            //        var facade = new TournamentFacade();
            //        contacts = facade.GetTeamContacts(teamId);
            //    }
            //    catch (Exception exception)
            //    {
            //        Logger.Error(exception.Message);
            //        Logger.Error(exception.StackTrace);
            //    }

            //    if (contacts != null)
            //    {
            //        foreach (var contact in contacts)
            //        {
            //            contactMapper.InsertOne(contact);

            //            var teamDataMapper = new TeamDataMapper();

            //            // Check if team is in database
            //            var team = teamDataMapper.Get(contact.TeamId);
            //            if (team.TeamId == 0 && team.SportId == 0)
            //            {
            //                var myTeam = new Team { TeamId = contact.TeamId, TeamName = contact.TeamName, SportId = sportId };
            //                teamDataMapper.InsertOne(myTeam);
            //            }

            //            teamDataMapper.DeleteTeamContactMap(contact.TeamId, contact.PersonId);
            //            teamDataMapper.InsertTeamContactMap(contact.TeamId, contact.PersonId);

            //            // Check if Role is in database
            //            var teamFunction = teamRoleMapper.GetTeamRoleById(contact.RoleId);
            //            if (teamFunction.FunctionTypeId == 0)
            //            {
            //                var function = new Function { FunctionTypeId = contact.RoleId, FunctionTypeName = contact.RoleName };
            //                teamRoleMapper.InsertTeamRole(function);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        return null;
            //    }
            //}

            return contacts.OrderBy(s => s.PersonId).ToList();
        }

        /// <summary>
        /// The correct contact persons.
        /// </summary>
        /// <param name="contactPersons">
        /// The contact persons.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<ContactPerson> CorrectContactPersons(List<ContactPerson> contactPersons)
        {
            var contacts = new List<ContactPerson>();

            foreach (var contact in contactPersons)
            {
                var c = new ContactPerson();

                var fullname = contact.FullName;
                if (fullname != null)
                {
                    // Referees name parts are splitted by comma (,)
                    if (fullname.Contains(','))
                    {
                        var nameSplit = fullname.Split(',');

                        if (nameSplit.Count() == 2)
                        {
                            c.FirstName = nameSplit[1].Trim();
                            c.LastName = nameSplit[0].Trim();

                            contact.FirstName = nameSplit[1].Trim();
                            contact.LastName = nameSplit[0].Trim();

                            var arrayFullName = new[] { contact.FirstName, contact.LastName };
                            contact.FullName = string.Join(" ", arrayFullName);
                        }
                    }
                    else
                    {
                        var nameSplit = fullname.Split(' ');

                        if (nameSplit.Count() == 2)
                        {
                            contact.FirstName = nameSplit[0];
                            contact.LastName = nameSplit[1];
                        }
                        else
                        {
                            // We let firstname be the first in the index, the rest is lastname
                            contact.FirstName = nameSplit.First();
                            contact.LastName = string.Join(" ", nameSplit.Skip(1));
                        }
                    }
                }
                else
                {
                    // Creating full name
                    var nameParts = new[] { contact.FirstName, contact.LastName };
                    contact.FullName = string.Join(" ", nameParts); // contact.FirstName + " " + contact.LastName;
                }

                c.Email = contact.Email;
                c.RoleName = contact.RoleName;
                c.FullName = contact.FullName;
                c.FirstName = contact.FirstName;
                c.LastName = contact.LastName;
                c.MobilePhone = contact.MobilePhone;
                c.HomePhone = contact.HomePhone;
                c.Active = 1; // Setting it to active as we have gotten it from the database

                contacts.Add(c);
            }

            return contacts;
        }
    }
}