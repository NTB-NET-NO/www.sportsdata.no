﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match remote data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Matches
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Data.Interfaces;
    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Interfaces;
    using NTB.SportsData.Facade.NFF.ApiAccess;
    using NTB.SportsData.Facade.NIF.ApiAccess;

    /// <summary>
    /// The match remote data mapper.
    /// </summary>
    public class MatchRemoteDataMapper : IMatchDataMapper
    {
        /// <summary>
        /// The get matches by tournament id.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Match> GetMatchesByTournamentId(Tournament tournament)
        {
            IApiFacade facade;
            if (tournament.SportId == 16)
            {
                facade = new SoccerApiFacade();
            }
            else
            {
                facade = new SportApiFacade();
            }

            return facade.GetTournamentMatches(tournament);
        }

        /// <summary>
        /// The get match by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Match GetMatchById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get todays matches by customer id.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public List<Match> GetTodaysMatchesByCustomerId(int customerId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get local matches by district id.
        /// </summary>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public List<Match> GetLocalMatchesByDistrictId(int districtId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get match comment.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="MatchComment"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public MatchComment GetMatchComment(int matchId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The store match comment.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <param name="matchComment">
        /// The match comment.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void StoreMatchComment(int matchId, string matchComment)
        {
            throw new NotImplementedException();
        }
    }
}