<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:i="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:api="http://schemas.datacontract.org/2004/07/NTB.SportsData.Domain.Classes"
    exclude-result-prefixes="xs i api"
    
    version="1.0">
    <xsl:output indent="yes" method="xml"/>
    
    <xsl:template match="/">
        <sports-content>
            <tournament>
                <tournament-metadata>
                    <xsl:attribute name="tournament-key">
                        <xsl:value-of select="SportsData/TournamentId"/>
                    </xsl:attribute>
                    <xsl:attribute name="tournament-name">
                        <xsl:value-of select="SportsData/TournamentName"/>
                    </xsl:attribute>
                </tournament-metadata>
                <tournament-division>
                    <standing>
                        <standing-metadata alignment-scope="events-all" />
                        <xsl:for-each select="SportsData/TournamentTables/TeamResult">
                            <xsl:sort select="Position" order="ascending" data-type="number"/>
                            <team>
                                <team-metadata>
                                    <xsl:attribute name="team-key">
                                        <xsl:value-of select="OrgId"/>
                                    </xsl:attribute>
                                    <name>
                                        <xsl:attribute name="full">
                                            <xsl:value-of select="OrgName"/>
                                        </xsl:attribute>
                                    </name>
                                </team-metadata>
                                <team-stats alignment-scope="events-all">
                                    <outcome-totals>
                                        <xsl:attribute name="events-played">
                                            <xsl:value-of select="Matches"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="wins">
                                            <xsl:value-of select="Victories"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="losses">
                                            <xsl:value-of select="Losses"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="ties">
                                            <xsl:value-of select="Draws"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="standing-points">
                                            <xsl:value-of select="TotalPoints"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="points-scored-for">
                                            <xsl:value-of select="GoalsScored"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="points-scored-against">
                                            <xsl:value-of select="GoalsConceeded"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="points-difference">
                                            <xsl:value-of select="GoalDifference"/>
                                        </xsl:attribute>
                                    </outcome-totals>
                                    <rank>
                                        <xsl:attribute name="value">
                                            <xsl:value-of select="Position"/>
                                        </xsl:attribute>
                                    </rank>
                                    
                                </team-stats>
                                <team-stats alignment-scope="events-home">
                                    <outcome-totals>
                                        <xsl:attribute name="events-played">
                                            <xsl:value-of select="MatchesHome"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="wins">
                                            <xsl:value-of select="VictoriesFulltimeHome"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="losses">
                                            <xsl:value-of select="LossesFulltimeAway"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="ties">
                                            <xsl:value-of select="DrawsHome"/>
                                        </xsl:attribute>
                                    </outcome-totals>
                                </team-stats>
                                <team-stats alignment-scope="events-away">
                                <outcome-totals>
                                    <xsl:attribute name="events-played">
                                        <xsl:value-of select="MatchesAway"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="wins">
                                        <xsl:value-of select="VictoriesAway"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="losses">
                                        <xsl:value-of select="LossesAway"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="ties">
                                        <xsl:value-of select="DrawsAway"/>
                                    </xsl:attribute>
                                </outcome-totals>
                                </team-stats>
                            </team>    
                        </xsl:for-each>
                    </standing>
                </tournament-division>
            </tournament>
        </sports-content>
    </xsl:template>
    
</xsl:stylesheet>