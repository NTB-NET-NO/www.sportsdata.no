﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FederationDisciplineRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The federation discipline repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Services.NIF.Exceptions;

namespace NTB.SportsData.Services.NIF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using log4net;
    using log4net.Config;

    using NTB.SportsData.Services.NIF.Interfaces;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The federation discipline repository.
    /// </summary>
    public class FederationDisciplineRepository : IRepository<FederationDiscipline>, 
                                                  IDisposable, 
                                                  IFederationDisciplineDataMapper
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FederationRepository));

        /// <summary>
        ///     The ntb bpassword.
        /// </summary>
        private readonly string _ntbPassword = ConfigurationManager.AppSettings["NIFServicesPassword"];

        /// <summary>
        ///     The ntb busername.
        /// </summary>
        private readonly string _ntbUsername = ConfigurationManager.AppSettings["NIFServicesUsername"];

        /// <summary>
        ///     The organization service client.
        /// </summary>
        private readonly OrgServiceClient _orgServiceClient = new OrgServiceClient();

        /// <summary>
        /// Initializes a new instance of the <see cref="FederationDisciplineRepository"/> class.
        /// </summary>
        public FederationDisciplineRepository()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }

            if (this._orgServiceClient.ClientCredentials != null)
            {
                this._orgServiceClient.ClientCredentials.UserName.UserName = this._ntbUsername;
                this._orgServiceClient.ClientCredentials.UserName.Password = this._ntbPassword;
            }
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get federation disciplines.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="Exception">
        /// Throws an exception on error
        /// </exception>
        public List<FederationDiscipline> GetFederationDisciplines(int orgId)
        {
            var emptyRequest = new EmptyRequest();
            var response = this._orgServiceClient.GetFederations(emptyRequest);

            if (!response.Success)
            {
                throw new SportsDataException (response.ErrorCode + @": " + response.ErrorMessage);
            }
            
            var federations = response.Federations;

            var federationDisciplines = new List<FederationDiscipline>();
            if (federations.Length == 0)
            {
                return new List<FederationDiscipline>();
            }
            var tempfederation = federations.Where(f => f.OrgId == orgId).ToList();
            var federation = new Federation();
            if (tempfederation.Count == 1)
            {
                federation = tempfederation.Single();
            }

            if (federation != null && federation.Disciplines != null)
            {
                var disciplinesString = federation.Disciplines;
                if (disciplinesString.Contains("&amp;"))
                {
                    disciplinesString = disciplinesString.Replace("&amp;", "&");
                }

                var disciplinesParts = disciplinesString.Split(';');
                foreach (var disciplinesPart in disciplinesParts)
                {
                    var discipline = new FederationDiscipline();

                    // Splitting this string
                    string[] disciplinesMinorPart = disciplinesPart.Split(':');

                    discipline.ActivityId = Convert.ToInt32(disciplinesMinorPart[0]);
                    discipline.ActivityCode = Convert.ToInt32(disciplinesMinorPart[1]);
                    discipline.ActivityName = disciplinesMinorPart[2];

                    federationDisciplines.Add(discipline);
                }
            }
            return federationDisciplines;
        }

        /// <summary>
        /// The get federation discipline.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="FederationDiscipline"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public FederationDiscipline GetFederationDiscipline(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(FederationDiscipline domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<FederationDiscipline> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(FederationDiscipline domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(FederationDiscipline domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<FederationDiscipline> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="FederationDiscipline"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public FederationDiscipline Get(int id)
        {
            throw new NotImplementedException();
        }
    }
}