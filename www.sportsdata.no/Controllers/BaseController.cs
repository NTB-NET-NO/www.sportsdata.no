﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseController.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The base controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Controllers
{
    using System;
    using System.Runtime.Caching;
    using System.Web.Mvc;

    /// <summary>
    /// The base controller.
    /// </summary>
    public class BaseController : Controller
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseController"/> class.
        /// </summary>
        public BaseController()
        {
            var policy = new CacheItemPolicy { AbsoluteExpiration = DateTime.UtcNow.AddMinutes(20) };

            if (System.Web.HttpContext.Current.Request.UserHostAddress != null)
            {
                MemoryCache.Default.Add(System.Web.HttpContext.Current.Request.UserHostAddress, "User", policy);
            }
        }
    }
}