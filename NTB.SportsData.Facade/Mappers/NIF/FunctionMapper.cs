﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FunctionMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The function mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The function mapper.
    /// </summary>
    public class FunctionMapper : BaseMapper<Function, Domain.Classes.Function>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Function, Domain.Classes.Function> mapper)
        {
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.LastName, y => y.LastName);
            mapper.Relate(x => x.FunctionTypeId, y => y.FunctionTypeId);
            mapper.Relate(x => x.FunctionTypeName, y => y.FunctionTypeName);
        }
    }
}