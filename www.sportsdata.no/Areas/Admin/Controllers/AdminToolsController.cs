﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Web.Areas.Admin.Models;
using NTB.SportsData.Web.Areas.Admin.Models.ViewModels;
using NTB.SportsData.Web.Areas.Admin.Repositories;

namespace NTB.SportsData.Web.Areas.Admin.Controllers
{
    /// <summary>
    ///     The admin tools controller.
    /// </summary>
    [Authorize]
    public class AdminToolsController : Controller
    {
        #region Fields

        /// <summary>
        ///     The _tools models.
        /// </summary>
        private ToolsModels toolsModels = new ToolsModels();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The activate season.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult ActivateSeason(int id)
        {
            var model = new SeasonModel();
            model.ActivateSeason(id);
            var sportModel = new SportModel();

            var sport = sportModel.GetSportIdBySeasonId(id);

            return this.RedirectToAction("GetSeasonsByOrgId", new {id = sport.OrgId});
        }

        /// <summary>
        ///     The activate sport.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult ActivateSport(int id)
        {
            var model = new SportModel();
            model.Activate(id);
            return this.RedirectToAction("GetFederations");
        }

        /// <summary>
        ///     The add age categories.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult AddAgeCategories(int id)
        {
            var model = new FederationModel();
            model.InsertFederationAgeCategories(id);
            this.ViewBag.OrgId = id;
            return this.RedirectToAction("EditAgeCategories", "AdminTools", new {id});
        }

        /// <summary>
        ///     The add federation disciplines.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="collection">
        ///     The collection.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        [HttpPost]
        public ActionResult AddFederationDisciplines(int id, FormCollection collection)
        {
            var federationModel = new FederationModel();
            federationModel.AddFederationDisciplines(collection["disciplines[]"], id);
            return this.RedirectToAction("GetFederations");
        }

        /// <summary>
        ///     The change age category.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult ChangeAgeCategory()
        {
            var toolsModels = new ToolsModels();

            return this.View("ChangeAgeCategory", toolsModels.GetAgeCategories(16));
        }

        /// <summary>
        ///     The check tournament.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult CheckTournament()
        {
            return this.View();
        }

        /// <summary>
        ///     The check tournament.
        /// </summary>
        /// <param name="collection">
        ///     The collection.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        [HttpPost]
        public ActionResult CheckTournament(FormCollection collection)
        {
            var tournamentId = 0;
            foreach (var key in collection.AllKeys)
            {
                tournamentId = Convert.ToInt32(collection[key]);
            }

            var toolsModel = new ToolsModels();
            if (tournamentId > 0)
            {
                var tournament = toolsModel.GetTournament(tournamentId);

                if (tournament != null)
                {
                    return this.View("TournamentInfo", tournament);
                }
            }

            return this.View();
        }

        /// <summary>
        ///     Creating the Age Category Table
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult CreateAgeCategoryTable()
        {
            var toolsModels = new ToolsModels();
            toolsModels.PopulateNffAgeCategoryTable();
            toolsModels.PopulateNifAgeCategoryTable();

            return this.View("ChangeAgeCategory");
        }

        /// <summary>
        ///     The create customer xml.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult CreateCustomerXml()
        {
            var customerModel = new CustomerRepository();
            var customers = customerModel.GetAllCustomers();
            var adminToolModels = new AdminToolModel();
            adminToolModels.CreateCustomersXml(customers);
            return this.View("Index");
        }

        /// <summary>
        ///     The create municipality table.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult CreateMunicipalityTable()
        {
            var toolsModels = new ToolsModels();

            toolsModels.CreateCountyTable();

            toolsModels.CreateRegionsTable();

            return this.View("Index");
        }

        /// <summary>
        ///     The de activate season.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult DeActivateSeason(int id)
        {
            var model = new SeasonModel();
            model.DeActivateSeason(id);
            var sportModel = new SportModel();

            var sport = sportModel.GetSportIdBySeasonId(id);

            return this.RedirectToAction("GetSeasonsByOrgId", new {id = sport.OrgId});
        }

        /// <summary>
        ///     The delete season.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult DeleteSeason(int id)
        {
            var model = new SeasonModel();
            var sportModel = new SportModel();

            var sport = sportModel.GetSportIdBySeasonId(id);

            // Now we delete the season
            model.DeleteSeason(new Season {SeasonId = id});

            return this.RedirectToAction("GetSeasons", model.GetSeasonsBySportId(sport.Id));
        }

        /// <summary>
        ///     The delete sport.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult DeleteSport(int id)
        {
            this.toolsModels = new ToolsModels();
            this.toolsModels.DeleteSport(id);
            return this.View("ListSports", this.toolsModels.ListSports());
        }

        /// <summary>
        ///     The edit age categories.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult EditAgeCategories(int id)
        {
            var model = new FederationModel();
            this.ViewBag.OrgId = id;
            return this.View(model.GetFederationAgeCategories(id));
        }

        /// <summary>
        ///     The edit age categories.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="collection">
        ///     The collection.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        [HttpPost]
        public ActionResult EditAgeCategories(int id, FormCollection collection)
        {
            var model = new FederationModel();

            model.UpdateFederationAgeCategories(id, collection);
            return this.RedirectToAction("GetFederations");
        }

        /// <summary>
        ///     The edit season.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="collection">
        ///     The collection.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        [HttpPost]
        public ActionResult EditSeason(int id, FormCollection collection)
        {
            var model = new SeasonModel();
            model.UpdateSeason(id, collection);
            var sportId = Convert.ToInt32(collection["SportId"]);
            return this.RedirectToAction("GetSeasons", new {id = sportId});
        }

        /// <summary>
        ///     The edit season.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult EditSeason(int id)
        {
            var model = new SeasonModel();
            return this.View(model.GetSeasonBySeasonId(id));
        }

        /// <summary>
        ///     Updating the database after doing necesarry changes
        /// </summary>
        /// <param name="collection">
        ///     Form collection
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        [HttpPost]
        public ActionResult EditSelectedAgeCategory(FormCollection collection)
        {
            var toolsModels = new ToolsModels();
            toolsModels.UpdateAgeCategory(collection);

            const int sportId = 16;
            return this.View("ChangeAgeCategory", toolsModels.GetAgeCategories(sportId));

            // ToolsModels toolsModels = new ToolsModels();
            // return View("EditSelectedAgeCategory", toolsModels.GetAgeCategoryById(id));
        }

        /// <summary>
        ///     Editing one selected Age Category
        /// </summary>
        /// <param name="id">
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult EditSelectedAgeCategory(int id)
        {
            var toolsModels = new ToolsModels();
            return this.View("EditSelectedAgeCategory", toolsModels.GetAgeCategoryById(id));
        }

        /// <summary>
        ///     The get age categories for sport.
        /// </summary>
        /// <param name="collection">
        ///     The collection.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        [HttpPost]
        public ActionResult GetAgeCategoriesForSport(FormCollection collection)
        {
            var formCollection = collection["sport"].Split('|');

            var sportId = Convert.ToInt32(formCollection[0]);
            var federationId = Convert.ToInt32(formCollection[1]);
            var toolsModels = new ToolsModels {FederationId = federationId, SportId = sportId};
            return this.View("GetAgeCategoriesForSport", toolsModels.GetRemoteAgeCategoriesById());
        }

        /// <summary>
        ///     The get federations.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult GetFederations()
        {
            var model = new FederationModel();
            return this.View(model.FederationRemoteRepository.GetFederations());
        }

        /// <summary>
        ///     The get matches.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult GetMatches(int id)
        {
            var model = new MatchModel();
            var sportId = Convert.ToInt32(this.Request.Params["sportId"]);
            return this.View("ListMatches", model.GetMatches(id, sportId));
        }

        /// <summary>
        ///     The get new tournaments.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult GetNewTournaments(int id)
        {
            var repository = new SeasonRepository();
            return this.View("GetNewTournaments", repository.GetSeasonsBySportId(id));
        }

        public ActionResult GetSeasonsByOrgId(int id)
        {
            var dataMapper = new SportRepository();
            var sportId = dataMapper.GetSportIdFromOrgId(id);

            var model = new SeasonModel();
            var seasons = new List<Season>(model.GetSeasonsBySportId(sportId));

            var remoteRepository = new FederationRemoteRepository();

            var federationDisciplines = remoteRepository.GetFederationDisciplines(id);

            model.InsertSeasons(sportId, seasons);

            var view = new SeasonView {SportId = id, FederationDisciplines = federationDisciplines, Seasons = seasons};
            return this.View("GetSeasons", view);
        }

        /// <summary>
        ///     The get seasons.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult GetSeasons(int id)
        {
            var dataMapper = new OrganizationRepository();
            var sportId = id;
            var org = dataMapper.GetOrganizationBySportId(sportId);
            var orgId = 0;
            if (org != null)
            {
                orgId = org.OrganizationId;
            }
            else
            {
                return null;
            }

            var model = new SeasonModel();
            var seasons = new List<Season>(model.GetSeasonsBySportId(sportId));

            var remoteRepository = new FederationRemoteRepository();

            var federationDisciplines = remoteRepository.GetFederationDisciplines(orgId);

            model.InsertSeasons(sportId, seasons);

            var view = new SeasonView {SportId = id, FederationDisciplines = federationDisciplines, Seasons = seasons};
            return this.View(view);
        }

        /// <summary>
        ///     The get tournament seasons.
        /// </summary>
        /// <param name="collection">
        ///     The collection.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        [HttpPost]
        public ActionResult GetTournamentSeasons(FormCollection collection)
        {
            var sportId = 0;
            if (collection["sport"] != null)
            {
                sportId = Convert.ToInt32(collection["sport"]);
            }

            if (sportId == 0)
            {
                return this.RedirectToAction("Tournaments");
            }

            this.Session["SportId"] = sportId;

            var model = new SeasonModel();

            return this.View(model.GetSeasonsBySportId(sportId)
                .ToList());
        }

        /// <summary>
        ///     The get new tournaments.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult GetTournaments(int? id)
        {
            try
            {
                if (id == null)
                {
                    return this.RedirectToAction("Tournaments");
                }
                var toolsModels = new ToolsModels();
                toolsModels.GetTournamentsBySportId((int) id);
                return this.View("ListSports", this.toolsModels.ListSports());
            }
            catch (Exception exception)
            {
                return this.RedirectToAction("Index");
            }
        }

        /// <summary>
        ///     The get tournaments method.
        ///     TODO: This need some refactoring.
        ///     TODO: We need to get the tournaments from the remote data store and not the local one
        ///     TODO: The list of tournaments should be the complete one and not just one we currently have in the database.
        /// </summary>
        /// <param name="collection">
        ///     The collection.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        [HttpPost]
        public ActionResult GetTournaments(FormCollection collection)
        {
            var seasonId = 0;
            if (collection["seasons"] != null)
            {
                seasonId = Convert.ToInt32(collection["seasons"]);
            }

            if (seasonId == 0)
            {
                return this.View("ListTournaments", new TournamentListViewModel());
            }

            // We should get only tournaments for this season
            var model = new TournamentModel();
            var ageCategoryDefinitionModel = new AgeCategoryDefinitionModel();
            var ageCategoryDefinitions = ageCategoryDefinitionModel.GetAgeCategoryDefinitions();

            var tournaments = model.GetTournamentsBySeasonId(seasonId);

            foreach (var tournament in tournaments)
            {
                if (tournament.AgeCategoryDefinitionId == 1)
                {
                    if (tournament.TournamentName.ToLower()
                        .Contains("kvinner") || tournament.TournamentName.ToLower()
                            .Contains("menn"))
                    {
                        tournament.AgeCategoryDefinitionId = 2;
                    }
                    else
                    {
                        tournament.AgeCategoryDefinitionId = 1;
                    }
                }
            }

            var sportId = Convert.ToInt32(this.Session["SportId"]);
            var tournamentListViewModel = new TournamentListViewModel
            {
                SportId = sportId,
                SeasonId = seasonId,
                AgeCategoryDefinitions = ageCategoryDefinitions,
                Tournaments = tournaments
            };
            return this.View("ListTournaments", tournamentListViewModel);
        }

        /// <summary>
        ///     The index.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult Index()
        {
            return this.View();
        }

        /// <summary>
        ///     The list sports.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult ListSports()
        {
            this.toolsModels = new ToolsModels();

            return this.View(this.toolsModels.ListSports());
        }

        public ActionResult RenewAgeCategories(int id)
        {
            var model = new AgeCategoryModel();
            model.RenewAgeCategories(id);
            
            var viewModel = new AgeCategoryViewModel
            {
                OrgId = id,
                AgeCategories = model.GetFederationAgeCategories(id)
            };
            return this.PartialView("ListAgeCategories", viewModel);
        }

        public ActionResult ListAgeCategories(int id)
        {
            var model = new AgeCategoryModel();
            var viewModel = new AgeCategoryViewModel();
            viewModel.OrgId = id;
            viewModel.AgeCategories = model.GetFederationAgeCategories(id);
            return this.PartialView("ListAgeCategories", viewModel);
        }

        /// <summary>
        ///     The select age category sport.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult SelectAgeCategorySport()
        {
            return this.View("SelectAgeCategorySport", this.toolsModels.GetSports());
        }

        /// <summary>
        ///     The set seasons disciplines.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="collection">
        ///     The collection.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        [HttpPost]
        public ActionResult SetSeasonsDisciplines(int id, FormCollection collection)
        {
            var disciplines = collection["discipline[]"].Split(',');
            var seasonIds = collection["seasonid[]"].Split(',');

            var size = disciplines.Length;

            var seasonDisciplines = new Dictionary<int, int>();
            for (var i = 0; i < size; i++)
            {
                if (disciplines[i].Trim() == string.Empty)
                {
                    continue;
                }

                seasonDisciplines.Add(Convert.ToInt32(seasonIds[i]), Convert.ToInt32(disciplines[i]));
            }

            var model = new SeasonModel();
            model.SetSeasonDisciplines(seasonDisciplines);

            // Remember to change this
            return this.RedirectToAction("GetSeasonsByOrgId", "AdminTools", new {area = "Admin", id});
        }

        /// <summary>
        ///     The tournaments.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult Tournaments()
        {
            var model = new SportModel();
            return this.View(model.GetSports());
        }

        /// <summary>
        ///     The update current tournaments.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult UpdateCurrentTournaments()
        {
            var toolsModel = new ToolsModels();
            toolsModel.UpdateTournaments();

            return this.View("Index");
        }

        /// <summary>
        ///     The update sports table.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult UpdateSportsTable()
        {
            return this.View(this.toolsModels.UpdateSportsTable());
        }

        /// <summary>
        ///     The update tournaments.
        /// </summary>
        /// <param name="collection">
        ///     The collection.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        [HttpPost]
        public ActionResult UpdateTournaments(FormCollection collection)
        {
            var sportId = Convert.ToInt32(collection["sportId"]);
            var toolsModels = new ToolsModels {SportId = sportId};

            toolsModels.UpdateTournamentsBySeason(collection);

            return this.RedirectToAction("index");
        }

        /// <summary>
        ///     The update tournaments with age category.
        /// </summary>
        /// <param name="collection">
        ///     The collection.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        [HttpPost]
        public ActionResult UpdateTournamentsWithAgeCategory(FormCollection collection)
        {
            var sportId = 0;
            var seasonId = 0;
            if (collection["sport"] != null)
            {
                sportId = Convert.ToInt32(collection["sport"]);
            }

            if (collection["season"] != null)
            {
                seasonId = Convert.ToInt32(collection["season"]);
            }

            var ageCategoryDefinitionValues = collection["alderskategori[]"].Split(',');
            var tournamentIds = collection["tournamentId[]"].Split(',');

            var size = ageCategoryDefinitionValues.Length;

            var ageCategoryDefintionDictionary = new Dictionary<int, int>();
            for (var i = 0; i < size; i++)
            {
                if (ageCategoryDefinitionValues[i].Trim() == string.Empty)
                {
                    continue;
                }

                var currentTournamentId = Convert.ToInt32(tournamentIds[i]);
                if (!ageCategoryDefintionDictionary.ContainsKey(currentTournamentId))
                {
                    ageCategoryDefintionDictionary.Add(
                        Convert.ToInt32(currentTournamentId),
                        Convert.ToInt32(ageCategoryDefinitionValues[i]));
                }
            }

            var model = new TournamentModel();
            sportId = model.UpdateTournamentAgeCategoryDefinitition(ageCategoryDefintionDictionary, sportId, seasonId);

            var ageCategoryDefinitionModel = new AgeCategoryDefinitionModel();
            var ageCategoryDefinitions = ageCategoryDefinitionModel.GetAgeCategoryDefinitions();

            var tournamentListViewModel = new TournamentListViewModel
            {
                SportId = sportId,
                AgeCategoryDefinitions = ageCategoryDefinitions,
                Tournaments = model.GetTournamentsBySportId(sportId, seasonId)
            };
            return this.View("ListTournaments", tournamentListViewModel);
        }

        /// <summary>
        ///     The vedlikehold.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult Vedlikehold()
        {
            var toolsModel = new ToolsModels();

            return this.View(toolsModel.GetSeasons());
        }

        /// <summary>
        ///     The view federation.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult ViewFederation(int id)
        {
            this.ViewBag.OrgId = id;
            var model = new FederationModel();
            return this.View(model.FederationRemoteRepository.GetFederation(id));
        }

        #endregion
    }
}