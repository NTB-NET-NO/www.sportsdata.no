// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectSerializer.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   Creates the object serializer class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Models.XML
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     Creates the object serializer class
    /// </summary>
    public class ObjectSerializer
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the serializable object property
        /// </summary>
        public object SerializableObject { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The Serialize Object serialized to xml
        /// </summary>
        /// <param name="matches">
        /// A List of matches
        /// </param>
        /// <returns>
        /// String in XML
        /// </returns>
        public string SerializeObject(List<Match> matches)
        {
            var serializer = new XmlSerializer(typeof(List<Match>), new XmlRootAttribute("Matches") { Namespace = string.Empty });
            var encoding = Encoding.UTF8;
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t", Encoding = encoding, OmitXmlDeclaration = false };

            using (var textWriter = new StringWriter())
            {
                using (var writer = XmlWriter.Create(textWriter, settings))
                {
                    // xDoc.CreateNavigator().AppendChild();
                    serializer.Serialize(writer, matches, namespaces);

                    // output = XmlWriter.Create(xDoc.OuterXml, settings);
                }

                return textWriter.ToString().Replace("utf-16", "utf-8");
            }
        }

        /// <summary>
        /// The Serialize Object serialized to xml
        /// </summary>
        /// <param name="matches">
        /// A List of matches
        /// </param>
        /// <returns>
        /// String in XML
        /// </returns>
        public string SerializeObject(List<MatchExtended> matches)
        {
            var serializer = new XmlSerializer(typeof(List<MatchExtended>), new XmlRootAttribute("Matches") { Namespace = string.Empty });
            var encoding = Encoding.UTF8;
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t", Encoding = encoding, OmitXmlDeclaration = false };

            using (var textWriter = new StringWriter())
            {
                using (var writer = XmlWriter.Create(textWriter, settings))
                {
                    // xDoc.CreateNavigator().AppendChild();
                    serializer.Serialize(writer, matches, namespaces);

                    // output = XmlWriter.Create(xDoc.OuterXml, settings);
                }

                return textWriter.ToString().Replace("utf-16", "utf-8");
            }
        }

        /// <summary>
        /// The serialize object.
        /// </summary>
        /// <param name="seasons">
        /// The seasons.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string SerializeObject(List<Season> seasons)
        {
            var serializer = new XmlSerializer(typeof(List<Season>), new XmlRootAttribute("Season") { Namespace = string.Empty });

            var encoding = Encoding.UTF8;
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t", Encoding = encoding, OmitXmlDeclaration = false };

            using (var textWriter = new StringWriter())
            {
                using (var writer = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(writer, seasons, namespaces);
                }

                return textWriter.ToString().Replace("utf-16", "utf-8");
            }
        }

        /// <summary>
        /// Serialize a Tournament Object
        /// </summary>
        /// <param name="tournaments">
        /// The Tournaments
        /// </param>
        /// <returns>
        /// String containing an XML structure for this object
        /// </returns>
        public string SerializeObject(List<Tournament> tournaments)
        {
            var serializer = new XmlSerializer(typeof(List<Tournament>), new XmlRootAttribute("Tournaments") { Namespace = string.Empty });

            var encoding = Encoding.UTF8;
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t" };
            settings.Encoding = encoding;
            settings.OmitXmlDeclaration = false;

            using (var textWriter = new StringWriter())
            {
                using (var writer = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(writer, tournaments, namespaces);
                }

                return textWriter.ToString().Replace("utf-16", "utf-8");
            }
        }

        /// <summary>
        ///     Serializes a single tournament object
        /// </summary>
        /// <returns>String containing an XML structure for this object</returns>
        public string Serializer()
        {
            XmlSerializer serializer = null;

            if (this.SerializableObject != null)
            {
                if (this.SerializableObject.GetType() == typeof(List<Tournament>))
                {
                    serializer = new XmlSerializer(typeof(List<Tournament>), new XmlRootAttribute("Tournaments") { Namespace = string.Empty });
                }
                else if (this.SerializableObject.GetType() == typeof(Tournament))
                {
                    serializer = new XmlSerializer(typeof(Tournament), new XmlRootAttribute("Tournament") { Namespace = string.Empty });
                }
            }

            var encoding = Encoding.UTF8;
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t", Encoding = encoding, OmitXmlDeclaration = false };

            using (var textWriter = new StringWriter())
            {
                using (var writer = XmlWriter.Create(textWriter, settings))
                {
                    if (serializer != null)
                    {
                        serializer.Serialize(writer, this.SerializableObject, namespaces);
                    }
                }

                return textWriter.ToString().Replace("utf-16", "utf-8");
            }
        }

        #endregion
    }
}