﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccountViewModels.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The external login confirmation view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The external login confirmation view model.
    /// </summary>
    public class ExternalLoginConfirmationViewModel
    {
        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        [Required]
        [Display(Name = "Brukernavn")]
        public string UserName { get; set; }
    }

    /// <summary>
    /// The manage user view model.
    /// </summary>
    public class ManageUserViewModel
    {
        /// <summary>
        /// Gets or sets the old password.
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Gjeldende passord")]
        public string OldPassword { get; set; }

        /// <summary>
        /// Gets or sets the new password.
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "{0} Må være minst {2} tegn langt.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nytt passord")]
        public string NewPassword { get; set; }

        /// <summary>
        /// Gets or sets the confirm password.
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Bekreft nytt password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "Passordene er ikke like.")]
        public string ConfirmPassword { get; set; }
    }

    /// <summary>
    /// The login view model.
    /// </summary>
    public class LoginViewModel
    {
        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        [Required]
        [Display(Name = "Brukernavn")]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Passord")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether remember me.
        /// </summary>
        [Display(Name = "Husk meg?")]
        public bool RememberMe { get; set; }
    }

    /// <summary>
    /// The update view model.
    /// </summary>
    public class UpdateViewModel
    {
        /// <summary>
        /// Gets or sets the identity roles.
        /// </summary>
        public List<Role> IdentityRoles { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        [Required]
        [HiddenInput(DisplayValue = false)]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        [Required]
        [Display(Name = "Brukernavn")]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        [Required]
        [Display(Name = "Fornavn")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        [Required]
        [Display(Name = "Etternavn")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-post adresse")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the selected role id.
        /// </summary>
        [Required]
        [Display(Name = "Rolle")]
        public string SelectedRoleId { get; set; }

        /// <summary>
        /// Gets the role items.
        /// </summary>
        public IEnumerable<SelectListItem> RoleItems
        {
            get
            {
                return new SelectList(this.IdentityRoles, "Name", "Name");
            }
        }

        // public IEnumerable<IdentityRole> Roles { get; set; }
        // public IdentityRole SelectedRole { get; set; }

        /// <summary>
        /// Gets or sets the old role.
        /// </summary>
        [HiddenInput(DisplayValue = false)]
        public string OldRole { get; set; }

        /// <summary>
        /// Gets or sets the selected role name.
        /// </summary>
        public string SelectedRoleName { get; set; }
    }

    /// <summary>
    /// The register view model.
    /// </summary>
    public class RegisterViewModel
    {
        /// <summary>
        /// Gets or sets the identity roles.
        /// </summary>
        public List<Role> IdentityRoles { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        [Required]
        [Display(Name = "Brukernavn")]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        [Required]
        [Display(Name = "Fornavn")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        [Required]
        [Display(Name = "Etternavn")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "{0} må være minst {2} tegn langt.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Passord")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the confirm password.
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Bekreft passord")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Passordene er ikke like.")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-post adresse")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the selected role id.
        /// </summary>
        [Required]
        [Display(Name = "Rolle")]
        public string SelectedRoleId { get; set; }

        /// <summary>
        /// Gets the role items.
        /// </summary>
        public IEnumerable<SelectListItem> RoleItems
        {
            get
            {
                return new SelectList(this.IdentityRoles, "Name", "Name");
            }
        }
    }

    /// <summary>
    /// The reset password view model.
    /// </summary>
    public class ResetPasswordViewModel
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        [Required]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the confirm password.
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        [Required]
        public string Code { get; set; }
    }

    /// <summary>
    /// The forgot password view model.
    /// </summary>
    public class ForgotPasswordViewModel
    {
        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}