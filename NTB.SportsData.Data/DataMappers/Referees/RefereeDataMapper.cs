﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RefereeDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The referee data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Linq;

namespace NTB.SportsData.Data.DataMappers.Referees
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using log4net;

    using NTB.SportsData.Data.Interfaces.DataMappers.Referees;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The referee data mapper.
    /// </summary>
    public class RefereeDataMapper : IRefereeDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(RefereeDataMapper));

        /// <summary>
        /// The get referee by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ContactPerson> GetRefereeByMatchId(int matchId)
        {
            var contactPersons = new List<ContactPerson>();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetRefereesByMatchId", sqlConnection) { CommandType = CommandType.StoredProcedure };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return contactPersons;
                    }

                    while (sqlDataReader.Read())
                    {
                        var contactPerson = new ContactPerson
                                                {
                                                    PersonId = Convert.ToInt32(sqlDataReader["RefereeId"]), 
                                                    FirstName = sqlDataReader["FirstName"].ToString(), 
                                                    LastName = sqlDataReader["LastName"].ToString(), 
                                                    HomePhone = sqlDataReader["HomePhone"].ToString(), 
                                                    MobilePhone = sqlDataReader["MobilePhone"].ToString(), 
                                                    TeamId = Convert.ToInt32(sqlDataReader["OrgId"]), 
                                                    TeamName = sqlDataReader["OrgName"].ToString(), 
                                                    RoleName = sqlDataReader["RefereeType"].ToString(), 
                                                    Active = 1
                                                };

                        Logger.DebugFormat("Checking if referee is {0} in list of referees", contactPerson.FirstName + " " + contactPerson.LastName);

                        var foundPerson = contactPersons.Any(x => x.PersonId == contactPerson.PersonId);
                        if (foundPerson)
                        {
                            Logger.Debug("Found in list");
                            continue;
                        }


                        Logger.DebugFormat("Adding referee {0} to list of referees", contactPerson.FirstName + " " + contactPerson.LastName);
                        contactPersons.Add(contactPerson);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return contactPersons;
            }
        }
    }
}