// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FactorViewModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Web.Models
{
    /// <summary>
    ///     The factor view model.
    /// </summary>
    public class FactorViewModel
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the purpose.
        /// </summary>
        public string Purpose { get; set; }

        #endregion
    }
}