﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IContactPersonDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The ContactPersonDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The ContactPersonDataMapper interface.
    /// </summary>
    public interface IContactPersonDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The activate deactivate contact person.
        /// </summary>
        /// <param name="personId">
        /// The person id.
        /// </param>
        void ActivateDeactivateContactPerson(int personId);

        /// <summary>
        /// The delete contact team map.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="contactId">
        /// The contact id.
        /// </param>
        void DeleteContactTeamMap(int teamId, int contactId);

        /// <summary>
        /// The get contact person by team id.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<ContactPerson> GetContactPersonByTeamId(int teamId);

        /// <summary>
        /// The insert contact team map.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="contactId">
        /// The contact id.
        /// </param>
        void InsertContactTeamMap(int teamId, int contactId);

        #endregion
    }
}