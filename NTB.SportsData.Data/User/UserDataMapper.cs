﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using Microsoft.AspNet.Identity;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Input.Interfaces.Common;
using NTB.SportsData.Input.Interfaces.DataMappers;

namespace NTB.SportsData.Input.DataMappers.User
{
    public class UserDataMapper : IRepository<ApplicationUser>, IUserDataMapper, IDisposable
    {
        // internal static readonly ILog Logger = LogManager.GetLogger(typeof(UserDataMapper));
        readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int InsertOne(ApplicationUser domainobject)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertUser", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", domainobject.Id));
                    
                    sqlCommand.Parameters.Add(new SqlParameter("@UserName", domainobject.UserName));

                    sqlCommand.Parameters.Add(new SqlParameter("@Password", domainobject.PasswordHash));
                    
                    sqlCommand.Parameters.Add(new SqlParameter("@Email", domainobject.Email));
                    
                    sqlCommand.Parameters.Add(new SqlParameter("@FirstName", domainobject.FirstName));

                    sqlCommand.Parameters.Add(new SqlParameter("@LastName", domainobject.LastName));
                    
                    

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Dispose();

                    _logger.Info("User inserted successfully");

                    return 1;
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message);
                    _logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        _logger.Error(exception.InnerException.Message);
                        _logger.Error(exception.InnerException.StackTrace);
                    }

                    return 0;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertAll(List<ApplicationUser> domainobject)
        {
            foreach (var applicationUser in domainobject)
            {
                InsertOne(applicationUser);
            }
        }

        public void Update(ApplicationUser domainobject)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_UpdateUser", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", domainobject.Id));

                    sqlCommand.Parameters.Add(new SqlParameter("@UserName", domainobject.UserName));

                    sqlCommand.Parameters.Add(new SqlParameter("@PassWord", domainobject.PasswordHash));

                    sqlCommand.Parameters.Add(new SqlParameter("@Email", domainobject.Email));

                    sqlCommand.Parameters.Add(new SqlParameter("@FirstName", domainobject.FirstName));

                    sqlCommand.Parameters.Add(new SqlParameter("@LastName", domainobject.LastName));

                    sqlCommand.ExecuteNonQuery();

                    _logger.Info("User updated successfully");

                    sqlCommand.Dispose();

                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message);
                    _logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        _logger.Error(exception.InnerException.Message);
                        _logger.Error(exception.InnerException.StackTrace);
                    }

                    
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void Delete(ApplicationUser domainobject)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteUser", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", domainobject.Id));

                    sqlCommand.ExecuteNonQuery();

                    _logger.Info("User deleted successfully");

                    sqlCommand.Dispose();

                    
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message);
                    _logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        _logger.Error(exception.InnerException.Message);
                        _logger.Error(exception.InnerException.StackTrace);
                    }

                    
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public IQueryable<ApplicationUser> GetAll()
        {
            // now that we have the user, we can insert the rest of the information
            var applicationUsers = new List<ApplicationUser>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetAllUsers", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };


                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return applicationUsers.AsQueryable();
                    }

                    while (sqlDataReader.Read())
                    {
                        var applicationUser = new ApplicationUser
                        {
                            Id = sqlDataReader["Id"].ToString(),
                            FirstName = sqlDataReader["FirstName"].ToString(),
                            LastName = sqlDataReader["LastName"].ToString(),
                            Email = sqlDataReader["Email"].ToString(),
                            UserName = sqlDataReader["UserName"].ToString()
                        };

                        applicationUsers.Add(applicationUser);
                    }

                    return applicationUsers.AsQueryable();
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message);
                    _logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        _logger.Error(exception.InnerException.Message);
                        _logger.Error(exception.InnerException.StackTrace);
                    }

                    return applicationUsers.AsQueryable();

                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public ApplicationUser Get(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<ApplicationUser> GetUsers()
        {
            return GetAll();
        }

        public void InsertUser(ApplicationUser user)
        {
            InsertOne(user);
        }

        public void UpdateUser(ApplicationUser user)
        {
            Update(user);
        }

        public void DeleteUser(ApplicationUser user)
        {
            Delete(user);
        }

        public ApplicationUser GetUserByUserId(string userId)
        {
            _logger.Info("GetUserByUserId: " + userId);
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetUserByUserId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var user = new ApplicationUser();
                    while (sqlDataReader.Read())
                    {
                        user.FirstName = sqlDataReader["FirstName"].ToString();
                        user.Email = sqlDataReader["Email"].ToString();
                        user.Id = sqlDataReader["Id"].ToString();
                        user.LastName = sqlDataReader["LastName"].ToString();
                        user.UserName = sqlDataReader["UserName"].ToString();
                        user.PasswordHash = sqlDataReader["PasswordHash"].ToString();
                        user.SecurityStamp = sqlDataReader["SecurityStamp"].ToString();
                        

                        var role = new Domain.Classes.Role
                        {
                            Name = sqlDataReader["Name"].ToString(),
                            Id = sqlDataReader["RoleId"].ToString()
                        };

                        
                        user.Roles = new Collection<IRole> {role};
                    }

                    _logger.Info("User Found in database");
                    return user;


                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message);
                    _logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        _logger.Error(exception.InnerException.Message);
                        _logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;

                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public ApplicationUser GetUserByUserName(string userName)
        {

            _logger.Info("GetUserByUserName: " + userName);
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetUserByUserName", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserName", userName));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var user = new ApplicationUser();
                    while (sqlDataReader.Read())
                    {
                        user.FirstName = sqlDataReader["FirstName"].ToString();
                        user.Email = sqlDataReader["Email"].ToString();
                        user.Id = sqlDataReader["Id"].ToString();
                        user.LastName = sqlDataReader["LastName"].ToString();
                        user.UserName = sqlDataReader["UserName"].ToString();
                        user.PasswordHash = sqlDataReader["PasswordHash"].ToString();
                        user.SecurityStamp = sqlDataReader["SecurityStamp"].ToString();

                        var role = new Domain.Classes.Role
                        {
                            Name = sqlDataReader["Name"].ToString(),
                            Id = sqlDataReader["RoleId"].ToString()
                        };

                        
                        user.Roles = new Collection<IRole> {role};
                    }

                    return user;
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message);
                    _logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        _logger.Error(exception.InnerException.Message);
                        _logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public ApplicationUser GetUserByEmail(string email)
        {
            _logger.Info("GetUserByEmail: " + email);
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetUserByEmail", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@Email", email));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var user = new ApplicationUser();
                    while (sqlDataReader.Read())
                    {
                        user.FirstName = sqlDataReader["FirstName"].ToString();
                        user.Email = sqlDataReader["Email"].ToString();
                        user.Id = sqlDataReader["Id"].ToString();
                        user.LastName = sqlDataReader["LastName"].ToString();
                        user.UserName = sqlDataReader["UserName"].ToString();
                        user.PasswordHash = sqlDataReader["PasswordHash"].ToString();
                        user.SecurityStamp = sqlDataReader["SecurityStamp"].ToString();

                        var role = new Domain.Classes.Role
                        {
                            Name = sqlDataReader["Name"].ToString(),
                            Id = sqlDataReader["RoleId"].ToString()
                        };


                        user.Roles = new Collection<IRole> {role};
                    }

                    return user;
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message);
                    _logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        _logger.Error(exception.InnerException.Message);
                        _logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public bool IsUserInRoleByUserId(string userId)
        {
            _logger.Info("IsUserInRoleByUserId: " + userId);
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetUserRoleByUserId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return false;
                    }

                    
                    return true;
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message);
                    _logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        _logger.Error(exception.InnerException.Message);
                        _logger.Error(exception.InnerException.StackTrace);
                    }

                    return false;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}