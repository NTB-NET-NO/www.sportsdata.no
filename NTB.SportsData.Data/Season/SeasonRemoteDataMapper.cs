﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SeasonRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The season remote data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Season
{
    using System.Collections.Generic;

    using NTB.SportsData.Data.Interfaces;
    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Interfaces;
    using NTB.SportsData.Facade.NIF.ApiAccess;

    /// <summary>
    /// The season remote data mapper.
    /// </summary>
    public class SeasonRemoteDataMapper : ISeasonRemoteDataMapper
    {
        /// <summary>
        /// The get federation season by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Season> GetFederationSeasonByOrgId(int orgId)
        {
            IApiFacade facade = new SportApiFacade();

            return facade.GetSeasonsByOrgId(orgId);
        }
    }
}