﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.NFF.TournamentAccess;
using NTB.SportsData.Facade.NIF.SportAccess;
using NTB.SportsData.Web.Areas.Admin.Repositories;

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    public class AgeCategoryModel
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AgeCategoryModel));

        public void UpdateAgeCategory(FormCollection collection)
        {
            var ageCategory = new AgeCategory();
            ageCategory.CategoryId = Convert.ToInt32(collection["categoryId"]);
            ageCategory.CategoryName = collection["categoryName"];
            
            if (collection["minage"] == string.Empty)
            {
                ageCategory.MinAge = 0;
            }
            else
            {
                ageCategory.MinAge = Convert.ToInt32(collection["minage"]);
            }

            if (collection["maxage"] == string.Empty)
            {
                ageCategory.MaxAge = 0;
            }
            else
            {
                ageCategory.MaxAge = Convert.ToInt32(collection["maxage"]);
            }
            
            ageCategory.SportId = Convert.ToInt32(collection["selectedSportId"]);
            ageCategory.AgeCategoryDefinitionId = Convert.ToInt32(collection["ageCategoryDefinition"]);

            var repository = new AgeCategoryRepository();
            repository.Update(ageCategory);
        }

        public EditCategoryViewModel GetEditCategoryViewModel(int ageCategoryId)
        {
            var sportRepository = new SportRepository();
            var sports = sportRepository.GetAll().ToList();
            var ageCategoryDefinitionRepository = new AgeCategoryDefinitionRepository();
            var ageCategoryDefinitions = new List<AgeCategoryDefinition>(ageCategoryDefinitionRepository.GetAll());

            var repository = new AgeCategoryRepository();
            var ageCategory = repository.Get(ageCategoryId);

            var viewModel = new EditCategoryViewModel();
            viewModel.AgeCategoryDefinitions = ageCategoryDefinitions;
            viewModel.Sports = sports;
            viewModel.AgeCategoryId = ageCategory.CategoryId;
            viewModel.MaxAge = ageCategory.MaxAge;
            viewModel.MinAge = ageCategory.MinAge;
            viewModel.AgeCategoryName = ageCategory.CategoryName;
            viewModel.SelectedAgeCategoryDefinition = ageCategory.AgeCategoryDefinitionId;
            viewModel.SelectedSport = ageCategory.SportId;
            viewModel.OrganisationId = Convert.ToInt32(sports.Single(x => x.Id == ageCategory.SportId)
                .OrgId);

            return viewModel;
            
        }

        public List<AgeCategory> RenewAgeCategories(int orgId)
        {
            var ageCategories = new List<AgeCategory>();
            try
            {
                var repository = new AgeCategoryRepository();
                repository.DeleteAgeCategoriesByOrgId(orgId);

                ageCategories = repository.GetAgeCategoriesByOrgId(orgId);
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

            }

            return ageCategories;
        }

        public List<AgeCategory> GetFederationAgeCategories(int orgId)
        {
            var repository = new AgeCategoryRepository();
            var ageCategories = repository.GetAgeCategoriesByOrgId(orgId);

            var facade = new SportFacade();
            var remoteAgeCategories = facade.GetAgeCategories(orgId);

            var sportRepository = new SportRepository();
            var sportId = sportRepository.GetSportIdFromOrgId(orgId);
            if (ageCategories.Count == 0)
            {
                
                foreach (var remoteAgeCategory in remoteAgeCategories)
                {
                    Logger.DebugFormat("CategoryName: {0}", remoteAgeCategory.CategoryName);
                    Logger.DebugFormat("CategoryId: {0}", remoteAgeCategory.CategoryId);
                    Logger.DebugFormat("AgeCategoryDefinition: {0}", remoteAgeCategory.AgeCategoryDefinition);
                    Logger.DebugFormat("AgeCategoryDefinitionId: {0}", remoteAgeCategory.AgeCategoryDefinitionId);
                    
                    if (ageCategories.Contains(remoteAgeCategory))
                    {
                        continue;
                    }

                    if (remoteAgeCategory.AgeCategoryDefinitionId == 2)
                    {
                        if (remoteAgeCategory.CategoryName.ToLower().Contains("jenter") ||
                            remoteAgeCategory.CategoryName.ToLower().Contains("gutter"))
                        {
                            remoteAgeCategory.AgeCategoryDefinitionId = 1;
                            remoteAgeCategory.AgeCategoryDefinition = "Aldersbestemt";
                        }
                        else
                        {
                            remoteAgeCategory.AgeCategoryDefinitionId = 2;
                            remoteAgeCategory.AgeCategoryDefinition = "Senior";
                        }
                    }
                    remoteAgeCategory.SportId = sportId;
                    repository.InsertOne(remoteAgeCategory);
                }

                // repository.InsertAll(ageCategories);
                ageCategories = remoteAgeCategories;
            }

            Logger.Debug("===========================================================");
            Logger.DebugFormat("Number of local age categories: {0}", ageCategories.Count);
            Logger.DebugFormat("Number of remote age categories: {0}", remoteAgeCategories.Count);
            Logger.Debug("===========================================================");
            // The number of local age categories and remote age categories are not the same
            // There are fewer local age categories than remote
            if (ageCategories.Count <= remoteAgeCategories.Count)
            {
                repository.DeleteAgeCategoriesBySportId(sportId);
                foreach (var remoteAgeCategory in remoteAgeCategories)
                {
                    Logger.DebugFormat("CategoryName: {0}", remoteAgeCategory.CategoryName);
                    Logger.DebugFormat("CategoryId: {0}", remoteAgeCategory.CategoryId);
                    Logger.DebugFormat("AgeCategoryDefinition: {0}", remoteAgeCategory.AgeCategoryDefinition);
                    Logger.DebugFormat("AgeCategoryDefinitionId: {0}", remoteAgeCategory.AgeCategoryDefinitionId);
                    
                    if (ageCategories.Contains(remoteAgeCategory))
                    {
                        continue;
                    }

                    if (remoteAgeCategory.AgeCategoryDefinitionId == 2)
                    {
                        if (remoteAgeCategory.CategoryName.ToLower().Contains("jenter") ||
                            remoteAgeCategory.CategoryName.ToLower().Contains("gutter"))
                        {
                            remoteAgeCategory.AgeCategoryDefinitionId = 1;
                            remoteAgeCategory.AgeCategoryDefinition = "Aldersbestemt";
                        }
                        else
                        {
                            remoteAgeCategory.AgeCategoryDefinitionId = 2;
                            remoteAgeCategory.AgeCategoryDefinition = "Senior";
                        }
                    }
                    remoteAgeCategory.SportId = sportId;
                    repository.InsertOne(remoteAgeCategory);
                }

                ageCategories = remoteAgeCategories;
            }
            return ageCategories;
            
        }

        public List<AgeCategory> GetAgeCategories()
        {
            var repository = new AgeCategoryRepository();
            var ageCategories = new List<AgeCategory>(repository.GetAll());
            
            if (!ageCategories.Any())
            {
                // We have to use the services
                var tournamentFacade = new TournamentFacade();

                // Getting the data from the facade
                ageCategories = tournamentFacade.GetAgeCategoriesTournament();
            }

            return ageCategories;
        }

        public List<AgeCategory> GetAgeCategoriesBySportId(int sportId)
        {
            AgeCategoryRepository repository = new AgeCategoryRepository();

            
            var agecategories = new List<AgeCategory>(repository.GetAgeCategoryBySportId(sportId));

            if (!agecategories.Any())
            {
                if (sportId == 16)
                {
                    TournamentFacade facade = new TournamentFacade();
                    agecategories = facade.GetAgeCategoriesTournament();
                }
                else
                {
                    var orgRepository = new OrganizationRepository();
                    Organization organization = orgRepository.GetOrganizationBySportId(sportId);
                    SportFacade facade = new SportFacade();
                    agecategories = facade.GetAgeCategories(organization.SportId);
                }
            }

            return agecategories;
        }
    }
}