﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JobModel.cs" company="NTB">
//   Copyright Norsk Telegrambyrå
// </copyright>
// <summary>
//   The job model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Web.Areas.Admin.Repositories;

    /// <summary>
    /// The job model.
    /// </summary>
    public class JobModel
    {
        /// <summary>
        /// The update job.
        /// </summary>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <param name="jobName">
        /// The job name.
        /// </param>
        /// <param name="schedule">
        /// The schedule.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="ageCategoryDefinitionId">
        /// The age category definition id.
        /// </param>
        public void UpdateJob(int jobId, string jobName, bool schedule, int sportId, int ageCategoryDefinitionId)
        {
            // myJobs.UpdateCustomerJob(CustomerId, CustomerJobName);
            var repository = new JobRepository();
            var job = new Job
            {
                JobId = jobId,
                Schedule = schedule,
                JobName = jobName,
                AgeCategoryId = ageCategoryDefinitionId,
                SportId = sportId
            };

            repository.Update(job);
        }
    }
}