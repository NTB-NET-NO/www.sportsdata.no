﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationPasswordHasher.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Web.Areas.Input.Models.IdentityModels
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    using Microsoft.AspNet.Identity;

    /// <summary>
    ///     The application password.
    /// </summary>
    public class ApplicationPasswordHasher : IPasswordHasher
    {
        #region Public Methods and Operators

        /// <summary>
        /// The base 64 encode.
        /// </summary>
        /// <param name="plainText">
        /// The plain text.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string Base64Encode(string plainText)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        /// <summary>
        /// The hash password.
        /// </summary>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string HashPassword(string password)
        {
            var saltString = this.Base64Encode("Otter32k@ller!");

            byte[] salt;  
            byte[] buffer2;
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            using (var bytes = new Rfc2898DeriveBytes(password, 0x10, 0x3e8))
            {
                salt = bytes.Salt;
                buffer2 = bytes.GetBytes(0x20);
            }

            var dst = new byte[0x31];
            Buffer.BlockCopy(salt, 0, dst, 1, 0x10);
            Buffer.BlockCopy(buffer2, 0, dst, 0x11, 0x20);
            return Convert.ToBase64String(dst);
           
            
            /*
            byte[] passBytes = Encoding.Unicode.GetBytes(password);
            byte[] saltBytes = Convert.FromBase64String(saltString);
            var keyedHashAlgorithm = (KeyedHashAlgorithm)HashAlgorithm.Create("HMACSHA256");
            if (keyedHashAlgorithm == null)
            {
                return Convert.ToBase64String(keyedHashAlgorithm.ComputeHash(passBytes));
            }

            var keyBytes = new byte[keyedHashAlgorithm.Key.Length];
            int num1 = 0;

            while (true)
            {
                if (num1 >= keyBytes.Length)
                {
                    break;
                }

                int num2 = Math.Min(saltBytes.Length, keyBytes.Length - num1);

                Buffer.BlockCopy(saltBytes, 0, keyBytes, num1, num2);
                num1 = num1 + num2;
            }

            keyedHashAlgorithm.Key = keyBytes;

            return Convert.ToBase64String(keyedHashAlgorithm.ComputeHash(passBytes));
             */
        }

        /// <summary>
        /// The verify hashed password.
        /// </summary>
        /// <param name="hashedPassword">
        /// The hashed password.
        /// </param>
        /// <param name="providedPassword">
        /// The provided password.
        /// </param>
        /// <returns>
        /// The <see cref="PasswordVerificationResult"/>.
        /// </returns>
        public PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            byte[] buffer4;
            if (hashedPassword == null)
            {
                return PasswordVerificationResult.Failed;
            }

            if (providedPassword == null)
            {
                throw new ArgumentNullException("providedPassword");
            }

            byte[] src = Convert.FromBase64String(hashedPassword);
            if ((src.Length != 0x31) || (src[0] != 0))
            {
                return PasswordVerificationResult.Failed;
            }

            var dst = new byte[0x10];
            Buffer.BlockCopy(src, 1, dst, 0, 0x10);
            var buffer3 = new byte[0x20];
            Buffer.BlockCopy(src, 0x11, buffer3, 0, 0x20);
            using (var bytes = new Rfc2898DeriveBytes(providedPassword, dst, 0x3e8))
            {
                buffer4 = bytes.GetBytes(0x20);
            }

            if (this.ByteArraysEqual(buffer3, buffer4))
            {
                return PasswordVerificationResult.Success;
            }

            return PasswordVerificationResult.Failed;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The byte arrays equal.
        /// </summary>
        /// <param name="b1">
        /// The b 1.
        /// </param>
        /// <param name="b2">
        /// The b 2.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool ByteArraysEqual(byte[] b1, byte[] b2)
        {
            if (b1 == b2)
            {
                return true;
            }

            if (b1 == null || b2 == null)
            {
                return false;
            }

            if (b1.Length != b2.Length)
            {
                return false;
            }

            for (int i = 0; i < b1.Length; i++)
            {
                if (b1[i] != b2[i])
                {
                    return false;
                }
            }

            return true;
        }

        #endregion
    }
}