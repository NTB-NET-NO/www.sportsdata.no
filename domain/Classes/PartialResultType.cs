﻿namespace NTB.SportsData.Domain.Classes
{
    public class PartialResultType
    {
        public int TypeId { get; set; }

        public string TypeName { get; set; }

        public int ActivityId { get; set; }

        public int OrgId { get; set; }

        public int Sorting { get; set; }
    }
}