﻿namespace NTB.SportsData.Classes.Classes
{
    public class TeamResult
    {
        public int AwayPoints { get; set; }

        public int BonusPointsAway { get; set; }

        public int BonusPointsHome { get; set; }

        public int BonusPointsTotal { get; set; }

        public bool Dispensation { get; set; }

        public int Draws { get; set; }

        public int DrawsAway { get; set; }

        public int GoalDifference { get; set; }

        public int EntryId { get; set; }
            
        public int DrawsHome { get; set; }

        public double GoalRatio { get; set; }

        public string GoalsAwayFormatted { get; set; }
            
        public int GoalsConcededAway {get; set;}
            
        public int GoalsConcededHome {get; set;}
        
        public int GoalsConceeded {get; set;}
            
        public string GoalsHomeFormatted {get; set;}
            
        public int GoalsScored {get; set;}
            
        public int GoalsScoredAway {get; set;}
            
        public int GoalsScoredHome {get; set;}
            
        public int Losses {get; set;}
            
        public int LossesAway {get; set;}
            
        public int LossesFulltimeAway {get; set;}
            
        public int LossesFulltimeHome {get; set;}
            
        public int LossesFulltimeTotal {get; set;}
            
        public int LossesHome {get; set;}
            
        public int LossesOvertimeAway {get; set;}
            
        public int LossesOvertimeHome {get; set;}
            
        public int LossesOvertimeTotal {get; set;}
            
        public int LossesPenaltiesAway {get; set;}
            
        public int LossesPenaltiesHome {get; set;}
            
        public int LossesPenaltiesTotal {get; set;}
            
        public int Matches {get; set;}
            
        public int MatchesAway {get; set;}
            
        public int MatchesHome {get; set;}
            
        public int OrgId {get; set;}
            
        public string OrgName {get; set;}
            
        public int PartialPointsConceded {get; set;}
            
        public int PartialPointsConcededAway {get; set;}
            
        public int PartialPointsConcededHome {get; set;}
            
        public int PartialPointsDifference {get; set;}
            
        public int PartialPointsDifferenceAway {get; set;}
            
        public int PartialPointsDifferenceHome {get; set;}
            
        public int PartialPointsScored {get; set;}
            
        public int PartialPointsScoredAway {get; set;}
            
        public int PartialPointsScoredHome {get; set;}
    
        public int PointsAway {get; set;}
            
        public int PointsHome {get; set;}
            
        public int PointsStart {get; set;}
            
        public int Position {get; set;}
            
        public string Sort {get; set;}
            
        public string TeamPenalty {get; set;}
            
        public int TeamPenaltyNegative {get; set;}
            
        public int TeamPenaltyPositive {get; set;}
            
        public int TotalGoals {get; set;}
            
        public string TotalGoalsFormatted {get; set;}
            
        public int TotalMatches {get; set;}
            
        public int TotalPoints {get; set;}
            
        public int Victories {get; set;}
            
        public int VictoriesAway {get; set;}
            
        public int VictoriesFulltimeAway {get; set;}
            
        public int VictoriesFulltimeHome {get; set;}
            
        public int VictoriesFulltimeTotal {get; set;}
            
        public int VictoriesHome {get; set;}
            
        public int VictoriesOvertimeAway {get; set;}
            
        public int VictoriesOvertimeHome {get; set;}
            
        public int VictoriesOvertimeTotal {get; set;}
            
        public int VictoriesPenaltiesAway {get; set;}
            
        public int VictoriesPenaltiesHome {get; set;}
            
        public int VictoriesPenaltiesTotal {get; set;}
    }
}
