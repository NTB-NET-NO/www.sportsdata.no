﻿using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Interfaces;
using NTB.SportsData.Facade.Mappers.NIF;
using NTB.SportsData.Services.NIF.Interfaces;
using NTB.SportsData.Services.NIF.Repositories;

namespace NTB.SportsData.Facade.NIF.ApiAccess
{
    /// <summary>
    /// The sport api facade.
    /// </summary>
    public class SportApiFacade : IApiFacade
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SportApiFacade"/> class.
        /// </summary>
        public SportApiFacade()
        {
            this.teamDataMapper = new TeamRepository();
            this.tournamentDataMapper = new TournamentRepository();
            this.matchDataMapper = new MatchRepository();
            this.seasonDataMapper = new SeasonRepository();
        }

        #endregion

        public MatchExtended GetMatchExtendedByMatchId(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The hello world.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string HelloWorld()
        {
            return "Hello World";
        }

        /// <summary>
        /// The get team result standing.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TeamResult> GetTeamResultStanding(Tournament tournament)
        {
            var result = this.tournamentDataMapper.GetTournamentStanding(tournament.TournamentId);

            var mapper = new TeamResultMapper();

            return result.Select(row => mapper.Map(row, new TeamResult()))
                .ToList();
        }

        /// <summary>
        /// The get tournament standing.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public List<TournamentTable> GetTournamentStanding(Tournament tournament)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get tournaments by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// /// 
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Tournament> GetTournamentsByOrgId(int orgId, int seasonId)
        {
            var result = this.tournamentDataMapper.GetTournamentsByOrgIdAndSeasonId(orgId, seasonId);

            var mapper = new TournamentMapper();

            return result.Select(row => mapper.Map(row, new Tournament()))
                .ToList();
        }

        /// <summary>
        /// The get seasons by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Season> GetSeasonsByOrgId(int orgId)
        {
            var result = this.seasonDataMapper.GetFederationSeasonsByOrgId(orgId);

            var mapper = new SeasonMapper();

            return result.Select(row => mapper.Map(row, new Season()))
                .ToList();
        }

        /// <summary>
        /// The get matches by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="seasonId">
        /// The season Id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetMatchesByOrgId(int orgId, int seasonId)
        {
            var result = this.matchDataMapper.GetMatchesByOrgIdAndSeasonId(orgId, seasonId);

            var mapper = new MatchInfoMapper();

            return result.Select(row => mapper.Map(row, new Match()))
                .ToList();
        }

        public MatchExtended GetMatchByMatchId(int id)
        {
            throw new NotImplementedException();
        }

        #region Fields

        /// <summary>
        /// The _team data mapper.
        /// </summary>
        private readonly ITeamDataMapper teamDataMapper;

        /// <summary>
        /// The _tournament data mapper.
        /// </summary>
        private readonly ITournamentDataMapper tournamentDataMapper;

        /// <summary>
        /// The _match data mapper.
        /// </summary>
        private readonly IMatchDataMapper matchDataMapper;

        /// <summary>
        /// The _season data mapper.
        /// </summary>
        private readonly ISeasonDataMapper seasonDataMapper;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get tournament matches.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Match> GetTournamentMatches(Tournament tournament)
        {
            var result = this.matchDataMapper.GetMatchesByTournamentIdAndSportId(tournament.TournamentId, tournament.SportId);

            var mapper = new MatchMapper();
            return result.Select(row => mapper.Map(row, new Match()))
                .ToList();
        }

        /// <summary>
        /// The get tournament teams.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Team> GetTournamentTeams(int tournamentId)
        {
            var result = this.teamDataMapper.GetTournamentTeams(tournamentId);

            var mapper = new TeamMapper();

            return result.Select(row => mapper.Map(row, new Team()))
                .ToList();
        }

        #endregion
    }
}