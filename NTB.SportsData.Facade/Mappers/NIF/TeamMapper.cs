// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The team mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The team mapper.
    /// </summary>
    public class TeamMapper : BaseMapper<TournamentMatchTeam, Domain.Classes.Team>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<TournamentMatchTeam, Domain.Classes.Team> mapper)
        {
            mapper.Relate(x => x.TeamId, y => y.TeamId);
            mapper.Relate(x => x.Team, y => y.TeamName);
        }
    }
}