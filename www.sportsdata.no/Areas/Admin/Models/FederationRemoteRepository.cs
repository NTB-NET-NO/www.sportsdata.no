namespace NTB.SportsData.Web.Areas.Admin.Models
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.NIF.SportAccess;
    using NTB.SportsData.Web.Areas.Admin.Repositories;
    using log4net;

    public class FederationRemoteRepository
    {

        /// <summary>
        ///     The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(FederationRemoteRepository));

        public Federation GetFederation(int orgId)
        {
            var facade = new SportFacade();
            var federation = facade.GetFederationByOrgId(orgId);

            var disciplines = this.GetFederationDisciplines(orgId);
            
            federation.FederationDisciplines = disciplines;

            return federation;
        }



        public List<FederationDiscipline> AddFederationDisciplines(string disciplineList, int orgId)
        {
            // Creating the Federation Data Access object
            var repository = new FederationDisciplinesRepository
            {
                OrgId = orgId
            };

            repository.DeleteAll();

            // The disciplines comes in a comma-separated string
            string[] disciplines = disciplineList.Split(',');
            var federationDisciplines = new List<FederationDiscipline>();
            foreach (var discipline in disciplines)
            {
                string[] splittedDiscipline = discipline.Split('|');
                int disciplineId = Convert.ToInt32(splittedDiscipline[0]);
                string disciplineName = splittedDiscipline[1];

                var federationDiscipline = new FederationDiscipline
                {
                    ActivityId = disciplineId,
                    ActivityName = disciplineName
                };

                federationDisciplines.Add(federationDiscipline);
            }

            // Now we shall add these disciplines to the database

            return federationDisciplines;
        }

        /// <summary>
        ///     Get all the federations from the NIF API
        /// </summary>
        /// <returns></returns>
        public List<Federation> GetFederations()
        {
            var facade = new SportFacade();
            return facade.GetFederations();
        }

        /// <summary>
        ///     Get organisation id by Sport id
        /// </summary>
        /// <param name="sportId">Integer value</param>
        /// <returns>Return id of organization based on sport id </returns>
        public int GetOrgIdBySportId(int sportId)
        {
            var repository = new OrganizationRepository();
            return repository.GetOrganizationBySportId(sportId).OrganizationId;
        }

        /// <summary>
        ///     Get the federation disciplines
        /// </summary>
        /// <param name="orgId"></param>
        /// <returns></returns>
        public List<FederationDiscipline> GetFederationDisciplines(int orgId)
        {
            var facade = new SportFacade();

            var disciplines = new List<FederationDiscipline>(facade.GetFederationDisciplines(orgId));

            return disciplines;
        }
    }
}