﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccountBindingModels.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   AccountBindingModels.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Models
{
    // Models used as parameters to AccountController actions.
}