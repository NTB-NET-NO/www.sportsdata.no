﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsData.Services.NIF.Interfaces;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Services.NIF.Repositories
{
    public class PartialResultTypeRepository : IPartialResultTypeDataMapper
    {
        /// <summary>
        ///     The result team service client.
        /// </summary>

        /// <summary>
        ///     The tournament service client.
        /// </summary>
        private readonly ResultTeamServiceClient _serviceClient = new ResultTeamServiceClient();

        /// <summary>
        /// Initializes a new instance of the <see cref="RegionRepository"/> class.
        /// </summary>
        public PartialResultTypeRepository()
        {
            if (this._serviceClient.ClientCredentials != null)
            {
                this._serviceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NIFServicesUsername"];
                this._serviceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NIFServicesPassword"];
            }
        }

        public List<PartialResultType> GetPartialResultTypesForOrg(int id)
        {
            var request = new OrgIdRequest16(id);
            var response = this._serviceClient.GetPartialResultTypes(request);
            
            if (!response.Success) {
                return new List<PartialResultType>();
            }
            
            return response.PartialResultType.ToList();
        }
    }
}
