﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BinaryTextMediaTypeFormatter.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The binary text media type formatter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Formatters
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;

    using log4net;

    /// <summary>
    /// The binary text media type formatter.
    /// </summary>
    public class BinaryTextMediaTypeFormatter : MediaTypeFormatter
    {
        /// <summary>
        ///     The _log.
        /// </summary>
        private readonly ILog log = LogManager.GetLogger(typeof(BinaryTextMediaTypeFormatter));

        /// <summary>
        /// Initializes a new instance of the <see cref="BinaryTextMediaTypeFormatter"/> class.
        /// </summary>
        public BinaryTextMediaTypeFormatter()
        {
            this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/octet-stream"));
        }

        /// <summary>
        /// The read from stream async.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="readStream">
        /// The read stream.
        /// </param>
        /// <param name="content">
        /// The content.
        /// </param>
        /// <param name="formatterLogger">
        /// The formatter logger.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            var taskCompletionSource = new TaskCompletionSource<object>();
            try
            {
                var streamReader = new StreamReader(readStream);

                var rawDataString = streamReader.ReadToEnd();

                rawDataString = rawDataString.Replace("\0", string.Empty);
                var bytes = Encoding.UTF8.GetBytes(rawDataString);
                var stringOutput = Encoding.UTF8.GetString(bytes);
                
                taskCompletionSource.SetResult(stringOutput.Trim());
            }
            catch (Exception e)
            {
                this.log.Error(e);
                taskCompletionSource.SetException(e);
            }

            return taskCompletionSource.Task;
        }

        /// <summary>
        /// The can read type.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool CanReadType(Type type)
        {
            return type == typeof(string);
        }

        /// <summary>
        /// The can write type.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool CanWriteType(Type type)
        {
            return false;
        }

        /// <summary>
        /// The get string.
        /// </summary>
        /// <param name="bytes">
        /// The bytes.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        /// <summary>
        /// The get bytes.
        /// </summary>
        /// <param name="str">
        /// The str.
        /// </param>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        private static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

    }
}