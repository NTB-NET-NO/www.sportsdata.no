﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The team repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NIF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using NTB.SportsData.Services.NIF.Interfaces;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The team repository.
    /// </summary>
    public class TeamRepository : IRepository<MatchTeam>, IDisposable, ITeamDataMapper
    {
        /// <summary>
        ///     The tournament service client.
        /// </summary>
        private readonly TournamentServiceClient tournamentServiceClient = new TournamentServiceClient();

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamRepository"/> class.
        /// </summary>
        public TeamRepository()
        {
            if (this.tournamentServiceClient.ClientCredentials != null)
            {
                this.tournamentServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
                this.tournamentServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];
            }
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        ///     Throws a NotImplementedException because it is not implemented
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        ///     Throws a NotImplementedException because it is not implemented
        /// </exception>
        public int InsertOne(MatchTeam domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        ///     Throws a NotImplementedException because it is not implemented
        /// </exception>
        public void InsertAll(List<MatchTeam> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        ///     Throws a NotImplementedException because it is not implemented
        /// </exception>
        public void Update(MatchTeam domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        ///     Throws a NotImplementedException because it is not implemented
        /// </exception>
        public void Delete(MatchTeam domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        ///     Throws a NotImplementedException because it is not implemented
        /// </exception>
        public IQueryable<MatchTeam> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="MatchTeam"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        ///     Throws a NotImplementedException because it is not implemented
        /// </exception>
        public MatchTeam Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get team persons.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        ///     Throws a NotImplementedException because it is not implemented
        /// </exception>
        public List<Person> GetTeamPersons(int teamId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get tournament teams.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TournamentMatchTeam> GetTournamentTeams(int tournamentId)
        {
            var request = new TournamentIdRequest2 { TournamentId = tournamentId };

            var response = this.tournamentServiceClient.GetTournamentTeams(request);

            if (response.Success)
            {
                return response.TournamentMatchTeam.ToList();
            }

            return null;
        }
    }
}