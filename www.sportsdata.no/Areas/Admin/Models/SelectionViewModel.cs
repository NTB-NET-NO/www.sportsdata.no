﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SelectionViewModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   This is a model that shall return the needed data to show
//   Districts, Municipalities and Tournament
//   It shall be used for both Soccer (sport=16) and other sports (sport!=16)
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using log4net;
    using log4net.Config;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.NFF.TournamentAccess;
    using NTB.SportsData.Facade.NIF.SportAccess;
    using NTB.SportsData.Web.Areas.Admin.Repositories;

    /// <summary>
    ///     This is a model that shall return the needed data to show 
    ///     Districts, Municipalities and Tournament
    ///     It shall be used for both Soccer (sport=16) and other sports (sport!=16)
    /// </summary>
    public class SelectionViewModel
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SelectionViewModel));

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectionViewModel"/> class. 
        ///     The constructor
        /// </summary>
        public SelectionViewModel()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Get the selection view
        /// </summary>
        /// <param name="sportId">
        /// The sport id 
        /// </param>
        /// <param name="seasonId">
        /// the Season id
        /// </param>
        /// <param name="jobId">
        /// The Job Id
        /// </param>
        /// <returns>
        /// Returns the selection view
        /// </returns>
        public SelectionView GetSelectionView(int sportId, int seasonId, int jobId)
        {
            var defineViewModel = new DefineViewModel();
            var defineView = defineViewModel.Get(jobId);

            var municipalities = this.GetMunicipalities(sportId);
            var storedMunicipalities = this.GetStoredMunicipalities(jobId);
            var list = new List<Municipality>();

            foreach (var municipality in municipalities)
            {
                foreach (var storedMunicipality in storedMunicipalities.Where(storedMunicipality => storedMunicipality.MunicipalityId == municipality.MunicipalityId))
                {
                    municipality.Selected = true;
                }

                list.Add(municipality);
            }

            // returning the items
            return new SelectionView { Municipalities = list, Tournaments = this.GetTournaments(seasonId, jobId), Districts = this.GetDistricts(sportId), DefineView = defineView };
        }

        /// <summary>
        /// The get selection view.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <param name="selectedMunicipalities">
        /// The selected municipalities.
        /// </param>
        /// <returns>
        /// The <see cref="SelectionView"/>.
        /// </returns>
        public SelectionView GetSelectionView(int sportId, int seasonId, int jobId, string selectedMunicipalities)
        {
            return new SelectionView { Municipalities = this.GetSelectedMunicipalities(selectedMunicipalities), Tournaments = this.GetTournaments(seasonId, jobId) };
        }

        // }
        // return PartialView("GetNFFMunicipalities", new List<DetailsJobView>(repository.GetAll()));
        // };
        // SportId = sportId
        // JobId = jobId,
        // {
        // MunicipalityRepository repository = new MunicipalityRepository
        // {

        // if (sportId == 16)

        // GeographicModels nifGeographicModels = new GeographicModels
        // {
        // JobId = jobId,
        // AgeCategoryId = ageCategoryId,
        // CustomerId = customerId,
        // SeasonId = seasonId,
        // UseRemoteData = useRemoteData,
        // FederationId = federationdId,
        // SportId = sportId
        // };

        // DetailsJobView detailsJobView = nifGeographicModels.GetDetailsJobView();

        /// <summary>
        /// The get municipalities.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<Municipality> GetMunicipalities(int sportId)
        {
            var repository = new MunicipalityRepository();
            var municipalities = repository.GetMunicipalitiesBySportId(sportId);

            if (!municipalities.Any())
            {
                if (sportId == 16)
                {
                    var facade = new TournamentFacade();
                    municipalities = facade.GetMunicipalities();
                }
                else
                {
                    var facade = new SportFacade();
                    municipalities = facade.GetMunicipalities();
                }
            }

            return municipalities;
        }

        /// <summary>
        /// This method returns a list of stored municipalities
        /// </summary>
        /// <param name="jobId">
        /// The job id
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<Municipality> GetStoredMunicipalities(int jobId)
        {
            var municipalityRepository = new MunicipalityRepository();
            return municipalityRepository.GetMunicipalitiesByJobId(jobId);
        }

        /// <summary>
        /// The get selected municipalities.
        /// </summary>
        /// <param name="selectedMunicipalities">
        /// The selected municipalities.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<Municipality> GetSelectedMunicipalities(string selectedMunicipalities)
        {
            // Splitting the values into a string array of Ids
            var ids = selectedMunicipalities.Split('|');

            // Returning the list of municipalities only using the Ids.
            return ids.Select(id => new Municipality { MunicipalityId = Convert.ToInt32(id) }).ToList();
        }

        /// <summary>
        /// The get tournaments.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<Tournament> GetTournaments(int seasonId, int jobId)
        {
            var tournamentRepository = new TournamentRepository();
            tournamentRepository.SeasonId = seasonId;
            return tournamentRepository.GetTournamentsByJobId(jobId);
        }

        /// <summary>
        /// Gets the districts for sports
        /// </summary>
        /// <param name="sportId">
        /// The sport id
        /// </param>
        /// <returns>
        /// list if districts
        /// </returns>
        private List<District> GetDistricts(int sportId)
        {
            var repository = new DistrictRepository();
            var districts = new List<District>(repository.GetDistrictsBySportId(sportId));

            if (!districts.Any())
            {
                if (sportId == 16)
                {
                    var facade = new TournamentFacade();
                    districts = facade.GetDistricts();
                }
                else
                {
                    var facade = new SportFacade();
                    districts = facade.GetDistricts();
                }
            }

            return districts;
        }
    }
}