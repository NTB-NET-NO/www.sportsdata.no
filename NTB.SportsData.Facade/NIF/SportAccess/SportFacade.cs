﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NTB.SportsData.Common;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Mappers.NIF;
using NTB.SportsData.Facade.NIF.SportInterface;
using NTB.SportsData.Services.NIF.Interfaces;
using NTB.SportsData.Services.NIF.NIFProdService;
using NTB.SportsData.Services.NIF.Repositories;
using Federation = NTB.SportsData.Domain.Classes.Federation;
using FederationDiscipline = NTB.SportsData.Domain.Classes.FederationDiscipline;
using Function = NTB.SportsData.Domain.Classes.Function;
using Match = NTB.SportsData.Domain.Classes.Match;
using Season = NTB.SportsData.Domain.Classes.Season;
using Tournament = NTB.SportsData.Domain.Classes.Tournament;

namespace NTB.SportsData.Facade.NIF.SportAccess
{
    /// <summary>
    /// The sport facade.
    /// </summary>
    public class SportFacade : ISportFacade
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(SportFacade));

        /// <summary>
        /// The _class code data mapper.
        /// </summary>
        private readonly IClassCodeDataMapper classCodeDataMapper;

        /// <summary>
        /// The _county data mapper.
        /// </summary>
        private readonly ICountyDataMapper countyDataMapper;

        /// <summary>
        /// The _event data mapper.
        /// </summary>
        private readonly IEventDataMapper eventDataMapper;

        /// <summary>
        /// The _federation data mapper.
        /// </summary>
        private readonly IFederationDataMapper federationDataMapper;

        /// <summary>
        /// The _federation discipline data mapper.
        /// </summary>
        private readonly IFederationDisciplineDataMapper federationDisciplineDataMapper;

        /// <summary>
        /// The _function data mapper.
        /// </summary>
        private readonly IFunctionDataMapper functionDataMapper;

        /// <summary>
        /// The _match data mapper.
        /// </summary>
        private readonly IMatchDataMapper matchDataMapper;

        /// <summary>
        /// The _person data mapper.
        /// </summary>
        private readonly IPersonDataMapper personDataMapper;

        /// <summary>
        /// The _region data mapper.
        /// </summary>
        private readonly IRegionDataMapper regionDataMapper;

        /// <summary>
        /// The _season data mapper.
        /// </summary>
        private readonly ISeasonDataMapper seasonDataMapper;

        /// <summary>
        /// The _tournament data mapper.
        /// </summary>
        private readonly ITournamentDataMapper tournamentDataMapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="SportFacade"/> class.
        /// </summary>
        public SportFacade()
        {
            this.federationDataMapper = new FederationRepository();
            this.federationDisciplineDataMapper = new FederationDisciplineRepository();
            this.countyDataMapper = new CountyRepository();
            this.regionDataMapper = new RegionRepository();
            this.classCodeDataMapper = new ClassCodeRepository();
            this.seasonDataMapper = new SeasonRepository();
            this.tournamentDataMapper = new TournamentRepository();
            this.matchDataMapper = new MatchRepository();
            this.functionDataMapper = new FunctionRepository();
            this.personDataMapper = new PersonRepository();
            this.eventDataMapper = new EventDataMapper();
        }

        /// <summary>
        /// The get sports.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public List<FederationDiscipline> GetSports()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get sport.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="FederationDiscipline"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public FederationDiscipline GetSport(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get federations.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Federation> GetFederations()
        {
            try
            {
                var result = this.federationDataMapper.GetFederations();

                var mapper = new FederationMapper();

                return result.Select(row => mapper.Map(row, new Federation()))
                    .ToList();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return new List<Federation>();
            }
        }

        /// <summary>
        /// The get federation.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Federation"/>.
        /// </returns>
        public Federation GetFederation(int id)
        {
            var result = this.federationDataMapper.GetFederation(id);

            var mapper = new FederationMapper();

            return mapper.Map(result, new Federation());
        }

        /// <summary>
        /// The get federation by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="Federation"/>.
        /// </returns>
        public Federation GetFederationByOrgId(int orgId)
        {
            var result = this.federationDataMapper.GetFederationByOrgId(orgId);

            var mapper = new FederationMapper();

            return mapper.Map(result, new Federation());
        }

        /// <summary>
        /// The get federation disciplines.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<FederationDiscipline> GetFederationDisciplines(int orgId)
        {
            var result = this.federationDisciplineDataMapper.GetFederationDisciplines(orgId);

            var mapper = new FederationDisciplinesMapper();

            return result.Select(row => mapper.Map(row, new FederationDiscipline()))
                .ToList();
        }

        /// <summary>
        /// The get federation discipline.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="FederationDiscipline"/>.
        /// </returns>
        public FederationDiscipline GetFederationDiscipline(int id)
        {
            var result = this.federationDisciplineDataMapper.GetFederationDiscipline(id);

            var mapper = new FederationDisciplinesMapper();

            return mapper.Map(result, new FederationDiscipline());
        }

        /// <summary>
        /// The get districts.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<District> GetDistricts()
        {
            var mapper = new DistrictMapper();

            var result = this.countyDataMapper.GetCounties();

            return result.Select(row => mapper.Map(row, new District()))
                .ToList();
        }

        /// <summary>
        /// The get municipalities.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Municipality> GetMunicipalities()
        {
            var mapper = new MunicipalityMapper();

            var result = this.regionDataMapper.GetRegions();

            return result.Select(row => mapper.Map(row, new Municipality()))
                .ToList();
        }

        /// <summary>
        /// The get age categories.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<AgeCategory> GetAgeCategories(int orgId)
        {
            // var mapper = new AgeCategoryMapper();

            // List<ClassCode> result = this.classCodeDataMapper.GetClassCodes(orgId);
            var mapper = new TournamentClassMapper();

            var result = this.classCodeDataMapper.GetClassCodesByOrgId(orgId);

            var filteredResult = result.Where(x => x.IsValid)
                .ToList();

            return filteredResult.Select(row => mapper.Map(row, new AgeCategory()))
                .ToList();
        }

        /// <summary>
        /// The get class codes by tournament id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<AgeCategory> GetClassCodesByTournamentId(int tournamentId)
        {
            var mapper = new TournamentClassesMapper();

            var result = this.classCodeDataMapper.GetClassCodesByTournamentId(tournamentId);

            return result.Select(row => mapper.Map(row, new AgeCategory())).ToList();
        }

        // List<string> municipalities, int seasonId, int orgId

        /// <summary>
        /// The get tournament by municipalities.
        /// </summary>
        /// <param name="municipalities">
        /// The municipalities.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId, int orgId)
        {
            try
            {
                var mapper = new TournamentSummaryMapper();

                var result = this.tournamentDataMapper.GetTournamentByMunicipalities(municipalities, seasonId, orgId);

                if (result == null)
                {
                    return new List<Tournament>();
                }

                return result.Select(row => mapper.Map(row, new Tournament()))
                    .ToList();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return null;
            }

            
        }

        /// <summary>
        /// The get class codes.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<AgeCategory> GetClassCodes(int orgId)
        {
            this.GetAgeCategories(orgId);

            return new List<AgeCategory>();
        }

        /// <summary>
        /// The get federation seasons.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Season> GetFederationSeasons(int orgId)
        {
            var mapper = new SeasonMapper();

            var result = this.seasonDataMapper.GetFederationSeasonsByOrgId(orgId);

            return result.Select(row => mapper.Map(row, new Season()))
                .ToList();
        }

        /// <summary>
        /// The get matches by tournament id and sport id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetMatchesByTournamentIdAndSportId(int tournamentId, int sportId)
        {
            var mapper = new MatchMapper();

            var result = this.matchDataMapper.GetMatchesByTournamentIdAndSportId(tournamentId, sportId);

            return result.Select(row => mapper.Map(row, new Match()))
                .ToList();
        }

        /// <summary>
        /// The get todays matches.
        /// </summary>
        /// <param name="matchDate">
        /// The match date.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetTodaysMatches(DateTime matchDate, int seasonId)
        {
            var mapper = new MatchMapper();

            var result = this.matchDataMapper.GetTodaysMatches(matchDate, seasonId);

            return result.Select(row => mapper.Map(row, new Match()))
                .ToList();
        }

        /// <summary>
        /// The get todays events.
        /// </summary>
        /// <param name="eventDate">
        /// The event date.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<SportEvent> GetTodaysEvents(DateTime eventDate, int orgId)
        {
            var mapper = new EventMapper();

            var result = this.eventDataMapper.GetEvents(eventDate, orgId);

            return result.Select(row => mapper.Map(row, new SportEvent()))
                .ToList();
        }

        /// <summary>
        /// The get tournaments by season id.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentSummariesBySeasonAndOrgId(int seasonId, int orgId)
        {
            try
            {
                var mapper = new TournamentSummaryMapper();
                var result = this.tournamentDataMapper.GetTournamentSummariesBySeasonAndOrgId(seasonId, orgId);

                var filteredResult = result.Where(x => x.HasMatches)
                    .ToList();
                foreach (var res in filteredResult)
                {
                    try
                    {
                        var classCode = this.classCodeDataMapper.GetClassCodesByTournamentId(res.TournamentId);
                        if (classCode.Any())
                        {
                            Logger.DebugFormat("Adding Age Category for Tournament {0} ({1})", res.TournamentName, res.TournamentId);

                            res.TournamentClassIds = classCode[0].ClassId.ToString();
                        }
                        else
                        {
                            Logger.DebugFormat("No Age Category found for Tournament {0} ({1})", res.TournamentName, res.TournamentId);
                        }
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception);
                    }
                }

                var mappedResult = filteredResult.Select(row => mapper.Map(row, new Tournament()))
                    .ToList();

                return mappedResult;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return null;
            }            
        }

        /// <summary>
        /// The get tournaments by season id.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            var mapper = new TournamentMapper();
            var result = this.tournamentDataMapper.GetTournamentsBySeasonId(seasonId);

            foreach (var res in result)
            {
                var classCode = this.classCodeDataMapper.GetClassCodesByTournamentId(res.TournamentId);
                if (classCode.Any())
                {
                    res.ClassCodeId = classCode[0].ClassId;
                }
                else
                {
                    Logger.DebugFormat("No Age Category found for Tournament {0} ({1})", res.TournamentName, res.TournamentId);
                }
            }

            return result.Select(row => mapper.Map(row, new Tournament()))
                .ToList();
        }

        /// <summary>
        /// The get tournament by id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        public Tournament GetTournamentById(int tournamentId, int seasonId)
        {
            var mapper = new TournamentMapper();
            var result = this.tournamentDataMapper.GetTournamentById(tournamentId, seasonId);

            // Checking if result is null or not
            if (result == null)
            {
                return new Tournament();
            }

            return mapper.Map(result, new Tournament());
        }

        /// <summary>
        /// The insert match result.
        /// </summary>
        /// <param name="localMatch">
        /// The local match.
        /// </param>
        /// <param name="federationUserId">
        /// The id of the federation user so to be used when storing the result
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool InsertMatchResult(Match localMatch, int federationUserId)
        {
            if (federationUserId == 0)
            {
                throw new SportsDataInputException("The Federation User Id cannot be 0");
            }

            var mapper = new MatchMapper();

            var insertObject = mapper.Map(localMatch, new TournamentMatchExtended());

            this.matchDataMapper.InsertMatch(insertObject, federationUserId);

            return true;
        }

        /// <summary>
        /// The get referees by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<ContactPerson> GetRefereesByMatchId(int matchId)
        {
            try
            {
                var mapper = new RefereeMapper();

                var result = this.matchDataMapper.GetRefereeByMatchId(matchId);

                // Adding more information
                var contactPersons = result.Select(row => mapper.Map(row, new ContactPerson()))
                    .ToList();

                foreach (var contactPerson in contactPersons)
                {
                    var simplePerson = this.personDataMapper.GetSimplePersonById(contactPerson.PersonId);
                    contactPerson.FirstName = simplePerson.FirstName;
                    contactPerson.LastName = simplePerson.LastName;
                    contactPerson.HomePhone = simplePerson.ContactInfo.PhoneHome;
                    contactPerson.MobilePhone = simplePerson.ContactInfo.PhoneMobile;
                }

                return contactPersons;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return new List<ContactPerson>();
            }
        }

        /// <summary>
        /// The get contact persons by team id.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<ContactPerson> GetContactPersonsByTeamId(int teamId)
        {
            try
            {
                var mapper = new SquadIndividualMapper();

                var result = this.personDataMapper.GetPersonsByTeamId(teamId);

                // Adding more information
                var contactPersons = result.Select(row => mapper.Map(row, new ContactPerson()))
                    .ToList();

                foreach (var contactPerson in contactPersons)
                {
                    var simplePerson = this.personDataMapper.GetSimplePersonById(contactPerson.PersonId);

                    contactPerson.HomePhone = simplePerson.ContactInfo.PhoneHome;
                    contactPerson.MobilePhone = simplePerson.ContactInfo.PhoneMobile;
                    contactPerson.MobilePhone = simplePerson.ContactInfo.PhoneMobile;
                }

                return contactPersons;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return new List<ContactPerson>();
            }
        }

        /// <summary>
        /// Get the contact persons for the match by the matchId
        /// </summary>
        /// <param name="matchId">
        /// id of the match
        /// </param>
        /// <returns>
        /// List of contact persons
        /// </returns>
        public List<ContactPerson> GetContactPersonsByMatchId(int matchId)
        {
            try
            {
                var mapper = new SquadIndividualMapper();

                var result = this.personDataMapper.GetPersonsByMatchId(matchId);

                // Adding more information
                var contactPersons = result.Select(row => mapper.Map(row, new ContactPerson()))
                    .ToList();

                foreach (var contactPerson in contactPersons)
                {
                    var simplePerson = this.personDataMapper.GetSimplePersonById(contactPerson.PersonId);

                    contactPerson.HomePhone = simplePerson.ContactInfo.PhoneHome;
                    contactPerson.MobilePhone = simplePerson.ContactInfo.PhoneMobile;
                    contactPerson.MobilePhone = simplePerson.ContactInfo.PhoneMobile;
                }

                return contactPersons;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return new List<ContactPerson>();
            }
        }

        /// <summary>
        /// The update match result.
        /// </summary>
        /// <param name="localMatch">
        /// The local match.
        /// </param>
        /// <param name="federationUserId">
        /// The id of the federation user so to be used when storing the result
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool UpdateMatchResult(Match localMatch, int federationUserId)
        {
            if (federationUserId == 0)
            {
                throw new SportsDataInputException("The Federation User Id cannot be 0");
            }

            var mapper = new MatchMapper();

            var updateObject = mapper.Map(localMatch, new TournamentMatchExtended());

            this.matchDataMapper.UpdateMatch(updateObject, federationUserId);

            return true;
        }

        /// <summary>
        /// The get class codes by class ids.
        /// </summary>
        /// <param name="classIds">
        /// The class ids.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<AgeCategory> GetClassCodesByClassIds(int[] classIds)
        {
            var mapper = new TournamentClassMapper();

            var result = this.classCodeDataMapper.GetClassCodesByClassIds(classIds);

            return result.Select(row => mapper.Map(row, new AgeCategory()))
                .ToList();
        }

        /// <summary>
        /// The get team functions.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Function> GetTeamFunctions(int teamId, int sportId)
        {
            var mapper = new FunctionMapper();

            var result = this.functionDataMapper.GetTeamFunctions(teamId);

            return result.Select(row => mapper.Map(row, new Function()))
                .ToList();
        }

        /// <summary>
        /// The get team contact.
        /// </summary>
        /// <param name="personId">
        /// The person id.
        /// </param>
        /// <returns>
        /// The <see cref="ContactPerson"/>.
        /// </returns>
        public ContactPerson GetTeamContact(int personId)
        {
            var mapper = new PersonMapper();

            var result = this.personDataMapper.GetPersonById(personId);

            return mapper.Map(result, new ContactPerson());
        }
    }
}