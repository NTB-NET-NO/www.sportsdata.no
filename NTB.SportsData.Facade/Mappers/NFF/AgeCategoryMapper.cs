// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AgeCategoryMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The age category mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NFF
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The age category mapper.
    /// </summary>
    public class AgeCategoryMapper : BaseMapper<AgeCategoryTournament, AgeCategory>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<AgeCategoryTournament, AgeCategory> mapper)
        {
            mapper.Relate(x => x.AgeCategoryId, y => y.CategoryId);
            mapper.Relate(x => x.AgeCategoryName, y => y.CategoryName);
            mapper.Relate(x => x.MinAge, y => y.MinAge);
            mapper.Relate(x => x.MaxAge, y => y.MaxAge);
        }
    }
}