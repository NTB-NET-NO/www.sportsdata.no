﻿using System.Collections.Generic;

namespace NTB.SportsData.Domain.Classes
{
    public class AdvancedCheckClubViewModel
    {
        public List<Municipality> Municipalities { get; set; }

        public List<District> Districts { get; set; }

        public List<Country>  Countries { get; set; }

        public List<Club> Clubs { get; set; }
    }
}
