﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ManageInfoViewModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The Manage Info View Model
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Models
{
    using System.Collections.Generic;

    /// <summary>
    ///     The Manage Info View Model
    /// </summary>
    public class ManageInfoViewModel
    {
        /// <summary>
        /// Gets or sets the local login provider.
        /// </summary>
        public string LocalLoginProvider { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the logins.
        /// </summary>
        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        /// <summary>
        /// Gets or sets the external login providers.
        /// </summary>
        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }
}