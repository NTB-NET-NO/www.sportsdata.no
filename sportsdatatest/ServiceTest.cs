﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceTest.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The service test.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.UnitTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Services.NIF.NIFProdService;
    using Services.NIF.Repositories;

    /// <summary>
    /// The service test.
    /// </summary>
    [TestClass]
    public class ServiceTest
    {
        /// <summary>
        /// The test get Class Codes By TournamentId
        /// </summary>
        [TestMethod]
        public void GetClassCodesByTournamentId()
        {
            var service = new ClassCodeRepository();

            const int TournamentId = 330060;

            var actual = service.GetClassCodesByTournamentId(TournamentId);

            var expected = 0;

            // this test just checks if the service is working or not.
            Assert.AreNotEqual(expected, actual);
        }

        /// <summary>
        /// The test get Referee By Match Id
        /// </summary>
        [TestMethod]
        public void GetRefereeByMatchId()
        {
            var service = new MatchRepository();

            const int MatchId = 5042509;

            var actual = service.GetRefereeByMatchId(MatchId).Count;

            const int Expected = 4;

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The test get tournament standing.
        /// </summary>
        [TestMethod]
        public void TestGetTournamentStanding()
        {
            var service = new TournamentRepository();

            service.GetTournamentStanding(328910);
        }

        /// <summary>
        /// The test not found tournament by id.
        /// </summary>
        [TestMethod]
        public void TestNotFoundTournamentById()
        {
            var service = new TournamentRepository();
            var tournamentId = 326924;
            var seasonId = 200739;

            var actual = service.GetTournamentById(tournamentId, seasonId);

            Assert.IsNull(actual);
        }

        /// <summary>
        /// Testing Get Tournament By Municipalities
        /// </summary>
        [TestMethod]
        public void GetTournamentByMunicipalities()
        {
            var service = new TournamentRepository();

            var municipality = "500030";
            var municipalities = new List<string>();
            municipalities.Add(municipality);

            var seasonId = 200739;

            var orgId = 372;

            var actual = service.GetTournamentByMunicipalities(municipalities, seasonId, orgId).Count;

            var expected = 125;

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///     Testing Get Tournaments By OrgId and SeasonId
        /// </summary>
        [TestMethod]
        public void GetTournamentsByOrgIdAndSeasonId()
        {
            var orgId = 752320;

            var seasonId = 200739;

            var service = new TournamentRepository();

            var actual = service.GetTournamentsByOrgIdAndSeasonId(orgId, seasonId);

            var expected = new List<Tournament>();

            // CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AllItemsAreUnique(actual);
        }

        /// <summary>
        ///     Testing Get Tournaments By OrgId and SeasonId
        /// </summary>
        [TestMethod]
        public void GetTournamentsByOrgIdAndSeasonId3()
        {
            var orgId = 752320;

            var seasonId = 200739;

            var service = new TournamentRepository();

            var actual = service.GetTournamentsByOrgIdAndSeasonId(orgId, seasonId);

            var expected = new List<Tournament>();

            // CollectionAssert.AreEqual(expected, actual);
            Assert.IsInstanceOfType(actual, typeof(List<Tournament>));
        }

        /// <summary>
        ///     Testing Get Tournaments By SeasonId
        /// </summary>
        [TestMethod]
        public void GetTournamentsBySeasonId()
        {
            var seasonId = 200739;

            var service = new TournamentRepository();

            var actual = service.GetTournamentsBySeasonId(seasonId).Count;

            var expected = 2067;

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// The test get tournament by id.
        /// </summary>
        [TestMethod]
        public void TestGetTournamentById()
        {
            var service = new TournamentRepository();
            var tournamentId = 330060;
            var seasonId = 200739;

            var actual = service.GetTournamentById(tournamentId, seasonId).TournamentId;

            var expected = 330060;

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// The test get tournament standing exception.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FaultException))]
        public void TestGetTournamentStandingException()
        {
            try
            {
                var service = new TournamentRepository();

                service.GetTournamentStanding(326924);
            }
            catch (FaultException exception)
            {
                Assert.IsTrue(exception.Message.Contains("instance of an object"));

                throw;
            }
        }

        /// <summary>
        ///     Testing Get Federation Season By OrgId
        /// </summary>
        [TestMethod]
        public void GetFederationSeasonByOrgId()
        {
            var service = new SeasonRepository();

            var orgId = 372;

            var actual = service.GetFederationSeasonsByOrgId(orgId).SingleOrDefault(x => x.SeasonId == 200739).SeasonId;

            var expected = 200739;

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// The get tournament teams.
        /// </summary>
        [TestMethod]
        public void GetTournamentTeams()
        {
            var service = new TeamRepository();

            const int TournamentId = 330060;

            var actual = service.GetTournamentTeams(TournamentId).Count;

            const int Expected = 12;

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The get persons by match id.
        /// </summary>
        [TestMethod]
        public void GetPersonsByMatchId()
        {
            var service = new PersonRepository();

            const int MatchId = 5042509;

            var actual = service.GetPersonsByMatchId(MatchId).Count;

            const int Expected = 0;

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The get persons by team id.
        /// </summary>
        [TestMethod]
        public void GetPersonsByTeamId()
        {
            var service = new PersonRepository();

            const int OrgId = 223979;

            var actual = service.GetPersonsByTeamId(OrgId).Count;

            const int Expected = 9;

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The GetSimplePersonById
        /// </summary>
        [TestMethod]
        public void GetSimplePersonById()
        {
            var service = new PersonRepository();

            const int Id = 1487448;

            var response = service.GetSimplePersonById(Id);

            var actual = response.FirstName + " " + response.LastName;
            
            const string Expected = "Joachim Tennes";

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The GetSimplePersonById
        /// </summary>
        [TestMethod]
        public void GetPersonById()
        {
            var service = new PersonRepository();

            const int Id = 6802892;

            var response = service.Get(Id);

            var actual = response.FullName;

            const string Expected = "Erlend Aam";

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The GetSimplePersonById
        /// </summary>
        [TestMethod]
        public void GetTeamFunctions()
        {
            var service = new FunctionRepository();

            const int Id = 223979;

            var actual = service.GetTeamFunctions(Id).Count;

            const int Expected = 0;

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The GetSimplePersonById
        /// </summary>
        [TestMethod]
        public void GetFederationByOrgId()
        {
            var service = new FederationRepository();

            const int Id = 372;
            service.GetFederationByOrgId(Id);
        }

        /// <summary>
        /// The GetFederationByOrgIdException
        /// </summary>
        [ExpectedException(typeof(InvalidOperationException))]
        [TestMethod]
        public void GetFederationByOrgIdException()
        {
            var service = new FederationRepository();

            const int Id = 999;
            service.GetFederationByOrgId(Id);
        }

        /// <summary>
        /// The Get Match Result.
        /// </summary>
        [TestMethod]
        public void GetMatchResult()
        {
            var service = new MatchRepository();

            const int Id = 5042500;

            var actual = service.GetMatchResult(Id).HomeGoals;

            const int Expected = 3;

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The insert match.
        /// </summary>
        [TestMethod]
        public void InsertMatch()
        {
            var service = new MatchRepository();

            var matchObject = new TournamentMatchExtended()
                                  {
                                      AwayGoals = 0,
                                      HomeGoals = 1,
                                      MatchId = 6398393 
                                  };
            var federationUserId = 1;
            service.InsertMatch(matchObject, federationUserId);
        }
    }
}