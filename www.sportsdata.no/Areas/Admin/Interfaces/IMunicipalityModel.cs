﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Interfaces
{
    interface IMunicipalityModel
    {
        void InsertMunicipalities(List<Municipality> municipalities, int jobid);
        List<Municipality> GetMunicipalities();
        List<Municipality> GetSelectedMunicipalities(string selectedMunicipalities, int jobId);
    }
}
