﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NTB.SportsData.Data.DataMappers.Gender;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    public class GenderModel
    {
        public List<Gender> GetAllGenders()
        {
            var genderRepository = new GenderDataMapper();
            var genders = genderRepository.GetGenders();
            if (genders == null)
            {
                genders = new List<Gender>();
                var femaleGender = new Gender
                {
                    Id = 2,
                    Name = "Kvinne"
                };
                
                var maleGender = new Gender
                {
                    Id = 1,
                    Name = "Mann"
                };
                genders.Add(femaleGender);
                genders.Add(maleGender);

                genderRepository.InsertGender(maleGender);
                genderRepository.InsertGender(femaleGender);
            }

            return genders;
        }
    }
}