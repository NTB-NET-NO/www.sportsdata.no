﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NTB.SportsData.UnitTest
{
    using System.IO;
    using System.Net;
    using System.Security.Cryptography;
    using System.Text;

    [TestClass]
    public class APIUnitTests
    {
        /// <summary>
        /// The user name.
        /// </summary>
        private const string UserName = "apiAS2014";

        /// <summary>
        /// The password.
        /// </summary>
        private const string Password = "2014AS10";

        /// <summary>
        /// The base url.
        /// </summary>
        public const string BaseUrl = "http://dev.sportsdata.no";

        /// <summary>
        /// The convert to m d 5.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ConvertToMd5(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return string.Empty;
            }

            // step 1, calculate MD5 hash from input
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            var sb = new StringBuilder();
            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        /// <summary>
        /// The base 64.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string Base64(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            var myByte = Encoding.ASCII.GetBytes(value);
            return Convert.ToBase64String(myByte);
        }

        /// <summary>
        /// The create timestamp.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string CreateTimestamp()
        {
            var utcnow = DateTime.UtcNow;

            var timestamp = utcnow.ToString("yyyyMMdd") + "T" + utcnow.ToString("HHmmss") + "Z";

            return timestamp;
        }

        /// <summary>
        /// The v 2_create signature.
        /// </summary>
        /// <param name="apiUsername">
        /// The api username.
        /// </param>
        /// <param name="timestamp">
        /// The timestamp.
        /// </param>
        /// <param name="uri">
        /// The uri.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string V2CreateSignature(string apiUsername, string timestamp, string uri)
        {
            // string createStringToHash = apiUsername + timestamp + uri;
            // byte[] keyInByes = Encoding.UTF8.GetBytes(createStringToHash);
            var tosigndata = apiUsername + timestamp + uri;

            var alg = MD5.Create();
            var enc = new UTF8Encoding();

            var hash = alg.ComputeHash(enc.GetBytes(tosigndata));
            var hashedString = Convert.ToBase64String(hash);

            return hashedString;
        }

        /// <summary>
        /// The test match.
        /// </summary>
        [TestMethod]
        public void TestMatch()
        {
            var matchId = 6541556;
            var serviceurl = string.Format("{0}{1}", @"/Api/api/Match/Match/", matchId);
            var url = string.Format("{0}{1}", BaseUrl, serviceurl);

            var tokenRequest = WebRequest.Create(url);
            var timestamp = this.CreateTimestamp();

            string result;

            tokenRequest.Method = "GET";

            tokenRequest.ContentType = "application/json";
            tokenRequest.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
            

            tokenRequest.Headers.Add("timestamp", timestamp);
            tokenRequest.Headers.Add("signature", this.V2CreateSignature("apiAS2014", timestamp, serviceurl));
            tokenRequest.Credentials = new NetworkCredential(UserName, Password);

            // Get the response to our token request.
            var tokenResponse = tokenRequest.GetResponse();

            // Read the first line of the response body.
            result = new StreamReader(tokenResponse.GetResponseStream()).ReadToEnd()
                .Replace("\r\n", string.Empty);
        }

        /// <summary>
        /// The test match extended.
        /// </summary>
        [TestMethod]
        public void TestMatchExtended()
        {
            var matchId = 6390198;
            var serviceurl = string.Format("{0}{1}", @"/Api/api/Match/GetMatchExtended/", matchId);
            var url = string.Format("{0}{1}", BaseUrl, serviceurl);

            var tokenRequest = WebRequest.Create(url);
            var timestamp = this.CreateTimestamp();

            string result;

            tokenRequest.Method = "GET";

            tokenRequest.ContentType = "application/json";
            tokenRequest.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");


            tokenRequest.Headers.Add("timestamp", timestamp);
            tokenRequest.Headers.Add("signature", this.V2CreateSignature("apiAS2014", timestamp, serviceurl));
            tokenRequest.Credentials = new NetworkCredential(UserName, Password);

            // Get the response to our token request.
            var tokenResponse = tokenRequest.GetResponse();

            // Read the first line of the response body.
            result = new StreamReader(tokenResponse.GetResponseStream()).ReadToEnd()
                .Replace("\r\n", string.Empty);
        }
    }
}
