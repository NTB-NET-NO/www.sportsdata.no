// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Web.Areas.Admin.Repositories
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using log4net;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The customers.
    /// </summary>
    public class CustomerRepository : IEnumerable<Customer>
    {
        #region Static Fields

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CustomerRepository));

        #endregion

        #region Fields

        /// <summary>
        ///     The _customers.
        /// </summary>
        private readonly List<Customer> _customers = new List<Customer>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The activate customer.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ActivateCustomer(int customerId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_Customer_Activate", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    // Returning true value
                    return true;
                }
                catch (Exception exception)
                {
                    Logger.Info(exception.Message);
                    Logger.Info(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Info(exception.InnerException.Message);
                        Logger.Info(exception.InnerException.StackTrace);
                    }

                    return false;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The add one customer.
        /// </summary>
        /// <param name="customer">
        /// The customer.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool AddOneCustomer(Customer customer)
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
                {
                    var sqlCommand = new SqlCommand("SportsData_Customer_Insert", sqlConnection);
                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerName", customer.Name));

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerEmail", customer.Email));

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerAddress", customer.Address));

                    sqlCommand.Parameters.Add(new SqlParameter("@PostalCode", customer.PostalCode));

                    sqlCommand.Parameters.Add(new SqlParameter("@City", customer.City));

                    sqlCommand.Parameters.Add(new SqlParameter("@TTCustomerId", customer.TtCustomerId));

                    sqlCommand.Parameters.Add(new SqlParameter("@MediaHouseId", customer.MediaHouseId));

                    sqlCommand.Parameters.Add(new SqlParameter("@ApiUsername", customer.ApiUsername));

                    sqlCommand.Parameters.Add(new SqlParameter("@ApiPassword", customer.ApiPassword));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlConnection.Open();

                    sqlCommand.ExecuteNonQuery();

                    sqlConnection.Close();

                    return true;
                }
            }
            catch (Exception exception)
            {
                Logger.Info(exception.Message);
                Logger.Info(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    Logger.Info(exception.InnerException.Message);
                    Logger.Info(exception.InnerException.StackTrace);
                }

                return false;
            }
        }

        /// <summary>
        /// The deactivate customer.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool DeactivateCustomer(int customerId)
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
                {
                    var sqlCommand = new SqlCommand("SportsData_Customer_Deactivate", sqlConnection);
                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlConnection.Open();

                    sqlCommand.ExecuteNonQuery();

                    sqlConnection.Close();

                    return true;
                }
            }
            catch (Exception exception)
            {
                Logger.Info(exception.Message);
                Logger.Info(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    Logger.Info(exception.InnerException.Message);
                    Logger.Info(exception.InnerException.StackTrace);
                }

                return false;
            }
        }

        /// <summary>
        /// The edit one customer.
        /// </summary>
        /// <param name="customer">
        /// The customer.
        /// </param>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool EditOneCustomer(Customer customer, int customerId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_Customer_Update", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerName", customer.Name));

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerEmail", customer.Email));

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerAddress", customer.Address));

                    sqlCommand.Parameters.Add(new SqlParameter("@PostalCode", customer.PostalCode));

                    sqlCommand.Parameters.Add(new SqlParameter("@City", customer.City));

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TTCustomerId", customer.TtCustomerId));

                    sqlCommand.Parameters.Add(new SqlParameter("@MediaHouseId", customer.MediaHouseId));

                    sqlCommand.Parameters.Add(new SqlParameter("@ApiUsername", customer.ApiUsername));

                    sqlCommand.Parameters.Add(new SqlParameter("@ApiPassword", customer.ApiPassword));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlConnection.Open();

                    sqlCommand.ExecuteNonQuery();

                    sqlConnection.Close();

                    return true;
                }
                catch (Exception exception)
                {
                    Logger.Info(exception.Message);
                    Logger.Info(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Info(exception.InnerException.Message);
                        Logger.Info(exception.InnerException.StackTrace);
                    }

                    return false;
                }
            }
        }

        /// <summary>
        ///     The get all customers.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Customer> GetAllCustomers()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    // @Done: Move this code to the database
                    var sqlCommand = new SqlCommand("SportsData_Customer_Get", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    // Sending a null parameter just so that we are sure it has a null value
                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", DBNull.Value));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new List<Customer>();
                    }

                    while (sqlDataReader.Read())
                    {
                        var customer = new Customer
                                           {
                                               CustomerId = Convert.ToInt32(sqlDataReader["CustomerId"]), 
                                               Name = sqlDataReader["CustomerName"].ToString(), 
                                               Address = sqlDataReader["CustomerAddress"].ToString(), 
                                               Email = sqlDataReader["CustomerEmail"].ToString(), 
                                               City = sqlDataReader["City"].ToString(), 
                                               PostalCode = sqlDataReader["PostalCode"].ToString(), 
                                               Active = Convert.ToInt32(sqlDataReader["Activated"]), 
                                           };

                        if (sqlDataReader["TTCustomerId"] != DBNull.Value)
                        {
                            customer.TtCustomerId = Convert.ToInt32(sqlDataReader["TTCustomerId"]);
                        }

                        this._customers.Add(customer);
                    }

                    sqlDataReader.Close();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return this._customers;
        }

        /// <summary>
        /// The get customers by tournament id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Customer> GetCustomersByTournamentId(int tournamentId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    Logger.Debug("TournamentId: " + tournamentId);

                    // Now we are to get a list of customers for this Tournament
                    var sqlCustomerCommand = new SqlCommand("SportsData_GetCustomerByTournamentId", sqlConnection);
                    sqlCustomerCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    sqlCustomerCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlCustomerCommand.Connection.State == ConnectionState.Closed)
                    {
                        sqlCustomerCommand.Connection.Open();
                    }

                    SqlDataReader dataReader = sqlCustomerCommand.ExecuteReader(CommandBehavior.CloseConnection);

                    var customers = new List<Customer>();
                    if (!dataReader.HasRows)
                    {
                        return new List<Customer>();
                    }

                    while (dataReader.Read())
                    {
                        var customer = new Customer();
                        if (dataReader["CustomerId"] != DBNull.Value)
                        {
                            customer.CustomerId = Convert.ToInt32(dataReader["CustomerId"].ToString());
                        }

                        if (dataReader["TTCustomerId"] != DBNull.Value)
                        {
                            customer.TtCustomerId = Convert.ToInt32(dataReader["TTCustomerId"].ToString());
                        }

                        // Adding to customers-list
                        customers.Add(customer);
                    }

                    return customers;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return null;
        }

        /// <summary>
        ///     The get enumerator.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerator" />.
        /// </returns>
        // ReSharper disable FunctionRecursiveOnAllPaths
        public IEnumerator<Customer> GetEnumerator()
        {
            // ReSharper restore FunctionRecursiveOnAllPaths
            return this.GetEnumerator();
        }

        /// <summary>
        /// GetOneCustomer returns information related to the selected customer.
        /// </summary>
        /// <param name="customerId">
        /// Integer value holding the id of the customer we are to return information about
        /// </param>
        /// <returns>
        /// Returns a Customer Object
        /// </returns>
        public Customer GetOneCustomer(int customerId)
        {
            var customer = new Customer();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_Customer_Get", sqlConnection);
                sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        customer.CustomerId = Convert.ToInt32(sqlDataReader["CustomerId"]);
                        customer.Name = sqlDataReader["CustomerName"].ToString(); // myReader.GetName(1);
                        customer.Address = sqlDataReader["CustomerAddress"].ToString();
                        customer.Email = sqlDataReader["CustomerEmail"].ToString();
                        customer.PostalCode = sqlDataReader["PostalCode"].ToString();
                        customer.City = sqlDataReader["City"].ToString();
                        if (sqlDataReader["MediaHouseId"] != DBNull.Value)
                        {
                            customer.MediaHouseId = Convert.ToInt32(sqlDataReader["MediaHouseId"]);
                        }

                        if (sqlDataReader["TTCustomerId"] != DBNull.Value)
                        {
                            customer.TtCustomerId = Convert.ToInt32(sqlDataReader["TTCustomerId"]);
                        }

                        if (sqlDataReader["ApiUsername"] != DBNull.Value)
                        {
                            customer.ApiUsername = sqlDataReader["ApiUsername"].ToString();
                        }


                        if (sqlDataReader["ApiPassword"] != DBNull.Value)
                        {
                            customer.ApiPassword = sqlDataReader["ApiPassword"].ToString();
                        }
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();
            }

            return customer;
        }

        #endregion

        #region Explicit Interface Methods

        /// <summary>
        ///     The get enumerator.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerator" />.
        /// </returns>
        // ReSharper disable RedundantNameQualifier
        IEnumerator IEnumerable.GetEnumerator()
        {
            // ReSharper restore RedundantNameQualifier
            return this.GetEnumerator();
        }

        #endregion
    }
}