﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Input.Interfaces.Common;
using NTB.SportsData.Input.Interfaces.DataMappers;

namespace NTB.SportsData.Input.DataMappers.Teams
{
    public class TeamRoleDataMapper : IRepository<Domain.Classes.Function>, IDisposable, ITeamRoleDataMapper
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(TeamDataMapper));

        public int InsertOne(Domain.Classes.Function domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Domain.Classes.Function> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Domain.Classes.Function domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Domain.Classes.Function domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Domain.Classes.Function> GetAll()
        {
            throw new NotImplementedException();
        }

        public Domain.Classes.Function Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void DeleteTeamRole(int functionId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteTeamRole", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@RoleId", functionId));
                    

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertTeamRole(Domain.Classes.Function function)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertTeamRole", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@RoleId", function.FunctionTypeId));
                    sqlCommand.Parameters.Add(new SqlParameter("@RoleName", function.FunctionTypeName));


                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public Domain.Classes.Function GetTeamRoleById(int functionId)
        {
            var function = new Domain.Classes.Function();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetTeamRoleById", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@RoleId", functionId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return function;
                    }

                    while (sqlDataReader.Read())
                    {
                        function.FunctionTypeId = Convert.ToInt32(sqlDataReader["TeamRoleId"]);
                        function.FunctionTypeName = sqlDataReader["TeamRoleName"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return function;
            }
        }
    }
}