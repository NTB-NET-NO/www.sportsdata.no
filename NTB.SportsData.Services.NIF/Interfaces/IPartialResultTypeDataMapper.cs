﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Services.NIF.Interfaces
{
    public interface IPartialResultTypeDataMapper
    {
        List<PartialResultType> GetPartialResultTypesForOrg(int id);
    }
}
