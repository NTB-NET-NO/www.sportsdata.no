﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RoleDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The role data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Data.DataMappers.Role
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;

    using log4net;

    using NTB.SportsData.Data.Interfaces.DataMappers;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The role data mapper.
    /// </summary>
    public class RoleDataMapper : IRoleDataMapper
    {
        #region Fields

        /// <summary>
        ///     The _logger.
        /// </summary>
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add user to role.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        public void AddUserToRole(ApplicationUser user, string roleName)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var role = new Role();
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_AddUserToRole", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", user.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@RoleName", roleName));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The delete role.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void DeleteRole(Role user)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get role by user id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="Role"/>.
        /// </returns>
        public Role GetRoleByUserId(string userId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var role = new Role();
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetRoleByUserId", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return role;
                    }

                    while (sqlDataReader.Read())
                    {
                        role.Id = sqlDataReader["Id"].ToString();
                        role.Name = sqlDataReader["Name"].ToString();
                    }

                    return role;
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }

                    return role;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get role by user name.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="Role"/>.
        /// </returns>
        public Role GetRoleByUserName(string userName)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var role = new Role();
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetRoleByUserName", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserName", userName));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return role;
                    }

                    while (sqlDataReader.Read())
                    {
                        role.Id = sqlDataReader["Id"].ToString();
                        role.Name = sqlDataReader["Name"].ToString();
                    }

                    return role;
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }

                    return role;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        ///     The get roles.
        /// </summary>
        /// <returns>
        ///     The <see cref="IQueryable" />.
        /// </returns>
        public IQueryable<Role> GetRoles()
        {
            // now that we have the user, we can insert the rest of the information
            var roles = new List<Role>();
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetUserRoles", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return roles.AsQueryable();
                    }

                    while (sqlDataReader.Read())
                    {
                        var role = new Role();

                        role.Id = sqlDataReader["Id"].ToString();
                        role.Name = sqlDataReader["Name"].ToString();

                        roles.Add(role);
                    }

                    return roles.AsQueryable();
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }

                    return roles.AsQueryable();
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert role.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertRole(Role user)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The remove user from role.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="roleId">
        /// The role id.
        /// </param>
        public void RemoveUserFromRole(ApplicationUser user, string roleId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var role = new Role();
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_RemoveUserFromRole", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", user.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@RoleId", roleId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The update role.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void UpdateRole(Role user)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}