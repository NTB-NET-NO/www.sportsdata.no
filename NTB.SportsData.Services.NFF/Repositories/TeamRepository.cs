﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The team repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NFF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using NTB.SportsData.Services.NFF.Interfaces;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The team repository.
    /// </summary>
    public class TeamRepository : IRepository<Team>, IDisposable, ITeamDataMapper
    {
        /// <summary>
        /// The _service client.
        /// </summary>
        private readonly OrganizationServiceClient _serviceClient = new OrganizationServiceClient();

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamRepository"/> class.
        /// </summary>
        public TeamRepository()
        {
            // Setting up the OrganizationClient
            if (this._serviceClient.ClientCredentials != null)
            {
                this._serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                this._serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
            }
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(Team domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<Team> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Team domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Team domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<Team> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Team"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Team Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get tournament teams.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Team> GetTournamentTeams(int tournamentId)
        {
            return new List<Team>(this._serviceClient.GetTeamsByTournament(tournamentId, null));
        }

        /// <summary>
        /// The get teams by municipality.
        /// </summary>
        /// <param name="municipalityId">
        /// The municipality id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Team> GetTeamsByMunicipality(int municipalityId)
        {
            return new List<Team>(this._serviceClient.GetTeamsByMunicipality(municipalityId, null));
        }

        /// <summary>
        /// The get team persons.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<OrganizationPerson> GetTeamPersons(int teamId)
        {
            var teamInfo = this._serviceClient.GetTeam(teamId, true);

            if (teamInfo.TeamPersons == null)
            {
                return new List<OrganizationPerson>();
            }

            return
                teamInfo.TeamPersons.Select(
                    teamPerson =>
                    new OrganizationPerson
                        {
                            TeamId = teamInfo.TeamId, 
                            TeamName = teamInfo.TeamName, 
                            FirstName = teamPerson.FirstName, 
                            SurName = teamPerson.SurName, 
                            RoleId = teamPerson.RoleId, 
                            RoleName = teamPerson.RoleName, 
                            Email = teamPerson.Email, 
                            MobilePhone = teamPerson.MobilePhone, 
                            PersonId = teamPerson.PersonId
                        }).ToList();
        }
    }
}