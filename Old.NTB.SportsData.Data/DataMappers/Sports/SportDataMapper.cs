// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SportDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The sport data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.DataMappers.Sports
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using NTB.SportsData.Data.Interfaces.Common;
    using NTB.SportsData.Data.Interfaces.DataMappers;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The sport data mapper.
    /// </summary>
    public class SportDataMapper : IRepository<Sport>, IDisposable, ISportDataMapper
    {
        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(Sport domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<Sport> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Sport domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void Delete(Sport domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_DeleteSport", sqlConnection)
                                     {
                                         CommandType =
                                             CommandType
                                             .StoredProcedure
                                     };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.Id));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Sport> GetAll()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // DONE: Move code to SportsData Stored Procedures
                var sqlCommand = new SqlCommand("SportsData_GetSports", sqlConnection)
                                     {
                                         CommandType =
                                             CommandType
                                             .StoredProcedure
                                     };

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                var sports = new List<Sport>();
                while (sqlDataReader.Read())
                {
                    var sport = new Sport();
                    if (sqlDataReader["Sport"] != DBNull.Value)
                    {
                        sport.Name = sqlDataReader["Sport"].ToString();
                    }

                    if (sqlDataReader["SportId"] != DBNull.Value)
                    {
                        sport.Id = Convert.ToInt32(sqlDataReader["SportId"].ToString());
                    }

                    if (sqlDataReader["SingleSport"] != DBNull.Value)
                    {
                        sport.HasIndividualResult = (int)sqlDataReader["SingleSport"];
                    }

                    if (sqlDataReader["TeamSport"] != DBNull.Value)
                    {
                        sport.HasTeamResults = (int)sqlDataReader["TeamSport"];
                    }

                    if (sqlDataReader["OrgId"] != DBNull.Value)
                    {
                        sport.OrgId = (int)sqlDataReader["OrgId"];
                    }

                    if (sqlDataReader["OrgName"] != DBNull.Value)
                    {
                        sport.OrgName = (string)sqlDataReader["OrgName"];
                    }

                    sports.Add(sport);
                }

                sqlDataReader.Close();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return sports.AsQueryable();
            }
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Sport"/>.
        /// </returns>
        public Sport Get(int id)
        {
            // returning only the selected sport
            return this.GetAll().Single(s => s.Id == id);
        }

        /// <summary>
        /// The get sport id from org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetSportIdFromOrgId(int orgId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand("SportsData_GetSportIdByOrgId", sqlConnection)
                                     {
                                         CommandType =
                                             CommandType
                                             .StoredProcedure
                                     };

                sqlCommand.Parameters.Add(new SqlParameter("@OrgId", orgId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var sqlDataReader = sqlCommand.ExecuteReader();
                var sportId = 0;

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        sportId = Convert.ToInt32(sqlDataReader["SportId"]);
                    }
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return sportId;
            }
        }

        /// <summary>
        /// The get sport id by season id.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetSportIdBySeasonId(int seasonId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand("SportsData_GetSportIdBySeasonId", sqlConnection)
                                     {
                                         CommandType =
                                             CommandType
                                             .StoredProcedure
                                     };

                sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", seasonId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var sqlDataReader = sqlCommand.ExecuteReader();
                var sportId = 0;

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        sportId = Convert.ToInt32(sqlDataReader["SportId"]);
                    }
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return sportId;
            }
        }
    }
}