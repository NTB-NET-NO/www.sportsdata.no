﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace sogapi.Models.SportsMl
{
    /// <summary>
    /// The schedule.
    /// </summary>
    [XmlRoot("schedule")]
    [JsonObject("official")]
    public class Schedule
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [XmlAttribute("id")]
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the schedule meta data.
        /// </summary>
        [XmlElement("schedule-metadata")]
        [JsonProperty("scheduleMetadata", NullValueHandling = NullValueHandling.Ignore)]
        public ScheduleMetaData ScheduleMetaData { get; set; }

        /// <summary>
        /// Gets or sets the sports event.
        /// </summary>
        [XmlElement("sports-event")]
        [JsonProperty("sportsEvent", NullValueHandling = NullValueHandling.Ignore)]
        public SportsEvent[] SportsEvent { get; set; }
    }
}