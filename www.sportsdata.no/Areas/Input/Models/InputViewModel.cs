﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputViewModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The input view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Areas.Input.Models
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Data.DataMappers.Districts;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The input view model.
    /// </summary>
    public class InputViewModel
    {
        /// <summary>
        /// Gets or sets the user matches.
        /// </summary>
        public List<Match> UserMatches { get; set; }

        /// <summary>
        /// Gets or sets the user districts.
        /// </summary>
        public List<District> UserDistricts { get; set; }

        /// <summary>
        /// The get user matches.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        public void GetUserMatches(Guid userId)
        {
            var userMatchModel = new MatchRegisterModels();
            this.GetUserDistricts(userId);
            this.UserMatches = userMatchModel.GetUserMatches(userId, this.UserDistricts);
        }

        /// <summary>
        /// The get user districts.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        public void GetUserDistricts(Guid userId)
        {
            var districtMapper = new DistrictDataMapper();
            var districts = districtMapper.GetDistrictsByUserId(userId);

            this.UserDistricts = districts;
        }

        /// <summary>
        /// The get unregistered matches.
        /// </summary>
        public void GetUnregisteredMatches()
        {
            var model = new MatchRegisterModels();
            this.UserMatches = model.GetMatchesUnregisterable();
        }
    }
}