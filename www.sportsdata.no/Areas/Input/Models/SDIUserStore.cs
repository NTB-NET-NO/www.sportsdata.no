﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using NTB.SportsData.Web.Areas.Input.Models.IdentityModels;

namespace NTB.SportsData.Web.Areas.Input.Models
{
    public class SDIUserStore : IUserStore<User>
    {
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task CreateAsync(User user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(User user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(User user)
        {
            throw new NotImplementedException();
        }

        public Task<User> FindByIdAsync(string userId)
        {
            throw new NotImplementedException();
        }

        public Task<User> FindByNameAsync(string userName)
        {
            throw new NotImplementedException();
        }
    }
}