﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Web.Areas.Admin.Interfaces
{
    interface IDistrictDataMapper
    {
        List<District> GetDistrictsBySportId(int sportId);
    }
}
