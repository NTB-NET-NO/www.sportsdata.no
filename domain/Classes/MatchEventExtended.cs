﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchEventExtended.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match event extended.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Domain.Classes
{
    /// <summary>
    /// The match event extended.
    /// </summary>
    public class MatchEventExtended
    {
        /// <summary>
        /// Gets or sets the match event type id.
        /// </summary>
        public int MatchEventTypeId { get; set; }

        /// <summary>
        /// Gets or sets the match event type.
        /// </summary>
        public string MatchEventType { get; set; }

        /// <summary>
        /// Gets or sets the match event name.
        /// </summary>
        public string MatchEventName { get; set; }

        /// <summary>
        /// Gets or sets the away goals.
        /// </summary>
        public int? AwayGoals { get; set; }

        /// <summary>
        /// Gets or sets the home goals.
        /// </summary>
        public int? HomeGoals { get; set; }

        /// <summary>
        /// Gets or sets the match event id.
        /// </summary>
        public int MatchEventId { get; set; }

        /// <summary>
        /// Gets or sets the match id.
        /// </summary>
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the minute.
        /// </summary>
        public int? Minute { get; set; }

        /// <summary>
        /// Gets or sets the person id.
        /// </summary>
        public int? PersonId { get; set; }

        /// <summary>
        /// Gets or sets the player id.
        /// </summary>
        public int? PlayerId { get; set; }

        /// <summary>
        /// Gets or sets the player name.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether second yellow card.
        /// </summary>
        public bool SecondYellowCard { get; set; }

        /// <summary>
        /// Gets or sets the team name.
        /// </summary>
        public string TeamName { get; set; }

        /// <summary>
        /// Gets or sets the team id.
        /// </summary>
        public int? TeamId { get; set; }

        /// <summary>
        /// Gets or sets the connected to event.
        /// </summary>
        public string ConnectedToEvent { get; set; }
    }
}