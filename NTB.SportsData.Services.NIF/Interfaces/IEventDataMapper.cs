﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISportResultMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The SportResultMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Services.NIF.Interfaces
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The SportResultMapper interface.
    /// </summary>
    public interface IEventDataMapper
    {
        /// <summary>
        /// The get events.
        /// </summary>
        /// <param name="eventDate">
        /// The event date.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<EventSearchResult> GetEvents(DateTime eventDate, int orgId);
    }
}