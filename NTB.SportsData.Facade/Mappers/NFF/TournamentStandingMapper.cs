﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentStandingMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament standing mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NFF
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The tournament standing mapper.
    /// </summary>
    public class TournamentStandingMapper : BaseMapper<TournamentTableTeam, TeamResult>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<TournamentTableTeam, TeamResult> mapper)
        {
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TeamId, y => y.OrgId);
            mapper.Relate(x => x.TeamNameInTournament, y => y.OrgName);
            mapper.Relate(x => x.JerseyColor, y => y.JerseyColor);
            mapper.Relate(x => x.ShortsColor, y => y.ShortsColor);
            mapper.Relate(x => x.TablePosition, y => y.Position);
            mapper.Relate(x => x.TotalMatchesPlayed, y => y.TotalMatches);
            mapper.Relate(x => x.TotalMatchesWon, y => y.Victories);
            mapper.Relate(x => x.TotalMatchesDraw, y => y.Draws);
            mapper.Relate(x => x.TotalMatchesLost, y => y.Losses);
            mapper.Relate(x => x.TotalGoalsScored, y => y.GoalsScored);
            mapper.Relate(x => x.TotalGoalsAgainst, y => y.GoalsConceeded);
            mapper.Relate(x => x.TotalGoalDifference, y => y.GoalDifference);
            mapper.Relate(x => x.TotalPoints, y => y.TotalPoints);
            mapper.Relate(x => x.AwayMatchesPlayed, y => y.MatchesAway);
            mapper.Relate(x => x.AwayMatchesWon, y => y.VictoriesAway);
            mapper.Relate(x => x.AwayMatchesDraw, y => y.DrawsAway);
            mapper.Relate(x => x.AwayMatchesLost, y => y.LossesAway);
            mapper.Relate(x => x.AwayGoalsScored, y => y.GoalsScoredAway);
            mapper.Relate(x => x.AwayGoalsAgainst, y => y.GoalsConcededAway);
            mapper.Relate(x => x.AwayGoalDifference, y => y.AwayGoalDifference);
            mapper.Relate(x => x.HomeMatchesPlayed, y => y.MatchesHome);
            mapper.Relate(x => x.HomeMatchesWon, y => y.VictoriesHome);
            mapper.Relate(x => x.HomeMatchesDraw, y => y.DrawsHome);
            mapper.Relate(x => x.HomeMatchesLost, y => y.LossesHome);
            mapper.Relate(x => x.HomeGoalsScored, y => y.GoalsScoredHome);
            mapper.Relate(x => x.HomeGoalsAgainst, y => y.GoalsConcededHome);
            mapper.Relate(x => x.HomeGoalDifference, y => y.HomeGoalDifference);
        }
    }
}