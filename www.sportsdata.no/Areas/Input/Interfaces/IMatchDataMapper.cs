﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMatchDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The MatchDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Areas.Input.Interfaces
{
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The MatchDataMapper interface.
    /// </summary>
    internal interface IMatchDataMapper
    {
        /// <summary>
        /// The insert match result.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        void InsertMatchResult(Match match);
    }
}