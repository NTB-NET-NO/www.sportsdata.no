﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HomeController.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The home controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Areas.Calendar.Controllers
{
    using System.Web.Mvc;

    using Models;

    /// <summary>
    /// The home controller.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// This method shall show todays events from all sports
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            // Creating the Calendar Model
            var model = new CalendarModel();

            // Get Todays matches from all sports
            var todaysMatches = model.GetTodaysMatches();

            // Creating a View Model
            // var viewModel = new CalendarViewModel().PopulateList(todaysMatches);
            
            // Return this to view
            return this.View(todaysMatches);
        }
    }
}