﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Mailer.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The mailer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Utilities
{
    using System.Configuration;
    using System.Net.Mail;

    /// <summary>
    /// The mailer.
    /// </summary>
    public class Mailer
    {
        /// <summary>
        /// The mail send.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void MailSend(string message)
        {
            // Getting the receivers from the configuration file
            var mailReceivers = ConfigurationManager.AppSettings["ErrorReceivers"].Split(',');

            // Creating message
            var mailMessage = new MailMessage { Body = message, From = new MailAddress("505@ntb.no"), Subject = "Ajax Error from www.sportsdata.no" };

            // Adding to list of receivers
            foreach (var mailAddress in mailReceivers)
            {
                mailMessage.To.Add(mailAddress);
            }

            var client = new SmtpClient { Port = 25, DeliveryMethod = SmtpDeliveryMethod.Network, UseDefaultCredentials = false, Host = "mail.ntb.no" };

            // Sending the message
            client.Send(mailMessage);
        }
    }
}