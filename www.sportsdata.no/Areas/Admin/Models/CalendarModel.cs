﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CalendarModel.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The calendar model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Web.Areas.Admin.Repositories;

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    /// <summary>
    ///     The calendar model.
    /// </summary>
    public class CalendarModel
    {
        /// <summary>
        ///     The get calendar items.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>Dictionary</cref>
        ///     </see>
        ///     .
        /// </returns>
        public Dictionary<int, List<Domain.Classes.Calendar>> GetCalendarItems()
        {
            // Creating and populating the sports list which we need for the dropdown
            var sportRepository = new SportRepository();
            var listSports = new List<Sport>(sportRepository.GetAll());


            var dictionary = new Dictionary<int, List<Domain.Classes.Calendar>>();
            var calendarRepository = new CalendarRepository();
            foreach (Sport sport in listSports)
            {
                calendarRepository.SportId = sport.Id;

                dictionary.Add(sport.Id, new List<Domain.Classes.Calendar>(calendarRepository.GetAll()));
            }

            return dictionary;
        }

        /// <summary>
        ///     The get calendar items.
        /// </summary>
        /// <param name="sportId">
        ///     The sport id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Domain.Classes.Calendar> GetCalendarItems(int sportId)
        {
            var repository = new CalendarRepository
                {
                    SportId = sportId
                };

            return new List<Domain.Classes.Calendar>(repository.GetAll());
        }
    }
}