﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace sog.Models.SportsMl
{
    /// <summary>
    /// The sub scores.
    /// </summary>
    [JsonObject("subScores")]
    public class SubScores
    {
        /// <summary>
        /// Gets or sets the uniform number.
        /// </summary>
        [XmlIgnore]
        [JsonProperty("subScore", NullValueHandling = NullValueHandling.Ignore)]
        public SubScore[] SubScore { get; set; }
    }
}