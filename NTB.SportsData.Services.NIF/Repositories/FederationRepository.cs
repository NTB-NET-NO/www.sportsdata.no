﻿namespace NTB.SportsData.Services.NIF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net;

    using log4net;
    using log4net.Config;

    using NTB.SportsData.Services.NIF.Interfaces;
    using NTB.SportsData.Services.NIF.NIFProdService;

    public class FederationRepository : IRepository<Federation>, IDisposable, IFederationDataMapper
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FederationRepository));

        /// <summary>
        ///     The ntb bpassword.
        /// </summary>
        private readonly string _ntbPassword = ConfigurationManager.AppSettings["NIFServicesPassword"];

        /// <summary>
        ///     The ntb busername.
        /// </summary>
        private readonly string _ntbUsername = ConfigurationManager.AppSettings["NIFServicesUsername"];

        /// <summary>
        ///     The organization service client.
        /// </summary>
        private readonly OrgServiceClient _orgServiceClient = new OrgServiceClient();

        public FederationRepository()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }

            // Setting up the system
            ServicePointManager.ServerCertificateValidationCallback = (servicePointSender, certificate, chain, sslPolicyErrors) => true; // Ignoring cert

            if (this._orgServiceClient.ClientCredentials != null)
            {
                this._orgServiceClient.ClientCredentials.UserName.UserName = this._ntbUsername;
                this._orgServiceClient.ClientCredentials.UserName.Password = this._ntbPassword;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Federation GetFederationByOrgId(int orgId)
        {
            var emptyRequest = new EmptyRequest();
            var response = this._orgServiceClient.GetFederations(emptyRequest);

            if (!response.Success)
            {
                throw new Exception(response.ErrorCode + @": " + response.ErrorMessage);
            }

            return response.Federations.Single(e => e.OrgId == orgId);
        }

        public List<Federation> GetFederations()
        {
            // Now we must get the federations and find out who has results defined at NIF.
            var emptyRequest = new EmptyRequest();
            var federationResponse = this._orgServiceClient.GetFederations(emptyRequest);

            if (!federationResponse.Success)
            {
                return null;
            }

            return federationResponse.Federations.ToList();
        }

        public Federation GetFederation(int id)
        {
            throw new NotImplementedException();
        }

        public int InsertOne(Federation domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Federation> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Federation domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Federation domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Federation> GetAll()
        {
            throw new NotImplementedException();
        }

        public Federation Get(int id)
        {
            throw new NotImplementedException();
        }
    }
}