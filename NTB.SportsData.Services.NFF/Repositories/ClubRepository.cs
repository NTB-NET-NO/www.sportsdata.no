﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClubRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The club repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NFF.Repositories
{
    using System.Collections.Generic;
    using System.Configuration;

    using NTB.SportsData.Services.NFF.Interfaces;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The club repository.
    /// </summary>
    public class ClubRepository : IClubDataMapper
    {
        /// <summary>
        /// The _service client.
        /// </summary>
        private readonly OrganizationServiceClient _serviceClient = new OrganizationServiceClient();

        /// <summary>
        /// Initializes a new instance of the <see cref="ClubRepository"/> class.
        /// </summary>
        public ClubRepository()
        {
            // Setting up the OrganizationClient
            if (this._serviceClient.ClientCredentials != null)
            {
                this._serviceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServicesUsername"];
                this._serviceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicesPassword"];
            }
        }

        /// <summary>
        /// The get tournament clubs.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Club> GetTournamentClubs(int tournamentId)
        {
            return new List<Club>(this._serviceClient.GetClubsByTournament(tournamentId, null));
        }

        /// <summary>
        /// The get clubs by municipality.
        /// </summary>
        /// <param name="municipalityId">
        /// The municipality id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Club> GetClubsByMunicipality(int municipalityId)
        {
            var teams = this._serviceClient.GetTeamsByMunicipality(municipalityId, null);

            var clubs = new List<Club>();
            foreach (var team in teams)
            {
                var club = this._serviceClient.GetClub(team.ClubId, true);

                clubs.Add(club);
            }

            return clubs;
        }

        /// <summary>
        /// The get club persons.
        /// </summary>
        /// <param name="clubId">
        /// The club id.
        /// </param>
        /// <returns>
        /// The <see cref="Club"/>.
        /// </returns>
        public Club GetClubPersons(int clubId)
        {
            var club = this._serviceClient.GetClub2(clubId, true, true);
            return club;
        }
    }
}