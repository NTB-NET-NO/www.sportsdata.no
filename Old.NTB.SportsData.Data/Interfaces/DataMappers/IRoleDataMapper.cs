﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRoleDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The RoleDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    using System.Linq;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The RoleDataMapper interface.
    /// </summary>
    public interface IRoleDataMapper
    {
        /// <summary>
        /// The get roles.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        IQueryable<Role> GetRoles();

        /// <summary>
        /// The insert role.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        void InsertRole(Role user);

        /// <summary>
        /// The update role.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        void UpdateRole(Role user);

        /// <summary>
        /// The delete role.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        void DeleteRole(Role user);

        /// <summary>
        /// The get role by user id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="Role"/>.
        /// </returns>
        Role GetRoleByUserId(string userId);

        /// <summary>
        /// The get role by user name.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="Role"/>.
        /// </returns>
        Role GetRoleByUserName(string userName);

        /// <summary>
        /// The add user to role.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        void AddUserToRole(ApplicationUser user, string roleName);

        /// <summary>
        /// The remove user from role.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        void RemoveUserFromRole(ApplicationUser user, string roleName);
    }
}