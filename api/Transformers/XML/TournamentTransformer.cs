﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   Turns Raw XML to SportsML for Tournaments
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Transformers.XML
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Xsl;

    using NTB.SportsData.Api.Interfaces;

    /// <summary>
    ///     Turns Raw XML to SportsML for Tournaments
    /// </summary>
    public class TournamentTransformer : ITransformer
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the xslt file.
        /// </summary>
        public string XsltFile { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The transform object.
        /// </summary>
        /// <param name="document">
        /// The document.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Throws an exception if no xslt file has been defined
        /// </exception>
        public string TransformObject(string document)
        {
            if (string.IsNullOrEmpty(this.XsltFile))
            {
                throw new Exception("No XsltFile defined. Cannot continue");
            }

            var transformer = new XslCompiledTransform();
            string xsltPath = ConfigurationManager.AppSettings["sportsmlxsltfilepath"];
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(document);

            transformer.Load(xsltPath + @"\" + this.XsltFile);

            var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t" };
            settings.Encoding = Encoding.UTF8;
            settings.OmitXmlDeclaration = false;

            using (var textWriter = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(textWriter, settings))
                {
                    transformer.Transform(xmlDoc, writer);
                }

                return textWriter.ToString();
            }
        }

        #endregion
    }
}