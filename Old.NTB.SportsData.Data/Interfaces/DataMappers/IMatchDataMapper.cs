﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMatchDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The MatchDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The MatchDataMapper interface.
    /// </summary>
    public interface IMatchDataMapper
    {
        /// <summary>
        /// The get matches by tournament id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Match> GetMatchesByTournamentId(int tournamentId);

        /// <summary>
        /// The get local matches by district id.
        /// </summary>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Match> GetLocalMatchesByDistrictId(int districtId);

        /// <summary>
        /// The get match comment.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="MatchComment"/>.
        /// </returns>
        MatchComment GetMatchComment(int matchId);

        /// <summary>
        /// The store match comment.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <param name="matchComment">
        /// The match comment.
        /// </param>
        void StoreMatchComment(int matchId, string matchComment);
    }
}