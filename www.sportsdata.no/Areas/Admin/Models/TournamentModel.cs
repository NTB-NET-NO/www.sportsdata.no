﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NTB.SportsData.Data.DataMappers.Fetch;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.NFF.TournamentAccess;
using NTB.SportsData.Facade.NIF.SportAccess;
using NTB.SportsData.Web.Areas.Admin.Interfaces;
using NTB.SportsData.Web.Areas.Admin.Repositories;

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    // using Tournaments = NTBSportsDataAdmin.Models.RemoteDataAccess.Tournaments;

    // This shall be the main TournamentModel that we will use to model the tournament and municipality

    /// <summary>
    ///     The tournament models.
    /// </summary>
    public class TournamentModel : ITournamentModel
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentModel));

        public EditTournamentViewModel GetTournamentViewModelByTournamentId(int tournamentId)
        {
            var viewModel = new EditTournamentViewModel();
            var repository = new TournamentRepository();
            var tournament = repository.GetTournamentByTournamentid(tournamentId);

            var sportRepository=  new SportRepository();
            viewModel.Sports = sportRepository.GetAll().ToList();

            var genderModel = new GenderModel();
            viewModel.Genders = genderModel.GetAllGenders();
            

            var ageCategoryRepository = new AgeCategoryDefinitionRepository();
            viewModel.AgeCategoryDefinitions = ageCategoryRepository.GetAll().ToList();

            viewModel.TournamentName = tournament.TournamentName;
            viewModel.TournamentId = tournament.TournamentId;
            viewModel.HiddenAgeCategoryId = Convert.ToInt32(tournament.AgeCategoryId);
            viewModel.HiddenSeasonId = tournament.SeasonId;
            viewModel.HiddenTournamentTypeId = tournament.TournamentTypeId;
            viewModel.TournamentNumber = tournament.TournamentNumber;
            viewModel.SelectedSport = tournament.SportId;
            viewModel.SelectedGender = tournament.GenderId;
            viewModel.SelectedAgeCategoryDefinition = tournament.AgeCategoryDefinitionId;

            return viewModel;
        }
        /// <summary>
        /// The store tournaments.
        /// </summary>
        /// <param name="selectedTournaments">
        /// The selected Tournaments.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> NormalizeTournaments(string selectedTournaments)
        {
            // Splitting the tournaments that is separated by comma
            if (string.IsNullOrEmpty(selectedTournaments))
            {
                return new List<Tournament>();
            }

            var splittedTournaments = selectedTournaments.Split(',');

            return splittedTournaments.Select(tournamentIds => tournamentIds.Split('/'))
                .Select(
                    splittedTournament =>
                        new Tournament
                        {
                            TournamentId = Convert.ToInt32(splittedTournament[1]),
                            TournamentNumber = splittedTournament[0]
                        })
                .ToList();
        }

        /// <summary>
        /// This method shall insert a tournament into the tournament database table
        /// </summary>
        /// <param name="tournament">
        /// The tournament object
        /// </param>
        public void InsertTournament(Tournament tournament)
        {
            var repository = new TournamentRepository();
            repository.InsertOne(tournament);
        }

        /// <summary>
        /// Insets selected tournaments into database
        /// </summary>
        /// <param name="selectedTournaments">
        /// The Selected tournaments
        /// </param>
        /// <param name="jobId">
        /// The job id
        /// </param>
        /// <param name="ageCategoryId">
        /// the age category id
        /// </param>
        /// <param name="customerId">
        /// the customer id
        /// </param>
        /// <param name="sportId">
        /// the sport id
        /// </param>
        public void InsertSelectedTournaments(List<Tournament> selectedTournaments, int jobId, int ageCategoryId,
            int customerId, int sportId)
        {
            try
            {
                this.DeleteTournamentByJobId(jobId);

                // We are now adding 
                if (selectedTournaments.Count == 0)
                {
                    return;
                }

                // Now we are inserting information about the tournament in the database
                var customerTournamentRepository = new CustomerTournamentRepository();

                // we have to add more data to the tournament object
                var tournaments =
                    selectedTournaments.Select(
                        tournament =>
                            new Tournament
                            {
                                TournamentId = tournament.TournamentId,
                                TournamentNumber = tournament.TournamentNumber,
                                JobId = jobId,
                                AgeCategoryId = ageCategoryId,
                                SportId = sportId
                            })
                        .ToList();

                customerTournamentRepository.InsertAll(tournaments);
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                Logger.Error(e.StackTrace);
            }
        }

        /// <summary>
        /// The get tournaments by job id.
        /// </summary>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsByJobId(int jobId)
        {
            var repository = new TournamentRepository();
            var tournaments = repository.GetTournamentByJobId(jobId);

            return tournaments;
        }

        /// <summary>
        /// The get tournaments by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="seasonId">
        /// The season Id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsBySportId(int sportId, int seasonId)
        {
            var repository = new TournamentRepository();
            return new List<Tournament>(repository.GetTournamentsBySportId(sportId, seasonId));
        }

        /// <summary>
        /// The get tournament.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        public Tournament GetTournament(int tournamentId)
        {
            var repository = new TournamentRepository();
            return repository.Get(tournamentId);
        }

        /// <summary>
        /// The delete tournament.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        public void DeleteTournament(int tournamentId)
        {
            // We shall remove all tournaments related to this job
            var repository = new TournamentRepository();
            var tournament = new Tournament {TournamentId = tournamentId};

            repository.Delete(tournament);
        }

        /// <summary>
        /// The delete tournament by job id.
        /// </summary>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        public void DeleteTournamentByJobId(int jobId)
        {
            // We shall remove all tournaments related to this job
            var repository = new TournamentRepository();
            repository.DeleteTournamentByJobId(jobId);
        }

        /// <summary>
        /// The get tournaments for view.
        /// </summary>
        /// <param name="municipalities">
        /// The municipalities.
        /// </param>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <param name="ageCategory">
        /// The age category.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsForView(List<Municipality> municipalities, int jobId, int ageCategory,
            int orgId, int sportId, int seasonId)
        {
            var repository = new AgeCategoryRepository();
            var categories = repository.GetAgeCategoryBySportId(sportId)
                .Where(x => x.AgeCategoryDefinitionId == ageCategory);

            var selectedMunicipalities = municipalities.Select(municipality => municipality.MunicipalityId.ToString())
                .ToList();

            if (orgId == 0)
            {
                var organizationRepository = new OrganizationRepository();
                orgId = organizationRepository.GetOrganizationBySportId(sportId)
                    .OrganizationId;
            }

            Logger.DebugFormat("We are working on results for orgid {0}", orgId);
            List<Tournament> tournaments;
            if (orgId == 365)
            {
                var facade = new TournamentFacade {AgeCategory = ageCategory};
                tournaments = facade.GetTournamentByMunicipalities(selectedMunicipalities, seasonId);
                foreach (var tournament in tournaments)
                {
                    tournament.SportId = sportId;
                }
            }
            else if (orgId == 2000)
            {
                var clubRepository = new ClubRepository();
                var clubs = clubRepository.GetClubsByMunicipalities(selectedMunicipalities, orgId);

                var facade = new Facade.Profixio.ProfixioFacade.TournamentFacade();

                // Only sending the club Ids
                var clubIds = (from c in clubs select c.ClubId).ToList();
                tournaments = facade.GetTournamentByClubs(clubIds, seasonId);
            }
            else
            {
                var facade = new SportFacade();
                tournaments = facade.GetTournamentByMunicipalities(selectedMunicipalities, seasonId, orgId);

                // Making sure we are inserting the correct data
                var newTournaments = new List<Tournament>();
                foreach (var tournament in tournaments)
                {
                    tournament.SportId = sportId;

                    var tournamentList = newTournaments.Where(x => x.TournamentId == tournament.TournamentId)
                        .ToList();
                    if (!tournamentList.Any())
                    {
                        newTournaments.Add(tournament);
                    }
                }

                tournaments = newTournaments;
            }

            Logger.DebugFormat(
                "We have {0} number of tournaments after getting tournaments for selected municipalities.",
                tournaments.Count);

            var tournamentRepository = new TournamentRepository();

            // We are inserting the tournaments in the database - if they not there of course
            tournamentRepository.InsertAll(tournaments);

            var filteredTournament = new List<Tournament>();

            if (sportId == 16)
            {
                filteredTournament = tournaments.Where(x => categories.Any(c => c.CategoryId == x.AgeCategoryId))
                    .ToList();
            }
            else
            {
                var localTournaments = tournamentRepository.GetTournamentsBySportId(sportId, seasonId);
                localTournaments = localTournaments.Where(x => x.AgeCategoryDefinitionId == ageCategory)
                    .ToList();

                foreach (var remoteTournament in tournaments)
                {
                    var rt = remoteTournament;
                    foreach (var lt in localTournaments.Where(lt => rt.TournamentId == lt.TournamentId))
                    {
                        rt.AgeCategoryDefinitionId = lt.AgeCategoryDefinitionId;
                        rt.SportId = lt.SportId;

                        filteredTournament.Add(rt);
                    }
                }
            }

            Logger.DebugFormat("We have {0} number of tournaments after age category filtering.",
                filteredTournament.Count);

            // If we do not get any hits, it could be because we have not set the different age categories, or something else, 
            // so we set the filtered tournaments to the original list
            if (filteredTournament.Count == 0)
            {
                filteredTournament = tournaments;
            }

            var storedTournaments = this.GetTournamentsByJobId(jobId);
            foreach (
                var t in
                    from t in filteredTournament
                    from s in storedTournaments.Where(s => t.TournamentId == s.TournamentId)
                    select t)
            {
                t.TournamentSelected = true;
            }

            return filteredTournament;
        }

        /// <summary>
        /// The get tournaments not stored.
        /// </summary>
        /// <param name="storedtournaments">
        /// The storedtournaments.
        /// </param>
        /// <param name="tournamentsNotStored">
        /// The tournaments not stored.
        /// </param>
        /// <param name="filteredTournament">
        /// The filtered tournament.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsNotStored(List<Tournament> storedtournaments,
            List<Tournament> tournamentsNotStored, List<Tournament> filteredTournament)
        {
            List<Tournament> tns;
            if (storedtournaments.Count == 0)
            {
                tns = filteredTournament;
            }
            else
            {
                // Filtering out the tournaments we have filtered against those we have in the database.
                tns = filteredTournament.Except(storedtournaments)
                    .ToList();

                return tns;
            }

            return tns;
        }

        /// <summary>
        /// The get tournaments for job.
        /// </summary>
        /// <param name="selectedTournaments">
        /// The selected tournaments.
        /// </param>
        /// <param name="selectedMunicipalities">
        /// The selected municipalities.
        /// </param>
        /// <param name="ageCategoryId">
        /// The age category id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <param name="sportId">
        /// The sport id
        /// </param>
        /// <param name="federationId">
        /// The federation id.
        /// </param>
        /// <param name="useFiks">
        /// The use fiks.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsForJob(List<Tournament> selectedTournaments,
            List<Municipality> selectedMunicipalities, int ageCategoryId, int seasonId, int jobId, int sportId,
            int federationId, bool useFiks)
        {
            var tournamentsForJob = new List<Tournament>();

            Logger.Info("Selected MunicipalitiesDataAccess: " + selectedMunicipalities);

            try
            {
                List<Tournament> availableTournaments;

                if (sportId == 16)
                {
                    var facade = new TournamentFacade();

                    var stringSelectedMunicipalites =
                        selectedMunicipalities.Select(municipality => municipality.MunicipalityId.ToString())
                            .ToList();

                    availableTournaments = facade.GetTournamentByMunicipalities(stringSelectedMunicipalites, seasonId);
                }
                else
                {
                    var facade = new SportFacade();

                    var ageCategoryRepository = new AgeCategoryRepository {SportId = sportId};

                    var listMunicipalities =
                        selectedMunicipalities.Select(municipality => municipality.MunicipalityId.ToString())
                            .ToList();
                    availableTournaments = facade.GetTournamentByMunicipalities(listMunicipalities, seasonId,
                        federationId);
                }

                CheckExistingTournaments(sportId, availableTournaments);

                tournamentsForJob.AddRange(
                    availableTournaments.Select(
                        selectedTournament =>
                            new Tournament
                            {
                                TournamentId = selectedTournament.TournamentId,
                                TournamentName = selectedTournament.TournamentName,
                                AgeCategoryId = selectedTournament.AgeCategoryId,
                                DistrictId = selectedTournament.DistrictId,
                                GenderId = selectedTournament.GenderId,
                                MunicipalityId = selectedTournament.MunicipalityId,
                                SeasonId = selectedTournament.SeasonId,
                                SportId = selectedTournament.SportId,
                                TournamentNumber = selectedTournament.TournamentNumber
                            })
                        .Select(tournament => AddSelectedTournament(selectedTournaments, tournament)));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                Logger.Error(e.StackTrace);

                return null;
            }

            return tournamentsForJob.OrderByDescending(tournament => tournament.AgeCategoryId)
                .ToList();
        }

        /// <summary>
        /// The get tournament by id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        public Tournament GetTournamentById(int tournamentId, int seasonId)
        {
            var facade = new SportFacade();
            return facade.GetTournamentById(tournamentId, seasonId);
        }

        /// <summary>
        /// The get tournaments by season id.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            var seasonRepository = new SeasonRepository();
            var season = seasonRepository.Get(seasonId);

            if (season.SeasonId == 0)
            {
                return new List<Tournament>();
            }

            var repository = new TournamentRepository();
            var localTournaments = new List<Tournament>(repository.GetTournamentsBySeasonId(seasonId));
            
            // We need to find out which sport this season belongs to
            
            var organisationRepository = new OrganizationRepository();
            var org = organisationRepository.GetOrganizationBySportId(season.SportId);

            Logger.DebugFormat("Checking when we got the latest list of tournaments by orgId {0}", org.OrganizationId);
            var fetchDataMapper = new FetchDataMapper();
            var latestFetch = fetchDataMapper.GetLatestTournamentFetchDateTimeByOrgId(org.OrganizationId);
            if (latestFetch != null)
            {
                Logger.DebugFormat("Latest update was {0}", latestFetch.LatestFetchDateTime.ToShortDateString());
            }
            var update = false;

            if (latestFetch != null)
            {
                Logger.DebugFormat("Latest fetch {0}", latestFetch.LatestFetchDateTime);
                var latestFetchTime = (DateTime.Now - latestFetch.LatestFetchDateTime).TotalHours;
                Logger.DebugFormat("The time difference between now and latest fetch: {0}", latestFetchTime);
                if (Math.Round(latestFetchTime) < 24)
                {
                    Logger.Debug("Not found, so we return the local tournaments");
                    return localTournaments;
                }
                else
                {
                    update = true;
                }
            }

            
            var remoteTournaments = new List<Tournament>();
            if (season.SportId == 16)
            {
                // We are to get soccer-tournaments
                var facade = new TournamentFacade();
                remoteTournaments.AddRange(facade.GetTournamentsBySeasonId(seasonId));
            }
            else
            {
                // We are to get NIF-tournaments
                var facade = new SportFacade();
                remoteTournaments.AddRange(facade.GetTournamentSummariesBySeasonAndOrgId(seasonId, season.OrgId));
            }

            // If the number of tournaments are equal, then we can return the local one
            Logger.DebugFormat("remoteTournaments contains {0} objects, while localTournaments contains {1} objects", 
                remoteTournaments.Count, localTournaments.Count);

            // Updating the local tournament table. This because we want to be sure we have the newest data
            localTournaments = this.UpdateLocalTournamentTable(remoteTournaments, localTournaments, season, repository);

            fetchDataMapper.UpdateTournamentFetchDateTime(org.OrganizationId);

            return localTournaments;
        }

        public List<Tournament> UpdateLocalTournamentTable(List<Tournament> remoteTournaments, List<Tournament> localTournaments, Season season, TournamentRepository repository)
        {
            // Now we must find the remote Tournaments not in our database
            // @todo: We must insert or update the table, and we shouldn't check if the list is shorter/longer than the original one.
            foreach (var remoteTournament in remoteTournaments)
            {
                var currentRemoteTournament = remoteTournament;
                if (!localTournaments.Contains(currentRemoteTournament))
                {
                    if (currentRemoteTournament.TournamentName.ToLower()
                        .Contains("gutter") ||
                        currentRemoteTournament.TournamentName.ToLower()
                            .Contains("jenter"))
                    {
                        currentRemoteTournament.AgeCategoryDefinitionId = 1;
                        currentRemoteTournament.AgeCategoryDefinition = "aldersbestemt";
                    }
                    else if (currentRemoteTournament.TournamentName.ToLower()
                        .Contains("kvinner") ||
                             currentRemoteTournament.TournamentName.ToLower()
                                 .Contains("menn"))
                    {
                        currentRemoteTournament.AgeCategoryDefinitionId = 2;
                        currentRemoteTournament.AgeCategoryDefinition = "senior";
                    }
                    else
                    {
                        currentRemoteTournament.AgeCategoryDefinitionId = 1;
                        currentRemoteTournament.AgeCategoryDefinition = "aldersbestemt";
                    }

                    currentRemoteTournament.SportId = season.SportId;
                    repository.InsertOne(currentRemoteTournament);

                    var index = localTournaments.FindIndex(x => x.TournamentId == currentRemoteTournament.TournamentId);
                    var foundTournament = localTournaments.Where(x => x.TournamentId == currentRemoteTournament.TournamentId);

                    if (foundTournament.Any())
                    {
                        localTournaments[index] = currentRemoteTournament;
                    }
                    else
                    {
                        localTournaments.Add(currentRemoteTournament);
                    }
                }
                else
                {
                    // @todo: Update the table... 
                    // @todo: This in case the data is new or not
                    // We should update
                    Logger.DebugFormat("Updating the tournament {0} ({1})", currentRemoteTournament.TournamentName, currentRemoteTournament.TournamentId);
                    repository.Update(currentRemoteTournament);
                }
            }
            return localTournaments;
            
        }

        /// <summary>
        /// The update tournament age category definitition.
        /// </summary>
        /// <param name="ints">
        /// The ints.
        /// </param>
        /// <param name="sportId">
        /// The sport Id.
        /// </param>
        /// <param name="seasonId">
        /// The season Id.
        /// </param>
        /// /// 
        /// ///
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int UpdateTournamentAgeCategoryDefinitition(Dictionary<int, int> ints, int sportId, int seasonId)
        {
            var repository = new TournamentRepository();

            var seasonRepository = new SeasonRepository();

            var season = seasonRepository.GetSeason(new Season() {SeasonId = seasonId, SportId = sportId});

            foreach (var kvp in ints)
            {
                // We should check if the tournament is in the database
                var result = repository.GetTournamentByTournamentid(kvp.Key);

                if (result.TournamentId > 0)
                {
                    // We have the tournament
                    repository.UpdateTournamentAgeCategoryDefinition(kvp.Key, kvp.Value);
                }
                else
                {
                    // I have to get and get this again
                    var facade = new SportFacade();
                    var tournament = facade.GetTournamentById(kvp.Key, seasonId);

                    // Bandy exception
                    if (sportId == 4)
                    {
                        tournament.SportId = season.FederationDiscipline;
                    }

                    repository.InsertOne(tournament);
                }
            }

            if (sportId == 4)
            {
                return season.FederationDiscipline;
            }

            return sportId;
        }

        /// <summary>
        /// The check existing tournaments.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="availableTournaments">
        /// The available tournaments.
        /// </param>
        private static void CheckExistingTournaments(int sportId, List<Tournament> availableTournaments)
        {
            var tournamentRepository = new TournamentRepository {SportId = sportId};

            foreach (var tournament in from tournament in availableTournaments
                let foundTournament = tournamentRepository.Search(tournament)
                where foundTournament == false
                select tournament)
            {
                // We shall insert the tournament
                tournamentRepository.InsertOne(tournament);
            }
        }

        /// <summary>
        /// The add selected tournament.
        /// </summary>
        /// <param name="selectedTournaments">
        /// The selected tournaments.
        /// </param>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        private static Tournament AddSelectedTournament(List<Tournament> selectedTournaments, Tournament tournament)
        {
            /** 
             * check if the tournament number is in either selected tournament or available tournament.
             * If it is we add some more parameters to the tournament object
             */
            if (selectedTournaments.Any(tournamentJob => tournament.TournamentNumber == tournamentJob.TournamentNumber))
            {
                tournament.TournamentSelected = true;
                tournament.TournamentAccepted = 1;
            }

            return tournament;
        }

        public void UpdateSelectedTournament(int id, System.Web.Mvc.FormCollection collection)
        {
            var tournament = new Tournament();
            tournament.TournamentId = id;
            tournament.TournamentName = collection["tournamentName"];
            tournament.TournamentNumber = collection["tournamentNumber"];
            tournament.GenderId = Convert.ToInt32(collection["genders"]);
            tournament.SportId = Convert.ToInt32(collection["selectedSportId"]);
            tournament.SeasonId = Convert.ToInt32(collection["seasonId"]);
            tournament.AgeCategoryId = Convert.ToInt32(collection["ageCategoryId"]);
            tournament.DistrictId = 0;
            tournament.TournamentTypeId = Convert.ToInt32(collection["tournamentTypeId"]);
            tournament.AgeCategoryDefinitionId = Convert.ToInt32(collection["ageCategoryDefinition"]);

            var repository = new TournamentRepository();
            repository.Update(tournament);
        }
    }
}