﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentClassesMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament classes mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The tournament classes mapper.
    /// </summary>
    public class TournamentClassesMapper : BaseMapper<TournamentClasses, AgeCategory>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<TournamentClasses, AgeCategory> mapper)
        {
            mapper.Relate(x => x.ClassId, y => y.CategoryId);
            mapper.Relate(x => x.ClassName, y => y.CategoryName);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
        }
    }
}