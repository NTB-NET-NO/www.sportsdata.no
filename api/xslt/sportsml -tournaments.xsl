<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">
    <xsl:output indent="yes"/>
    
    
    <xsl:template match="/">
        <sports-content>
            <xsl:for-each select="/Tournaments/Tournament">
                <sports-event>
                    <event-metadata>
                        <xsl:attribute name="event-key">
                            <xsl:value-of select="TournamentId" />
                        </xsl:attribute>
                        <xsl:attribute name="event-name">
                            <xsl:value-of select="TournamentName"/>
                        </xsl:attribute>
                        <xsl:attribute name="start-date-time">
                            <xsl:value-of select="StartDate" />
                        </xsl:attribute>
                        
                        <xsl:attribute name="end-date-time">
                            <xsl:value-of select="EndDate" />
                        </xsl:attribute>
                        
                        <sports-content-codes>
                            <sports-content-code>
                                <xsl:attribute name="code-type">
                                    <xsl:text>sport</xsl:text>
                                </xsl:attribute>
                                <xsl:attribute name="code-name">
                                    <xsl:value-of select="SportName"/>
                                </xsl:attribute>
                                <xsl:attribute name="code-key">
                                    <xsl:value-of select="SportId"/>
                                </xsl:attribute>
                            </sports-content-code>  
                        </sports-content-codes>
                    </event-metadata>
                </sports-event>    
            </xsl:for-each>
            
            
        </sports-content>
    </xsl:template>
</xsl:stylesheet>