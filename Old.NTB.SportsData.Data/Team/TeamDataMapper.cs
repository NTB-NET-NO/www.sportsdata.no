﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The team data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Team
{
    /// <summary>
    /// The team data mapper.
    /// </summary>
    internal class TeamDataMapper
    {
    }
}