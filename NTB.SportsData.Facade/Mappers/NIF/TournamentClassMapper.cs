﻿using Glue;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.Mappers.Base;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Facade.Mappers.NIF
{
    /// <summary>
    /// The tournament class mapper.
    /// </summary>
    public class TournamentClassMapper : BaseMapper<TournamentClass, AgeCategory>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<TournamentClass, AgeCategory> mapper)
        {
            mapper.Relate(x => x.ActivityId, y => y.ActivityId);
            mapper.Relate(x => x.ClassId, y => y.CategoryId);
            mapper.Relate(x => x.ClassName, y => y.CategoryName);
            mapper.Relate(x => x.ToAge, y => y.MaxAge);
            mapper.Relate(x => x.FromAge, y => y.MinAge);
            mapper.Relate(x => x.Sex, y => y.Gender);
            mapper.Relate(x => x.OrgIdOwner, y => y.OrgId);
            mapper.Relate(x => x.IsValid, y => y.Valid);
        }
    }
}