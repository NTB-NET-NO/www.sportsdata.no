﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileClientController.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The sports controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Api.Controllers
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Web;
    using System.Web.Http;
    using System.Xml;

    using log4net;

    using NTB.SportsData.Api.Formatters;

    /// <summary>
    /// The sports controller.
    /// </summary>
    public class FileClientController : ApiController
    {
        /// <summary>
        ///     The _log.
        /// </summary>
        private readonly ILog log = LogManager.GetLogger(typeof(FileClientController));

        /// <summary>
        /// The odf.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// </returns>
        [HttpPost]
        [Route("Api/FileClient/OdfBinaryReceiver")]
        public HttpResponseMessage OdfBinaryReceiver([FromBody] string context)
        {
            try
            {
                var outputDirectory = ConfigurationManager.AppSettings["FileReceiverOutputDirectory"];
                this.log.DebugFormat("We are to deliver the file to folder {0}", outputDirectory);

                var di = new DirectoryInfo(outputDirectory);
                if (!di.Exists)
                {
                    di.Create();
                }

                File.WriteAllText(string.Format("{0}\\ODF_{1}.xml", outputDirectory, DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff")), context);

                return this.Request.CreateResponse(HttpStatusCode.OK, "value");
            }
            catch (Exception exception)
            {
                this.log.Error(exception);
                this.log.Error(exception.StackTrace);

                return this.Request.CreateResponse(HttpStatusCode.BadRequest, "value");
            }
        }

        /// <summary>
        /// The receive sports data.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// </returns>
        [HttpPost]
        [Route("Api/FileClient/OdfTextReceiver")]
        public HttpResponseMessage OdfTextReceiver([FromBody] string context)
        {
            try
            {
                var outputDirectory = ConfigurationManager.AppSettings["FileReceiverOutputDirectory"];
                this.log.DebugFormat("We are to deliver the file to folder {0}", outputDirectory);

                var di = new DirectoryInfo(outputDirectory);
                if (!di.Exists)
                {
                    di.Create();
                }

                File.WriteAllText(string.Format("{0}\\ODF_{1}.xml", outputDirectory, DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff")), context);

                return this.Request.CreateResponse(HttpStatusCode.OK, "value");
            }
            catch (Exception exception)
            {
                this.log.Error(exception);
                this.log.Error(exception.StackTrace);

                return this.Request.CreateResponse(HttpStatusCode.OK, "value");
                // return this.Request.CreateResponse(HttpStatusCode.BadRequest, "value");
            }
        }

        /// <summary>
        /// The receive sports data.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// </returns>
        [HttpPost]
        [Route("Api/FileClient/Odf")]
        public HttpResponseMessage Odf([FromBody] string context)
        {
            try
            {
                var outputDirectory = ConfigurationManager.AppSettings["FileReceiverOutputDirectory"];
                this.log.DebugFormat("We are to deliver the file to folder {0}", outputDirectory);

                var di = new DirectoryInfo(outputDirectory);
                if (!di.Exists)
                {
                    di.Create();
                }

                File.WriteAllText(string.Format("{0}\\ODF_{1}.xml", outputDirectory, DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff")), context);

                return this.Request.CreateResponse(HttpStatusCode.OK, "value");
            }
            catch (Exception exception)
            {
                this.log.Error(exception);
                this.log.Error(exception.StackTrace);

                return this.Request.CreateResponse(HttpStatusCode.OK, "value");
            }
        }
    }
}