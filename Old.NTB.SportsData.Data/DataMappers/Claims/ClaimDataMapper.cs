﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClaimsDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The claims data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Data.DataMappers.Claims
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;
    using System.Security.Claims;

    using log4net;

    using NTB.SportsData.Data.Interfaces.Common;
    using NTB.SportsData.Data.Interfaces.DataMappers;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The claims data mapper.
    /// </summary>
    public class ClaimDataMapper : IRepository<ApplicationUser>, IClaimDataMapper, IDisposable
    {
        #region Fields

        /// <summary>
        ///     The _logger.
        /// </summary>
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add user claim.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="claim">
        /// The claim.
        /// </param>
        public void AddUserClaim(ApplicationUser user, Claim claim)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsData_AddUserClaim", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", user.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@ClaimType", claim.Type));
                    sqlCommand.Parameters.Add(new SqlParameter("@ClaumValue", claim.Value));

                    sqlCommand.ExecuteNonQuery();

                    this._logger.Info("User Inserted successfully");

                    sqlCommand.Dispose();
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void Delete(ApplicationUser domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        ///     Not implemented
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ApplicationUser"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public ApplicationUser Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get all.
        /// </summary>
        /// <returns>
        ///     The <see cref="IQueryable" />.
        /// </returns>
        /// <exception cref="NotImplementedException">
        ///     Not implemented
        /// </exception>
        public IQueryable<ApplicationUser> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get user claims.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Claim> GetUserClaims(ApplicationUser user)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsData_GetUserClaims", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", user.Id));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new List<Claim>();
                    }

                    var claims = new List<Claim>();
                    while (sqlDataReader.Read())
                    {
                        var claim = new Claim(
                            sqlDataReader["ClaimType"].ToString(), 
                            sqlDataReader["ClaimValue"].ToString());
                        claims.Add(claim);
                    }

                    sqlCommand.Dispose();

                    return claims;
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }

                    return new List<Claim>();
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void InsertAll(List<ApplicationUser> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public int InsertOne(ApplicationUser domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The remove user claim.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="claim">
        /// The claim.
        /// </param>
        public void RemoveUserClaim(ApplicationUser user, Claim claim)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsData_RemoveUserClaim", sqlConnection)
                                         {
                                             CommandType =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", user.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@ClaimType", claim.Type));
                    sqlCommand.Parameters.Add(new SqlParameter("@ClaumValue", claim.Value));

                    sqlCommand.ExecuteNonQuery();

                    this._logger.Info("User Inserted successfully");

                    sqlCommand.Dispose();
                }
                catch (Exception exception)
                {
                    this._logger.Error(exception.Message);
                    this._logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        this._logger.Error(exception.InnerException.Message);
                        this._logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(ApplicationUser domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}