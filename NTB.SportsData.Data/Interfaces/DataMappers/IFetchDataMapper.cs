﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTB.SportsData.Domain.Classes;

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    public interface IFetchDataMapper
    {
        void UpdateTournamentFetchDateTime(int orgId);
        DataBaseFetch GetLatestTournamentFetchDateTimeByOrgId(int orgId);
    }
}
