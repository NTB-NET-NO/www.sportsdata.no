﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMatchDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The MatchDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NIF.Interfaces
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The MatchDataMapper interface.
    /// </summary>
    public interface IMatchDataMapper
    {
        /// <summary>
        /// The get matches by tournament id and sport id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<TournamentMatchExtended> GetMatchesByTournamentIdAndSportId(int tournamentId, int sportId);

        /// <summary>
        /// The get todays matches.
        /// </summary>
        /// <param name="matchDate">
        /// The match date.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<TournamentMatchExtended> GetTodaysMatches(DateTime matchDate, int seasonId);

        /// <summary>
        /// The get referee by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Referee> GetRefereeByMatchId(int matchId);

        /// <summary>
        /// The insert match.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <param name="federationUserId">
        /// The id of the federation
        /// </param>
        void UpdateMatch(TournamentMatchExtended match, int federationUserId);

        /// <summary>
        /// The update match.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <param name="federationUserId">
        /// The id of the federation
        /// </param>
        void InsertMatch(TournamentMatchExtended match, int federationUserId);

        /// <summary>
        /// The get matches by org id and season id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="seasonId">
        /// The seasonid.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<MatchInfo1> GetMatchesByOrgIdAndSeasonId(int orgId, int seasonId);
    }
}