﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserDistrictDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The user district data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.DataMappers.UserDistrict
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Data.Interfaces.Common;
    using NTB.SportsData.Data.Interfaces.DataMappers;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The user district data mapper.
    /// </summary>
    public class UserDistrictDataMapper : IRepository<UserDistrictMapper>, IUserDistrictDataMapper, IDisposable
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(UserDistrictDataMapper));

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(UserDistrictMapper domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertAll(List<UserDistrictMapper> domainobject)
        {
            foreach (var userDistrictMapper in domainobject)
            {
                this.InsertUserMap(userDistrictMapper);
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void Update(UserDistrictMapper domainobject)
        {
            // [SportsDataInput_DeleteUserDistrictMap]
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void Delete(UserDistrictMapper domainobject)
        {
            // Deleting work jobs
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteUserDistrictMap", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));

                    sqlCommand.Parameters.Add(new SqlParameter("@WorkDate", DateTime.Today.Date));

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Dispose();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<UserDistrictMapper> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="UserDistrictMapper"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public UserDistrictMapper Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete mapping by date.
        /// </summary>
        /// <param name="dateTime">
        /// The date time.
        /// </param>
        public void DeleteMappingByDate(DateTime dateTime)
        {
            // Deleting work jobs
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteAllUserDistrictMap", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@WorkDate", dateTime));

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Dispose();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert user map.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertUserMap(UserDistrictMapper domainobject)
        {
            foreach (var userId in domainobject.UserIds)
            {
                // now that we have the user, we can insert the rest of the information
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    try
                    {
                        if (sqlConnection.State == ConnectionState.Closed)
                        {
                            sqlConnection.Open();
                        }

                        // running the command
                        var sqlCommand = new SqlCommand("SportsDataInput_InsertUserDistrictMap", sqlConnection)
                                             {
                                                 CommandType
                                                     =
                                                     CommandType
                                                     .StoredProcedure
                                             };

                        sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));

                        sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                        sqlCommand.Parameters.Add(new SqlParameter("@WorkDate", DateTime.Today.Date));

                        sqlCommand.ExecuteNonQuery();

                        sqlCommand.Dispose();
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);

                        if (exception.InnerException != null)
                        {
                            Logger.Error(exception.InnerException.Message);
                            Logger.Error(exception.InnerException.StackTrace);
                        }
                    }
                    finally
                    {
                        if (sqlConnection.State == ConnectionState.Open)
                        {
                            sqlConnection.Close();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The get user by user id and district id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool GetUserByUserIdAndDistrictId(Guid userId, int districtId)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetWorkShiftUserByUserId", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", districtId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return false;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// The get all user sessions by user id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<DateTime> GetAllUserSessionsByUserId(Guid userId)
        {
            // GetAllUserSessionsByUserId
            // now that we have the user, we can insert the rest of the information
            var listOfDates = new List<DateTime>();

            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetAllUserSessionsByUserId", sqlConnection)
                                         {
                                             CommandType
                                                 =
                                                 CommandType
                                                 .StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return listOfDates;
                    }

                    while (sqlDataReader.Read())
                    {
                        var dateTime = Convert.ToDateTime(sqlDataReader["WorkDate"]);

                        listOfDates.Add(dateTime);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return listOfDates;
            }
        }
    }
}