﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerExtended.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The player extended.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Domain.Classes
{
    /// <summary>
    /// The player extended.
    /// </summary>
    public class PlayerExtended
    {
        /// <summary>
        /// Gets or sets the player id.
        /// </summary>
        public int? PlayerId { get; set; }

        /// <summary>
        /// Gets or sets the player name.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the sur name.
        /// </summary>
        public string SurName { get; set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// Gets or sets the person id.
        /// </summary>
        public int? PersonId { get; set; }

        /// <summary>
        /// Gets or sets the substituted with player id.
        /// </summary>
        public int? SubstitutedWithPlayerId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether debutant.
        /// </summary>
        public bool Debutant { get; set; }

        /// <summary>
        /// Gets or sets the position id.
        /// </summary>
        public int? PositionId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether substitute.
        /// </summary>
        public bool Substitute { get; set; }

        /// <summary>
        /// Gets or sets the player shirt number.
        /// </summary>
        public int? PlayerShirtNumber { get; set; }
    }
}