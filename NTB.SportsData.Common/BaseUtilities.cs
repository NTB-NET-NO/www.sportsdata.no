﻿using System;
using System.Collections.Generic;

namespace NTB.SportsData.Common
{
    public class BaseUtilities
    {
        public BaseUtilities()
        {
            ListOfDistrictAndUsers = new Dictionary<int, List<Guid>>();
        }

        public static Dictionary<int, List<Guid>> ListOfDistrictAndUsers { get; set; }

        public static Dictionary<int, Guid> SplitStringIntoDistrictAndGuid(string inputString, char splitCharacter)
        {
            string[] splitString = inputString.Split(splitCharacter);
            int district = Convert.ToInt32(splitString[0]);
            var userid = new Guid(splitString[1]);
            return new Dictionary<int, Guid> {{district, userid}};
        }

        public static Dictionary<int, List<Guid>> SplitStringIntoListOfUsers(string inputString, char splitCharacter)
        {
            string[] splitString = inputString.Split(splitCharacter);
            int district = Convert.ToInt32(splitString[0]);

            var userId = new Guid(splitString[1]);

            if (ListOfDistrictAndUsers == null)
            {
                ListOfDistrictAndUsers = new Dictionary<int, List<Guid>>();
            }

            if (ListOfDistrictAndUsers.ContainsKey(district))
            {
                var listOfUsers = new List<Guid>(ListOfDistrictAndUsers[district]);
                if (!listOfUsers.Contains(userId))
                {
                    listOfUsers.Add(userId);
                }

                ListOfDistrictAndUsers[district] = listOfUsers;
            }
            else
            {
                var listOfUsers = new List<Guid>
                {
                    userId
                };
                ListOfDistrictAndUsers.Add(district, listOfUsers);
            }

            return ListOfDistrictAndUsers;
        }
    }
}