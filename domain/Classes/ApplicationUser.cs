﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationUser.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved
// </copyright>
// <summary>
//   The application user.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Domain.Classes
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Security.Claims;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity;

    /// <summary>
    ///     The application user.
    /// </summary>
    public class ApplicationUser : IUser
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ApplicationUser" /> class.
        /// </summary>
        public ApplicationUser()
        {
            this.Id = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationUser"/> class.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        public ApplicationUser(string username)
            : this()
        {
            this.UserName = username;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the access failed count.
        /// </summary>
        public int AccessFailedCount { get; set; }

        /// <summary>
        ///     Gets or sets the application id.
        /// </summary>
        public Guid ApplicationId { get; set; }

        /// <summary>
        ///     Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether email confirmed.
        /// </summary>
        public bool EmailConfirmed { get; set; }

        /// <summary>
        ///     Gets or sets the first name.
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     Gets or sets the last login date.
        /// </summary>
        public DateTime LastLoginDate { get; set; }

        /// <summary>
        ///     Gets or sets the last name.
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether lockout enabled.
        /// </summary>
        public bool LockoutEnabled { get; set; }

        /// <summary>
        ///     Gets or sets the lockout end date utc.
        /// </summary>
        public DateTime? LockoutEndDateUtc { get; set; }

        /// <summary>
        ///     Gets or sets the lowered email.
        /// </summary>
        public string LoweredEmail { get; set; }

        /// <summary>
        ///     Gets or sets the password hash.
        /// </summary>
        public string PasswordHash { get; set; }

        /// <summary>
        ///     Gets or sets the phone number.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether phone number confirmed.
        /// </summary>
        public bool PhoneNumberConfirmed { get; set; }

        /// <summary>
        ///     Gets or sets the roles.
        /// </summary>
        public ICollection<IRole> Roles { get; set; }

        /// <summary>
        ///     Gets or sets the security stamp.
        /// </summary>
        public string SecurityStamp { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether two factor enabled.
        /// </summary>
        public bool TwoFactorEnabled { get; set; }

        /// <summary>
        ///     Gets or sets the user id.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///     Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The generate user identity async.
        /// </summary>
        /// <param name="manager">
        /// The manager.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            ClaimsIdentity userIdentity =
                await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            // Add custom user claims here
            return userIdentity;
        }

        #endregion
    }
}