﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITournamentDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TournamentDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NIF.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The TournamentDataMapper interface.
    /// </summary>
    public interface ITournamentDataMapper
    {
        /// <summary>
        /// The get tournament by municipalities.
        /// </summary>
        /// <param name="municipalities">
        /// The municipalities.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<TournamentSummary> GetTournamentByMunicipalities(List<string> municipalities, int seasonId, int orgId);

        /// <summary>
        /// The get tournament by municipalities.
        /// </summary>
        /// <param name="selectedRegions">
        /// The selected regions.
        /// </param>
        /// <param name="ageCategories">
        /// The age categories.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentByMunicipalities(List<Region> selectedRegions, List<ClassCode> ageCategories, int seasonId, int jobId, int orgId);

        /// <summary>
        /// The get tournament standing.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<TeamResult> GetTournamentStanding(int tournamentId);

        /// <summary>
        /// Method to get this seasons tournaments
        /// </summary>
        /// <param name="seasonId">
        /// Integer with season id
        /// </param>
        /// <returns>
        /// List of tournaments
        /// </returns>
        List<Tournament> GetTournamentsBySeasonId(int seasonId);

        /// <summary>
        /// The get tournament by id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        Tournament GetTournamentById(int tournamentId, int seasonId);

        /// <summary>
        /// The get tournaments by org id and season id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentsByOrgIdAndSeasonId(int orgId, int seasonId);

        List<TournamentSummary> GetTournamentSummariesBySeasonAndOrgId(int seasonId, int orgId);
    }
}