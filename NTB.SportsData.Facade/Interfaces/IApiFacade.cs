﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IApiFacade.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The ApiFacade interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The ApiFacade interface.
    /// </summary>
    public interface IApiFacade
    {
        /// <summary>
        /// The get match by match id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        MatchExtended GetMatchByMatchId(int id);

        /// <summary>
        /// The get match extended by match id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="MatchExtended"/>.
        /// </returns>
        MatchExtended GetMatchExtendedByMatchId(int id);

        #region Public Methods and Operators

        /// <summary>
        /// The hello world.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string HelloWorld();

        /// <summary>
        /// The get tournament standing.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<TournamentTable> GetTournamentStanding(Tournament tournament);

        /// <summary>
        /// The get team result standing.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<TeamResult> GetTeamResultStanding(Tournament tournament);

        /// <summary>
        /// The get tournament teams.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Team> GetTournamentTeams(int tournamentId);

        /// <summary>
        /// The get tournament matches.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Match> GetTournamentMatches(Tournament tournament);

        /// <summary>
        /// The get tournaments by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="seaonsId">
        /// The seaons Id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentsByOrgId(int orgId, int seaonsId);

        /// <summary>
        /// The get seasons by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Season> GetSeasonsByOrgId(int orgId);

        /// <summary>
        /// The get matches by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// /// 
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Match> GetMatchesByOrgId(int orgId, int seasonId);

        #endregion
    }
}