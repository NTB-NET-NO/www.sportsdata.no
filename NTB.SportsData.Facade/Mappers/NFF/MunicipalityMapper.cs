// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MunicipalityMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The municipality mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NFF
{
    using Glue;

    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The municipality mapper.
    /// </summary>
    internal class MunicipalityMapper : BaseMapper<Municipality, Domain.Classes.Municipality>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Municipality, Domain.Classes.Municipality> mapper)
        {
            mapper.Relate(x => x.Id, y => y.MunicipalityId);
            mapper.Relate(x => x.Name, y => y.MuniciaplityName);
        }
    }
}