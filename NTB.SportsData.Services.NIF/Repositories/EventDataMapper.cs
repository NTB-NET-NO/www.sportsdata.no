﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SportResultMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The sport result mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Services.NIF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Services.NIF.Interfaces;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The sport result mapper.
    /// </summary>
    public class EventDataMapper : IEventDataMapper
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(EventDataMapper));

        /// <summary>
        /// The result service client.
        /// </summary>
        private readonly EventServiceClient client = new EventServiceClient();

        /// <summary>
        ///     The organization service client.
        /// </summary>
        private readonly OrgServiceClient orgServiceClient = new OrgServiceClient();

        /// <summary>
        /// Initializes a new instance of the <see cref="EventDataMapper"/> class.
        /// </summary>
        public EventDataMapper()
        {
            if (this.client.ClientCredentials != null)
            {
                this.client.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NIFServicesUsername"];
                this.client.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NIFServicesPassword"];
            }

            if (this.orgServiceClient.ClientCredentials == null)
            {
                return;
            }

            this.orgServiceClient.ClientCredentials.UserName.UserName =
                ConfigurationManager.AppSettings["NIFServicesUsername"];
            this.orgServiceClient.ClientCredentials.UserName.Password =
                ConfigurationManager.AppSettings["NIFServicesPassword"];
        }

        /// <summary>
        /// The get events.
        /// </summary>
        /// <param name="eventDate">
        /// The event Date.
        /// </param>
        /// <param name="orgId">
        /// The org Id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<EventSearchResult> GetEvents(DateTime eventDate, int orgId)
        {
            if (orgId == 0)
            {
                return new List<EventSearchResult>();
            }

            var request = new EventSearchRequest { ActivityId = orgId, StartDate = eventDate, EndDate = eventDate };
            var response = this.client.SearchEvents(request);

            if (response.Success)
            {
                return response.Events.ToList();
            }

            throw new Exception(response.ErrorMessage);
        }
    }
}