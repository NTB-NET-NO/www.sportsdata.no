// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IndexViewModel.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Web.Models
{
    using System.Collections.Generic;

    using Microsoft.AspNet.Identity;

    /// <summary>
    ///     The index view model.
    /// </summary>
    public class IndexViewModel
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets a value indicating whether browser remembered.
        /// </summary>
        public bool BrowserRemembered { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether has password.
        /// </summary>
        public bool HasPassword { get; set; }

        /// <summary>
        ///     Gets or sets the logins.
        /// </summary>
        public IList<UserLoginInfo> Logins { get; set; }

        /// <summary>
        ///     Gets or sets the phone number.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether two factor.
        /// </summary>
        public bool TwoFactor { get; set; }

        #endregion
    }
}