﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPersonDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The PersonDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NIF.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The PersonDataMapper interface.
    /// </summary>
    public interface IPersonDataMapper
    {
        /// <summary>
        /// The get person by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Person"/>.
        /// </returns>
        Person GetPersonById(int id);

        /// <summary>
        /// The get simple person by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="SimplePerson"/>.
        /// </returns>
        SimplePerson GetSimplePersonById(int id);

        /// <summary>
        /// The get persons by match id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<SquadIndividual> GetPersonsByMatchId(int id);

        /// <summary>
        /// The get persons by team id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<SquadIndividual> GetPersonsByTeamId(int id);
    }
}