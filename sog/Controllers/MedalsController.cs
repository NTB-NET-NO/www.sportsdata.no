﻿using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using sog.Contexts;
using sog.Models.SportsMl;
using Formatting = Newtonsoft.Json.Formatting;

namespace sog.Controllers
{
    public class MedalsController : ApiController
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private readonly ILog logger = LogManager.GetLogger("SogApiFileAppender");

        /// <summary>
        /// Gets the medalists by country
        /// </summary>
        /// <param name="id">The country code</param>
        /// <returns>Either JSON or XML based on the Accept header in the request</returns>
        [HttpGet]
        [Route("~/api/Medals/GetMedalistsByCountry/{id}")]
        public HttpResponseMessage GetMedalistsByCountry(string id)
        {
            // Find out if this is a JSON or an XML request
            var accept = this.Request.Headers.Accept.ToString();

            var medalistsContext = new MedalistsContext();
            var files = medalistsContext.GetFiles(ConfigurationManager.AppSettings["medalistsinputpath"]);
            var distinctFiles = medalistsContext.GetMedalistsFiles(id, files);
            var distinctEvents = medalistsContext.GetDistinctEvents(distinctFiles);
            var sportsContents = medalistsContext.GetSportsContents(distinctEvents);
            var sportsContent = medalistsContext.GetMedalistSportsContent(id, sportsContents);

            var response = new HttpResponseMessage(HttpStatusCode.OK);

            if (accept == "")
            {
                accept = "text/xml";
            }

            if (accept == "application/json")
            {
                // The json return 
                var json = JsonConvert.SerializeObject(
                    sportsContent,
                    Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    });

                response.Content = new StringContent(json);

                return response;
            }

            var xmlserializer = new XmlSerializer(typeof(SportsContent));

            var settings = new XmlWriterSettings();
            settings.Encoding = new UnicodeEncoding(false, false); // no BOM in a .NET string
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            using (var textWriter = new StringWriter())
            {
                // 
                using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    xmlserializer.Serialize(xmlWriter, sportsContent);
                }

                response.Content = new StringContent(textWriter.ToString());
            }

            return response;
        }

        /// <summary>
        /// Get the Medal Statistic for the Olympic Games
        /// </summary>
        /// <returns>The medal standing from the Olympic Games. Either XML or JSON depending on the header accept parameter</returns>
        [HttpGet]
        [Route("~/api/Medals/GetMedalStatistic")]
        public HttpResponseMessage GetMedalStatistic()
        {
            // Find out if this is a JSON or an XML request
            var accept = this.Request.Headers.Accept.ToString();

            var medalsContext = new MedalContext();
            var xmlDoc = medalsContext.GetMedalsFromDisk();
            var sportsContent = medalsContext.GetSportsContent(xmlDoc);

            var response = new HttpResponseMessage(HttpStatusCode.OK);

            if (accept == "")
            {
                accept = "text/xml";
            }

            if (accept == "application/json")
            {
                // The json return 
                var json = JsonConvert.SerializeObject(
                    sportsContent,
                    Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    });

                response.Content = new StringContent(json);

                return response;
            }

            var xmlserializer = new XmlSerializer(typeof(SportsContent));

            var settings = new XmlWriterSettings();
            settings.Encoding = new UnicodeEncoding(false, false); // no BOM in a .NET string
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            using (var textWriter = new StringWriter())
            {
                // 
                using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    xmlserializer.Serialize(xmlWriter, sportsContent);
                }

                response.Content = new StringContent(textWriter.ToString());
            }

            return response;
        }
    }
}