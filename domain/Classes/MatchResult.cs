﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchResult.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match result.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Domain.Classes
{
    using System;

    /// <summary>
    /// The match result.
    /// </summary>
    public class MatchResult
    {
        /// <summary>
        /// Gets or sets the away team goals.
        /// </summary>
        public int? AwayTeamGoals { get; set; }

        /// <summary>
        /// Gets or sets the home team goals.
        /// </summary>
        public int? HomeTeamGoals { get; set; }

        /// <summary>
        /// Gets or sets the last change date.
        /// </summary>
        public DateTime? LastChangeDate { get; set; }

        /// <summary>
        /// Gets or sets the match id.
        /// </summary>
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the result id.
        /// </summary>
        public int ResultId { get; set; }

        /// <summary>
        /// Gets or sets the result type id.
        /// </summary>
        public int ResultTypeId { get; set; }

        /// <summary>
        /// Gets or sets the result type name.
        /// </summary>
        public string ResultTypeName { get; set; }
    }
}