﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTB.SportsData.Data.Interfaces.DataMappers.Referees
{
    using NTB.SportsData.Domain.Classes;

    public interface IRefereeDataMapper
    {
        List<ContactPerson> GetRefereeByMatchId(int matchId);
    }
}
