﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament remote data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Data.Tournaments
{
    using System.Collections.Generic;

    using NTB.SportsData.Data.Interfaces;
    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Interfaces;
    using NTB.SportsData.Facade.NFF.ApiAccess;
    using NTB.SportsData.Facade.NIF.ApiAccess;

    /// <summary>
    ///     The tournament remote data mapper.
    /// </summary>
    public class TournamentRemoteDataMapper : ITournamentRemoteDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get tournament standings.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TournamentTable> GetTournamentStandings(Tournament tournament)
        {
            IApiFacade facade;
            if (tournament.SportId == 16)
            {
                facade = new SoccerApiFacade();
            }
            else
            {
                facade = new SportApiFacade();
            }

            return facade.GetTournamentStanding(tournament);
        }

        #endregion
    }
}