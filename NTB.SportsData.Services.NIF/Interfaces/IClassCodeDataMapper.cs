﻿using System.Collections.Generic;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Services.NIF.Interfaces
{
    public interface IClassCodeDataMapper
    {
        List<ClassCode> GetClassCodes(int orgId);

        List<TournamentClass> GetClassCodesByOrgId(int orgId);

        List<TournamentClasses> GetClassCodesByTournamentId(int tournamentId);

        List<TournamentClass> GetClassCodesByClassIds(int[] classIds);
    }
}