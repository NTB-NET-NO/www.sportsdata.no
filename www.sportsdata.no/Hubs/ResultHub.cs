﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResultHub.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Hubs
{
    using System;

    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The result hub.
    /// </summary>
    [HubName("resulter")]
    public class ResultHub : Hub
    {
        /// <summary>
        /// The hello.
        /// </summary>
        public void Hello()
        {
            this.Clients.All.hello();
        }

        /// <summary>
        /// The receive result.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <param name="homeTeamGoal">
        /// The home team goal.
        /// </param>
        /// <param name="awayTeamGoal">
        /// The away team goal.
        /// </param>
        public void ReceiveResult(string matchId, string homeTeamGoal, string awayTeamGoal)
        {
            // Making sure we don't push empty notifications
            if (matchId == string.Empty)
            {
                return;
            }

            if (homeTeamGoal == string.Empty)
            {
                return;
            }

            if (awayTeamGoal == string.Empty)
            {
                return;
            }

            var matchResult = new Match();
            matchResult.MatchId = Convert.ToInt32(matchId);
            matchResult.HomeTeamGoal = Convert.ToInt32(homeTeamGoal);
            matchResult.AwayTeamGoal = Convert.ToInt32(awayTeamGoal);

            this.Clients.All.pushResult(matchResult);
        }
    }
}