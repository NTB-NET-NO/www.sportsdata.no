﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PersonMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The person mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;

    using Person = NTB.SportsData.Services.NIF.NIFProdService.Person;

    /// <summary>
    /// The person mapper.
    /// </summary>
    public class PersonMapper : BaseMapper<Person, ContactPerson>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Person, ContactPerson> mapper)
        {
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.LastName, y => y.LastName);
            mapper.Relate(x => x.HomeAddress.PhoneHome, y => y.HomePhone);
            mapper.Relate(x => x.HomeAddress.PhoneMobile, y => y.MobilePhone);
            mapper.Relate(x => x.HomeAddress.PhoneWork, y => y.OfficePhone);
            mapper.Relate(x => x.HomeAddress.Email, y => y.Email);
        }
    }
}