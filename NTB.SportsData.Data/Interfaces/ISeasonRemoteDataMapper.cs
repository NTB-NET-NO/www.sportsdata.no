﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISeasonRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The SeasonRemoteDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The SeasonRemoteDataMapper interface.
    /// </summary>
    public interface ISeasonRemoteDataMapper
    {
        /// <summary>
        /// The get federation season by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Season> GetFederationSeasonByOrgId(int orgId);
    }
}