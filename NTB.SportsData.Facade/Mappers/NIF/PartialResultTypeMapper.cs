﻿using Glue;
using NTB.SportsData.Common;
using NTB.SportsData.Services.NIF.NIFProdService;

namespace NTB.SportsData.Facade.Mappers.NIF
{
    public class PartialResultTypeMapper: BaseMapper<PartialResultType, Domain.Classes.PartialResultType>
    {
        protected override void SetUpMapper(Mapping<PartialResultType, Domain.Classes.PartialResultType> mapper)
        {
            mapper.Relate(x => x.PartialResultTypeId, y => y.TypeId);
            mapper.Relate(x => x.PartialResultTypeName, y => y.TypeName);
            mapper.Relate(x => x.ActivityId, y => y.ActivityId);
            mapper.Relate(x => x.OrgIdOwner, y => y.OrgId);
            mapper.Relate(x => x.Sorting, y => y.Sorting);
        }
    }
}
