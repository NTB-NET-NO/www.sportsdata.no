﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMatchDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The MatchDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The MatchDataMapper interface.
    /// </summary>
    public interface IMatchDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        MatchExtended Get(int id);

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="tournament">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        IEnumerable<Match> GetAll(Tournament tournament);

        /// <summary>
        /// Get the customers matches for today
        /// </summary>
        /// <param name="customerId">
        /// integer holding the customer id
        /// </param>
        /// <returns>
        /// list of matches
        /// </returns>
        IEnumerable<Match> GetTodaysMatches(int customerId);

        #endregion
    }
}