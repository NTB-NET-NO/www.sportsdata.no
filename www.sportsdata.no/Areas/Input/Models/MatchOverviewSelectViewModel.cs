﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using log4net;
using NTB.SportsData.Data.DataMappers.Seasons;
using NTB.SportsData.Data.DataMappers.Sports;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.NFF.TournamentAccess;
using NTB.SportsData.Facade.NIF.SportAccess;

namespace NTB.SportsData.Web.Areas.Input.Models
{
    public class MatchOverviewSelectViewModel
    {
        private readonly object _lockMe = new object();

        private readonly ILog _log = LogManager.GetLogger(typeof(MatchOverviewSelectViewModel));

        public SelectListItem SelectedMonth { get; set; }

        public IEnumerable<SelectListItem> Months
        {
            get
            {
                return DateTimeFormatInfo
                    .InvariantInfo
                    .MonthNames
                    .Select((monthName, index) => new SelectListItem
                    {
                        Value = (index + 1).ToString(),
                        Text = monthName
                    });
            }
        }

        public Dictionary<DateTime, int> GetTotalMatchesForSelectedMonth(int selectedMonth)
        {
            this._log.Debug("In GetTotalMatchesForSelectedMonth. Starting to fetch");
            var dataMapper = new SportDataMapper();
            var seasonDataMapper = new SeasonDataMapper();

            var sports = dataMapper.GetAll();

            var matches = new List<Match>();

            try
            {
                Parallel.ForEach(
                    sports, (currentSport) =>
                    {
                        lock (this._lockMe)
                        {
                            this._log.DebugFormat("Sport Id: {0}", currentSport.Id);
                        }
                        if (currentSport.Id == 16)
                        {
                            return;
                        }

                        if (currentSport.Id == 227)
                        {
                            return;
                        }

                        if (currentSport.Id == 38)
                        {
                            return;
                        }

                        if (currentSport.Id == 36)
                        {
                            return;
                        }

                        var seasons = seasonDataMapper.GetAll()
                            .Where(x => x.SeasonActive);

                        if (!seasons.Any())
                        {
                            return;
                        }

                        var sportFacade = new SportFacade();
                        var soccerFacade = new TournamentFacade();

                        foreach (var currentSeason in seasons)
                        {
                            lock (this._lockMe)
                            {
                                this._log.DebugFormat("Season Id: {0}, Sport Id: {1}", currentSeason.SeasonId, currentSeason.SportId);
                            }
                            if (currentSeason.SportId == 16)
                            {
                                // We shall do the soccer stuff
                                var tournament = soccerFacade.GetTournamentsBySeasonId(currentSeason.SeasonId);
                                continue;
                            }
                            if (currentSeason.SportId == 227)
                            {
                                continue;
                            }

                            if (currentSeason.SportId == 38)
                            {
                                continue;
                            }

                            if (currentSeason.SportId == 36)
                            {
                                continue;
                            }

                            var tournaments = sportFacade.GetTournamentsBySeasonId(currentSeason.SeasonId);

                            foreach (var tournament in tournaments)
                            {
                                var currentTournament = tournament;
                                // In production we must get the ageCategoryDefinitionId for this tournament and not use filters
                                if (!(
                                    currentTournament.TournamentName.ToLower().Contains("kvinner") || 
                                    currentTournament.TournamentName.ToLower().Contains("menn") || 
                                    currentTournament.TournamentName.ToLower().Contains("k0") || 
                                    currentTournament.TournamentName.ToLower().Contains("m0")))
                                {
                                    lock (this._lockMe)
                                    {
                                        this._log.Debug("Not a senior tournament. We will continue");
                                    }
                                    continue;
                                }

                                try
                                {
                                    var remoteMatches = sportFacade.GetMatchesByTournamentIdAndSportId(tournament.TournamentId, tournament.SportId);

                                    lock (this._lockMe)
                                    {
                                        this._log.Debug("Adding matches");
                                        matches.AddRange(remoteMatches.ToList());
                                    }
                                }
                                catch (Exception exception)
                                {
                                    // Something
                                    lock (this._lockMe)
                                    {
                                        this._log.Error(exception);
                                    }
                                    
                                }
                            }
                        }
                    });
                lock (this._lockMe)
                {
                    this._log.Debug("Done fetching in parallell. Now doing the finishing touches");
                }
                var selectedDate = DateTime.Today.AddMonths(1);
                var month = selectedDate.Month;

                var year = selectedDate.Year;

                var listOfMatchesPerDay = new Dictionary<DateTime, int>();
                foreach (var date in AllDatesInMonth(year, month))
                {
                    var matchesCount = matches.Count(x => x.MatchDate == date.Date);
                    listOfMatchesPerDay.Add(date.Date, matchesCount);
                }

                lock (this._lockMe)
                {
                    this._log.Debug("Returning the list of matches per day");
                }
                return listOfMatchesPerDay;
            }
            catch (Exception exception)
            {
                //this.txtOutput.Text += exception.Message;
                //this.txtOutput.Text += exception.StackTrace;

                //if (exception.InnerException != null)
                //{
                //    this.txtOutput.Text += exception.InnerException.Message;
                //    this.txtOutput.Text += exception.InnerException.StackTrace;
                //}

                //this.txtOutput.Refresh();
                this._log.Error(exception);
                return null;
            }
        }

        public static IEnumerable<DateTime> AllDatesInMonth(int year, int month)
        {
            var days = DateTime.DaysInMonth(year, month);
            for (var day = 1; day <= days; day++)
            {
                yield return new DateTime(year, month, day);
            }
        }
    }
}