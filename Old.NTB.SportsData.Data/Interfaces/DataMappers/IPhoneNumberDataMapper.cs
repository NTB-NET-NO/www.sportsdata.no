﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPhoneNumberDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The PhoneNumberDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    /// <summary>
    ///     The PhoneNumberDataMapper interface.
    /// </summary>
    internal interface IPhoneNumberDataMapper
    {
    }
}