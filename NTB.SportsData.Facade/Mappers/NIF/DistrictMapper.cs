﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DistrictMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The district mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Facade.Mappers.Base;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The district mapper.
    /// </summary>
    public class DistrictMapper : BaseMapper<Region, District>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Region, District> mapper)
        {
            mapper.Relate(x => x.RegionId, y => y.DistrictId);
            mapper.Relate(x => x.RegionName, y => y.DistrictName);
        }
    }
}