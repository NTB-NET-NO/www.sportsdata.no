﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SeasonRepository.cs" company="">
//   
// </copyright>
// <summary>
//   The season repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NIF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using Interfaces;
    using NIFProdService;

    /// <summary>
    /// The season repository.
    /// </summary>
    public class SeasonRepository : IRepository<Season>, IDisposable, ISeasonDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Season domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Season"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Season Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<Season> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get federation season by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Season> GetFederationSeasonsByOrgId(int orgId)
        {
            var serviceClient = new SeasonServiceClient();

            if (serviceClient.ClientCredentials != null)
            {
                serviceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NIFServicesUsername"];
                serviceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NIFServicesPassword"];
            }

            // System.Net.ServicePointManager.ServerCertificateValidationCallback =
            // (servicePointSender, certificate, chain, sslPolicyErrors) => true; // Ignoring cert
            var request = new SeasonRequest { OrgId = orgId };

            var response = serviceClient.GetFederationSeasons(request);

            serviceClient.Close();

            return response.Success ? new List<Season>(response.Season) : new List<Season>();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<Season> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(Season domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Season domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}