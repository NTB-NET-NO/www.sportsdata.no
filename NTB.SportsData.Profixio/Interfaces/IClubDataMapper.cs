﻿using System.Collections.Generic;
using NTB.SportsData.Services.Profixio.ProfixioServiceReference;

namespace NTB.SportsData.Services.Profixio.Interfaces
{
    public interface IClubDataMapper
    {
        List<TournamentClub> GetClubsByTournament(int tournamentId);
        List<MatchGroup> GetTournamentByClubs(List<int> clubIds, int tournamentId);
    }
}
