﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITeamRoleDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TeamRoleDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The TeamRoleDataMapper interface.
    /// </summary>
    public interface ITeamRoleDataMapper
    {
        /// <summary>
        /// The delete team role.
        /// </summary>
        /// <param name="functionId">
        /// The function id.
        /// </param>
        void DeleteTeamRole(int functionId);

        /// <summary>
        /// The insert team role.
        /// </summary>
        /// <param name="function">
        /// The function.
        /// </param>
        void InsertTeamRole(Function function);

        /// <summary>
        /// The get team role by id.
        /// </summary>
        /// <param name="functionId">
        /// The function id.
        /// </param>
        /// <returns>
        /// The <see cref="Function"/>.
        /// </returns>
        Function GetTeamRoleById(int functionId);
    }
}