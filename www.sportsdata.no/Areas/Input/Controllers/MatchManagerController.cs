﻿using System;
using System.Web.Mvc;
using NTB.SportsData.Web.Areas.Input.Models;

namespace NTB.SportsData.Web.Areas.Input.Controllers
{
    /// <summary>
    /// The match manager controller.
    /// </summary>
    [Authorize(Roles = "Administrator,Bruker")]
    public class MatchManagerController : Controller
    {
        // GET: /MatchManager/

        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            var model = new MatchManagementModels();
            if (model.GetMatchManagementListView() != null)
            {
                return this.View(model.GetMatchManagementListView());
            }

            return this.View("NoData");
        }

        // GET: /MatchManager/Details/5

        /// <summary>
        /// The details.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Details(int id)
        {
            return this.View();
        }

        // GET: /MatchManager/Create

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Create()
        {
            return this.View();
        }

        // POST: /MatchManager/Create

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        // GET: /MatchManager/Edit/5

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Edit(int id)
        {
            return this.View();
        }

        // POST: /MatchManager/Edit/5

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        // GET: /MatchManager/Delete/5

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Delete(int id)
        {
            return this.View();
        }

        // POST: /MatchManager/Delete/5

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }


        public ActionResult MatchOverview()
        {
            return this.View();
        }

        [HttpPost]
        public ActionResult GetMatchOverview(FormCollection collection)
        {
            var selectedItem = Convert.ToInt32(collection["month"]);
            var viewModel = new MatchOverviewSelectViewModel();
            var model = viewModel.GetTotalMatchesForSelectedMonth(selectedItem);
            return this.View(model);
        }



        /// <summary>
        /// The job setup.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult JobSetup(FormCollection collection)
        {
            var model = new MatchManagementModels();
            model.StoreJobs(collection);

            return this.RedirectToAction("Index");
        }
    }
}