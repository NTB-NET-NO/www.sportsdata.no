﻿using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.NFF.TournamentAccess;
using NTB.SportsData.Web.Areas.Admin.Repositories;

namespace NTB.SportsData.Web.Areas.Admin.Models
{
    public class DistrictModel
    {
        /// <summary>
        ///     Method to get the districts used.
        ///     If not found in the database, then we will use the services
        /// </summary>
        /// <returns></returns>
        public List<District> GetDistricts()
        {
            var repository = new DistrictRepository();
            var districts = new List<District>(repository.GetAll());

            if (districts.Count == 0)
            {
                districts.Clear();

                var facade = new TournamentFacade();
                
                districts.AddRange(facade.GetDistricts());
            }

            return districts;
        }
    }
}