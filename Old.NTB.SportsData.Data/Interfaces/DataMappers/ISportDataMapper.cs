﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISportDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The SportDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces.DataMappers
{
    /// <summary>
    /// The SportDataMapper interface.
    /// </summary>
    public interface ISportDataMapper
    {
        /// <summary>
        /// The get sport id by season id.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int GetSportIdBySeasonId(int seasonId);

        /// <summary>
        /// The get sport id from org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int GetSportIdFromOrgId(int orgId);
    }
}