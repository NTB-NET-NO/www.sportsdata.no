<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:i="http://www.w3.org/2001/XMLSchema-instance" 
    
    exclude-result-prefixes="xs i"
    
    version="1.0">
    <xsl:output method="text" indent="yes"/>
    <xsl:variable name="newline">
        <xsl:text>
        </xsl:text>
    </xsl:variable>
    
    <xsl:template match="/">
        <xsl:text>{"standing": </xsl:text>
        <xsl:value-of select="$newline"/>
        <xsl:text>{</xsl:text>
        <xsl:text>"competitionname": </xsl:text>
        <xsl:text>"</xsl:text>
        <xsl:value-of select="SportsData/TournamentName"/>
        <xsl:text>",</xsl:text>
        <xsl:value-of select="$newline"/>
        <xsl:text>"competitionid": </xsl:text>
        <xsl:value-of select="SportsData/TournamentId"/>
        <xsl:text>,</xsl:text>
        <xsl:value-of select="$newline"/>
        <xsl:text>"generateddate": "</xsl:text>
        <xsl:value-of select="SportsData/DocumentGenerated/@value"/>
        <xsl:text>",</xsl:text>
        <xsl:value-of select="$newline"/>
        <xsl:text>"teams": [</xsl:text>
        <xsl:value-of select="$newline"/>
        <xsl:for-each select="SportsData/TournamentTables/TeamResult">
            <xsl:sort select="Sort" order="ascending" data-type="number"/>
            <xsl:text>{</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"teamid": </xsl:text>
            <xsl:value-of select="OrgId"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"teamname": </xsl:text>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="OrgName"/>
            <xsl:text>"</xsl:text>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"rank": </xsl:text>
            <xsl:value-of select="Position"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"totalplayed": </xsl:text>
            <xsl:value-of select="TotalMatches"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"totalwins": </xsl:text>
            <xsl:value-of select="Victories"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"totalties": </xsl:text>
            <xsl:value-of select="Draws"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"totallosses": </xsl:text>
            <xsl:value-of select="Losses"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"totalpoints": </xsl:text>
            <xsl:value-of select="TotalPoints"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"points_scored_for": </xsl:text>
            <xsl:value-of select="GoalsConcededHome"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"points_scored_against": </xsl:text>
            <xsl:value-of select="GoalsConcededAway"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"points_difference": </xsl:text>
            <xsl:value-of select="GoalDifference"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"homeplayed": </xsl:text>
            <xsl:value-of select="MatchesHome"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"homewins": </xsl:text>
            <xsl:value-of select="VictoriesHome"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"hometies": </xsl:text>
            <xsl:value-of select="DrawsHome"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"homelosses": </xsl:text>
            <xsl:value-of select="LossesHome"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"awayplayed": </xsl:text>
            <xsl:value-of select="MatchesAway"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"awaywins": </xsl:text>
            <xsl:value-of select="VictoriesAway"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"awayties": </xsl:text>
            <xsl:value-of select="DrawsAway"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newline"/>
            <xsl:text>"awaylosses": </xsl:text>
            <xsl:value-of select="LossesAway"/>
            <xsl:value-of select="$newline"/>
            <xsl:text>}</xsl:text>
            
            <xsl:value-of select="$newline"/>
            <xsl:if test="position() != last()">
                <xsl:text>,</xsl:text>
                <xsl:value-of select="$newline"/>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>]</xsl:text>
        <xsl:value-of select="$newline"/>
        <xsl:text>}</xsl:text>
        <xsl:value-of select="$newline"/>
        <xsl:text>}</xsl:text>
        <xsl:value-of select="$newline"/>
    </xsl:template>
</xsl:stylesheet>