﻿using System.Configuration;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using sog.Models.SportsMl;

namespace sog.Contexts
{
    public class MedalContext
    {
        public FileInfo GetLatestFile(string path)
        {
            var di = new DirectoryInfo(path);
            var files = di.GetFiles("*.xml");

            if (files.Length == 0)
            {
                return null;
            }

            return (from f in files
                              orderby f.LastWriteTime descending
                              select f).First();
            
        }

        public XmlDocument GetMedalsFromDisk()
        {
            var medalsPath = ConfigurationManager.AppSettings["medalinputpath"];

            var medalsFile = this.GetLatestFile(medalsPath);

            var xmlDoc = new XmlDocument();
            xmlDoc.Load(medalsFile.FullName);

            return xmlDoc;
        }

        public SportsContent GetSportsContent(XmlDocument xmlDoc)
        {
            // now we shall serialize
            using (var stringReader = new StringReader(xmlDoc.InnerXml))
            {
                using (var xmlReader = XmlReader.Create(stringReader))
                {
                    var startElement = "sports-content";

                    xmlReader.ReadToDescendant(startElement);

                    var xmlserializer = new XmlSerializer(typeof(SportsContent), new XmlRootAttribute(startElement));

                    return (SportsContent) xmlserializer.Deserialize(xmlReader.ReadSubtree());
                }
            }
        }
    }
}