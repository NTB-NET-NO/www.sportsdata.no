﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamContext.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The team context.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Models.Teams
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Api.Interfaces;
    using NTB.SportsData.Domain.Classes;

    /// <summary>
    ///     The team context.
    /// </summary>
    public class TeamContext : ITeamDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get tournament teams.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public List<Team> GetTournamentTeams(int tournamentId, int sportId)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}