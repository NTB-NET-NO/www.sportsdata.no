using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Web.Areas.Admin.Interfaces;

namespace NTB.SportsData.Web.Areas.Admin.Repositories
{
    /// <summary>
    ///     The season repository.
    /// </summary>
    public class SeasonRepository : IRepository<Season>, IDisposable, ISeasonDataMapper
    {
        #region Static Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SeasonRepository));

        #endregion

        #region Constructors and Destructors

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The activate season.
        /// </summary>
        /// <param name="seasonId">
        ///     The season id.
        /// </param>
        public void ActivateSeason(int seasonId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"SportsData_ActivateSeasonBySeasonId", sqlConnection) {CommandType = CommandType.StoredProcedure};

                sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", seasonId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        ///     The de activate season.
        /// </summary>
        /// <param name="seasonId">
        ///     The season id.
        /// </param>
        public void DeActivateSeason(int seasonId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"SportsData_DeActivateSeasonBySeasonId", sqlConnection) {CommandType = CommandType.StoredProcedure};

                sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", seasonId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        ///     Delete one season from databsae
        /// </summary>
        /// <param name="domainobject">
        ///     Domain object containing at least the season id
        /// </param>
        public void Delete(Season domainobject)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"SportsData_DeleteSeasonBySeasonId", sqlConnection) {CommandType = CommandType.StoredProcedure};

                sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));
                sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        ///     The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Returns one season from the database
        /// </summary>
        /// <param name="id">
        /// </param>
        /// <returns>
        ///     The <see cref="Season" />.
        /// </returns>
        public Season Get(int id)
        {
            try
            {
                if (id == 0)
                {
                    return new Season();
                }

                Logger.DebugFormat("We are trying to find season with season id {0}", id);
                
                var seasons = this.GetAll();
                foreach (var season in seasons)
                {
                    Logger.DebugFormat("Season {0} with id {1}", season.SeasonName, season.SeasonId);

                    if (season.SeasonId == id)
                    {
                        Logger.Debug("Found!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        return season;
                    }
                }

                // return GetAll().Single(s => s.SeasonId == id);
                return new Season();
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return new Season();
            }
        }

        /// <summary>
        ///     Get all seasons from the database
        /// </summary>
        /// <returns>
        ///     The <see cref="IQueryable" />.
        /// </returns>
        public IQueryable<Season> GetAll()
        {
            var seasons = new List<Season>();

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"SportsData_GetSeasons", sqlConnection) {CommandType = CommandType.StoredProcedure};

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", DBNull.Value));

                sqlConnection.Open();

                using (var sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["SportId"] == DBNull.Value)
                        {
                            continue;
                        }

                        var season = new Season
                        {
                            SeasonActive = true   
                        };

                        season.SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]);
                        season.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        season.SeasonName = sqlDataReader["SeasonName"].ToString();
                        season.SeasonStartDate = Convert.ToDateTime(sqlDataReader["SeasonStartDate"]);
                        season.SeasonEndDate = Convert.ToDateTime(sqlDataReader["SeasonEndDate"]);
                        season.OrgId = Convert.ToInt32(sqlDataReader["OrgId"]);

                        if (sqlDataReader["DisciplineId"] != DBNull.Value)
                        {
                            season.FederationDiscipline = Convert.ToInt32(sqlDataReader["DisciplineId"]);
                        }

                        var seasonActive = sqlDataReader["SeasonActive"].ToString()
                            .ToLower();
                        if (seasonActive == "false" || seasonActive == "0")
                        {
                            season.SeasonActive = false;
                        }

                        if (season.SportId == 4)
                        {
                            // Just adding a breakpoint
                            // var foo = 12;
                        }

                        seasons.Add(season);
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();
            }

            Logger.InfoFormat("Dataset contains {0} items", seasons.Count);

            return seasons.AsQueryable();
        }

        /// <summary>
        ///     The get season.
        /// </summary>
        /// <param name="season">
        ///     The season.
        /// </param>
        /// <returns>
        ///     The <see cref="Season" />.
        /// </returns>
        public Season GetSeason(Season season)
        {
            try
            {
                Logger.DebugFormat("We are trying to find season with season id {0} and sportid {1}", season.SeasonId, season.SportId);
                var seasons = this.GetAll();

                // We are returning the object we found in the database, as the season object we inject may or may not be complete
                return seasons.SingleOrDefault(x => x.SportId == season.SportId && x.SeasonId == season.SeasonId);

                // Code below is marked out as the LINQ-syntax above does the same thing

                //foreach (var databaseSeason in seasons)
                //{
                //    Logger.DebugFormat("Season {0} with id {1} and sport {2}", season.SeasonName, season.SeasonId, season.SportId);

                //    if (season.SeasonId == databaseSeason.SeasonId && season.SportId == databaseSeason.SportId)
                //    {
                //        // Returning the databaseSeason object we've found 
                //        return databaseSeason;
                //    }
                //}

                // return GetAll().Single(s => s.SeasonId == id);
                // return new Season();
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return new Season();
            }
        }

        /// <summary>
        ///     The get seasons by org id.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Season> GetSeasonsByOrgId(int id)
        {
            var seasons = new List<Season>();

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand(@"SportsData_GetSeasonsByOrgId", sqlConnection) {CommandType = CommandType.StoredProcedure};

                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return seasons;
                    }

                    while (sqlDataReader.Read())
                    {
                        var season = new Season
                        {
                            SeasonActive = true
                        };
                        season.SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]);
                        season.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        season.SeasonName = sqlDataReader["SeasonName"].ToString();
                        season.SeasonStartDate = Convert.ToDateTime(sqlDataReader["SeasonStartDate"].ToString());
                        season.SeasonEndDate = Convert.ToDateTime(sqlDataReader["SeasonEndDate"].ToString());
                        season.FederationDiscipline = sqlDataReader["DisciplineId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DisciplineId"]);
                        season.OrgId = id;


                        // season.FederationDiscipline = sqlDataReader["DisciplineId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DisciplineId"]);
                        var seasonactive = sqlDataReader["SeasonActive"].ToString()
                            .ToLower();
                        if (seasonactive == "false" || seasonactive == "0")
                        {
                            season.SeasonActive = false;
                        }

                        seasons.Add(season);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return seasons;
        }

        /// <summary>
        ///     The get seasons by sport id.
        /// </summary>
        /// <param name="sportId">
        ///     The sport id.
        /// </param>
        /// <returns>
        ///     The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Season> GetSeasonsBySportId(int sportId)
        {
            var seasons = new List<Season>();

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"SportsData_GetSeasons", sqlConnection) {CommandType = CommandType.StoredProcedure};

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                sqlConnection.Open();

                using (var sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        var season = new Season
                        {
                            SeasonActive = true
                        };

                        // Doing it this way because it is easier to find out where something goes wrong!
                        season.SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]);
                        season.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        season.SeasonName = sqlDataReader["SeasonName"].ToString();
                        season.SeasonStartDate = Convert.ToDateTime(sqlDataReader["SeasonStartDate"].ToString());
                        season.FederationDiscipline = sqlDataReader["DisciplineId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DisciplineId"]);
                        season.SeasonEndDate = Convert.ToDateTime(sqlDataReader["SeasonEndDate"].ToString());
                        season.OrgId = Convert.ToInt32(sqlDataReader["OrgId"]);

                        var seasonactive = sqlDataReader["SeasonActive"].ToString()
                            .ToLower();
                        if (seasonactive == "false" || seasonactive == "0")
                        {
                            season.SeasonActive = false;
                        }

                        seasons.Add(season);
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();
            }

            return seasons;
        }

        /// <summary>
        ///     The insert all.
        /// </summary>
        /// <param name="domainobject">
        ///     The domainobject.
        /// </param>
        public void InsertAll(List<Season> domainobject)
        {
            foreach (var season in domainobject)
            {
                this.InsertOne(season);
            }
        }

        /// <summary>
        ///     The insert one.
        /// </summary>
        /// <param name="domainobject">
        ///     The domainobject.
        /// </param>
        /// <returns>
        ///     The <see cref="int" />.
        /// </returns>
        public int InsertOne(Season domainobject)
        {
            Logger.Info("About to insert " + domainobject.SeasonName);
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand("SportsData_InsertSeason", sqlConnection);

                    if (domainobject.SeasonStartDate < DateTime.Parse("1/1/1753"))
                    {
                        domainobject.SeasonStartDate = DateTime.Today;
                    }

                    if (domainobject.SeasonEndDate < DateTime.Parse("1/1/1753"))
                    {
                        domainobject.SeasonEndDate = DateTime.Today;
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonName", domainobject.SeasonName));
                    sqlCommand.Parameters.Add(new SqlParameter("@StartDate", domainobject.SeasonStartDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@EndDate", domainobject.SeasonEndDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonActive", domainobject.SeasonActive));
                    sqlCommand.Parameters.Add(new SqlParameter("@DisciplineId", domainobject.FederationDiscipline));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Parameters.Clear();

                    sqlCommand.CommandText = "SELECT @@IDENTITY";
                    sqlCommand.CommandType = CommandType.Text;

                    // int insertId = Convert.ToInt32(sqlCommand.ExecuteScalar());
                    sqlCommand.Dispose();

                    Logger.Info(domainobject.SeasonName + " inserted successfully");

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlCommand.Connection.Close();
                    }

                    return 1;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return 0;
            }
        }

        /// <summary>
        ///     The update.
        /// </summary>
        /// <param name="domainobject">
        ///     The domainobject.
        /// </param>
        public void Update(Season domainobject)
        {
            Logger.Info("About to update " + domainobject.SeasonName);
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand("SportsData_UpdateSeason", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonName", domainobject.SeasonName));
                    sqlCommand.Parameters.Add(new SqlParameter("@StartDate", domainobject.SeasonStartDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@EndDate", domainobject.SeasonEndDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonActive", domainobject.SeasonActive.ToString()));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    Logger.Info(domainobject.SeasonName + " updated successfully");

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlCommand.Connection.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }

        /// <summary>
        ///     The update season with discipline.
        /// </summary>
        /// <param name="seasonId">
        ///     The season id.
        /// </param>
        /// <param name="disciplineId">
        ///     The discipline id.
        /// </param>
        public void UpdateSeasonWithDiscipline(int seasonId, int disciplineId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand(@"SportsData_UpdateSeasonDiscipline", sqlConnection) {CommandType = CommandType.StoredProcedure};

                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", seasonId));

                    sqlCommand.Parameters.Add(new SqlParameter("@DisciplineId", disciplineId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        #endregion
    }
}