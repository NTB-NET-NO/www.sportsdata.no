// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Facade.Mappers.NIF
{
    using Glue;

    using Base;
    using Services.NIF.NIFProdService;

    /// <summary>
    /// The tournament mapper.
    /// </summary>
    public class TournamentMapper : BaseMapper<Tournament, Domain.Classes.Tournament>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Tournament, Domain.Classes.Tournament> mapper)
        {
            mapper.Relate(x => x.ClassCodeId, y => y.AgeCategoryId);
            mapper.Relate(x => x.Division, y => y.Division);
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.ActivityId, y => y.SportId);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentName, y => y.TournamentName);
            mapper.Relate(x => x.TournamentNo, y => y.TournamentNumber);
        }
    }
}