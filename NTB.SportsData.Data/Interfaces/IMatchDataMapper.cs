﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMatchDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The MatchDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Data.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Domain.Classes;

    /// <summary>
    /// The MatchDataMapper interface.
    /// </summary>
    public interface IMatchDataMapper
    {
        /// <summary>
        /// The get matches by tournament id.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Match> GetMatchesByTournamentId(Tournament tournament);

        /// <summary>
        /// The get match by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        MatchExtended GetMatchById(int id);

        /// <summary>
        /// The get todays matches by customer id.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Match> GetTodaysMatchesByCustomerId(int customerId);

        /// <summary>
        /// The get match extended by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="MatchExtended"/>.
        /// </returns>
        MatchExtended GetMatchExtendedById(int id);
    }
}