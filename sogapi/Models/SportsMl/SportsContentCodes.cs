﻿using System.Xml.Serialization;

namespace sogapi.Models.SportsMl
{
    /// <summary>
    /// The sports content codes.
    /// </summary>
    [XmlRoot("sports-content-codes")]
    public class SportsContentCodes
    {
        /// <summary>
        /// Gets or sets the sports content code.
        /// </summary>
        [XmlElement("sports-content-code")]
        public SportsContentCode[] SportsContentCode { get; set; }
    }
}