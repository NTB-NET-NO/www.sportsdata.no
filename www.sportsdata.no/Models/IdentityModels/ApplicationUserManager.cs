﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationUserManager.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The application user manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Web.Models.IdentityModels
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;

    using Microsoft.Ajax.Utilities;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin;

    using Data.DataMappers.User.Web;
    using Data.Interfaces.DataMappers;
    using Domain.Classes;

    /// <summary>
    /// The application user manager.
    /// </summary>
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        /// <summary>
        /// The _datamapper.
        /// </summary>
        private IUserDataMapper _datamapper;

        // public UserManager() : base(new UserStore<User>(new ApplicationDbContext()))
        // {
        // this.PasswordHasher = new SqlPasswordHasher();
        // }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationUserManager"/> class.
        /// </summary>
        /// <param name="store">
        /// The store.
        /// </param>
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="options">
        /// The options.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="ApplicationUserManager"/>.
        /// </returns>
        public static ApplicationUserManager Create(
            IdentityFactoryOptions<ApplicationUserManager> options, 
            IOwinContext context)
        {
            var manager = new ApplicationUserManager(new ApplicationUserStore());

            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
                                        {
                                            AllowOnlyAlphanumericUserNames = false, 
                                            RequireUniqueEmail = true
                                        };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
                                            {
                                                RequiredLength = 6, 
                                                RequireNonLetterOrDigit = true, 
                                                RequireDigit = true, 
                                                RequireLowercase = true, 
                                                RequireUppercase = true, 
                                            };

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug in here.
            manager.RegisterTwoFactorProvider(
                "PhoneCode", 
                new PhoneNumberTokenProvider<ApplicationUser> { MessageFormat = "Your security code is: {0}" });
            manager.RegisterTwoFactorProvider(
                "EmailCode", 
                new EmailTokenProvider<ApplicationUser>
                    {
                        Subject = "Security Code", 
                        BodyFormat = "Your security code is: {0}"
                    });
            manager.EmailService = new EmailService();

            // manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }

            return manager;
        }

        /// <summary>
        /// The create identity async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="authenticationType">
        /// The authentication type.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NullReferenceException">
        /// </exception>
        public override Task<ClaimsIdentity> CreateIdentityAsync(ApplicationUser user, string authenticationType)
        {
            if (user == null)
            {
                throw new NullReferenceException("user");
            }

            if (authenticationType.IsNullOrWhiteSpace())
            {
                throw new NullReferenceException("authenticationType");
            }

            this._datamapper = new UserDataMapper();

            var claims = this._datamapper.GetUserByUserId(user.Id);

            var role = claims.Roles.SingleOrDefault(x => x.Name != string.Empty) ?? new Role { Name = "Bruker" };

            var identity = new ClaimsIdentity(
                new[] { new Claim(ClaimTypes.Name, user.UserName), }, 
                DefaultAuthenticationTypes.ApplicationCookie, 
                ClaimTypes.Name, 
                ClaimTypes.Role);

            identity.AddClaim(new Claim(ClaimTypes.Role, role.Name));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));
            identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
            identity.AddClaim(
                new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", user.Id));
            identity.AddClaim(new Claim(ClaimTypes.GivenName, user.FirstName + " " + user.LastName));
            identity.AddClaim(new Claim(ClaimTypes.Sid, user.Id)); // OK to store userID here?

            return Task.Factory.StartNew(() => identity);

            // return base.CreateIdentityAsync(user, authenticationType);
        }

        /// <summary>
        /// The is in role async.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="role">
        /// The role.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NullReferenceException">
        /// </exception>
        public override Task<bool> IsInRoleAsync(string userId, string role)
        {
            if (userId.IsNullOrWhiteSpace())
            {
                throw new NullReferenceException("userId");
            }

            if (role.IsNullOrWhiteSpace())
            {
                throw new NullReferenceException("role");
            }

            return Task.Factory.StartNew(
                () =>
                    {
                        this._datamapper = new UserDataMapper();
                        return this._datamapper.IsUserInRoleByUserId(userId);
                    });

            // return base.IsInRoleAsync(userId, role);
        }

        /// <summary>
        /// The find async.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NullReferenceException">
        /// </exception>
        public override Task<ApplicationUser> FindAsync(string userName, string password)
        {
            if (userName.IsNullOrWhiteSpace())
            {
                throw new NullReferenceException("userName");
            }

            if (password.IsNullOrWhiteSpace())
            {
                throw new NullReferenceException("userName");
            }

            

            return Task.Factory.StartNew(
                () =>
                    {
                        this._datamapper = new UserDataMapper();
                        var user = this._datamapper.GetUserByUserName(userName);

                        if (user != null)
                        {
                            var result = this.PasswordHasher.VerifyHashedPassword(user.PasswordHash, password);

                            if (result == PasswordVerificationResult.Success)
                            {
                                return user;
                            }
                        }
                        
                        return null;
                    });

            // return base.FindAsync(userName, password);
        }

        public override Task<bool> CheckPasswordAsync(ApplicationUser user, string password)
        {
            return Task.FromResult<bool>(true);
        }
    }
}