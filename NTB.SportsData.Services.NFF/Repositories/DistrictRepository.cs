﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DistrictRepository.cs" company="">
//   
// </copyright>
// <summary>
//   The district repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NFF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.ServiceModel;

    using NTB.SportsData.Services.NFF.Interfaces;
    using NTB.SportsData.Services.NFF.NFFProdService;

    /// <summary>
    /// The district repository.
    /// </summary>
    public class DistrictRepository : IRepository<District>, IDisposable, IDistrictDataMapper
    {
        #region Fields

        /// <summary>
        ///     The service client.
        /// </summary>
        public OrganizationServiceClient ServiceClient = new OrganizationServiceClient();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(District domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="District"/>.
        /// </returns>
        public District Get(int id)
        {
            return this.GetAll().Single(s => s.DistrictId == id);
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<District> GetAll()
        {
            if (this.ServiceClient.State == CommunicationState.Faulted)
            {
                this.ServiceClient.Close();
                this.ServiceClient = null;

                this.ServiceClient = new OrganizationServiceClient();
            }

            if (this.ServiceClient.ClientCredentials == null)
            {
                return this.ServiceClient.GetDistricts().AsQueryable();
            }

            this.ServiceClient.ClientCredentials.UserName.UserName =
                ConfigurationManager.AppSettings["NFFServicesUsername"];
            this.ServiceClient.ClientCredentials.UserName.Password =
                ConfigurationManager.AppSettings["NFFServicesPassword"];

            return this.ServiceClient.GetDistricts().AsQueryable();
        }

        /// <summary>
        /// The get district.
        /// </summary>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <returns>
        /// The <see cref="District"/>.
        /// </returns>
        public District GetDistrict(int districtId)
        {
            return this.GetAll().Single(s => s.DistrictId == districtId);
        }

        /// <summary>
        /// The get districts.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<District> GetDistricts()
        {
            if (this.ServiceClient.State == CommunicationState.Faulted)
            {
                this.ServiceClient.Close();
                this.ServiceClient = null;

                this.ServiceClient = new OrganizationServiceClient();
            }

            if (this.ServiceClient.ClientCredentials == null)
            {
                return this.ServiceClient.GetDistricts().ToList();
            }

            this.ServiceClient.ClientCredentials.UserName.UserName =
                ConfigurationManager.AppSettings["NFFServicesUsername"];
            this.ServiceClient.ClientCredentials.UserName.Password =
                ConfigurationManager.AppSettings["NFFServicesPassword"];

            return this.ServiceClient.GetDistricts().ToList();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<District> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(District domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(District domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}