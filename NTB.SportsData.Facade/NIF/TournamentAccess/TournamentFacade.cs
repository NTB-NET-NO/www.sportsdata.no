﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Domain.Classes;
using NTB.SportsData.Facade.NIF.TournamentInterface;

namespace NTB.SportsData.Facade.NIF.TournamentAccess
{
    public class TournamentFacade : ITournamentFacade
    {
        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId)
        {
            throw new NotImplementedException();
        }

        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            throw new NotImplementedException();
        }

        public List<Tournament> GetTournamentsByTeam(int teamId, int seasonId)
        {
            throw new NotImplementedException();
        }

        public List<Match> GetTodaysMatches(DateTime matchDate)
        {
            return new List<Match>();
        }


        public List<Match> GetMatchesByDistrict(int districtId, DateTime matchDate)
        {
            throw new NotImplementedException();
        }

        public List<Tournament> GetSeasonTournaments(int seasonId)
        {
            throw new NotImplementedException();
        }
    }
}