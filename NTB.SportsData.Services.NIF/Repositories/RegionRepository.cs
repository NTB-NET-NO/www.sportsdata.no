﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegionRepository.cs" company="">
//   
// </copyright>
// <summary>
//   The region repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Services.NIF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using NTB.SportsData.Services.NIF.Interfaces;
    using NTB.SportsData.Services.NIF.NIFProdService;

    /// <summary>
    /// The region repository.
    /// </summary>
    public class RegionRepository : IRepository<Region>, IDisposable, IRegionDataMapper
    {
        #region Fields

        /// <summary>
        ///     The tournament service client.
        /// </summary>
        private readonly RegionServiceClient _serviceClient = new RegionServiceClient();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RegionRepository"/> class.
        /// </summary>
        public RegionRepository()
        {
            if (this._serviceClient.ClientCredentials != null)
            {
                this._serviceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NIFServicesUsername"];
                this._serviceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NIFServicesPassword"];
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Region domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Region"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Region Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<Region> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get regions.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Region> GetRegions()
        {
            var request = new EmptyRequest8();
            var response = this._serviceClient.GetMuncipalities(request);

            return response.Success ? response.Regions.ToList() : new List<Region>();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<Region> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(Region domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Region domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}