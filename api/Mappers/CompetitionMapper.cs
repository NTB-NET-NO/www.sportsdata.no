﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CompetitionMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The competition mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Api.Mappers
{
    using Glue;

    using NTB.SportsData.Domain.Classes;
    using NTB.SportsData.Domain.Classes.JSON;

    /// <summary>
    ///     The competition mapper.
    /// </summary>
    public class CompetitionMapper : BaseMapper<Tournament, Competition>
    {
        #region Methods

        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Tournament, Competition> mapper)
        {
            mapper.Relate(x => x.TournamentId, y => y.Competitionid);
            mapper.Relate(x => x.TournamentName, y => y.CompetitionName);
            mapper.Relate(x => x.SportName, y => y.SportName);
            mapper.Relate(x => x.SportId, y => y.SportId);
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.SeasonName, y => y.SeasonName);
            mapper.Relate(x => x.OrganizationId, y => y.OrganizationId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.OrganizationName, y => y.OrganizationName);
            mapper.Relate(x => x.OrganizationNameShort, y => y.OrganizationNameShort);
            mapper.Relate(x => x.AgeCategoryId, y => y.AgeCategoryId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.DisciplineId, y => y.DisciplineId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.DisciplineName, y => y.DisciplineName);
        }

        #endregion
    }
}